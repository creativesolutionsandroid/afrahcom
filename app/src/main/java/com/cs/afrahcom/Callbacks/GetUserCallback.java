package com.cs.afrahcom.Callbacks;

import android.util.Log;

import com.cs.afrahcom.models.User;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 20-02-2018.
 */

public class GetUserCallback {

    public interface IGetUserResponse {
        void onCompleted(User user);
    }

    private IGetUserResponse mGetUserResponse;
    private GraphRequest.Callback mCallback;

    public GetUserCallback(final IGetUserResponse getUserResponse) {

        mGetUserResponse = getUserResponse;
        mCallback = new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                User user = null;
                try {
                    JSONObject userObj = response.getJSONObject();
                    if (userObj == null) {
                        return;
                    }
                    user = jsonToUser(userObj);

                } catch (JSONException e) {
                    // Handle exception ...
                }

                // Handled by ProfileActivity
                mGetUserResponse.onCompleted(user);
            }
        };
    }

    private User jsonToUser(JSONObject user) throws JSONException {
        Log.i("TAG","user "+user);
//        Uri picture = Uri.parse(user.getJSONObject("picture").getJSONObject("data").getString
//                ("url"));
        String name = user.getString("name");
//        String id = user.getString("id");
        String email = null;
        if (user.has("email")) {
            email = user.getString("email");
        }

        // Build permissions display string
//        StringBuilder builder = new StringBuilder();
//        JSONArray perms = user.getJSONObject("permissions").getJSONArray("data");
//        builder.append("Permissions:\n");
//        for (int i = 0; i < perms.length(); i++) {
//            builder.append(perms.getJSONObject(i).get("permission")).append(": ").append(perms
//                    .getJSONObject(i).get("status")).append("\n");
//        }
//        String permissions = builder.toString();

        return new User(name, email);
    }

    public GraphRequest.Callback getCallback() {
        return mCallback;
    }
}
