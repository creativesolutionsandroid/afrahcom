package com.cs.afrahcom.dialogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cs.afrahcom.R;

/**
 * Created by CS on 11-01-2018.
 */

public class OfflineBookingDialog extends DialogFragment {

    View rootView;

    public static OfflineBookingDialog newInstance() {
        return new OfflineBookingDialog();
    }

//    public OfflineBookingDialog(){
//
//    }

//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        View layout = inflater.inflate(R.layout.activity_offline_booking, null);
//
//        /** Add modifications on your layout if needed */
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        // Add your custom layout to the builder
//        builder.setView(layout);
//
//        return builder.create();
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_offline_booking, container, false);


        return rootView;
    }
}
