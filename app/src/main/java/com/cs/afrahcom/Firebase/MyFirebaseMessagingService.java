package com.cs.afrahcom.Firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.HostNotificationsActivity;
import com.cs.afrahcom.activity.MainActivity;
import com.cs.afrahcom.activity.UserNotificationsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


/**
 * Created by CS on 10/28/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    String title, icon;
    Intent intent;
    Context mContext;
    SharedPreferences userPrefs;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        if (remoteMessage.getNotification() != null) {
            Intent intent = null;
            userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
            if(userPrefs.getString("mode","").equals("")){
                intent = new Intent(this, MainActivity.class);
            }
            else if(userPrefs.getString("mode","").equals("user")){
                intent = new Intent(this, UserNotificationsActivity.class);
            }
            else if(userPrefs.getString("mode","").equals("host")){
                intent = new Intent(this, HostNotificationsActivity.class);
            }

            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
            Notification noti = new Notification.Builder(MyFirebaseMessagingService.this)
                    .setContentTitle("Afrahcom")
                    .setContentText(remoteMessage.getNotification().getBody())
                    .setSmallIcon(R.drawable.notification_icon)
                    .setColor(Color.parseColor("#3F51B5"))
                    .setAutoCancel(true)
                    .setTicker(remoteMessage.getNotification().getBody())
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setContentIntent(resultPendingIntent).build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // hide the notification after its selected
            noti.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify((int) System.currentTimeMillis(), noti);
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
        }

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

        }

    }
}