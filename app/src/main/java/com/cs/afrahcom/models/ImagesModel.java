package com.cs.afrahcom.models;

import java.io.Serializable;

/**
 * Created by CS on 23-11-2017.
 */

public class ImagesModel implements Serializable {

    String imageName, source;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
