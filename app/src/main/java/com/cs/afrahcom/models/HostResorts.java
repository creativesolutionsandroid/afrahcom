package com.cs.afrahcom.models;

import java.io.Serializable;

/**
 * Created by CS on 14-11-2017.
 */

public class HostResorts implements Serializable {

    String resortId, resortname, resortnameAr, resorttype;

    public String getResortnameAr() {
        return resortnameAr;
    }

    public void setResortnameAr(String resortnameAr) {
        this.resortnameAr = resortnameAr;
    }

    public String getResorttype() {
        return resorttype;
    }

    public void setResorttype(String resorttype) {
        this.resorttype = resorttype;
    }

    public String getResortId() {
        return resortId;
    }

    public void setResortId(String resortId) {
        this.resortId = resortId;
    }

    public String getResortname() {
        return resortname;
    }

    public void setResortname(String resortname) {
        this.resortname = resortname;
    }
}
