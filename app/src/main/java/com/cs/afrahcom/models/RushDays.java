package com.cs.afrahcom.models;

import java.io.Serializable;

/**
 * Created by CS on 18-12-2017.
 */

public class RushDays implements Serializable {

    String startDate;
    String endDate;
    String rate;
    String rushId;

    public String getRushId() {
        return rushId;
    }

    public void setRushId(String rushId) {
        this.rushId = rushId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}
