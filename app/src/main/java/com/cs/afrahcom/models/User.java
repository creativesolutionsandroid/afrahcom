package com.cs.afrahcom.models;

/**
 * Created by CS on 20-02-2018.
 */

public class User {

    public User(String name, String email){
        this.email = email;
        this.name = name;
    }
    String email, name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
