package com.cs.afrahcom.models;

/**
 * Created by CS on 02-10-2017.
 */

public class Cities {

    String CityName;
    String CityId;

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }
}
