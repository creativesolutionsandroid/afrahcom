package com.cs.afrahcom.models;

/**
 * Created by CS on 25-10-2017.
 */

public class FavoriteResorts {

    String ResortId;
    String ResortName;
    String City;
    String ResortAddress;
    String ResortImage;
    String source;
    String Price;
    String Latitude;
    String Longitude;
    String Distance;
    String Approved;


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getResortId() {
        return ResortId;
    }

    public void setResortId(String resortId) {
        ResortId = resortId;
    }

    public String getResortName() {
        return ResortName;
    }

    public void setResortName(String resortName) {
        ResortName = resortName;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getResortAddress() {
        return ResortAddress;
    }

    public void setResortAddress(String resortAddress) {
        ResortAddress = resortAddress;
    }

    public String getResortImage() {
        return ResortImage;
    }

    public void setResortImage(String resortImage) {
        ResortImage = resortImage;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getApproved() {
        return Approved;
    }

    public void setApproved(String approved) {
        Approved = approved;
    }
}
