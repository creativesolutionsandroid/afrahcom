package com.cs.afrahcom.models;

/**
 * Created by CS on 25-10-2017.
 */

public class OrderHistory {

    String BookingId;
    String StartDate;
    String ResortName;
    String NoOfMen;
    String NoOfWomen;
    String ResortType;
    String bookingStatus;
    String paymenyStatus;
    String reservationDate;
    String endDate;
    String userName;
    String IscateringSelected;
    String mobile;
    String payMode;
    String totalAmount;
    String advanceAmount;
    String invoiceNumber;
    String isKitchenSelected;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getIsKitchenSelected() {
        return isKitchenSelected;
    }

    public void setIsKitchenSelected(String isKitchenSelected) {
        this.isKitchenSelected = isKitchenSelected;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIscateringSelected() {
        return IscateringSelected;
    }

    public void setIscateringSelected(String iscateringSelected) {
        IscateringSelected = iscateringSelected;
    }

    public String getNoOfMen() {
        return NoOfMen;
    }

    public void setNoOfMen(String noOfMen) {
        NoOfMen = noOfMen;
    }

    public String getNoOfWomen() {
        return NoOfWomen;
    }

    public void setNoOfWomen(String noOfWomen) {
        NoOfWomen = noOfWomen;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getPaymenyStatus() {
        return paymenyStatus;
    }

    public void setPaymenyStatus(String paymenyStatus) {
        this.paymenyStatus = paymenyStatus;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getResortName() {
        return ResortName;
    }

    public void setResortName(String resortName) {
        ResortName = resortName;
    }

    public String getResortType() {
        return ResortType;
    }

    public void setResortType(String resortType) {
        ResortType = resortType;
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }
}
