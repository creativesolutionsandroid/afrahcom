package com.cs.afrahcom.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by CS on 03-10-2017.
 */

public class ResortDetailsFiltered implements Serializable{

    String ResortId;
    String ResortName;
    String ResortNameAr;
    String City;
    String CityId;
    String ResortAddress;
    String AfrahcomAdvance;
    String ResortType;
    String ResortTypeId;
    String ResortImage;
    String NoOfCapacity;
    String ActualPrice;
    String DiscountedPrice;
    String Desc;
    String DescAr;
    String Rating;
    String Latitude;
    String Longitude;
    String CheckInDate;
    String CheckOutDate;
    String Approved;
    String distance;
    String isFavorite;
    String bookingID;
    String startDate;
    String endDate;
    String source;
    String discount;
    String rate;
    String bannerImage;
    String total, subTotal, subvat, disc, netAmount;
    int minCapacity, maxCapacity;
    String satMenPrice, satWomenPrice, satFamilyPrice, satKitchenPrice;
    String sunMenPrice, sunWomenPrice, sunFamilyPrice, sunKitchenPrice;
    String monMenPrice, monWomenPrice, monFamilyPrice, monKitchenPrice;
    String tueMenPrice, tueWomenPrice, tueFamilyPrice, tueKitchenPrice;
    String wedMenPrice, wedWomenPrice, wedFamilyPrice, wedKitchenPrice;
    String thursMenPrice, thursWomenPrice, thursFamilyPrice, thursKitchenPrice;
    String friMenPrice, friWomenPrice, friFamilyPrice, friKitchenPrice;
    String VatAvailability, IbanNumber, VatNumber, BankName;
    ArrayList<ImagesModel> resortImagesArray = new ArrayList<>();
    ArrayList<Facilities> resortFacilities = new ArrayList<>();

    public ArrayList<ImagesModel> getResortImagesArray() {
        return resortImagesArray;
    }

    public void setResortImagesArray(ArrayList<ImagesModel> resortImagesArray) {
        this.resortImagesArray = resortImagesArray;
    }

    public ArrayList<Facilities> getResortFacilities() {
        return resortFacilities;
    }

    public void setResortFacilities(ArrayList<Facilities> resortFacilities) {
        this.resortFacilities = resortFacilities;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getSubvat() {
        return subvat;
    }

    public void setSubvat(String subvat) {
        this.subvat = subvat;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public String getResortTypeId() {
        return ResortTypeId;
    }

    public void setResortTypeId(String resortTypeId) {
        ResortTypeId = resortTypeId;
    }

    public int getMinCapacity() {
        return minCapacity;
    }

    public void setMinCapacity(int minCapacity) {
        this.minCapacity = minCapacity;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public String getDescAr() {
        return DescAr;
    }

    public void setDescAr(String descAr) {
        DescAr = descAr;
    }

    public String getVatAvailability() {
        return VatAvailability;
    }

    public void setVatAvailability(String vatAvailability) {
        VatAvailability = vatAvailability;
    }

    public String getIbanNumber() {
        return IbanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        IbanNumber = ibanNumber;
    }

    public String getVatNumber() {
        return VatNumber;
    }

    public void setVatNumber(String vatNumber) {
        VatNumber = vatNumber;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getAfrahcomAdvance() {
        return AfrahcomAdvance;
    }

    public void setAfrahcomAdvance(String afrahcomAdvance) {
        AfrahcomAdvance = afrahcomAdvance;
    }

    public String getResortNameAr() {
        return ResortNameAr;
    }

    public void setResortNameAr(String resortNameAr) {
        ResortNameAr = resortNameAr;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getDiscountedPrice() {
        return DiscountedPrice;
    }

    public void setDiscountedPrice(String discountedPrice) {
        DiscountedPrice = discountedPrice;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getResortId() {
        return ResortId;
    }

    public void setResortId(String resortId) {
        ResortId = resortId;
    }

    public String getResortName() {
        return ResortName;
    }

    public void setResortName(String resortName) {
        ResortName = resortName;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getResortAddress() {
        return ResortAddress;
    }

    public void setResortAddress(String resortAddress) {
        ResortAddress = resortAddress;
    }

    public String getResortType() {
        return ResortType;
    }

    public void setResortType(String resortType) {
        ResortType = resortType;
    }

    public String getResortImage() {
        return ResortImage;
    }

    public void setResortImage(String resortImage) {
        ResortImage = resortImage;
    }

    public String getNoOfCapacity() {
        return NoOfCapacity;
    }

    public void setNoOfCapacity(String noOfCapacity) {
        NoOfCapacity = noOfCapacity;
    }

    public String getActualPrice() {
        return ActualPrice;
    }

    public void setActualPrice(String actualPrice) {
        ActualPrice = actualPrice;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(String checkInDate) {
        CheckInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return CheckOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        CheckOutDate = checkOutDate;
    }

    public String getApproved() {
        return Approved;
    }

    public void setApproved(String approved) {
        Approved = approved;
    }

    public String getSatMenPrice() {
        return satMenPrice;
    }

    public void setSatMenPrice(String satMenPrice) {
        this.satMenPrice = satMenPrice;
    }

    public String getSatWomenPrice() {
        return satWomenPrice;
    }

    public void setSatWomenPrice(String satWomenPrice) {
        this.satWomenPrice = satWomenPrice;
    }

    public String getSatFamilyPrice() {
        return satFamilyPrice;
    }

    public void setSatFamilyPrice(String satFamilyPrice) {
        this.satFamilyPrice = satFamilyPrice;
    }

    public String getSatKitchenPrice() {
        return satKitchenPrice;
    }

    public void setSatKitchenPrice(String satKitchenPrice) {
        this.satKitchenPrice = satKitchenPrice;
    }

    public String getSunMenPrice() {
        return sunMenPrice;
    }

    public void setSunMenPrice(String sunMenPrice) {
        this.sunMenPrice = sunMenPrice;
    }

    public String getSunWomenPrice() {
        return sunWomenPrice;
    }

    public void setSunWomenPrice(String sunWomenPrice) {
        this.sunWomenPrice = sunWomenPrice;
    }

    public String getSunFamilyPrice() {
        return sunFamilyPrice;
    }

    public void setSunFamilyPrice(String sunFamilyPrice) {
        this.sunFamilyPrice = sunFamilyPrice;
    }

    public String getSunKitchenPrice() {
        return sunKitchenPrice;
    }

    public void setSunKitchenPrice(String sunKitchenPrice) {
        this.sunKitchenPrice = sunKitchenPrice;
    }

    public String getMonMenPrice() {
        return monMenPrice;
    }

    public void setMonMenPrice(String monMenPrice) {
        this.monMenPrice = monMenPrice;
    }

    public String getMonWomenPrice() {
        return monWomenPrice;
    }

    public void setMonWomenPrice(String monWomenPrice) {
        this.monWomenPrice = monWomenPrice;
    }

    public String getMonFamilyPrice() {
        return monFamilyPrice;
    }

    public void setMonFamilyPrice(String monFamilyPrice) {
        this.monFamilyPrice = monFamilyPrice;
    }

    public String getMonKitchenPrice() {
        return monKitchenPrice;
    }

    public void setMonKitchenPrice(String monKitchenPrice) {
        this.monKitchenPrice = monKitchenPrice;
    }

    public String getTueMenPrice() {
        return tueMenPrice;
    }

    public void setTueMenPrice(String tueMenPrice) {
        this.tueMenPrice = tueMenPrice;
    }

    public String getTueWomenPrice() {
        return tueWomenPrice;
    }

    public void setTueWomenPrice(String tueWomenPrice) {
        this.tueWomenPrice = tueWomenPrice;
    }

    public String getTueFamilyPrice() {
        return tueFamilyPrice;
    }

    public void setTueFamilyPrice(String tueFamilyPrice) {
        this.tueFamilyPrice = tueFamilyPrice;
    }

    public String getTueKitchenPrice() {
        return tueKitchenPrice;
    }

    public void setTueKitchenPrice(String tueKitchenPrice) {
        this.tueKitchenPrice = tueKitchenPrice;
    }

    public String getWedMenPrice() {
        return wedMenPrice;
    }

    public void setWedMenPrice(String wedMenPrice) {
        this.wedMenPrice = wedMenPrice;
    }

    public String getWedWomenPrice() {
        return wedWomenPrice;
    }

    public void setWedWomenPrice(String wedWomenPrice) {
        this.wedWomenPrice = wedWomenPrice;
    }

    public String getWedFamilyPrice() {
        return wedFamilyPrice;
    }

    public void setWedFamilyPrice(String wedFamilyPrice) {
        this.wedFamilyPrice = wedFamilyPrice;
    }

    public String getWedKitchenPrice() {
        return wedKitchenPrice;
    }

    public void setWedKitchenPrice(String wedKitchenPrice) {
        this.wedKitchenPrice = wedKitchenPrice;
    }

    public String getThursMenPrice() {
        return thursMenPrice;
    }

    public void setThursMenPrice(String thursMenPrice) {
        this.thursMenPrice = thursMenPrice;
    }

    public String getThursWomenPrice() {
        return thursWomenPrice;
    }

    public void setThursWomenPrice(String thursWomenPrice) {
        this.thursWomenPrice = thursWomenPrice;
    }

    public String getThursFamilyPrice() {
        return thursFamilyPrice;
    }

    public void setThursFamilyPrice(String thursFamilyPrice) {
        this.thursFamilyPrice = thursFamilyPrice;
    }

    public String getThursKitchenPrice() {
        return thursKitchenPrice;
    }

    public void setThursKitchenPrice(String thursKitchenPrice) {
        this.thursKitchenPrice = thursKitchenPrice;
    }

    public String getFriMenPrice() {
        return friMenPrice;
    }

    public void setFriMenPrice(String friMenPrice) {
        this.friMenPrice = friMenPrice;
    }

    public String getFriWomenPrice() {
        return friWomenPrice;
    }

    public void setFriWomenPrice(String friWomenPrice) {
        this.friWomenPrice = friWomenPrice;
    }

    public String getFriFamilyPrice() {
        return friFamilyPrice;
    }

    public void setFriFamilyPrice(String friFamilyPrice) {
        this.friFamilyPrice = friFamilyPrice;
    }

    public String getFriKitchenPrice() {
        return friKitchenPrice;
    }

    public void setFriKitchenPrice(String friKitchenPrice) {
        this.friKitchenPrice = friKitchenPrice;
    }

    /*Comparator for sorting the list by roll no*/
    public static Comparator<ResortDetailsFiltered> priceSort = new Comparator<ResortDetailsFiltered>() {

        public int compare(ResortDetailsFiltered s1, ResortDetailsFiltered s2) {

            float price1 = Float.parseFloat(s1.getDiscountedPrice());
            float price2 = Float.parseFloat(s2.getDiscountedPrice());

	   /*For ascending order*/
            return Float.compare(price1,price2);

	   /*For descending order*/
//            return price2-price1;
        }};

    public static Comparator<ResortDetailsFiltered> distanceSort = new Comparator<ResortDetailsFiltered>() {

        public int compare(ResortDetailsFiltered s1, ResortDetailsFiltered s2) {

            float price1 = Float.parseFloat(s1.getDistance());
            float price2 = Float.parseFloat(s2.getDistance());

	   /*For ascending order*/
            return Float.compare(price1,price2);

	   /*For descending order*/
//            return price2-price1;
        }};
}
