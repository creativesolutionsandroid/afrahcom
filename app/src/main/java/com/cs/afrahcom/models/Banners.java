package com.cs.afrahcom.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by CS on 02-10-2017.
 */

public class Banners implements Serializable{

    String ResortId;
    String ResortName;
    String City;
    String ResortAddress;

    public ArrayList<String> getResortImagesArray() {
        return resortImagesArray;
    }

    public void setResortImagesArray(ArrayList<String> resortImagesArray) {
        this.resortImagesArray = resortImagesArray;
    }

    String ResortType;
    String ResortImage;
    String NoOfCapacity;
    String Price;
    String Latitude;
    String Longitude;
    String Approved;
    String Rating;
    ArrayList<String> resortImagesArray = new ArrayList<>();

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getResortId() {
        return ResortId;
    }

    public void setResortId(String resortId) {
        ResortId = resortId;
    }

    public String getResortName() {
        return ResortName;
    }

    public void setResortName(String resortName) {
        ResortName = resortName;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getResortAddress() {
        return ResortAddress;
    }

    public void setResortAddress(String resortAddress) {
        ResortAddress = resortAddress;
    }

    public String getResortType() {
        return ResortType;
    }

    public void setResortType(String resortType) {
        ResortType = resortType;
    }

    public String getResortImage() {
        return ResortImage;
    }

    public void setResortImage(String resortImage) {
        ResortImage = resortImage;
    }

    public String getNoOfCapacity() {
        return NoOfCapacity;
    }

    public void setNoOfCapacity(String noOfCapacity) {
        NoOfCapacity = noOfCapacity;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getApproved() {
        return Approved;
    }

    public void setApproved(String approved) {
        Approved = approved;
    }
}
