package com.cs.afrahcom.models;

/**
 * Created by CS on 02-10-2017.
 */

public class ResortType {

    String ResortTypeId;
    String ResortTypeName;
    String ResortTypeName_Ar;

    public String getResortTypeName_Ar() {
        return ResortTypeName_Ar;
    }

    public void setResortTypeName_Ar(String resortTypeName_Ar) {
        ResortTypeName_Ar = resortTypeName_Ar;
    }

    public String getResortTypeId() {
        return ResortTypeId;
    }

    public void setResortTypeId(String resortTypeId) {
        ResortTypeId = resortTypeId;
    }

    public String getResortTypeName() {
        return ResortTypeName;
    }

    public void setResortTypeName(String resortTypeName) {
        ResortTypeName = resortTypeName;
    }
}
