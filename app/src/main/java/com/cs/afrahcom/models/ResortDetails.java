package com.cs.afrahcom.models;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by CS on 03-10-2017.
 */

public class ResortDetails implements Serializable{

    String ResortId;
    String ResortName;
    String ResortNameAr;
    String City;
    String Description;
    String DescriptionAr;
    String ResortAddress;
    String ResortType;
    String AdvanceAmount;
    String ResortImage;
    String minCapacity;
    String maxCapacity;
    String ActualPrice;
    String DiscountedPrice;
    String Rating;
    String Latitude;
    String Longitude;
    String CheckInDate;
    String CheckOutDate;
    String Approved;
    String distance;
    String isFavorite;
    String kitchenPrice;
    String saturdayMenPrice;
    String saturdayWomenPrice;
    String sundayMenPrice;
    String sundayWomenPrice;
    String mondayMenPrice;
    String mondayWomenPrice;
    String tuesdayMenPrice;
    String tuesdayWomenPrice;
    String wednesdayMenPrice;
    String wednesdayWomenPrice;
    String thursadayMenPrice;
    String thursdayWomenPrice;
    String fridayMenPrice;
    String fridayWomenPrice;
    String discount;
    String cityId;
    String VatAvailability, IbanNumber, VatNumber, BankName;
    String sundayFamilyPrice, mondayFamilyPrice, tuesdayFamilyPrice, wednesdayFamilyPrice, saturdayFamilyPrice, thursdayFamilyPrice, fridayFamilyPrice;
    ArrayList<ImagesModel> resortImagesArray = new ArrayList<>();
    ArrayList<RushDays> rushDays = new ArrayList<>();
    ArrayList<Discounts> Discounts = new ArrayList<>();

    public ArrayList<com.cs.afrahcom.models.Discounts> getDiscounts() {
        return Discounts;
    }

    public void setDiscounts(ArrayList<com.cs.afrahcom.models.Discounts> discounts) {
        Discounts = discounts;
    }

    public ArrayList<RushDays> getRushDays() {
        return rushDays;
    }

    public void setRushDays(ArrayList<RushDays> rushDays) {
        this.rushDays = rushDays;
    }

    public ArrayList<ImagesModel> getResortImagesArray() {
        return resortImagesArray;
    }

    public void setResortImagesArray(ArrayList<ImagesModel> resortImagesArray) {
        this.resortImagesArray = resortImagesArray;
    }

    ArrayList<Facilities> resortFacilities = new ArrayList<>();

    public ArrayList<Facilities> getResortFacilities() {
        return resortFacilities;
    }

    public void setResortFacilities(ArrayList<Facilities> resortFacilities) {
        this.resortFacilities = resortFacilities;
    }

    public String getSundayFamilyPrice() {
        return sundayFamilyPrice;
    }

    public void setSundayFamilyPrice(String sundayFamilyPrice) {
        this.sundayFamilyPrice = sundayFamilyPrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getVatAvailability() {
        return VatAvailability;
    }

    public void setVatAvailability(String vatAvailability) {
        VatAvailability = vatAvailability;
    }

    public String getIbanNumber() {
        return IbanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        IbanNumber = ibanNumber;
    }

    public String getVatNumber() {
        return VatNumber;
    }

    public void setVatNumber(String vatNumber) {
        VatNumber = vatNumber;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getMondayFamilyPrice() {
        return mondayFamilyPrice;
    }

    public void setMondayFamilyPrice(String mondayFamilyPrice) {
        this.mondayFamilyPrice = mondayFamilyPrice;
    }

    public String getTuesdayFamilyPrice() {
        return tuesdayFamilyPrice;
    }

    public void setTuesdayFamilyPrice(String tuesdayFamilyPrice) {
        this.tuesdayFamilyPrice = tuesdayFamilyPrice;
    }

    public String getSaturdayFamilyPrice() {
        return saturdayFamilyPrice;
    }

    public void setSaturdayFamilyPrice(String saturdayFamilyPrice) {
        this.saturdayFamilyPrice = saturdayFamilyPrice;
    }

    public String getWednesdayFamilyPrice() {
        return wednesdayFamilyPrice;
    }

    public void setWednesdayFamilyPrice(String wednesdayFamilyPrice) {
        this.wednesdayFamilyPrice = wednesdayFamilyPrice;
    }

    public String getThursdayFamilyPrice() {
        return thursdayFamilyPrice;
    }

    public void setThursdayFamilyPrice(String thursdayFamilyPrice) {
        this.thursdayFamilyPrice = thursdayFamilyPrice;
    }

    public String getFridayFamilyPrice() {
        return fridayFamilyPrice;
    }

    public void setFridayFamilyPrice(String fridayFamilyPrice) {
        this.fridayFamilyPrice = fridayFamilyPrice;
    }

    public String getAdvanceAmount() {
        return AdvanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        AdvanceAmount = advanceAmount;
    }

    public String getResortNameAr() {
        return ResortNameAr;
    }

    public void setResortNameAr(String resortNameAr) {
        ResortNameAr = resortNameAr;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDescriptionAr() {
        return DescriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        DescriptionAr = descriptionAr;
    }

    public String getSaturdayMenPrice() {
        return saturdayMenPrice;
    }

    public void setSaturdayMenPrice(String saturdayMenPrice) {
        this.saturdayMenPrice = saturdayMenPrice;
    }

    public String getSaturdayWomenPrice() {
        return saturdayWomenPrice;
    }

    public void setSaturdayWomenPrice(String saturdayWomenPrice) {
        this.saturdayWomenPrice = saturdayWomenPrice;
    }

    public String getSundayMenPrice() {
        return sundayMenPrice;
    }

    public void setSundayMenPrice(String sundayMenPrice) {
        this.sundayMenPrice = sundayMenPrice;
    }

    public String getSundayWomenPrice() {
        return sundayWomenPrice;
    }

    public void setSundayWomenPrice(String sundayWomenPrice) {
        this.sundayWomenPrice = sundayWomenPrice;
    }

    public String getMondayMenPrice() {
        return mondayMenPrice;
    }

    public void setMondayMenPrice(String mondayMenPrice) {
        this.mondayMenPrice = mondayMenPrice;
    }

    public String getMondayWomenPrice() {
        return mondayWomenPrice;
    }

    public void setMondayWomenPrice(String mondayWomenPrice) {
        this.mondayWomenPrice = mondayWomenPrice;
    }

    public String getTuesdayMenPrice() {
        return tuesdayMenPrice;
    }

    public void setTuesdayMenPrice(String tuesdayMenPrice) {
        this.tuesdayMenPrice = tuesdayMenPrice;
    }

    public String getTuesdayWomenPrice() {
        return tuesdayWomenPrice;
    }

    public void setTuesdayWomenPrice(String tuesdayWomenPrice) {
        this.tuesdayWomenPrice = tuesdayWomenPrice;
    }

    public String getWednesdayMenPrice() {
        return wednesdayMenPrice;
    }

    public void setWednesdayMenPrice(String wednesdayMenPrice) {
        this.wednesdayMenPrice = wednesdayMenPrice;
    }

    public String getWednesdayWomenPrice() {
        return wednesdayWomenPrice;
    }

    public void setWednesdayWomenPrice(String wednesdayWomenPrice) {
        this.wednesdayWomenPrice = wednesdayWomenPrice;
    }

    public String getThursadayMenPrice() {
        return thursadayMenPrice;
    }

    public void setThursadayMenPrice(String thursadayMenPrice) {
        this.thursadayMenPrice = thursadayMenPrice;
    }

    public String getThursdayWomenPrice() {
        return thursdayWomenPrice;
    }

    public void setThursdayWomenPrice(String thursdayWomenPrice) {
        this.thursdayWomenPrice = thursdayWomenPrice;
    }

    public String getFridayMenPrice() {
        return fridayMenPrice;
    }

    public void setFridayMenPrice(String fridayMenPrice) {
        this.fridayMenPrice = fridayMenPrice;
    }

    public String getFridayWomenPrice() {
        return fridayWomenPrice;
    }

    public void setFridayWomenPrice(String fridayWomenPrice) {
        this.fridayWomenPrice = fridayWomenPrice;
    }

    public String getKitchenPrice() {
        return kitchenPrice;
    }

    public void setKitchenPrice(String kitchenPrice) {
        this.kitchenPrice = kitchenPrice;
    }

    public String getMinCapacity() {
        return minCapacity;
    }

    public void setMinCapacity(String minCapacity) {
        this.minCapacity = minCapacity;
    }

    public String getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(String maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getDiscountedPrice() {
        return DiscountedPrice;
    }

    public void setDiscountedPrice(String discountedPrice) {
        DiscountedPrice = discountedPrice;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getResortId() {
        return ResortId;
    }

    public void setResortId(String resortId) {
        ResortId = resortId;
    }

    public String getResortName() {
        return ResortName;
    }

    public void setResortName(String resortName) {
        ResortName = resortName;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getResortAddress() {
        return ResortAddress;
    }

    public void setResortAddress(String resortAddress) {
        ResortAddress = resortAddress;
    }

    public String getResortType() {
        return ResortType;
    }

    public void setResortType(String resortType) {
        ResortType = resortType;
    }

    public String getResortImage() {
        return ResortImage;
    }

    public void setResortImage(String resortImage) {
        ResortImage = resortImage;
    }

    public String getActualPrice() {
        return ActualPrice;
    }

    public void setActualPrice(String actualPrice) {
        ActualPrice = actualPrice;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(String checkInDate) {
        CheckInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return CheckOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        CheckOutDate = checkOutDate;
    }

    public String getApproved() {
        return Approved;
    }

    public void setApproved(String approved) {
        Approved = approved;
    }

    /*Comparator for sorting the list by roll no*/
    public static Comparator<ResortDetails> priceSort = new Comparator<ResortDetails>() {

        public int compare(ResortDetails s1, ResortDetails s2) {

            float price1 = Float.parseFloat(s1.getActualPrice());
            float price2 = Float.parseFloat(s2.getActualPrice());
            Log.i("TAG","price changed");

	   /*For ascending order*/
            return Float.compare(price1,price2);

	   /*For descending order*/
//            return price2-price1;
        }};
}
