package com.cs.afrahcom.models;

import java.io.Serializable;

/**
 * Created by CS on 11-10-2017.
 */

public class FoodDetails implements Serializable{

    String FoodId;
    String FoodName;
    String ResortId;
    String FoodDescription;
    String FoodImage;
    Boolean IsFavourite;
    Boolean IsBreakFast;
    Boolean IsLunch;
    Boolean IsDinner;
    String Rating;
    String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFoodId() {
        return FoodId;
    }

    public void setFoodId(String foodId) {
        FoodId = foodId;
    }

    public String getFoodName() {
        return FoodName;
    }

    public void setFoodName(String foodName) {
        FoodName = foodName;
    }

    public String getResortId() {
        return ResortId;
    }

    public void setResortId(String resortId) {
        ResortId = resortId;
    }

    public String getFoodDescription() {
        return FoodDescription;
    }

    public void setFoodDescription(String foodDescription) {
        FoodDescription = foodDescription;
    }

    public String getFoodImage() {
        return FoodImage;
    }

    public void setFoodImage(String foodImage) {
        FoodImage = foodImage;
    }

    public Boolean getFavourite() {
        return IsFavourite;
    }

    public void setFavourite(Boolean favourite) {
        IsFavourite = favourite;
    }

    public Boolean getBreakFast() {
        return IsBreakFast;
    }

    public void setBreakFast(Boolean breakFast) {
        IsBreakFast = breakFast;
    }

    public Boolean getLunch() {
        return IsLunch;
    }

    public void setLunch(Boolean lunch) {
        IsLunch = lunch;
    }

    public Boolean getDinner() {
        return IsDinner;
    }

    public void setDinner(Boolean dinner) {
        IsDinner = dinner;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }
}
