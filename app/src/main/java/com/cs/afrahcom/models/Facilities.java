package com.cs.afrahcom.models;

import java.io.Serializable;

/**
 * Created by CS on 18-10-2017.
 */

public class Facilities implements Serializable{

    String facilityName;
    String facilityNameAr;
    String facilityImage;
    String FacilityId;

    public String getFacilityId() {
        return FacilityId;
    }

    public void setFacilityId(String facilityId) {
        FacilityId = facilityId;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getFacilityNameAr() {
        return facilityNameAr;
    }

    public void setFacilityNameAr(String facilityNameAr) {
        this.facilityNameAr = facilityNameAr;
    }

    public String getFacilityImage() {
        return facilityImage;
    }

    public void setFacilityImage(String facilityImage) {
        this.facilityImage = facilityImage;
    }
}
