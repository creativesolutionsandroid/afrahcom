package com.cs.afrahcom;

import com.cs.afrahcom.models.Cities;

import java.util.ArrayList;

/**
 * Created by CS on 21-09-2017.
 */

public class Constants {

//    static String TEST_URL = "http://csadms.com/AfrahcomServices/";
//    public static String SOURCE1_URL = "http://csadms.com/Afrahcomservices/Images/";
//    public static String SOURCE2_URL = "http://csadms.com/AfrahComBackEnd/UploadedFiles/";

    static String TEST_URL = "http://services.afrahcomapp.com/";
    public static String SOURCE1_URL = "http://services.afrahcomapp.com/Images/";
    public static String SOURCE2_URL = "http://backend.afrahcomapp.com/UploadedFiles/";

    public static String VERIFY_MOBILE = TEST_URL+"api/User/VerifyMobileNo?MobileNo=";
    public static String REGISTRATION_URL = TEST_URL+"api/User/RegisterUser";
    public static String LOGIN_URL = TEST_URL+"api/User/SignIn?mobile=";
    public static String FORGOT_PASSWORD_URL = TEST_URL+"api/User/ForgotPassword?Mobile=";
    public static String RESET_PASSWORD_URL = TEST_URL + "api/User/ResetPassword?MobileNo=";
    public static String GET_BANNERS_URL = TEST_URL + "api/Resort/ResortBanner?UserId=";
    public static String RESORT_SEARCH_URL = TEST_URL + "api/Resort/Search";
    public static String GET_FOOD_DETAILS = TEST_URL + "api/Food/GetFavFood?userid=";
    public static String INSERT_FAVORITE_RESORT_URL = TEST_URL + "api/Resort/InsertFavResort";
    public static String DELETE_FAVORITE_RESORT_URL = TEST_URL + "api/Resort/DeleteFavResort";
    public static String INSERT_FAVORITE_FOOD_URL = TEST_URL + "api/Food/InsertFavFood";
    public static String DELETE_FAVORITE_FOOD_URL = TEST_URL + "api/Food/DeleteFavFood";
    public static String INSERT_ORDER_URL = TEST_URL + "api/Order/InsertOrder";
    public static String UPDATE_ORDER_STATUS_URL = TEST_URL + "api/Order/UpdateOrder";
    public static String TRACK_ORDER_URL = TEST_URL + "api/Order/GetTrackingOrder?Bookingid=";
    public static String ORDER_HISTORY_URL = TEST_URL + "api/Order/GetOrderHistory?userid=";
    public static String FAV_RESORTS_URL = TEST_URL + "api/Resort/GetUserFavResort?userid=";
    public static String CANCEL_ORDER_URL = TEST_URL + "api/Order/CancelOrder";
    public static String EDIT_PROFILE_URL = TEST_URL + "api/User/updateprofile";
    public static String CHANGE_PASSWORD_URL = TEST_URL + "api/User/ChangePassword/";
    public static String INSERT_CATERING_URL = TEST_URL + "api/Food/InsertCatering";
    public static String INSERT_RATING_URL = TEST_URL + "api/Resort/FoodResortRating";
    public static String CHECK_AVAILABILITY_URL = TEST_URL + "api/Resort/IsPropertyAvailableOn";
    public static String USER_NOTIFICATIONS_URL = TEST_URL + "api/Resort/GetUserNotification?userid=";
    public static String CHANGE_LANGUAGE_URL = TEST_URL + "api/User/UpdateUserlanguage?UserId=";

    public static String HOST_RESORTS_URL = TEST_URL + "api/Resort/GetResortById?HostId=";
    public static String RESORT_DASHBOARD_URL = TEST_URL + "api/Resort/Dashboard?resortid=";
    public static String HOST_REGISTRATION_URL = TEST_URL+"api/Host/HostSignUp";
    public static String HOST_LOGIN_URL = TEST_URL+"api/Host/HostLogin?mobileno=";
    public static String VERIFY_MOBILE_HOST = TEST_URL+"api/Host/HostMobileVerification?MobileNo=";
    public static String RESORT_ORDER_HISTORY_URL = TEST_URL + "api/Resort/ResortBookingHistory?ResortId=";
    public static String HOST_FORGOT_PASSWORD_URL = TEST_URL+"api/Host/HostForgotPassword?Mobile=";
    public static String HOST_RESET_PASSWORD_URL = TEST_URL + "api/Host/HostResetPassword?MobileNo=";
    public static String UPLOAD_IMAGE_URL = TEST_URL + "api/Resort/PostTest";
    public static String RESORT_REGISTRATION_URL = TEST_URL+"api/Resort/InsertResort";
    public static String RESORT_UPDATE_URL = TEST_URL+"api/Resort/EditResort";
    public static String HOST_EDIT_PROFILE_URL = TEST_URL + "api/Host/hostUpdateProfile";
    public static String HOST_CHANGE_PASSWORD_URL = TEST_URL + "api/Host/HostChangePassword/";
    public static String INSERT_RUSH_DAYS_URL = TEST_URL+"api/Resort/InsertRushDays";
    public static String DELETE_RUSH_DAYS_URL = TEST_URL+"api/Resort/DeleteRushDays";
    public static String INSERT_DISCOUNT_URL = TEST_URL+"api/Resort/InsertDiscount";
    public static String DELETE_DISCOUNT_URL = TEST_URL+"api/Resort/DeleteDiscount";
    public static String CANCEL_OFFLINE_BOOKING_URL = TEST_URL+"api/Order/Inactiveoofflineorder?bookingid=";
    public static String UPDATE_PAYMENT_STATUS_URL = TEST_URL+"api/Order/Updateorderpaymentstatus?bookingid=";
    public static String TRANSACTION_HISTORY_URL = TEST_URL + "api/Resort/TrackingHistory?ResortId=";
    public static String HOST_NOTIFICATIONS_URL = TEST_URL + "api/Resort/GetHostNotification?userid=";
    public static String HOST_ORDER_HISTORY_URL = TEST_URL + "api/Resort/HostBookingHistory?HostId=";

    public static String startDate;
    public static String endDate;
    public static String bookingType;
    public static String resortTypeId;
    public static String noOfGuests;
    public static String noOfMen;
    public static String noOfWomen;
    public static int isKitchenSelected = 0;
    public static int isCateringSelected = 0;
    public static int isBreakfastSelected = 0;
    public static int isLunchSelected = 0;
    public static int isDinnerSelected = 0;
    public static int isSnacksSelected = 0;
    public static ArrayList<String> SelectedFoodIds =  new ArrayList<>();
    public static ArrayList<Cities> CityArrayList =  new ArrayList<>();

    public static float VAT = 5;  /*5 percent*/

    public static String convertToArabic(String value)
    {
        String newValue = (value
                .replaceAll("١","1" ).replaceAll("٢","2" )
                .replaceAll("٣","3" ).replaceAll("٤","4" )
                .replaceAll("٥","5" ).replaceAll("٦","6" )
                .replaceAll("٧","7" ).replaceAll("٨","8")
                .replaceAll("٩","9" ).replaceAll("٠","0" )
                .replaceAll("٫",".").replaceAll("٬",","));
        return newValue;
    }
}
