package com.cs.afrahcom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.models.OrderHistory;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class TransactionHistoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderHistory> historyArrayList = new ArrayList<>();
    String language;

    public TransactionHistoryAdapter(Context context, ArrayList<OrderHistory> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvresortName, tvReservationDate, tvPayMode, tvAmount;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_transaction_history, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_transaction_history_ar, null);
            }

            holder.tvresortName = (TextView) convertView.findViewById(R.id.userName);
            holder.tvReservationDate = (TextView) convertView.findViewById(R.id.reservationDate);
            holder.tvPayMode = (TextView) convertView.findViewById(R.id.payMode);
            holder.tvAmount = (TextView) convertView.findViewById(R.id.amount);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final DecimalFormat priceFormat1 = new DecimalFormat("##,##,###");
        holder.tvresortName.setText(historyArrayList.get(position).getUserName());
        try {
            holder.tvAmount.setText(priceFormat1.format(Float.parseFloat(historyArrayList.get(position).getTotalAmount())));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            holder.tvAmount.setText(historyArrayList.get(position).getTotalAmount());
        }

        if(historyArrayList.get(position).getPayMode().equalsIgnoreCase("2")){
            if(language.equalsIgnoreCase("En")) {
                holder.tvPayMode.setText("Credit card");
            }
            else{
                holder.tvPayMode.setText("بطاقة ائتمان");
            }
        }
        else if(historyArrayList.get(position).getPayMode().equalsIgnoreCase("1")){
            if(language.equalsIgnoreCase("En")) {
                holder.tvPayMode.setText("Pay at property");
            }
            else{
                holder.tvPayMode.setText("الدفع  في الموقع القاعة");
            }
        }
        else{
            if(language.equalsIgnoreCase("En")) {
                holder.tvPayMode.setText("Pay at property");
            }
            else{
                holder.tvPayMode.setText("الدفع  في الموقع القاعة");
            }
        }

        try {
            String[] reservDate = historyArrayList.get(position).getStartDate().split("T");
            String[] date = reservDate[0].split("-");
            String[] endDate = historyArrayList.get(position).getEndDate().split("T");
            String[] date1 = endDate[0].split("-");
            holder.tvReservationDate.setText(date[2]+"-"+date[1]+"-"+date[0]+" / "+date1[2]+"-"+date1[1]+"-"+date1[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}