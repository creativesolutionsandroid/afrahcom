package com.cs.afrahcom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class HorizontalGridAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<String> imagesList = new ArrayList<>();

    public HorizontalGridAdapter(Context context, ArrayList<String> imagesList) {
        this.context = context;
        this.imagesList = imagesList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return imagesList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView mCheckBoxImage, imgCancel;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.bigimage_popup, null);

            holder.mCheckBoxImage = (ImageView) convertView.findViewById(R.id.big_image);
            holder.imgCancel = (ImageView) convertView.findViewById(R.id.imgCancel);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Glide.with(context).load(Constants.SOURCE1_URL + imagesList.get(position)).into(holder.mCheckBoxImage);

        holder.imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ResortDetailsActivity.mHorizontalGrid.setVisibility(View.GONE);
            }
        });

        return convertView;
    }

}
