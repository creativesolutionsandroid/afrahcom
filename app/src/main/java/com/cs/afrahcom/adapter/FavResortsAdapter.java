package com.cs.afrahcom.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.ResortDetailsFiltered;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class FavResortsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<ResortDetailsFiltered> favResortsArrayList = new ArrayList<>();
    String language;

    public FavResortsAdapter(Context context, ArrayList<ResortDetailsFiltered> favResortsArrayList, String language) {
        this.context = context;
        this.language = language;
        this.favResortsArrayList = favResortsArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return favResortsArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvresortName, tvResortAddress, tvDiscountedPrice, tvActualPrice, tvDistance;
        ImageView imgFavIcon, imgResort;
        RatingBar ratingBar;
        RelativeLayout approvedLayout, mainLayout;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_search_result, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_search_result_ar, null);
            }

            holder.tvresortName = (TextView) convertView.findViewById(R.id.result_resort_name);
            holder.tvResortAddress = (TextView) convertView.findViewById(R.id.result_resort_address);
            holder.tvDiscountedPrice = (TextView) convertView.findViewById(R.id.result_discounted_price);
            holder.tvActualPrice = (TextView) convertView.findViewById(R.id.result_price);
            holder.tvDistance = (TextView) convertView.findViewById(R.id.result_distance);

            holder.imgFavIcon = (ImageView) convertView.findViewById(R.id.serach_fav_icon);
            holder.imgResort = (ImageView) convertView.findViewById(R.id.img_banner);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.rating_bar);

            holder.approvedLayout = (RelativeLayout) convertView.findViewById(R.id.approved_layout);
            holder.mainLayout = (RelativeLayout) convertView.findViewById(R.id.layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LayerDrawable stars = (LayerDrawable) holder.ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###");

        if(language.equalsIgnoreCase("En")) {
            holder.tvresortName.setText(favResortsArrayList.get(position).getResortName());
        }
        else{
            holder.tvresortName.setText(favResortsArrayList.get(position).getResortNameAr());
        }
        holder.tvResortAddress.setText(favResortsArrayList.get(position).getResortAddress());
        holder.tvActualPrice.setText(Constants.convertToArabic(favResortsArrayList.get(position).getDiscountedPrice()));
        holder.tvDistance.setText(favResortsArrayList.get(position).getDistance()+" KM");

        try {
            if(favResortsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("1")) {
                Glide.with(context.getApplicationContext()).load(Constants.SOURCE1_URL + favResortsArrayList.get(position).getResortImagesArray().get(0).getImageName()).into(holder.imgResort);
            }
            else {
                Glide.with(context.getApplicationContext()).load(Constants.SOURCE2_URL + favResortsArrayList.get(position).getResortImagesArray().get(0).getImageName()).into(holder.imgResort);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.tvDiscountedPrice.setVisibility(View.INVISIBLE);
        holder.imgFavIcon.setVisibility(View.INVISIBLE);
        holder.approvedLayout.setVisibility(View.INVISIBLE);

        if(favResortsArrayList.get(position).getRating().equals("0")){
            holder.ratingBar.setRating(5);
        }
        else{
            holder.ratingBar.setRating(Float.parseFloat(favResortsArrayList.get(position).getRating()));
        }

        if (favResortsArrayList.get(position).getApproved().equals("true")){
            holder.approvedLayout.setVisibility(View.VISIBLE);
        }
        else{
            holder.approvedLayout.setVisibility(View.GONE);
        }

//        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FavoriteResortsActivity.bannerPos = position;
//                ((FavoriteResortsActivity)context).loadPopup();
//            }
//        });

        return convertView;
    }
}