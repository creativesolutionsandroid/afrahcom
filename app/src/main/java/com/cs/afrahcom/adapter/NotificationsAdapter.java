package com.cs.afrahcom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.models.Notifications;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SKT on 22-02-2016.
 */
public class NotificationsAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Notifications> favOrderList = new ArrayList<>();
    String id, language;

    public NotificationsAdapter(Context context, ArrayList<Notifications> favOrderList, String language) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        // menu type count
        return 2;
    }

    public static class ViewHolder {
        TextView status, date,desc;
        ImageView imageIcon;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.list_notifications, null);
            }
            else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.list_notifications_ar, null);
            }


            holder.status = (TextView) convertView
                    .findViewById(R.id.messages_title);
            holder.date = (TextView) convertView
                    .findViewById(R.id.messages_date);
            holder.desc = (TextView) convertView
                    .findViewById(R.id.messages_text);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.desc.setText(favOrderList.get(position).getMessage());

        if(language.equalsIgnoreCase("En")) {
            if(favOrderList.get(position).getTitle().equalsIgnoreCase("new")){
                holder.status.setText("Booked");
            }
            else {
                holder.status.setText(favOrderList.get(position).getTitle());
            }
        }
        else{
            if(favOrderList.get(position).getTitle().equalsIgnoreCase("new")) {
                holder.status.setText("تم الحجز");
            }
            else if(favOrderList.get(position).getTitle().equalsIgnoreCase("pending")) {
                holder.status.setText("قيد الانتظار");
            }
            else if(favOrderList.get(position).getTitle().equalsIgnoreCase("close")) {
                holder.status.setText("مغلق");
            }
            else if(favOrderList.get(position).getTitle().equalsIgnoreCase("cancel")) {
                holder.status.setText("الغاء");
            }
            else if(favOrderList.get(position).getTitle().equalsIgnoreCase("Property Approved")) {
                holder.status.setText("تم اعتماد الموقع");
            }
            else if(favOrderList.get(position).getTitle().equalsIgnoreCase("Property Rejected")) {
                holder.status.setText("تم رفض طلبك");
            }
            else{
                holder.status.setText(favOrderList.get(position).getTitle());
            }
        }

//        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.US);
        Date dateObj = null;
        String[] parts = favOrderList.get(position).getDate().split(" ");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getDate().replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.date.setText(date);
        return convertView;
    }
}