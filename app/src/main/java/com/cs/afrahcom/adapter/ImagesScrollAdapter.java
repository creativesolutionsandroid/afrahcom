package com.cs.afrahcom.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.ResortDetailsActivity;
import com.cs.afrahcom.models.ImagesModel;

import java.util.ArrayList;


/**
 * Created by cs android on 23-02-2017.
 */

public class ImagesScrollAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ImageView imgBanner, imagCancel, dummyView;

    ArrayList<ImagesModel> imagesList;

    public ImagesScrollAdapter(Context context, ArrayList<ImagesModel> imagesList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.imagesList = imagesList;
    }

    @Override
    public int getCount() {
        return imagesList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = null;
        itemView = mLayoutInflater.inflate(R.layout.bigimage_popup, container, false);
        imgBanner =(ImageView)itemView.findViewById(R.id.big_image);
        imagCancel =(ImageView)itemView.findViewById(R.id.imgCancel);
        dummyView =(ImageView)itemView.findViewById(R.id.dummyView);

        if(imagesList.get(position).getSource().equals("1")) {
            Glide.with(mContext).load(Constants.SOURCE1_URL + imagesList.get(position).getImageName())
                    .placeholder(mContext.getResources().getDrawable(R.drawable.empty_photo)).into(imgBanner);
        }
        else if(imagesList.get(position).getSource().equals("2")) {
            Glide.with(mContext).load(Constants.SOURCE2_URL + imagesList.get(position).getImageName())
                    .placeholder(mContext.getResources().getDrawable(R.drawable.empty_photo)).into(imgBanner);
        }

        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ResortDetailsActivity.imagesLayout.setVisibility(View.GONE);
            }
        });

        dummyView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResortDetailsActivity.imagesLayout.setVisibility(View.GONE);
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
