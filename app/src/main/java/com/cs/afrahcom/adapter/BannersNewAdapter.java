package com.cs.afrahcom.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.MainActivity;
import com.cs.afrahcom.models.ResortDetailsFiltered;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class BannersNewAdapter extends PagerAdapter {
    public Context context;
    public LayoutInflater inflater;
    LinearLayout layout;
    ImageView imgBanner;
    public static int bannerPos = 0;
    public static ArrayList<ResortDetailsFiltered> galleryList = new ArrayList<>();

    public BannersNewAdapter(Context context, ArrayList<ResortDetailsFiltered> galleryList) {
        this.context = context;
        this.galleryList = galleryList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return galleryList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = null;
        itemView = inflater.inflate(R.layout.list_gallery, container, false);

        imgBanner = (ImageView) itemView.findViewById(R.id.gallery_image);
        layout = (LinearLayout) itemView.findViewById(R.id.layout);

//        Log.i("TAG","name "+galleryList.get(position).getBannerImage());
        Glide.with(context.getApplicationContext()).load(Constants.SOURCE2_URL + galleryList.get(position).getBannerImage()).into(imgBanner);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bannerPos = position;
                ((MainActivity)context).loadPopup();
            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
