package com.cs.afrahcom.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.FoodDetails;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 28-02-2017.
 */

public class CaterersAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<FoodDetails> foodList = new ArrayList<>();
    int favPosition = 0;
    AlertDialog alertDialog2;
    String userId;
    SharedPreferences userPrefs;
    Activity activity;
    String language;
    AlertDialog customDialog;

    public CaterersAdapter(Context context, ArrayList<FoodDetails> DinnerList, String language, Activity activity) {
        this.context = context;
        this.foodList = DinnerList;
        this.activity = activity;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return foodList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mFoodName;
        ImageView mFoodImage, mFavIcon, mFoodSelected;
        LinearLayout nameLayout;
        RatingBar ratingBar;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_food_selection, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_food_selection_ar, null);
            }
//
            holder.mFoodImage = (ImageView) convertView.findViewById(R.id.foodimage);
            holder.mFavIcon = (ImageView) convertView.findViewById(R.id.foodFavIcon);
            holder.mFoodSelected = (ImageView) convertView.findViewById(R.id.foodSelectIcon);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.rating_bar);
            holder.nameLayout = (LinearLayout) convertView.findViewById(R.id.nameLayout);

            holder.mFoodName = (TextView) convertView.findViewById(R.id.foodName);

            userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
            userId = userPrefs.getString("userId", null);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LayerDrawable stars = (LayerDrawable) holder.ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);

        holder.mFoodName.setText(foodList.get(position).getFoodName());

        Float rate;
        try {
        rate = Float.parseFloat(foodList.get(position).getRating());
        if(rate == 0){
            rate = (float)5;
        }
        else{
            rate = rate;
        }
        } catch (Exception e) {
            rate = (float)5;
            e.printStackTrace();
        }
        holder.ratingBar.setRating(rate);

        if(foodList.get(position).getSource().equals("1")) {
            Glide.with(context.getApplicationContext()).load(Constants.SOURCE1_URL + foodList.get(position).getFoodImage()).into(holder.mFoodImage);
        }
        else if(foodList.get(position).getSource().equals("2")) {
            Glide.with(context.getApplicationContext()).load(Constants.SOURCE2_URL + foodList.get(position).getFoodImage()).into(holder.mFoodImage);
        }
        holder.mFoodSelected.setVisibility(View.GONE);

        if(foodList.get(position).getFavourite()){
            holder.mFavIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
        }
        else{
            holder.mFavIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_unselected));
        }


        holder.nameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                // ...Irrelevant code for customizing the buttons and title
                int layout = R.layout.dialog_caterer_description;

                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(true);

                TextView title = (TextView) dialogView.findViewById(R.id.name);
                TextView desc = (TextView) dialogView.findViewById(R.id.foodDescription);
                ImageView cancel = (ImageView) dialogView.findViewById(R.id.image_cancel);

                title.setText(foodList.get(position).getFoodName());
                desc.setText(foodList.get(position).getFoodDescription());
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog2.dismiss();
                    }
                });

                alertDialog2 = dialogBuilder.create();
                alertDialog2.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog2.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });

        holder.mFavIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(userId == null){
//                    final iOSDialog iOSDialog = new iOSDialog(activity);
//                    iOSDialog.setTitle(context.getResources().getString(R.string.app_name));
//                    iOSDialog.setSubtitle("Please login to use this action.");
//                    iOSDialog.setPositiveLabel(context.getResources().getString(R.string.str_btn_ok));
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = activity.getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(context.getResources().getString(R.string.app_name));
                        yes.setText(context.getResources().getString(R.string.str_btn_ok));
                        desc.setText("Please login to use this action.");
                    }
                    else{
                        title.setText(context.getResources().getString(R.string.app_name_ar));
                        yes.setText(context.getResources().getString(R.string.str_btn_ok_ar));
                        desc.setText("هذه الخطوه تتطلب تسجيل الدخول");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = activity.getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    if (foodList.get(position).getFavourite()) {
                        favPosition = position;

                        JSONObject parentObj = new JSONObject();
                        try {
                            JSONObject mainObj = new JSONObject();
                            mainObj.put("FoodId", foodList.get(position).getFoodId());
                            mainObj.put("UserId", userId);

                            parentObj.put("FavouriteFoods", mainObj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", parentObj.toString());

                        new DeleteFavOrder().execute(parentObj.toString());
                    } else {
                        favPosition = position;

                        JSONObject parentObj = new JSONObject();
                        try {
                            JSONObject mainObj = new JSONObject();
                            mainObj.put("FoodId", foodList.get(position).getFoodId());
                            mainObj.put("UserId", userId);

                            parentObj.put("FavouriteFoods", mainObj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", parentObj.toString());

                        new InsertFavOrder().execute(parentObj.toString());
                    }
                }
            }
        });

        return convertView;
    }

    public class InsertFavOrder extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.INSERT_FAVORITE_FOOD_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            foodList.get(favPosition).setFavourite(true);
                            notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            } else {
                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            Snap sn = new Snap();
//            sn.insertfav();
            super.onPostExecute(result);

        }

    }
    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.DELETE_FAVORITE_FOOD_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            foodList.get(favPosition).setFavourite(false);
                            notifyDataSetChanged();
//                            new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL + userId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            } else {
                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            Snap sn = new Snap();
//            sn.insertfav();
            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
