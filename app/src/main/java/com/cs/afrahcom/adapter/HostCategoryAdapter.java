package com.cs.afrahcom.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.fragment.ResortRegistrationFragment;
import com.cs.afrahcom.models.ResortType;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class HostCategoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<ResortType> categoryList = new ArrayList<>();
    Boolean isManSelected, isWomanSelected, isFamilySelected;
    int selectedPos = 0;
    String language;
    public HostCategoryAdapter(Context context, ArrayList<ResortType> categoryList, String language) {
        this.context = context;
        this.categoryList = categoryList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return categoryList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView mCheckBox;
        TextView mCategoryName;
        EditText mPriceBefore, mPriceAfter;
        LinearLayout layout;
        RelativeLayout mainLayout;
        RelativeLayout manLayout, womanLayout, familyLayout;
        ImageView mManCB, mWomanCB, mFamilyCB;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_category, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_category_ar, null);
            }

            holder.mCategoryName = (TextView) convertView.findViewById(R.id.categoryName);
            holder.mPriceBefore = (EditText) convertView.findViewById(R.id.minCapacity);
            holder.mPriceAfter = (EditText) convertView.findViewById(R.id.maxCapacity);

            holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
            holder.mainLayout = (RelativeLayout) convertView.findViewById(R.id.mainLayout);
//            holder.womanLayout = (RelativeLayout) convertView.findViewById(R.id.womanLayout);
//            holder.familyLayout = (RelativeLayout) convertView.findViewById(R.id.familyLayout);
//
//            holder.mManCB = (ImageView) convertView.findViewById(R.id.manSelect);
//            holder.mWomanCB = (ImageView) convertView.findViewById(R.id.womanSelect);
//            holder.mFamilyCB = (ImageView) convertView.findViewById(R.id.familySelect);
            holder.mCheckBox = (ImageView) convertView.findViewById(R.id.categorySelect);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(language.equalsIgnoreCase("Ar")){
            holder.mainLayout.setRotationY(180);
        }
        holder.mCategoryName.setText(categoryList.get(position).getResortTypeName());

        if(ResortRegistrationFragment.selectedCategories.contains(categoryList.get(position).getResortTypeName())){
            holder.mCheckBox.setImageDrawable(context.getResources().getDrawable(R.drawable.catering_selected));
            holder.mPriceBefore.setFocusable(true);
            holder.mPriceBefore.setFocusableInTouchMode(true);
            holder.mPriceAfter.setFocusable(true);
            holder.mPriceAfter.setFocusableInTouchMode(true);
            selectedPos = position;
        }
        else{
            holder.mCheckBox.setImageDrawable(context.getResources().getDrawable(R.drawable.catering_unselected));
            holder.mPriceBefore.setFocusable(false);
            holder.mPriceBefore.setFocusableInTouchMode(false);
            holder.mPriceAfter.setFocusable(false);
            holder.mPriceAfter.setFocusableInTouchMode(false);
            holder.mPriceBefore.setText("");
            holder.mPriceAfter.setText("");
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!ResortRegistrationFragment.selectedCategories.contains(categoryList.get(position).getResortTypeName())) {
                    ResortRegistrationFragment.selectedCategories.clear();
                    ResortRegistrationFragment.resortTypeID = categoryList.get(position).getResortTypeId();
                    ResortRegistrationFragment.selectedCategories.add(categoryList.get(position).getResortTypeName());
                    ResortRegistrationFragment.minCapacity = "";
                    ResortRegistrationFragment.maxCapacity = "";
                    ResortRegistrationFragment.changePriceFields();
                    notifyDataSetChanged();
                }
            }
        });

        holder.mPriceBefore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(selectedPos == position) {
                    ResortRegistrationFragment.minCapacity = holder.mPriceBefore.getText().toString();
                }
            }
        });

        holder.mPriceAfter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(selectedPos == position) {
                    ResortRegistrationFragment.maxCapacity = holder.mPriceAfter.getText().toString();
                }
            }
        });

        return convertView;
    }

}
