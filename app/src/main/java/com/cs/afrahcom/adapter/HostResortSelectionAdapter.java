package com.cs.afrahcom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.HostResortSelectionActivity;
import com.cs.afrahcom.models.HostResorts;

import java.util.ArrayList;

/**
 * Created by CS on 15-11-2017.
 */

public class HostResortSelectionAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<HostResorts> historyArrayList = new ArrayList<>();
    String language;
    public HostResortSelectionAdapter(Context context, ArrayList<HostResorts> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvresortName, tvResortType;
        ImageView checkbox;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_resort_selection, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_resort_selection_ar, null);
            }

            holder.tvresortName = (TextView) convertView.findViewById(R.id.resortName);
            holder.tvResortType = (TextView) convertView.findViewById(R.id.resortType);
            holder.checkbox = (ImageView) convertView.findViewById(R.id.checkbox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(language.equalsIgnoreCase("En")) {
            holder.tvresortName.setText(historyArrayList.get(position).getResortname());

            if (historyArrayList.get(position).getResorttype().equals("1")) {
                holder.tvResortType.setText("Wedding Hall");
            } else if (historyArrayList.get(position).getResorttype().equals("3")) {
                holder.tvResortType.setText("Banquet Hall");
            } else if (historyArrayList.get(position).getResorttype().equals("2")) {
                holder.tvResortType.setText("Resort");
            }
        }
        else{
            holder.tvresortName.setText(historyArrayList.get(position).getResortnameAr());

            if (historyArrayList.get(position).getResorttype().equals("1")) {
                holder.tvResortType.setText("قصور الافراح");
            } else if (historyArrayList.get(position).getResorttype().equals("3")) {
                holder.tvResortType.setText("الفنادق");
            } else if (historyArrayList.get(position).getResorttype().equals("2")) {
                holder.tvResortType.setText("الشاليهات");
            }
        }

        if(HostResortSelectionActivity.resortID.equals(historyArrayList.get(position).getResortId())){
            holder.checkbox.setImageDrawable(context.getResources().getDrawable(R.drawable.catering_selected));
        }
        else{
            holder.checkbox.setImageDrawable(context.getResources().getDrawable(R.drawable.catering_unselected));
        }

        return convertView;
    }
}
