package com.cs.afrahcom.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.SignInActivity;
import com.cs.afrahcom.models.ResortDetailsFiltered;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 15-06-2016.
 */
public class SearchResultsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    int favPosition = 0;
    String userId;
    SharedPreferences userPrefs;
    Activity mActivity;
    String language;
    AlertDialog customDialog;
    ArrayList<ResortDetailsFiltered> resortDetailsArrayList = new ArrayList<>();

    public SearchResultsAdapter(Context context, ArrayList<ResortDetailsFiltered> orderList, Activity mActivity, String language) {
        this.context = context;
        this.resortDetailsArrayList = orderList;
        this.mActivity = mActivity;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return resortDetailsArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvresortName, tvResortAddress, tvDiscountedPrice, tvActualPrice, tvDistance;
        ImageView imgFavIcon, imgResort, discountIcon;
        RatingBar ratingBar;
        RelativeLayout approvedLayout;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_search_result, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_search_result_ar, null);
            }

            holder.tvresortName = (TextView) convertView.findViewById(R.id.result_resort_name);
            holder.tvResortAddress = (TextView) convertView.findViewById(R.id.result_resort_address);
            holder.tvDiscountedPrice = (TextView) convertView.findViewById(R.id.result_discounted_price);
            holder.tvActualPrice = (TextView) convertView.findViewById(R.id.result_price);
            holder.tvDistance = (TextView) convertView.findViewById(R.id.result_distance);

            holder.imgFavIcon = (ImageView) convertView.findViewById(R.id.serach_fav_icon);
            holder.imgResort = (ImageView) convertView.findViewById(R.id.img_banner);
            holder.discountIcon = (ImageView) convertView.findViewById(R.id.discountIcon);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.rating_bar);

            holder.approvedLayout = (RelativeLayout) convertView.findViewById(R.id.approved_layout);

            userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LayerDrawable stars = (LayerDrawable) holder.ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(context.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###");
//        final DecimalFormat distanceFormat = new DecimalFormat("0.00");

        if(language.equalsIgnoreCase("En")) {
            holder.tvresortName.setText(resortDetailsArrayList.get(position).getResortName());
        }
        else{
            holder.tvresortName.setText(resortDetailsArrayList.get(position).getResortNameAr());
        }
        holder.tvResortAddress.setText(resortDetailsArrayList.get(position).getResortAddress());
        holder.tvActualPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getDiscountedPrice())))));
        holder.tvDiscountedPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getActualPrice())))));
        holder.tvDistance.setText((resortDetailsArrayList.get(position).getDistance())+" KM");

        if(resortDetailsArrayList.get(position).getResortImagesArray().size()>0) {
            if (resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("1")) {
                Glide.with(context.getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                        .placeholder(context.getResources().getDrawable(R.drawable.empty_photo)).into(holder.imgResort);
            } else if (resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("2")) {
                Glide.with(context.getApplicationContext()).load(Constants.SOURCE2_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                        .placeholder(context.getResources().getDrawable(R.drawable.empty_photo)).into(holder.imgResort);
            }
        }
        else{
            holder.imgResort.setImageDrawable(context.getResources().getDrawable(R.drawable.empty_photo));
        }

        if(Float.parseFloat(resortDetailsArrayList.get(position).getDiscount()) == 0){
            holder.discountIcon.setVisibility(View.GONE);
        }
        else{
            holder.discountIcon.setVisibility(View.VISIBLE);
        }

        if(resortDetailsArrayList.get(position).getRating().equals("0")){
            holder.ratingBar.setRating(5);
        }
        else{
            holder.ratingBar.setRating(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getRating())));
        }

        holder.tvDiscountedPrice.setPaintFlags(holder.tvDiscountedPrice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);

        if(resortDetailsArrayList.get(position).getDiscountedPrice().equals(resortDetailsArrayList.get(position).getActualPrice())){
            holder.tvDiscountedPrice.setVisibility(View.INVISIBLE);
        }
        else{
            holder.tvDiscountedPrice.setVisibility(View.VISIBLE);
        }

        if(resortDetailsArrayList.get(position).getIsFavorite().equalsIgnoreCase("true")){
            holder.imgFavIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
        }
        else{
            holder.imgFavIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_unselected));
        }

        if (resortDetailsArrayList.get(position).getApproved().equals("true")){
            holder.approvedLayout.setVisibility(View.VISIBLE);
        }
        else{
            holder.approvedLayout.setVisibility(View.GONE);
        }

        holder.imgFavIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userId = userPrefs.getString("userId", null);
                if(userId == null) {
//                    final iOSDialog iOSDialog = new iOSDialog(mActivity);
//                    iOSDialog.setTitle(context.getResources().getString(R.string.app_name));
//                    iOSDialog.setSubtitle("Please Login to use this action");
//                    iOSDialog.setNegativeLabel(context.getResources().getString(R.string.str_btn_ok));
//                    iOSDialog.setPositiveLabel("Login");
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setNegativeListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                            context.startActivity(new Intent(context, SignInActivity.class));
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mActivity);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = mActivity.getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(context.getResources().getString(R.string.app_name));
                        yes.setText(context.getResources().getString(R.string.str_btn_ok));
                        no.setText("Login");
                        desc.setText("Please login to use this action.");
                    }
                    else{
                        title.setText(context.getResources().getString(R.string.app_name_ar));
                        yes.setText(context.getResources().getString(R.string.str_btn_ok_ar));
                        no.setText("تسجيل الدخول");
                        desc.setText("هذه الخطوه تتطلب تسجيل الدخول");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            context.startActivity(new Intent(context, SignInActivity.class));
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = mActivity.getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                } else {
                    if (resortDetailsArrayList.get(position).getIsFavorite().equalsIgnoreCase("true")) {
                        favPosition = position;

                        JSONObject parentObj = new JSONObject();
                        try {
                            JSONObject mainObj = new JSONObject();
                            mainObj.put("ResortId", resortDetailsArrayList.get(position).getResortId());
                            mainObj.put("UserId", userId);

                            parentObj.put("FavouriteResorts", mainObj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", parentObj.toString());

                        new DeleteFavOrder().execute(parentObj.toString());
                    } else {
                        favPosition = position;

                        JSONObject parentObj = new JSONObject();
                        try {
                            JSONObject mainObj = new JSONObject();
                            mainObj.put("ResortId", resortDetailsArrayList.get(position).getResortId());
                            mainObj.put("UserId", userId);

                            parentObj.put("FavouriteResorts", mainObj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", parentObj.toString());

                        new InsertFavOrder().execute(parentObj.toString());
                    }
                }
            }
        });

        return convertView;
    }

    public class InsertFavOrder extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
//            dialog = ProgressDialog.show(context, "",
//                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.INSERT_FAVORITE_RESORT_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            resortDetailsArrayList.get(favPosition).setIsFavorite("true");
                            notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            } else {
                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            Snap sn = new Snap();
//            sn.insertfav();
            super.onPostExecute(result);

        }

    }
    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.DELETE_FAVORITE_RESORT_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            resortDetailsArrayList.get(favPosition).setIsFavorite("false");
                            notifyDataSetChanged();
//                            new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL + userId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            } else {
                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            Snap sn = new Snap();
//            sn.insertfav();
            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}