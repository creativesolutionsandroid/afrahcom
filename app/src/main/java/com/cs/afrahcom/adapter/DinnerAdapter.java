package com.cs.afrahcom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.CateringSearchActivity;
import com.cs.afrahcom.models.FoodDetails;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class DinnerAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<FoodDetails> DinnerList = new ArrayList<>();
    String language;
    public DinnerAdapter(Context context, ArrayList<FoodDetails> DinnerList, String language) {
        this.context = context;
        this.language = language;
        this.DinnerList = DinnerList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return DinnerList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mCheckBoxText;
        ImageView mCheckBoxImage;
        LinearLayout grid_layout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_filter_facilities_grid, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_filter_facilities_grid_ar, null);
            }
//
            holder.mCheckBoxText = (TextView) convertView.findViewById(R.id.checkboxText);
            holder.mCheckBoxImage = (ImageView) convertView.findViewById(R.id.checkboxImage);
            holder.grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);
//
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(CateringSearchActivity.dinnerSelected.contains(DinnerList.get(position).getFoodName())){
            holder.mCheckBoxImage.setImageDrawable(context.getResources().getDrawable(R.drawable.catering_selected));
        }
        else{
            holder.mCheckBoxImage.setImageDrawable(context.getResources().getDrawable(R.drawable.catering_unselected));
        }


        holder.mCheckBoxText.setText(DinnerList.get(position).getFoodName());
        holder.mCheckBoxText.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));

        return convertView;
    }

}
