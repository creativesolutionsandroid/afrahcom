package com.cs.afrahcom.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.HostEditResortDetailsActivity;
import com.cs.afrahcom.models.Facilities;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class HostEditFacilitiesAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Facilities> facilitiesList = new ArrayList<>();
    String language;
    public HostEditFacilitiesAdapter(Context context, ArrayList<Facilities> facilitiesList, String language) {
        this.context = context;
        this.language = language;
        this.facilitiesList = facilitiesList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return facilitiesList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mCheckBoxText;
        ImageView mCheckBoxImage;
        LinearLayout grid_layout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_filter_facilities_grid, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_filter_facilities_grid_ar, null);
            }

            holder.mCheckBoxText = (TextView) convertView.findViewById(R.id.checkboxText);
            holder.mCheckBoxImage = (ImageView) convertView.findViewById(R.id.checkboxImage);
            holder.grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(HostEditResortDetailsActivity.selectedFacilities.contains(facilitiesList.get(position).getFacilityName())){
            holder.mCheckBoxImage.setImageDrawable(context.getResources().getDrawable(R.drawable.catering_selected));
        }
        else{
            holder.mCheckBoxImage.setImageDrawable(context.getResources().getDrawable(R.drawable.catering_unselected));
        }

        if(language.equalsIgnoreCase("En")) {
            holder.mCheckBoxText.setText(facilitiesList.get(position).getFacilityName());
        }
        else{
            Log.i("TAG","fac "+facilitiesList.get(position).getFacilityNameAr());
            holder.mCheckBoxText.setText(facilitiesList.get(position).getFacilityNameAr());
        }

        holder.grid_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(HostEditResortDetailsActivity.selectedFacilities.contains(facilitiesList.get(position).getFacilityName())){
                    HostEditResortDetailsActivity.selectedFacilities.remove(facilitiesList.get(position).getFacilityName());
                    HostEditResortDetailsActivity.selectedFacilitiesId.remove(facilitiesList.get(position).getFacilityId());
                }
                else{
                    HostEditResortDetailsActivity.selectedFacilities.add(facilitiesList.get(position).getFacilityName());
                    HostEditResortDetailsActivity.selectedFacilitiesId.add(facilitiesList.get(position).getFacilityId());
                }
                notifyDataSetChanged();
            }
        });
        return convertView;
    }
}
