package com.cs.afrahcom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.ImagesModel;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class GalleryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<ImagesModel> galleryList = new ArrayList<>();

    public GalleryAdapter(Context context, ArrayList<ImagesModel> galleryList) {
        this.context = context;
        this.galleryList = galleryList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return galleryList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView mImage;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.list_gallery, null);

            holder.mImage = (ImageView) convertView.findViewById(R.id.gallery_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(galleryList.get(position).getSource().equals("1")) {
            Glide.with(context.getApplicationContext()).load(Constants.SOURCE1_URL + galleryList.get(position).getImageName())
                    .placeholder(context.getResources().getDrawable(R.drawable.empty_photo)).into(holder.mImage);
        }
        else if(galleryList.get(position).getSource().equals("2")) {
            Glide.with(context.getApplicationContext()).load(Constants.SOURCE2_URL + galleryList.get(position).getImageName())
                    .placeholder(context.getResources().getDrawable(R.drawable.empty_photo)).into(holder.mImage);
        }

        return convertView;
    }

}
