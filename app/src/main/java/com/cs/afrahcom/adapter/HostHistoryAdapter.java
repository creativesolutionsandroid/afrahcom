package com.cs.afrahcom.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.OrderHistory;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class HostHistoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderHistory> historyArrayList = new ArrayList<>();
    String language;

    public HostHistoryAdapter(Context context, ArrayList<OrderHistory> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvuserName, tvresortName, tvBookingStatus, tvPaymentStatus, tvReservationDate, tvNumofGuests, tvFood, tvMobile, tvInvoice,
                tvAdvance, tvTotalPrice;
        LinearLayout priceLayout, advanceLayout;
        ImageView payIcon;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_host_history, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_host_history_ar, null);
            }

            holder.tvresortName = (TextView) convertView.findViewById(R.id.propertyName);
            holder.tvuserName = (TextView) convertView.findViewById(R.id.resortName);
            holder.tvBookingStatus = (TextView) convertView.findViewById(R.id.bookingSatus);
            holder.tvPaymentStatus = (TextView) convertView.findViewById(R.id.paymentStatus);
            holder.tvReservationDate = (TextView) convertView.findViewById(R.id.reservationDate);
            holder.tvNumofGuests = (TextView) convertView.findViewById(R.id.numOfGuests);
            holder.tvFood = (TextView) convertView.findViewById(R.id.food);
            holder.tvInvoice = (TextView) convertView.findViewById(R.id.invoiceNo);
            holder.tvMobile = (TextView) convertView.findViewById(R.id.mobileNumber);
            holder.payIcon = (ImageView) convertView.findViewById(R.id.pin);
            holder.tvAdvance = (TextView) convertView.findViewById(R.id.historyAdvance);
            holder.tvTotalPrice = (TextView) convertView.findViewById(R.id.historyTotal);

            holder.advanceLayout = (LinearLayout) convertView.findViewById(R.id.advanceLayout);
            holder.priceLayout = (LinearLayout) convertView.findViewById(R.id.amountLayout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvresortName.setText(historyArrayList.get(position).getResortName());
        holder.tvuserName.setText(historyArrayList.get(position).getUserName());

        try {
            String[] reservDate = historyArrayList.get(position).getStartDate().split("T");
            String[] date = reservDate[0].split("-");
            String[] endDate = historyArrayList.get(position).getEndDate().split("T");
            String[] date1 = endDate[0].split("-");
            holder.tvReservationDate.setText(date[2]+"-"+date[1]+"-"+date[0]+" / "+date1[2]+"-"+date1[1]+"-"+date1[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        String[] invoiceParts = historyArrayList.get(position).getInvoiceNumber().split("-");
        holder.tvInvoice.setText("#"+historyArrayList.get(position).getInvoiceNumber());
        holder.tvMobile.setText("+"+historyArrayList.get(position).getMobile());

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###");
        holder.tvTotalPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(historyArrayList.get(position).getTotalAmount()))));
        holder.tvAdvance.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(historyArrayList.get(position).getAdvanceAmount()))));

        if(historyArrayList.get(position).getBookingStatus().equalsIgnoreCase("new")) {
            if(language.equalsIgnoreCase("En")) {
                holder.tvBookingStatus.setText("Booked");
            }
            else{
                holder.tvBookingStatus.setText("تم الحجز");
            }
        }
        else if(historyArrayList.get(position).getBookingStatus().equalsIgnoreCase("pending")) {
            if(language.equalsIgnoreCase("En")) {
                holder.tvBookingStatus.setText("Pending");
            }
            else{
                holder.tvBookingStatus.setText("قيد الانتظار");
            }
        }
        else if(historyArrayList.get(position).getBookingStatus().equalsIgnoreCase("close")) {
            if(language.equalsIgnoreCase("En")) {
                holder.tvBookingStatus.setText("Close");
            }
            else{
                holder.tvBookingStatus.setText("مغلق");
            }
        }
        else if(historyArrayList.get(position).getBookingStatus().equalsIgnoreCase("cancel")) {
            if(language.equalsIgnoreCase("En")) {
                holder.tvBookingStatus.setText("Cancel");
            }
            else{
                holder.tvBookingStatus.setText("الغاء");
            }
        }
        else if(historyArrayList.get(position).getBookingStatus().equalsIgnoreCase("rejected")) {
            if(language.equalsIgnoreCase("En")) {
                holder.tvBookingStatus.setText("Rejected");
            }
            else{
                holder.tvBookingStatus.setText("رفض");
            }
        }
        else{
            holder.tvBookingStatus.setText(historyArrayList.get(position).getBookingStatus());
        }

        if(historyArrayList.get(position).getBookingStatus().equalsIgnoreCase("cancel")
                || historyArrayList.get(position).getBookingStatus().equalsIgnoreCase("rejected")) {
            holder.tvBookingStatus.setTextColor(Color.parseColor("#FF0000"));
            holder.payIcon.setVisibility(View.INVISIBLE);
            holder.tvPaymentStatus.setVisibility(View.INVISIBLE);
            holder.advanceLayout.setVisibility(View.GONE);
            holder.priceLayout.setVisibility(View.GONE);
        }
        else{
            holder.tvBookingStatus.setTextColor(Color.parseColor("#4F5F6F"));
            holder.payIcon.setVisibility(View.VISIBLE);
            holder.tvPaymentStatus.setVisibility(View.VISIBLE);
        }

        if(historyArrayList.get(position).getPaymenyStatus().equalsIgnoreCase("Pending")){
            if(language.equalsIgnoreCase("En")) {
                holder.tvPaymentStatus.setText("Payment\nPending");
            }
            else{
                holder.tvPaymentStatus.setText("انتظار\nالدفع");
            }
            holder.tvPaymentStatus.setTextColor(Color.parseColor("#DE5F98"));
        }
        else{
            if(language.equalsIgnoreCase("En")) {
                holder.tvPaymentStatus.setText("Payment\nDone");
            }
            else{
                holder.tvPaymentStatus.setText("تم\nالدفع");
            }
            holder.tvPaymentStatus.setTextColor(Color.parseColor("#4F5F6F"));
        }

        int guests = 0;
        try {
            if(Integer.parseInt(historyArrayList.get(position).getNoOfMen())>0 && Integer.parseInt(historyArrayList.get(position).getNoOfWomen())>0){
                guests = Integer.parseInt(historyArrayList.get(position).getNoOfMen()) + Integer.parseInt(historyArrayList.get(position).getNoOfWomen());
            }
            else if(Integer.parseInt(historyArrayList.get(position).getNoOfMen())>0){
                guests = Integer.parseInt(historyArrayList.get(position).getNoOfMen());
            }
            else{
                guests = Integer.parseInt(historyArrayList.get(position).getNoOfWomen());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(language.equalsIgnoreCase("En")) {
            holder.tvNumofGuests.setText(guests + " guests in "
                    + historyArrayList.get(position).getResortType());
            if(historyArrayList.get(position).getIscateringSelected().equalsIgnoreCase("true") &&
                    historyArrayList.get(position).getIsKitchenSelected().equalsIgnoreCase("true")) {
                holder.tvFood.setVisibility(View.VISIBLE);
                holder.tvFood.setText("(Food + Kitchen)");
            }
            else if(historyArrayList.get(position).getIscateringSelected().equalsIgnoreCase("false") &&
                    historyArrayList.get(position).getIsKitchenSelected().equalsIgnoreCase("true")){
                holder.tvFood.setVisibility(View.VISIBLE);
                holder.tvFood.setText("(Kitchen)");
            }
            else if(historyArrayList.get(position).getIscateringSelected().equalsIgnoreCase("true") &&
                    historyArrayList.get(position).getIsKitchenSelected().equalsIgnoreCase("false")){
                holder.tvFood.setVisibility(View.VISIBLE);
                holder.tvFood.setText("(Food)");
            }
            else {
                holder.tvFood.setVisibility(View.GONE);
            }
        }
        else{
            if(historyArrayList.get(position).getIscateringSelected().equalsIgnoreCase("true") &&
                    historyArrayList.get(position).getIsKitchenSelected().equalsIgnoreCase("true")) {
                holder.tvFood.setVisibility(View.VISIBLE);
                holder.tvFood.setText("(الغذاء + مطبخ)");
            }
            else if(historyArrayList.get(position).getIscateringSelected().equalsIgnoreCase("false") &&
                    historyArrayList.get(position).getIsKitchenSelected().equalsIgnoreCase("true")){
                holder.tvFood.setVisibility(View.VISIBLE);
                holder.tvFood.setText("(مطبخ)");
            }
            else if(historyArrayList.get(position).getIscateringSelected().equalsIgnoreCase("true") &&
                    historyArrayList.get(position).getIsKitchenSelected().equalsIgnoreCase("false")){
                holder.tvFood.setVisibility(View.VISIBLE);
                holder.tvFood.setText("(الغذاء)");
            }
            else {
                holder.tvFood.setVisibility(View.GONE);
            }

            if (historyArrayList.get(position).getResortType().equalsIgnoreCase("wedding hall")) {
                holder.tvNumofGuests.setText(guests + " ضيف في قاعة القصر ");
            } else if (historyArrayList.get(position).getResortType().equalsIgnoreCase("banquet hall")) {
                holder.tvNumofGuests.setText(guests + " ضيف في قاعة الفنادق ");
            } else if (historyArrayList.get(position).getResortType().equalsIgnoreCase("resort")) {
                holder.tvNumofGuests.setText(guests + " ضيف في الشالية ");
            }
        }

        return convertView;
    }
}