package com.cs.afrahcom.adapter;

import android.content.Context;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.OrderHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.cs.afrahcom.R.id.reservationDate;

/**
 * Created by CS on 15-06-2016.
 */
public class HostPendingReservationsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderHistory> historyArrayList = new ArrayList<>();
    long remainingMillis = 60 * 2880 * 1000;
    String language;

    public HostPendingReservationsAdapter(Context context, String language, ArrayList<OrderHistory> orderList) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvuserName, tvMobileNumber, tvVisitingDate, tvtimer, tvNumofGuests, tvGuestType, tvReservationDate;
        CountDownTimer countDownTimer;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_pending_reservations, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_pending_reservations_ar, null);
            }

            holder.tvuserName = (TextView) convertView.findViewById(R.id.userName);
            holder.tvMobileNumber = (TextView) convertView.findViewById(R.id.mobileNumber);
            holder.tvVisitingDate = (TextView) convertView.findViewById(R.id.visitingDates);
            holder.tvReservationDate = (TextView) convertView.findViewById(reservationDate);
            holder.tvNumofGuests = (TextView) convertView.findViewById(R.id.guests);
            holder.tvtimer = (TextView) convertView.findViewById(R.id.timer);
            holder.tvGuestType = (TextView) convertView.findViewById(R.id.guestsType);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvuserName.setText(historyArrayList.get(position).getUserName());

        String[] reservDate = new String[0];
        try {
            reservDate = historyArrayList.get(position).getReservationDate().split("T");
            String[] date = reservDate[0].split("-");
            holder.tvReservationDate.setText(date[2]+"-"+date[1]+"-"+date[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String[] startDate = historyArrayList.get(position).getStartDate().split("T");
            String[] date1 = startDate[0].split("-");

            String[] endDate = historyArrayList.get(position).getEndDate().split("T");
            String[] date2 = endDate[0].split("-");
            holder.tvVisitingDate.setText("("+date1[2]+"-"+date1[1]+"-"+date1[0]+"/"+date2[2]+"-"+date2[1]+"-"+date2[0]+")");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Date bookedDate = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        String checkInDate = reservDate[0]+" "+reservDate[1];

        try {
            bookedDate = DateFormat.parse(checkInDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long startMillis = bookedDate.getTime();
        long currentMillis = calendar.getTimeInMillis();
        long endMillis = startMillis + (60 * 2880 * 1000);  /*48 hours*/
        remainingMillis = endMillis - currentMillis;

        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(remainingMillis),
                TimeUnit.MILLISECONDS.toMinutes(remainingMillis) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(remainingMillis) % TimeUnit.MINUTES.toSeconds(1));

        if(currentMillis>endMillis) {
            holder.tvtimer.setText("00:00:00");
        }
        else{
            holder.tvtimer.setText(Constants.convertToArabic(hms));
        }

        int guests = 0;
        try {
            if(Integer.parseInt(historyArrayList.get(position).getNoOfMen())>0 && Integer.parseInt(historyArrayList.get(position).getNoOfWomen())>0){
                guests = Integer.parseInt(historyArrayList.get(position).getNoOfMen()) + Integer.parseInt(historyArrayList.get(position).getNoOfWomen());
            }
            else if(Integer.parseInt(historyArrayList.get(position).getNoOfMen())>0){
                guests = Integer.parseInt(historyArrayList.get(position).getNoOfMen());
            }
            else{
                guests = Integer.parseInt(historyArrayList.get(position).getNoOfWomen());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tvNumofGuests.setText(""+guests);
        holder.tvMobileNumber.setText(historyArrayList.get(position).getMobile());
        if(historyArrayList.get(position).getIscateringSelected().equalsIgnoreCase("true")) {
            if(language.equalsIgnoreCase("Ar")) {
                if(historyArrayList.get(position).getResortType().equalsIgnoreCase("wedding hall")) {
                    holder.tvGuestType.setText("الغداء + الضيوف (قصور الافراح)");
                }
                else if(historyArrayList.get(position).getResortType().equalsIgnoreCase("banquet hall")) {
                    holder.tvGuestType.setText("الغداء + الضيوف (قاعات الفنادق)");
                }
                else if(historyArrayList.get(position).getResortType().equalsIgnoreCase("resort")) {
                    holder.tvGuestType.setText("الغداء + الضيوف (الشاليهات)");
                }
//                holder.tvGuestType.setText(historyArrayList.get(position).getResortType() + "الغذاء + الضيوف ");
            }
            else{
                holder.tvGuestType.setText("Guests + Food(" + historyArrayList.get(position).getResortType() + ")");
            }
        }
        else{
            if(language.equalsIgnoreCase("En")) {
                holder.tvGuestType.setText("Guests(" + historyArrayList.get(position).getResortType() + ")");
            }
            else{
                if(historyArrayList.get(position).getResortType().equalsIgnoreCase("wedding hall")) {
                    holder.tvGuestType.setText("الضيوف (قصور الافراح)");
                }
                else if(historyArrayList.get(position).getResortType().equalsIgnoreCase("banquet hall")) {
                    holder.tvGuestType.setText("الضيوف (قاعات الفنادق)");
                }
                else if(historyArrayList.get(position).getResortType().equalsIgnoreCase("resort")) {
                    holder.tvGuestType.setText("الضيوف (الشاليهات)");
                }
            }
        }

        return convertView;
    }
}