package com.cs.afrahcom.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.MainActivity;


public class DrawerListAdapter extends ArrayAdapter<String> {

    Context context;
    int layoutResourceId;
    private String[] data;
    int checkedPosition;
    String loggedinName, loggedinImage;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences countPrefs;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    public DrawerListAdapter(Context context, int layoutResourceId,
                             String[] data) {

        super(context, layoutResourceId, data);

        this.data = data;
        this.layoutResourceId = layoutResourceId;
        this.context = context;
    }

    public void setChecheckedPosition(int pos) {
        this.checkedPosition = pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        if (row == null) {

            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();

            holder.txtTitle = (TextView) row.findViewById(R.id.sidemenu_name);
            holder.tvLanguage = (TextView) row.findViewById(R.id.sidemenu_language);
            holder.tvCount = (TextView) row.findViewById(R.id.count);
            holder.sidemenu_list_layout = (RelativeLayout) row
                    .findViewById(R.id.sidemenu_list_layout);
            holder.sidemenu_viewline = (View) row
                    .findViewById(R.id.sidemenu_viewline);
            holder.layout = (RelativeLayout) row
                    .findViewById(R.id.menu_profile_layout);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if (position == 0) {
            holder.layout.setVisibility(View.VISIBLE);
            holder.sidemenu_list_layout.setVisibility(View.GONE);
        } else {
            holder.sidemenu_list_layout.setVisibility(View.VISIBLE);
            holder.layout.setVisibility(View.GONE);
            holder.txtTitle.setText(data[position]);
        }

        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        if(userPrefs.getString("mode","").equals("host") && position == 7){
            if(MainActivity.hostNotificationCount > 0) {
                holder.tvCount.setVisibility(View.VISIBLE);
                holder.tvCount.setText(""+MainActivity.hostNotificationCount);
            }
            else{
                holder.tvCount.setVisibility(View.GONE);
            }
            holder.tvLanguage.setVisibility(View.GONE);
        }
        else if(userPrefs.getString("mode","").equals("user") && position == 8){
            if(MainActivity.userNotificationCount > 0) {
                holder.tvCount.setVisibility(View.VISIBLE);
                holder.tvCount.setText(""+MainActivity.userNotificationCount);
            }
            else{
                holder.tvCount.setVisibility(View.GONE);
            }
            holder.tvLanguage.setVisibility(View.GONE);
        }
        else if(userPrefs.getString("mode","").equals("host") && position == 8){
            holder.tvCount.setVisibility(View.GONE);
            holder.tvLanguage.setVisibility(View.VISIBLE);
            if(language.equalsIgnoreCase("En")){
                holder.tvLanguage.setText("     عربي    ");
            }
            else {
                holder.tvLanguage.setText("English");
            }
        }
        else if(userPrefs.getString("mode","").equals("user") && position == 9){
            holder.tvCount.setVisibility(View.GONE);
            holder.tvLanguage.setVisibility(View.VISIBLE);
            if(language.equalsIgnoreCase("En")){
                holder.tvLanguage.setText("     عربي    ");
            }
            else {
                holder.tvLanguage.setText("English");
            }
        }
        else{
            holder.tvCount.setVisibility(View.GONE);
            holder.tvLanguage.setVisibility(View.GONE);
        }

        return row;
    }

    static class ViewHolder {
        RelativeLayout layout, sidemenu_list_layout;
        TextView txtTitle, tvLanguage, tvCount;
        View sidemenu_viewline;
    }

    public void languageUpdate(View v){
        Log.i("TAG","click event");
    }
}
