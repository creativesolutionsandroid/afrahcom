package com.cs.afrahcom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.SearchResultActivity;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class FacilitiesFilterAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<String> facilitiesList = new ArrayList<>();
    String language;

    public FacilitiesFilterAdapter(Context context, ArrayList<String> facilitiesList, String language) {
        this.context = context;
        this.facilitiesList = facilitiesList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return facilitiesList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mCheckBoxText;
        ImageView mCheckBoxImage;
        LinearLayout grid_layout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_filter_facilities_grid, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_filter_facilities_grid_ar, null);
            }

            holder.mCheckBoxText = (TextView) convertView.findViewById(R.id.checkboxText);
            holder.mCheckBoxImage = (ImageView) convertView.findViewById(R.id.checkboxImage);
            holder.grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(language.equalsIgnoreCase("Ar")){
            holder.grid_layout.setRotationY(180);
        }
        if(SearchResultActivity.facilitiesSelected.contains(facilitiesList.get(position))){
            holder.mCheckBoxImage.setImageDrawable(context.getResources().getDrawable(R.drawable.checkbox_selected));
        }
        else{
            holder.mCheckBoxImage.setImageDrawable(context.getResources().getDrawable(R.drawable.checkbox_unselected));
        }

        holder.mCheckBoxText.setText(facilitiesList.get(position));

        return convertView;
    }

}
