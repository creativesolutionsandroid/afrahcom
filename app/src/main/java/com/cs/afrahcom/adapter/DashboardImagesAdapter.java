package com.cs.afrahcom.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.ImagesModel;

import java.util.ArrayList;


/**
 * Created by cs android on 23-02-2017.
 */

public class DashboardImagesAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ImageView imgBanner;

    ArrayList<ImagesModel> bannerList;

    public DashboardImagesAdapter(Context context, ArrayList<ImagesModel> galleryList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.bannerList = galleryList;
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = null;
            itemView = mLayoutInflater.inflate(R.layout.list_dashboard_image, container, false);

        imgBanner =(ImageView)itemView.findViewById(R.id.gallery_image);

        if(bannerList.get(position).getSource().equals("1")) {
            Glide.with(mContext.getApplicationContext()).load(Constants.SOURCE1_URL + bannerList.get(position).getImageName()).into(imgBanner);
        }
        else if(bannerList.get(position).getSource().equals("2")){
            Glide.with(mContext.getApplicationContext()).load(Constants.SOURCE2_URL + bannerList.get(position).getImageName()).into(imgBanner);
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
