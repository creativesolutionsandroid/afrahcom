package com.cs.afrahcom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.Discounts;

import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class DiscountsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Discounts> resortDetailsArrayList = new ArrayList<>();
    String language;
    public DiscountsAdapter(Context context, ArrayList<Discounts> resortDetailsArrayList, String language) {
        this.context = context;
        this.resortDetailsArrayList = resortDetailsArrayList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return resortDetailsArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView tvStartDate, tvEndDate, tvRate;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_discounts, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_discounts_ar, null);
            }

            holder.tvStartDate = (TextView) convertView.findViewById(R.id.startDate);
            holder.tvEndDate = (TextView) convertView.findViewById(R.id.endDate);
            holder.tvRate = (TextView) convertView.findViewById(R.id.rate);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            String[] startDate = resortDetailsArrayList.get(position).getStartDate().split("T");
            String[] endDate = resortDetailsArrayList.get(position).getEndDate().split("T");

            String[] dates1 = startDate[0].split("-");
            String[] dates2 = endDate[0].split("-");

            holder.tvStartDate.setText(dates1[2]+"-"+dates1[1]+"-"+dates1[0]);
            holder.tvEndDate.setText(dates2[2]+"-"+dates2[1]+"-"+dates2[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Float rate = Float.parseFloat(resortDetailsArrayList.get(position).getDiscount());
        holder.tvRate.setText(""+ Constants.convertToArabic(rate.toString()));

        return convertView;
    }
}