package com.cs.afrahcom.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.ResortDetailsActivity;
import com.cs.afrahcom.models.ResortDetailsFiltered;

import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * Created by cs android on 23-02-2017.
 */

public class BannersAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ImageView imgBanner;
    TextView tvResortName, tvResortAddress, tvResortPrice;
    String language;
    RatingBar ratingBar;
    RelativeLayout approvedLayout, layout;

    ArrayList<ResortDetailsFiltered> bannerList;

    public BannersAdapter(Context context, ArrayList<ResortDetailsFiltered> bannerList, String language) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.language = language;
        this.bannerList = bannerList;
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = null;
        if(language.equalsIgnoreCase("En")) {
            itemView = mLayoutInflater.inflate(R.layout.list_banners, container, false);
        }
        else{
            itemView = mLayoutInflater.inflate(R.layout.list_banners_ar, container, false);
        }
        imgBanner =(ImageView)itemView.findViewById(R.id.img_banner);
        tvResortName =(TextView)itemView.findViewById(R.id.banner_resort_name);
        tvResortAddress =(TextView) itemView.findViewById(R.id.banner_resort_address);
        tvResortPrice =(TextView) itemView.findViewById(R.id.banner_resort_price);
        ratingBar =(RatingBar) itemView.findViewById(R.id.rating_bar);
        approvedLayout =(RelativeLayout) itemView.findViewById(R.id.approved_layout);
        layout =(RelativeLayout) itemView.findViewById(R.id.layout);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(mContext.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(mContext.getResources().getColor(R.color.ratingBarColorinBanners), PorterDuff.Mode.SRC_ATOP);

        if(language.equalsIgnoreCase("En")) {
            tvResortName.setText(bannerList.get(position).getResortName());
        }
        else{
            tvResortName.setText(bannerList.get(position).getResortNameAr());
        }
        tvResortAddress.setText(bannerList.get(position).getResortAddress());
        if(bannerList.get(position).getResortImagesArray().get(0).getSource().equals("1")) {
            Glide.with(mContext.getApplicationContext()).load(Constants.SOURCE1_URL + bannerList.get(position).getResortImagesArray().get(0).getImageName()).into(imgBanner);
        }
        else if(bannerList.get(position).getResortImagesArray().get(0).getSource().equals("2")){
            Glide.with(mContext.getApplicationContext()).load(Constants.SOURCE2_URL + bannerList.get(position).getResortImagesArray().get(0).getImageName()).into(imgBanner);
        }
         if(bannerList.get(position).getRating().equals("0")){
            ratingBar.setRating(5);
        }
        else{
            ratingBar.setRating(Float.parseFloat(bannerList.get(position).getRating()));
        }

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###");
        try {
            float price = Float.parseFloat(bannerList.get(position).getDiscountedPrice());
            tvResortPrice.setText(""+priceFormat.format(price));
        } catch (Exception e) {
            tvResortPrice.setText("2000");
            e.printStackTrace();
        }

        if (bannerList.get(position).getApproved().equals("true")){
            approvedLayout.setVisibility(View.VISIBLE);
        }
        else{
            approvedLayout.setVisibility(View.GONE);
        }

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ResortDetailsActivity.class);
                intent.putExtra("array",bannerList);
                intent.putExtra("position",position);
                intent.putExtra("class","banners");
                mContext.startActivity(intent);
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
