package com.cs.afrahcom.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;

import java.util.List;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 21-12-2017.
 */

public class MoreActivity extends AppCompatActivity implements View.OnClickListener{

    TextView english, arabic;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    SharedPreferences hostPrefs;
    String hostId;
    String userId;
    LinearLayout moreFb, moreTwitter, moreYoutube, moreInstagram, moreRate, aboutUs, contactUs;
    SharedPreferences userPrefs;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        languagePrefsEditor = languagePrefs.edit();
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_more);
        }
        else{
            setContentView(R.layout.activity_more_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostId = hostPrefs.getString("hostId", "0");

        moreFb = (LinearLayout) findViewById(R.id.more_fb);
        moreTwitter = (LinearLayout) findViewById(R.id.more_twitter);
        moreYoutube = (LinearLayout) findViewById(R.id.more_youtube);
        moreInstagram = (LinearLayout) findViewById(R.id.more_instagram);
        moreRate = (LinearLayout) findViewById(R.id.more_rate);
        aboutUs = (LinearLayout) findViewById(R.id.aboutus_layout);
        contactUs = (LinearLayout) findViewById(R.id.contactus_layout);
        english = (TextView) findViewById(R.id.lang_english);
        arabic = (TextView) findViewById(R.id.lang_arabic);

//        english.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                languagePrefsEditor.putString("language", "En");
//                languagePrefsEditor.apply();
//                new ChangeLanguage().execute(Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language=En");
////                startActivity(new Intent(MoreActivity.this, MoreActivity.class));
//            }
//        });
//
//        arabic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                languagePrefsEditor.putString("language", "Ar");
//                languagePrefsEditor.apply();
//                Log.i("TAG",""+Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language=Ar");
//                new ChangeLanguage().execute(Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language=Ar");
//
////                startActivity(new Intent(MoreActivity.this, MoreActivity.class));
////                setResult(RESULT_OK);
////                finish();
//            }
//        });

        moreFb.setOnClickListener(this);
        moreTwitter.setOnClickListener(this);
        moreYoutube.setOnClickListener(this);
        moreInstagram.setOnClickListener(this);
        moreRate.setOnClickListener(this);
        aboutUs.setOnClickListener(this);
        contactUs.setOnClickListener(this);
        english.setOnClickListener(this);
        arabic.setOnClickListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.more_fb:
                Intent fbIntent = new Intent(MoreActivity.this, SocialMediaWebView.class);
                fbIntent.putExtra("title", "Facebook");
                fbIntent.putExtra("url", "https://www.facebook.com/Afrahcomapp/");
                startActivity(fbIntent);
                break;

            case R.id.more_twitter:
                Intent twitterIntent = new Intent(MoreActivity.this, SocialMediaWebView.class);
                twitterIntent.putExtra("title", "Twitter");
                twitterIntent.putExtra("url", "https://twitter.com/AfrahcomApp");
                startActivity(twitterIntent);
                break;

            case R.id.more_youtube:
                Intent youtubeIntent = new Intent(MoreActivity.this, SocialMediaWebView.class);
                youtubeIntent.putExtra("title", "Youtube");
                youtubeIntent.putExtra("url", "https://www.youtube.com/channel/UCBhJGSwrmweDhpBziXI789w?disable_polymer=true");
                startActivity(youtubeIntent);
                break;

            case R.id.more_instagram:
                Intent instagramIntent = new Intent(MoreActivity.this, SocialMediaWebView.class);
                instagramIntent.putExtra("title", "Instagram");
                instagramIntent.putExtra("url", "https://www.instagram.com/afrahcomapp/");
                startActivity(instagramIntent);
                break;

            case R.id.more_rate:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.afrahcom")));
                break;

            case R.id.aboutus_layout:
                Intent aboutIntent = new Intent(MoreActivity.this, SocialMediaWebView.class);
                if(language.equalsIgnoreCase("En")) {
                    aboutIntent.putExtra("title", "About us");
                    aboutIntent.putExtra("url", "http://csadms.com/AfrahcomBackend/terms/AboutEn");
                }
                else{
                    aboutIntent.putExtra("title", "عنا");
                    aboutIntent.putExtra("url", "http://csadms.com/AfrahcomBackend/terms/AboutAr");
                }
                startActivity(aboutIntent);
                break;
            case R.id.contactus_layout:

                Intent i = null;
                try {
                    i = new Intent(Intent.ACTION_SEND);
                    i.setType("plain/text");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{"Supportteam@AfrahCOMapp.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT, "Afrahcom Experience");
                    i.putExtra(Intent.EXTRA_TITLE, "Afrahcom Experience");
                    final PackageManager pm = getPackageManager();
                    final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                    String className = null;
                    for (final ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                            className = info.activityInfo.name;

                            if (className != null && !className.isEmpty()) {
                                break;
                            }
                        }
                    }
                    i.setClassName("com.google.android.gm", className);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MoreActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lang_english:
                languagePrefsEditor.putString("language", "En");
                languagePrefsEditor.apply();
                new ChangeLanguage().execute(Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language=En");
//                startActivity(new Intent(MoreActivity.this, MoreActivity.class));
                break;
            case R.id.lang_arabic:
                languagePrefsEditor.putString("language", "Ar");
                languagePrefsEditor.apply();
                Log.i("TAG",""+Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language=Ar");
                new ChangeLanguage().execute(Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language=Ar");

                break;
        }
    }

    public class ChangeLanguage extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MoreActivity.this);
            dialog = new ACProgressFlower.Builder(MoreActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                String response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            setResult(RESULT_OK);
            finish();
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }
}
