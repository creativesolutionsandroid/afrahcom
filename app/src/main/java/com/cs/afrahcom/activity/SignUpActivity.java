package com.cs.afrahcom.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Callbacks.GetUserCallback;
import com.cs.afrahcom.Callbacks.UserRequest;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.Cities;
import com.cs.afrahcom.models.User;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 21-09-2017.
 */

public class SignUpActivity extends Activity implements View.OnClickListener, GetUserCallback.IGetUserResponse{

    private TextView tvForgetPassword, tvSignup, tvTerms;
    private EditText etFirstName, etMiddleName, etEmail, etMobile, etPassword;
    private String mNameStr, mMiddleNameStr, mFamilyNameStr, mEmailStr, mMobileStr, mPasswordStr;
    ImageView checkboxTnC;
    RelativeLayout tncLayout;
    TextView cityName;
    Spinner citySpinner;
    public static final String[] SMS_RECIEVER = {
            android.Manifest.permission.RECEIVE_SMS
    };
    public static final int SMS_REQUEST = 1;
    public static final int VERIFICATION_REQUEST = 2;
    public static final int RC_SIGN_IN = 9001;
    private static final String EMAIL = "email";
    private static final String PROFILE = "public_profile";
    RelativeLayout fbLoginbtn, googleLoginbtn;
    Context mContext;
    ImageView imgCancel;
    SharedPreferences languagePrefs;
    String language;
    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;
    int mCityPos = -1;
    Boolean isCitySet = false, isTermsChecked = false;
    AlertDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_signup);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_signup_ar);
        }
        mContext = this;
        FacebookSdk.sdkInitialize(getApplicationContext());

        tvForgetPassword = (TextView) findViewById(R.id.tv_forget_Password);
        tvSignup = (TextView) findViewById(R.id.tv_signup);

        etFirstName = (EditText) findViewById(R.id.et_signup_first_name);
        etMiddleName = (EditText) findViewById(R.id.et_signup_middle_name);
        cityName = (TextView) findViewById(R.id.et_signup_city);
        etEmail = (EditText) findViewById(R.id.et_signup_email);
        etMobile = (EditText) findViewById(R.id.et_signup_mobile);
        etPassword = (EditText) findViewById(R.id.et_signup_password);

        imgCancel = (ImageView) findViewById(R.id.image_cancel);
        checkboxTnC = (ImageView) findViewById(R.id.checkboxTnC);
        tncLayout = (RelativeLayout) findViewById(R.id.tncLayout);

        citySpinner = (Spinner) findViewById(R.id.city_spinner);

        tvTerms = (TextView) findViewById(R.id.tNcText);

        fbLoginbtn = (RelativeLayout) findViewById(R.id.btn_fb_login);
        googleLoginbtn = (RelativeLayout) findViewById(R.id.btn_gmail_login);

        final ArrayList<String> cityNames = new ArrayList<>();
        for(Cities city : Constants.CityArrayList){
            cityNames.add(city.getCityName());
        }
        ArrayAdapter<String> cityAdapter;
        cityAdapter = new ArrayAdapter<String>(SignUpActivity.this, R.layout.list_spinner, cityNames) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(18);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.colorSignupText));

                return v;
            }
        };
        citySpinner.setAdapter(cityAdapter);

//        Requesting permission for auto otp reading
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if(!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)){
                requestPermissions(SMS_RECIEVER, SMS_REQUEST);
            }
        }

        etMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(etMobile.getText().toString().equals("0")){
                    etMobile.setText("");
                }
            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(isCitySet) {
                    cityName.setText(cityNames.get(i));
                    mCityPos = i;
                    cityName.setTextSize(18);
                }
                else{
                    if(language.equalsIgnoreCase("En")) {
                        cityName.setText("City(Optional)");
                    }
                    else{
                        cityName.setText("المدينة (اختياري)");
                    }
                    isCitySet = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkboxTnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isTermsChecked){
                    isTermsChecked = true;
                    checkboxTnC.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                }
                else{
                    isTermsChecked = false;
                    checkboxTnC.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                }
            }
        });

//        FaceBook Integration
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(
                callbackManager,
                new FacebookCallback < LoginResult > () {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // Handle success
                        UserRequest.makeUserRequest(new GetUserCallback(SignUpActivity.this).getCallback());
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(getApplicationContext(),"Not able to fetch details", Toast.LENGTH_SHORT).show();
                    }
                }
        );

//        Google Integration
// Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        googleLoginbtn.setOnClickListener(this);
        fbLoginbtn.setOnClickListener(this);
        tvSignup.setOnClickListener(this);
        tvForgetPassword.setOnClickListener(this);
        imgCancel.setOnClickListener(this);
        tvTerms.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tNcText:
//                Intent intent = new Intent(SignUpActivity.this, WebViewActivity.class);
//                intent.putExtra("webview_toshow", "user");
//                startActivity(intent);
                Intent aboutIntent = new Intent(SignUpActivity.this, SocialMediaWebView.class);
                if(language.equalsIgnoreCase("En")) {
                    aboutIntent.putExtra("title", "Terms and Conditions");
                    aboutIntent.putExtra("url", "http://csadms.com/AfrahcomBackend/terms/index");
                }
                else{
                    aboutIntent.putExtra("title", "الأحكام والشروط");
                    aboutIntent.putExtra("url", "http://csadms.com/AfrahcomBackend/terms/termAr");
                }
                startActivity(aboutIntent);
                break;
            case R.id.btn_gmail_login:
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.btn_fb_login:
//                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(
                        this,
                        Arrays.asList(PROFILE, EMAIL)
                );
                break;
            case R.id.tv_forget_Password:
                Intent intent12 = new Intent(SignUpActivity.this, ForgotPasswordActivity.class);
                startActivity(intent12);
                break;

            case R.id.image_cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;

            case R.id.tv_signup:
                final JSONObject parent = new JSONObject();
                mNameStr = etFirstName.getText().toString();
                mFamilyNameStr = etMiddleName.getText().toString();
//                mFamilyNameStr = cityName.getText().toString();
                mMobileStr = etMobile.getText().toString();
                mEmailStr = etEmail.getText().toString().replaceAll(" ","");
                mPasswordStr = etPassword.getText().toString();
                String cityId = "";
                if(mCityPos != -1){
                    cityId = Constants.CityArrayList.get(mCityPos).getCityId();
                }
                if(mNameStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etFirstName.setError(getResources().getString(R.string.str_alert_name));
                    }
                    else{
                        etFirstName.setError(getResources().getString(R.string.str_alert_name_ar));
                    }
                }
                else if(mFamilyNameStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etMiddleName.setError(getResources().getString(R.string.str_signup_middle_name_hint));
                    }
                    else{
                        etMiddleName.setError(getResources().getString(R.string.str_signup_middle_name_hint_ar));
                    }
                }
                else if(mEmailStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etEmail.setError(getResources().getString(R.string.str_alert_email));
                    }
                    else{
                        etEmail.setError(getResources().getString(R.string.str_alert_email_ar));
                    }
                }
                else if(!isValidEmail(mEmailStr)){
                    if(language.equalsIgnoreCase("En")) {
                        etEmail.setError(getResources().getString(R.string.str_alert_valid_email));
                    }
                    else{
                        etEmail.setError(getResources().getString(R.string.str_alert_valid_email_ar));
                    }
                }
                else if(mMobileStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etMobile.setError(getResources().getString(R.string.str_alert_mobile));
                    }
                    else{
                        etMobile.setError(getResources().getString(R.string.str_alert_mobile_ar));
                    }
                }
                else if(mMobileStr.length() != 9){
                    if(language.equalsIgnoreCase("En")) {
                        etMobile.setError(getResources().getString(R.string.str_alert_valid_mobile));
                    }
                    else{
                        etMobile.setError(getResources().getString(R.string.str_alert_valid_mobile_ar));
                    }
                }
                else if (mPasswordStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etPassword.setError(getResources().getString(R.string.str_alert_password));
                    }
                    else{
                        etPassword.setError(getResources().getString(R.string.str_alert_password_ar));
                    }
                }
                else if(mPasswordStr.length()<8){
                    if(language.equalsIgnoreCase("En")) {
                        etPassword.setError(getResources().getString(R.string.str_alert_valid_password));
                    }
                    else{
                        etPassword.setError(getResources().getString(R.string.str_alert_valid_password_ar));
                    }
                }
                else if(!isTermsChecked){
                    showDialog("Please review and accept the terms of service", "من فضلك راجع وأقبل شروط الخدمة");
                }
                else{

                    try {
                        JSONArray mainItem = new JSONArray();

                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FullName",mNameStr);
                        mainObj.put("MiddleName","");
                        mainObj.put("FamilyName",mFamilyNameStr);
                        mainObj.put("CityId", cityId);
                        mainObj.put("Email", mEmailStr);
                        mainObj.put("Language", language);
                        mainObj.put("Mobile", "966"+mMobileStr);
                        mainObj.put("Password", mPasswordStr);
                        mainObj.put("DeviceToken", SplashScreenActivity.regId);
                        mainObj.put("DeviceType", "Android");
                        mainItem.put(mainObj);

                        parent.put("userdetails", mainItem);
                        Log.i("TAG", parent.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    Alert Dialog
//                    final iOSDialog iOSDialog = new iOSDialog(SignUpActivity.this);
//                    iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                    iOSDialog.setSubtitle("We will be verifying the mobile number:\n\n+966" +" " + mMobileStr + "\n\nIs this OK, or would you like to edit the number?");
//                    iOSDialog.setNegativeLabel(getResources().getString(R.string.str_btn_ok));
//                    iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_edit));
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setNegativeListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                            new GetVerificationCode().execute(Constants.VERIFY_MOBILE+ "966"+mMobileStr, parent.toString());
//                        }
//                    });
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            etMobile.requestFocus();
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setText(getResources().getString(R.string.str_btn_ok));
                    yes.setText(getResources().getString(R.string.str_btn_edit));
                    desc.setText("We will be verifying the mobile number:\n\n+966" + " " + mMobileStr + "\n\nIs this OK, or would you like to edit the number?");

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            etMobile.requestFocus();
                        }
                    });
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            new GetVerificationCode().execute(Constants.VERIFY_MOBILE+ "966"+mMobileStr+"&language="+language, parent.toString());
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;
        }
    }

    public class GetVerificationCode extends AsyncTask<String, Integer, String>{
        String  networkStatus;
        ACProgressFlower dialog;
        String response;
        String userData;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(SignUpActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                userData = params[1];
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                String otp = jo.getString("Success");
//                                String otp = jo1.getString("OTP");
//                                String phNo = jo1.getString("MobileNo");
//
                                Intent i = new Intent(SignUpActivity.this, VerifyOTPActivity.class);
                                i.putExtra("OTP", otp);
                                i.putExtra("phone_number", "966"+mMobileStr);
                                i.putExtra("user_data", userData);
                                i.putExtra("forgot",false);
                                i.putExtra("screen", "user");
                                startActivityForResult(i, VERIFICATION_REQUEST);

                            }catch (JSONException je){
                                String msg = jo.getString("Failure");
                                je.printStackTrace();

//                                final iOSDialog iOSDialog = new iOSDialog(SignUpActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(msg);
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText(msg);
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText(msg);
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == VERIFICATION_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }else if (requestCode == VERIFICATION_REQUEST && resultCode == RESULT_CANCELED){
            setResult(RESULT_CANCELED);
            finish();
        }
        else if(requestCode == RC_SIGN_IN){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onCompleted(User user) {
//        Results from Facebook
        if(user.getEmail() != null){
            etEmail.setText(user.getEmail());
        }
        String[] name = user.getName().split(" ");
        etFirstName.setText(name[0]);
        try {
            etMiddleName.setText(name[1]);
            etMiddleName.setSelection(etMiddleName.getText().length());
        } catch (Exception e) {
            e.printStackTrace();
        }
        etEmail.setSelection(etEmail.getText().length());
        etFirstName.setSelection(etFirstName.getText().length());
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            etEmail.setText(account.getEmail());
            etFirstName.setText(account.getGivenName());
            etMiddleName.setText(account.getFamilyName());
            etEmail.setSelection(etEmail.getText().length());
            etMiddleName.setSelection(etMiddleName.getText().length());
            etFirstName.setSelection(etFirstName.getText().length());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.i("TAG", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void showDialog(String description, String description_ar){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if(language.equalsIgnoreCase("En")) {
            title.setText(getResources().getString(R.string.app_name));
            yes.setText(getResources().getString(R.string.str_btn_ok));
            desc.setText(description);
        }
        else{
            title.setText(getResources().getString(R.string.app_name_ar));
            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
            desc.setText(description_ar);
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
