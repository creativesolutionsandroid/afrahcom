package com.cs.afrahcom.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.RushDaysAdapter;
import com.cs.afrahcom.models.ResortDetails;
import com.cs.afrahcom.models.RushDays;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 18-12-2017.
 */

public class RushDaysActivity extends AppCompatActivity {

    ArrayList<ResortDetails> resortDetailsArrayList = new ArrayList<>();
    RushDaysAdapter mAdapter;
    TextView addRushDays, noRushDays;
    SwipeMenuListView rushDaysListView;
    private static int ADD_RUSH_HOURS = 2;
    TextView propertyName;
    int pos;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    String language;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_rush_days);
        }
        else{
            setContentView(R.layout.activity_rush_days_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addRushDays = (TextView) findViewById(R.id.tv_add);
        noRushDays = (TextView) findViewById(R.id.noRushDays);
        propertyName = (TextView) findViewById(R.id.propertyName);
        resortDetailsArrayList = (ArrayList<ResortDetails>) getIntent().getSerializableExtra("array");
        rushDaysListView = (SwipeMenuListView) findViewById(R.id.rushDaysListView);

        if(language.equalsIgnoreCase("En")) {
            propertyName.setText(resortDetailsArrayList.get(0).getResortName());
        }
        else{
            propertyName.setText(resortDetailsArrayList.get(0).getResortNameAr());
        }

        if(resortDetailsArrayList.get(0).getRushDays().size() == 0){
            noRushDays.setVisibility(View.VISIBLE);
        }

        String resortID = resortDetailsArrayList.get(0).getResortId();
        new GetResortDetails().execute(Constants.RESORT_DASHBOARD_URL+resortID+
                "&UserId=0&UserType=2");

        addRushDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RushDaysActivity.this, AddRushDaysActivity.class);
                intent.putExtra("array", resortDetailsArrayList);
                intent.putExtra("edit", false);
                startActivityForResult(intent, ADD_RUSH_HOURS);
            }
        });

        // step 1. create a MenuCreator
        SwipeMenuCreator creator2 = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menu.getViewType()) {
                    case 0:

//                        SwipeMenuItem edit = new SwipeMenuItem(
//                                getApplicationContext());
//                        // set item background
//                        edit.setBackground(new ColorDrawable(Color.rgb(0x00,
//                                0xaa, 0xaa)));
//                        // set item width
//                        edit.setWidth(dp2px(90));
//
//                        edit.setTitle("Edit");
//
//                        // set item title fontsize
//                        edit.setTitleSize(18);
//                        // set item title font color
//                        edit.setTitleColor(Color.WHITE);
//
//                        menu.addMenuItem(edit);

                        // create menu of type 0
                        SwipeMenuItem delete = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        delete.setBackground(new ColorDrawable(Color.rgb(0xff,
                                0x00, 0x00)));
                        // set item width
                        delete.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            delete.setTitle("Delete");
                        }
                        else{
                            delete.setTitle("حذف");
                        }

                        // set item title fontsize
                        delete.setTitleSize(18);
                        // set item title font color
                        delete.setTitleColor(Color.WHITE);

                        menu.addMenuItem(delete);


                        break;
                }
            }

        };
        rushDaysListView.setMenuCreator(creator2);

        // step 2. listener item click event
        rushDaysListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
//                        final iOSDialog iOSDialog = new iOSDialog(RushDaysActivity.this);
//                        String appName, title, positive, negative;
//                        if(language.equalsIgnoreCase("En")){
//                            appName = getResources().getString(R.string.app_name);
//                            title = "Are you sure to delete the rush day?";
//                            positive = getResources().getString(R.string.str_btn_yes);
//                            negative = getResources().getString(R.string.str_btn_no);
//                        }
//                        else{
//                            appName = getResources().getString(R.string.app_name_ar);
//                            title = "هل أنت متأكد من حذف يوم الذروة؟";
//                            positive = getResources().getString(R.string.str_btn_yes_ar);
//                            negative = getResources().getString(R.string.str_btn_no_ar);
//                        }
//                        iOSDialog.setTitle(appName);
//                        iOSDialog.setSubtitle(title);
//                        iOSDialog.setPositiveLabel(positive);
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setNegativeLabel(negative);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                                pos = position;
//                                String rushId = resortDetailsArrayList.get(0).getRushDays().get(position).getRushId();
//                                String resortId = resortDetailsArrayList.get(0).getResortId();
//                                Log.i("TAG","rushId "+rushId);
//                                try {
//                                    JSONObject mainObj = new JSONObject();
//                                    JSONObject body = new JSONObject();
//
//                                    body.put("ResortId",resortId);
//                                    body.put("RushId",rushId);
//                                    mainObj.put("DeleteRush",body);
//                                    Log.i("TAG",mainObj.toString());
//
//                                    new DeleteRushDay().execute(mainObj.toString());
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
//                        iOSDialog.setNegativeListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    iOSDialog.dismiss();
//                                }
//                            });
//                        iOSDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RushDaysActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        String title, positive, negative;
                        if(language.equalsIgnoreCase("En")){
                            title = "Are you sure to delete the rush day?";
                            positive = getResources().getString(R.string.str_btn_yes);
                            negative = getResources().getString(R.string.str_btn_no);
                        }
                        else{
                            title = "هل أنت متأكد من حذف يوم الذروة؟";
                            positive = getResources().getString(R.string.str_btn_yes_ar);
                            negative = getResources().getString(R.string.str_btn_no_ar);
                        }

                        desc.setText(title);
                        yes.setText(positive);
                        no.setText(negative);

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                pos = position;
                                String rushId = resortDetailsArrayList.get(0).getRushDays().get(position).getRushId();
                                String resortId = resortDetailsArrayList.get(0).getResortId();
                                try {
                                    JSONObject mainObj = new JSONObject();
                                    JSONObject body = new JSONObject();

                                    body.put("ResortId",resortId);
                                    body.put("RushId",rushId);
                                    mainObj.put("DeleteRush",body);
                                    Log.i("TAG",mainObj.toString());

                                    new DeleteRushDay().execute(mainObj.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                        break;
                }
                return false;
            }
        });
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_RUSH_HOURS && resultCode == RESULT_OK){
            String resortID = resortDetailsArrayList.get(0).getResortId();
            new GetResortDetails().execute(Constants.RESORT_DASHBOARD_URL+resortID+
                    "&UserId=0&UserType=2");
        }
    }

    public class GetResortDetails extends AsyncTask<String, Integer, String> {
        String response;
        String networkStatus;
        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(RushDaysActivity.this);
            dialog = new ACProgressFlower.Builder(RushDaysActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(RushDaysActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            resortDetailsArrayList.clear();
                            JSONObject jo= new JSONObject(result);
                            JSONObject successObj = jo.getJSONObject("Success");

                            JSONArray resortArray = successObj.getJSONArray("ResortDetails");
                            JSONObject jsonObject = resortArray.getJSONObject(0);
                            ResortDetails resortDetails = new ResortDetails();
                            resortDetails.setResortName(jsonObject.getString("ResortName"));
                            resortDetails.setResortNameAr(jsonObject.getString("ResortName_Ar"));
                            resortDetails.setResortId(jsonObject.getString("ResortId"));
                            ArrayList<RushDays> rushDaysArrayList = new ArrayList<>();
                            JSONArray rushArray = jsonObject.getJSONArray("RushDays");
                            for (int i = 0; i < rushArray.length(); i++){
                                JSONObject obj = rushArray.getJSONObject(i);
                                RushDays rushDays = new RushDays();
                                rushDays.setRushId(obj.getString("RushId"));
                                rushDays.setStartDate(obj.getString("StartDate"));
                                rushDays.setEndDate(obj.getString("EndDate"));
                                rushDays.setRate(obj.getString("Rate"));
                                rushDaysArrayList.add(rushDays);
                            }
                            resortDetails.setRushDays(rushDaysArrayList);
                            resortDetailsArrayList.add(resortDetails);

                            mAdapter = new RushDaysAdapter(RushDaysActivity.this, rushDaysArrayList, language);
                            rushDaysListView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            if(rushDaysArrayList.size() == 0){
                                noRushDays.setVisibility(View.VISIBLE);
                            }
                            else{
                                noRushDays.setVisibility(View.GONE);
                            }

                        } catch (Exception e) {
                            Toast.makeText(RushDaysActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(RushDaysActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public class DeleteRushDay extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(RushDaysActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.DELETE_RUSH_DAYS_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String successObj = jo.getString("Success");
                            String resortID = resortDetailsArrayList.get(0).getResortId();
                            new GetResortDetails().execute(Constants.RESORT_DASHBOARD_URL+resortID+
                                    "&UserId=0&UserType=2");
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            } else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
