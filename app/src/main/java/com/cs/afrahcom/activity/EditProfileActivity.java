package com.cs.afrahcom.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.Cities;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 22-11-2017.
 */

public class EditProfileActivity extends AppCompatActivity {

    private EditText etFirstName, etMiddleName, etEmail, etMobile;
    private String mNameStr, mMiddleNameStr, mFamilyNameStr, mEmailStr, mMobileStr, mPasswordStr, cityId = "0";
    SharedPreferences.Editor userPrefEditor;
    TextView submit;
    String userId;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;
    String language;
    TextView cityName;
    Spinner citySpinner;
    int mCityPos = -1;
    Boolean isCitySet = false;
    AlertDialog customDialog;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_edit_profile);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_edit_profile_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etMiddleName = (EditText) findViewById(R.id.et_middle_name);
        cityName = (TextView) findViewById(R.id.et_signup_city);
        etEmail = (EditText) findViewById(R.id.et_email);
        etMobile = (EditText) findViewById(R.id.et_mobile);
        citySpinner = (Spinner) findViewById(R.id.city_spinner);

        etMobile.setFocusableInTouchMode(false);
        etMobile.setFocusable(false);
        submit = (TextView) findViewById(R.id.submit);

        final ArrayList<String> cityNames = new ArrayList<>();
        for(Cities city : Constants.CityArrayList){
            cityNames.add(city.getCityName());
        }
        ArrayAdapter<String> cityAdapter;
        cityAdapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.list_spinner, cityNames) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(18);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.colorSignupText));

                return v;
            }
        };

        citySpinner.setAdapter(cityAdapter);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(isCitySet) {
                    cityName.setText(cityNames.get(i));
                    mCityPos = i;
                    cityName.setTextSize(18);
                }
                else{
                    if(cityId.equals("0") || cityId.equalsIgnoreCase("null")) {
                        cityName.setText("");
                    }
                    else{
                        mCityPos = Integer.parseInt(cityId);
                        mCityPos = mCityPos - 1;
                        cityName.setText(Constants.CityArrayList.get(mCityPos).getCityName());
                    }
                    isCitySet = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final JSONObject parent = new JSONObject();
                mNameStr = etFirstName.getText().toString();
                mMiddleNameStr = etMiddleName.getText().toString();
//                mFamilyNameStr = etFamilyName.getText().toString();
                mMobileStr = etMobile.getText().toString();
                mEmailStr = etEmail.getText().toString().replaceAll(" ","");
                String cityId = "";
                if(mCityPos != -1){
                    cityId = Constants.CityArrayList.get(mCityPos).getCityId();
                }

                if(mNameStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etFirstName.setError(getResources().getString(R.string.str_alert_name));
                    }
                    else{
                        etFirstName.setError(getResources().getString(R.string.str_alert_name_ar));
                    }
                }
                else if(mMiddleNameStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etMiddleName.setError(getResources().getString(R.string.str_signup_middle_name_hint));
                    }
                    else{
                        etMiddleName.setError(getResources().getString(R.string.str_signup_middle_name_hint_ar));
                    }
                }
                else if(mEmailStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etEmail.setError(getResources().getString(R.string.str_alert_email));
                    }
                    else{
                        etEmail.setError(getResources().getString(R.string.str_alert_email_ar));
                    }
                }
                else if(!isValidEmail(mEmailStr)){
                    if(language.equalsIgnoreCase("En")) {
                        etEmail.setError(getResources().getString(R.string.str_alert_valid_email));
                    }
                    else{
                        etEmail.setError(getResources().getString(R.string.str_alert_valid_email_ar));
                    }
                }
                else if(mMobileStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etMobile.setError(getResources().getString(R.string.str_alert_mobile));
                    }
                    else{
                        etMobile.setError(getResources().getString(R.string.str_alert_mobile_ar));
                    }
                }
                else if(mMobileStr.length() != 9){
                    if(language.equalsIgnoreCase("En")) {
                        etMobile.setError(getResources().getString(R.string.str_alert_valid_mobile));
                    }
                    else{
                        etMobile.setError(getResources().getString(R.string.str_alert_valid_mobile_ar));
                    }
                }
                else {
                    try {
//                        JSONArray mainItem = new JSONArray();

                        JSONObject mainObj = new JSONObject();
                        mainObj.put("UserId", userId);
                        mainObj.put("FullName", mNameStr);
                        mainObj.put("MiddleName","");
                        mainObj.put("FamilyName",mMiddleNameStr);
                        mainObj.put("CityId", cityId);
                        mainObj.put("Email", mEmailStr);
                        mainObj.put("Language", "En");
                        mainObj.put("Mobile", "966" + mMobileStr);
                        mainObj.put("DeviceToken", SplashScreenActivity.regId);
                        mainObj.put("DeviceType", "Android");
//                        mainItem.put(mainObj);

                        parent.put("Updatedetails", mainObj);
                        Log.i("TAG", parent.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    new UpdateProfile().execute(parent.toString());
                }
            }
        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String response = userPrefs.getString("user_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                etFirstName.setText(userObjuect.getString("fullName"));
                mNameStr = userObjuect.getString("fullName");
                mMobileStr = userObjuect.getString("mobile");
                mMobileStr = mMobileStr.substring(3,12);
                etMobile.setText(""+mMobileStr);
                etEmail.setText(userObjuect.getString("email"));
                mEmailStr = userObjuect.getString("email");
                mPasswordStr = userObjuect.getString("Password");
                if(userObjuect.getString("MiddleName").equals("null")){
                    etMiddleName.setText("");
                    mMiddleNameStr = "";
                }
                else{
                    etMiddleName.setText(userObjuect.getString("MiddleName"));
                    mMiddleNameStr = userObjuect.getString("MiddleName");
                }
                if(userObjuect.getString("FamilyName").equals("null")){
//                    etFamilyName.setText("");
                    mFamilyNameStr = "";
                }
                else{
//                    etFamilyName.setText(userObjuect.getString("FamilyName"));
                    mFamilyNameStr = userObjuect.getString("FamilyName");
                }
                etFirstName.setSelection(etFirstName.length());
                try {
                    cityId = userObjuect.getString("CityId");
                } catch (JSONException e) {
                    e.printStackTrace();
                    cityId = "0";
                }
            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public class UpdateProfile extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
            dialog = new ACProgressFlower.Builder(EditProfileActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.EDIT_PROFILE_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(EditProfileActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(EditProfileActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
//                                JSONArray ja = jo.getJSONArray("succcess");
                                JSONObject jo1 = jo.getJSONObject("success");
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String MiddleName = jo1.getString("MiddleName");
                                String FamilyName = jo1.getString("FamilyName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("language");
                                String cityId = jo1.getString("CityId");
//                                String password = jo1.getString("Password");
//                                boolean isVerified = jo1.getBoolean("IsVerified");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("MiddleName", FamilyName);
                                    jsonObject.put("FamilyName", "");
                                    jsonObject.put("CityId", cityId);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("Password", mPasswordStr);
                                    jsonObject.put("language", language);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);
                                    userPrefEditor.commit();
                                    finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
                                je.printStackTrace();
//                                final iOSDialog iOSDialog = new iOSDialog(EditProfileActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(result);
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EditProfileActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText(result);
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText(result);
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(EditProfileActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(EditProfileActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                }
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
