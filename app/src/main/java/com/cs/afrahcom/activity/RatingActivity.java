package com.cs.afrahcom.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 04-12-2017.
 */

public class RatingActivity extends AppCompatActivity {

    RatingBar foodRatingBar, resortRatingBar;
    TextView submit, ratingText, cancel, rateFood, title;
    LinearLayout foodLayout;
    int foodRating, resortRating;
    String userId, resortName, resortId, foodId, bookingID;
    boolean isFoodAvailable = true;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_layout);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        resortName = getIntent().getStringExtra("resortName");
        resortId = getIntent().getStringExtra("resortId");
        foodId = getIntent().getStringExtra("foodId");
        bookingID = getIntent().getStringExtra("BookingId");

        cancel = (TextView) findViewById(R.id.rating_cancel);
        ratingText = (TextView) findViewById(R.id.rating_text);
        rateFood = (TextView) findViewById(R.id.rateFoodText);
        title = (TextView) findViewById(R.id.rate);

        foodRatingBar = (RatingBar) findViewById(R.id.foodRating);
        resortRatingBar = (RatingBar) findViewById(R.id.resortRating);
        submit = (TextView) findViewById(R.id.submit);
        foodLayout = (LinearLayout) findViewById(R.id.foodLayout);

        resortRatingBar.setRating(getIntent().getFloatExtra("rating",0));
        foodRatingBar.setRating(getIntent().getFloatExtra("rating",0));

        if(foodId.equalsIgnoreCase("0")){
            foodLayout.setVisibility(View.GONE);
            isFoodAvailable = false;
        }

        if(language.equalsIgnoreCase("En")) {
            ratingText.setText("Rate " + resortName);
        }
        else{
            title.setText("تقييم");
            ratingText.setText("تقييم القاعة "+resortName);
            rateFood.setText("تقييم الطعام");
            submit.setText(getResources().getString(R.string.str_submit_ar));
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        foodRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float ratingSelected, boolean fromUser) {

                if(ratingSelected<1){
                    foodRatingBar.setRating(1);
                }
            }
        });

        resortRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float ratingSelected, boolean fromUser) {

                if(ratingSelected<1){
                    resortRatingBar.setRating(1);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foodRating = Math.round(foodRatingBar.getRating());
                resortRating = Math.round(resortRatingBar.getRating());

                JSONObject parentObj = new JSONObject();
                JSONArray foodArray = new JSONArray();
                JSONArray resortArray = new JSONArray();
                JSONObject resortObj = new JSONObject();

                if(isFoodAvailable){
                    String[] foodIds = foodId.split(",");

                    for (int i = 0; i < foodIds.length; i++){
                        try {
                            JSONObject foodObj = new JSONObject();
                            foodObj.put("ResortId",resortId);
                            foodObj.put("FoodId",foodIds[i]);
                            foodObj.put("UserId", userId);
                            foodObj.put("Rating",foodRating);
                            foodArray.put(foodObj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    resortObj.put("ResortId", resortId);
                    resortObj.put("UserId",userId);
                    resortObj.put("Rating",resortRating);
                    resortObj.put("BookingId", bookingID);
                    resortObj.put("Comments","");
                    resortArray.put(resortObj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    parentObj.put("fooddetails", foodArray);
                    parentObj.put("resortdetails", resortArray);

                    Log.i("TAG",""+parentObj.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new InsertRatingURL().execute(parentObj.toString());
            }
        });
    }

    public class InsertRatingURL extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(RatingActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.INSERT_RATING_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String successObj = jo.getString("Success");

                            Toast.makeText(getApplicationContext(), "Rating submitted successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            } else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            Snap sn = new Snap();
//            sn.insertfav();
            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
