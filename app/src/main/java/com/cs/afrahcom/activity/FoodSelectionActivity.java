package com.cs.afrahcom.activity;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.TextView;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.FoodSelectionAdapter;
import com.cs.afrahcom.models.FoodDetails;
import com.cs.afrahcom.models.ResortDetailsFiltered;

import java.util.ArrayList;

import static com.cs.afrahcom.Constants.SelectedFoodIds;
import static com.cs.afrahcom.activity.CateringSearchActivity.breakFastSelected;
import static com.cs.afrahcom.activity.CateringSearchActivity.dinnerSelected;
import static com.cs.afrahcom.activity.CateringSearchActivity.lunchSelected;
import static com.cs.afrahcom.activity.ResortDetailsActivity.is48HrsSelected;
import static com.cs.afrahcom.activity.ResortDetailsActivity.isBookNowSelected;

/**
 * Created by CS on 26-09-2017.
 */

public class FoodSelectionActivity extends AppCompatActivity {

    TextView tvGuestCount, tvFilter, tvBack, tvSubmit, tvNoCaterers;
    Boolean isBreakFastSelected, isLunchSelected, isDinnerSelected, isSnacksSelected;
    ArrayList<FoodDetails> breakFastArrayList = new ArrayList<>();
    ArrayList<FoodDetails> lunchArrayList = new ArrayList<>();
    ArrayList<FoodDetails> dinnerArrayList = new ArrayList<>();
    ArrayList<FoodDetails> snacksArrayList = new ArrayList<>();
    ArrayList<FoodDetails> finalArrayList = new ArrayList<>();
    public static ArrayList<String> SelectedFoods = new ArrayList<>();
    GridView mFoodGrid;
    FoodSelectionAdapter mAdapter;
    String noOfGuest;
    ArrayList<ResortDetailsFiltered> resortDetailsArrayList = new ArrayList<>();
    int position;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    String language;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_food_selection);
        }
        else{
            setContentView(R.layout.activity_food_selection_ar);
        }

        position = getIntent().getIntExtra("position",0);
        resortDetailsArrayList = (ArrayList<ResortDetailsFiltered>) getIntent().getSerializableExtra("array");
        breakFastArrayList = (ArrayList<FoodDetails>) getIntent().getSerializableExtra("breakFastArray");
        lunchArrayList = (ArrayList<FoodDetails>) getIntent().getSerializableExtra("lunchArray");
        dinnerArrayList = (ArrayList<FoodDetails>) getIntent().getSerializableExtra("dinnerArray");
        snacksArrayList = (ArrayList<FoodDetails>) getIntent().getSerializableExtra("snacksArray");
        isBreakFastSelected = getIntent().getBooleanExtra("isBreakFastSelected",false);
        isLunchSelected = getIntent().getBooleanExtra("isLunchSelected",false);
        isDinnerSelected = getIntent().getBooleanExtra("isDinnerSelected",false);
        isSnacksSelected = getIntent().getBooleanExtra("isSnacksSelected",false);
        noOfGuest = Constants.noOfGuests;

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvGuestCount = (TextView) findViewById(R.id.tv_catering_count);
        tvFilter = (TextView) findViewById(R.id.tv_filter);
        tvBack = (TextView) findViewById(R.id.tv_back);
        tvSubmit = (TextView) findViewById(R.id.tv_submit);
        tvNoCaterers = (TextView) findViewById(R.id.noCaterers);

        mFoodGrid = (GridView) findViewById(R.id.foodSelectionGrid);

        SelectedFoods.clear();
        Constants.SelectedFoodIds.clear();

        if(!isSnacksSelected) {
            Constants.isSnacksSelected = 0;
            if (isBreakFastSelected & isLunchSelected & isDinnerSelected) {
                if(language.equalsIgnoreCase("En")) {
                    tvGuestCount.setText("Catering for " + noOfGuest + " Guests(BreakFast, Lunch & Dinner)");
                }
                else{
                    tvGuestCount.setText("قدرة المطعم لي" + noOfGuest + " شخص ");
                }

                for (int i = 0; i < lunchArrayList.size(); i++) {
                    for (int j = 0; j < dinnerArrayList.size(); j++) {
                        if (lunchArrayList.get(i).getFoodName().equals(dinnerArrayList.get(j).getFoodName())) {
                            for (int k = 0; k < breakFastArrayList.size(); k++) {
                                if (lunchArrayList.get(i).getFoodName().equals(breakFastArrayList.get(k).getFoodName())) {
                                    finalArrayList.add(breakFastArrayList.get(k));
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i < lunchSelected.size(); i++) {
                    for (int j = 0; j < dinnerSelected.size(); j++) {
                        if (lunchSelected.get(i).equals(dinnerSelected.get(j))) {
                            for (int k = 0; k < breakFastSelected.size(); k++) {
                                if (lunchSelected.get(i).equals(breakFastSelected.get(k))) {
                                    SelectedFoods.add(breakFastSelected.get(k));
                                }
                            }
                        }
                    }
                }

                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 1;
                Constants.isLunchSelected = 1;
                Constants.isDinnerSelected = 1;
            } else if (isBreakFastSelected & !isLunchSelected & !isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(BreakFast)");
                finalArrayList = breakFastArrayList;
                SelectedFoods = breakFastSelected;
                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 1;
                Constants.isLunchSelected = 0;
                Constants.isDinnerSelected = 0;
            } else if (!isBreakFastSelected & isLunchSelected & !isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(Lunch)");
                finalArrayList = lunchArrayList;
                SelectedFoods = lunchSelected;
                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 0;
                Constants.isLunchSelected = 1;
                Constants.isDinnerSelected = 0;
            } else if (!isBreakFastSelected & !isLunchSelected & isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(Dinner)");
                finalArrayList = dinnerArrayList;
                SelectedFoods = dinnerSelected;
                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 0;
                Constants.isLunchSelected = 0;
                Constants.isDinnerSelected = 1;
            } else if (isBreakFastSelected & isLunchSelected & !isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(BreakFast & Lunch)");

                for (int i = 0; i < breakFastArrayList.size(); i++) {
                    for (int j = 0; j < lunchArrayList.size(); j++) {
                        if (breakFastArrayList.get(i).getFoodName().equals(lunchArrayList.get(j).getFoodName())) {
                            finalArrayList.add(lunchArrayList.get(j));
                        }
                    }
                }

                for (int i = 0; i < breakFastSelected.size(); i++) {
                    for (int j = 0; j < lunchSelected.size(); j++) {
                        if (breakFastSelected.get(i).equals(lunchSelected.get(j))) {
                            SelectedFoods.add(lunchSelected.get(j));
                        }
                    }
                }

                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 1;
                Constants.isLunchSelected = 1;
                Constants.isDinnerSelected = 0;
            } else if (isBreakFastSelected & !isLunchSelected & isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(BreakFast & Dinner)");

                for (int i = 0; i < breakFastArrayList.size(); i++) {
                    for (int j = 0; j < dinnerArrayList.size(); j++) {
                        if (breakFastArrayList.get(i).getFoodName().equals(dinnerArrayList.get(j).getFoodName())) {
                            finalArrayList.add(dinnerArrayList.get(j));
                        }
                    }
                }

                for (int i = 0; i < breakFastSelected.size(); i++) {
                    for (int j = 0; j < dinnerSelected.size(); j++) {
                        if (dinnerSelected.get(j).equals(breakFastSelected.get(i))) {
                            SelectedFoods.add(dinnerSelected.get(j));
                        }
                    }
                }
                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 1;
                Constants.isLunchSelected = 0;
                Constants.isDinnerSelected = 1;
            } else if (!isBreakFastSelected & isLunchSelected & isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(Lunch & Dinner)");

                for (int i = 0; i < lunchArrayList.size(); i++) {
                    for (int j = 0; j < dinnerArrayList.size(); j++) {
                        if (lunchArrayList.get(i).getFoodName().equals(dinnerArrayList.get(j).getFoodName())) {
                            finalArrayList.add(dinnerArrayList.get(j));
                        }
                    }
                }

                for (int i = 0; i < lunchSelected.size(); i++) {
                    for (int j = 0; j < dinnerSelected.size(); j++) {
                        if (dinnerSelected.get(j).equals(lunchSelected.get(i))) {
                            SelectedFoods.add(dinnerSelected.get(j));
                        }
                    }
                }
                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 0;
                Constants.isLunchSelected = 1;
                Constants.isDinnerSelected = 1;
            }
        }
        else {
            Constants.isSnacksSelected = 1;
            if (isBreakFastSelected & isLunchSelected & isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(BreakFast, Lunch, snacks & sweet, Dinner)");

                for (int i = 0; i < lunchArrayList.size(); i++) {
                    for (int j = 0; j < dinnerArrayList.size(); j++) {
                        if (lunchArrayList.get(i).getFoodName().equals(dinnerArrayList.get(j).getFoodName())) {
                            for (int k = 0; k < breakFastArrayList.size(); k++) {
                                if (lunchArrayList.get(i).getFoodName().equals(breakFastArrayList.get(k).getFoodName())) {
                                    for (int l = 0; l < snacksArrayList.size(); l++) {
                                        if(lunchArrayList.get(i).getFoodName().equals(snacksArrayList.get(l).getFoodName())) {
                                            finalArrayList.add(snacksArrayList.get(l));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 1;
                Constants.isLunchSelected = 1;
                Constants.isDinnerSelected = 1;
            } else if (isBreakFastSelected & !isLunchSelected & !isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(BreakFast, snacks & sweet)");
                for (int i = 0; i < snacksArrayList.size(); i++) {
                    for (int j = 0; j < breakFastArrayList.size(); j++) {
                        if (snacksArrayList.get(i).getFoodName().equals(breakFastArrayList.get(j).getFoodName())) {
                            finalArrayList.add(breakFastArrayList.get(j));
                        }
                    }
                }

                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 1;
                Constants.isLunchSelected = 0;
                Constants.isDinnerSelected = 0;
            } else if (!isBreakFastSelected & isLunchSelected & !isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(Lunch, snacks & sweet)");
                for (int i = 0; i < snacksArrayList.size(); i++) {
                    for (int j = 0; j < lunchArrayList.size(); j++) {
                        if (snacksArrayList.get(i).getFoodName().equals(lunchArrayList.get(j).getFoodName())) {
                            finalArrayList.add(lunchArrayList.get(j));
                        }
                    }
                }
                SelectedFoods = lunchSelected;
                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 0;
                Constants.isLunchSelected = 1;
                Constants.isDinnerSelected = 0;
            } else if (!isBreakFastSelected & !isLunchSelected & isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(Dinner, snacks & sweet)");
                for (int i = 0; i < snacksArrayList.size(); i++) {
                    for (int j = 0; j < dinnerArrayList.size(); j++) {
                        if (snacksArrayList.get(i).getFoodName().equals(dinnerArrayList.get(j).getFoodName())) {
                            finalArrayList.add(dinnerArrayList.get(j));
                        }
                    }
                }
                SelectedFoods = dinnerSelected;
                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 0;
                Constants.isLunchSelected = 0;
                Constants.isDinnerSelected = 1;
            } else if (isBreakFastSelected & isLunchSelected & !isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(BreakFast, Lunch, snacks & sweet)");

                for (int i = 0; i < breakFastArrayList.size(); i++) {
                    for (int j = 0; j < lunchArrayList.size(); j++) {
                        if (breakFastArrayList.get(i).getFoodName().equals(lunchArrayList.get(j).getFoodName())) {
                            for (int l = 0; l < snacksArrayList.size(); l++) {
                                if(lunchArrayList.get(j).getFoodName().equals(snacksArrayList.get(l).getFoodName())) {
                                    finalArrayList.add(snacksArrayList.get(l));
                                }
                            }
                        }
                    }
                }

                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 1;
                Constants.isLunchSelected = 1;
                Constants.isDinnerSelected = 0;
            } else if (isBreakFastSelected & !isLunchSelected & isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(BreakFast, Dinner, snacks & sweet)");

                for (int i = 0; i < breakFastArrayList.size(); i++) {
                    for (int j = 0; j < dinnerArrayList.size(); j++) {
                        if (breakFastArrayList.get(i).getFoodName().equals(dinnerArrayList.get(j).getFoodName())) {
                            for (int l = 0; l < snacksArrayList.size(); l++) {
                                if(dinnerArrayList.get(j).getFoodName().equals(snacksArrayList.get(l).getFoodName())) {
                                    finalArrayList.add(snacksArrayList.get(l));
                                }
                            }
                        }
                    }
                }

                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 1;
                Constants.isLunchSelected = 0;
                Constants.isDinnerSelected = 1;
            } else if (!isBreakFastSelected & isLunchSelected & isDinnerSelected) {
                tvGuestCount.setText("Catering for " + noOfGuest + " Guests(Lunch, Dinner, snacks & sweet)");

                for (int i = 0; i < lunchArrayList.size(); i++) {
                    for (int j = 0; j < dinnerArrayList.size(); j++) {
                        if (lunchArrayList.get(i).getFoodName().equals(dinnerArrayList.get(j).getFoodName())) {
                            for (int l = 0; l < snacksArrayList.size(); l++) {
                                if(dinnerArrayList.get(j).getFoodName().equals(snacksArrayList.get(l).getFoodName())) {
                                    finalArrayList.add(snacksArrayList.get(l));
                                }
                            }
                        }
                    }
                }

                mAdapter = new FoodSelectionAdapter(FoodSelectionActivity.this, language, finalArrayList, FoodSelectionActivity.this);
                mFoodGrid.setAdapter(mAdapter);

                Constants.isBreakfastSelected = 0;
                Constants.isLunchSelected = 1;
                Constants.isDinnerSelected = 1;
            }
        }

        if(language.equalsIgnoreCase("Ar")){
            tvGuestCount.setText("قدرة المطعم لي" + noOfGuest + " شخص ");
        }

        if(finalArrayList.size() == 0){
            tvNoCaterers.setVisibility(View.VISIBLE);
        }

//        mFoodGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if(SelectedFoods.contains(finalArrayList.get(i).getFoodName())){
//                    SelectedFoods.remove(finalArrayList.get(i).getFoodName());
//                }
//                else{
//                    SelectedFoods.add(finalArrayList.get(i).getFoodName());
//                }
//                mAdapter.notifyDataSetChanged();
//            }
//        });

        tvFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                setResult(RESULT_CANCELED);
                finish();
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedFoods.size()!=0) {
                    Constants.isCateringSelected = 1;
                    SelectedFoodIds.clear();
                    for (int i = 0; i < finalArrayList.size(); i++){
                        for(int j = 0; j < SelectedFoods.size(); j++){
                            if(finalArrayList.get(i).getFoodName().equals(SelectedFoods.get(j))){
                                SelectedFoodIds.add(finalArrayList.get(i).getFoodId());
                            }
                        }
                    }

                    if (is48HrsSelected) {
//                        Intent intent = new Intent(FoodSelectionActivity.this, OrderTrackingActivity.class);
//                        intent.putExtra("array", resortDetailsArrayList);
//                        intent.putExtra("position", position);
//                        startActivity(intent);
                        setResult(RESULT_OK);
                        finish();
                    } else if (isBookNowSelected) {
                        Intent intent = new Intent(FoodSelectionActivity.this, ReservationActivity.class);
                        intent.putExtra("array", resortDetailsArrayList);
                        intent.putExtra("position", position);
                        intent.putExtra("class","booknow");
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(FoodSelectionActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                        startActivity(intent, options.toBundle());
                    }
                }
                else{
//                    final iOSDialog iOSDialog = new iOSDialog(FoodSelectionActivity.this);
//                    String appName, title, positive;
//                    if(language.equalsIgnoreCase("En")){
//                        appName = getResources().getString(R.string.app_name);
//                        title = "Please select atleast one Food";
//                        positive = getResources().getString(R.string.str_btn_ok);
//                    }
//                    else{
//                        appName = getResources().getString(R.string.app_name_ar);
//                        title = "يرجى تحديد مطعم واحد على الأقل";
//                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                    }
//                    iOSDialog.setTitle(appName);
//                    iOSDialog.setSubtitle(title);
//                    iOSDialog.setPositiveLabel(positive);
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FoodSelectionActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.app_name));
                        yes.setText(getResources().getString(R.string.str_btn_ok));
                        desc.setText("Please select atleast one Food");
                    }
                    else{
                        title.setText(getResources().getString(R.string.app_name_ar));
                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                        desc.setText("يرجى تحديد مطعم واحد على الأقل");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
            }
        });

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                setResult(RESULT_CANCELED);
                finish();
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
