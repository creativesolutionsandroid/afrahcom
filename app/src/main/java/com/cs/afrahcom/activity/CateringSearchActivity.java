package com.cs.afrahcom.activity;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.fragment.ResortSearchFragment;
import com.cs.afrahcom.models.FoodDetails;
import com.cs.afrahcom.models.ResortDetailsFiltered;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

import static com.cs.afrahcom.activity.ResortDetailsActivity.is48HrsSelected;
import static com.cs.afrahcom.activity.ResortDetailsActivity.isBookNowSelected;

/**
 * Created by CS on 11-10-2017.
 */

public class CateringSearchActivity extends AppCompatActivity implements View.OnClickListener{

    ArrayList<FoodDetails> breakFastArrayList = new ArrayList<>();
    ArrayList<FoodDetails> snacksArrayList = new ArrayList<>();
    ArrayList<FoodDetails> lunchArrayList = new ArrayList<>();
    ArrayList<FoodDetails> dinnerArrayList = new ArrayList<>();
    public static ArrayList<String> breakFastSelected = new ArrayList<>();
    public static ArrayList<String> lunchSelected = new ArrayList<>();
    public static ArrayList<String> dinnerSelected = new ArrayList<>();
//    CustomGridView breakFastGridView, lunchGridView, dinnerGridView;
//    BreakFastAdapter mBreakFastAdapter;
//    LunchAdapter mLunchAdapter;
//    DinnerAdapter mDinnerAdapter;
    LinearLayout breakFastTitle, lunchTitle, dinnerTitle, snacksTitle;
    ImageView cbBreakfast, cbLunch, cbDinner, cbSnacks;
    Boolean isBreakFastSelected = true, isLunchSelected = true, isDinnerSelected = true, isSnacksSelected = true;
    TextView tvSkip, tvSearch;
    TextView tvCheckIndate, tvCheckInMonth,  tvCheckInWeek, tvNumofGuests;
    AlertDialog alertDialog2;
    String userId;
    SharedPreferences userPrefs;
    ArrayList<ResortDetailsFiltered> resortDetailsArrayList = new ArrayList<>();
    int position;
    SharedPreferences languagePrefs;
    String language;
    private static final int FOOD_REQUEST = 1;
    AlertDialog customDialog;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_catering_search);
        }
        else{
            setContentView(R.layout.activity_catering_search_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        position = getIntent().getIntExtra("position",0);
        resortDetailsArrayList = (ArrayList<ResortDetailsFiltered>) getIntent().getSerializableExtra("array");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        if (userId == null) {
            userId = "0";
        }

        breakFastTitle = (LinearLayout) findViewById(R.id.breakfastTitle);
        lunchTitle = (LinearLayout) findViewById(R.id.lunchTitle);
        dinnerTitle = (LinearLayout) findViewById(R.id.dinnerTitle);
        snacksTitle = (LinearLayout) findViewById(R.id.snacksTitle);

        tvSkip = (TextView) findViewById(R.id.tv_skip);
        tvSearch = (TextView) findViewById(R.id.tv_search);

        tvCheckIndate = (TextView) findViewById(R.id.tv_checkin_date);
        tvCheckInMonth = (TextView) findViewById(R.id.tv_checkin_month);
        tvCheckInWeek = (TextView) findViewById(R.id.tv_checkin_week);
        tvNumofGuests = (TextView) findViewById(R.id.tv_num_of_guests);

        cbBreakfast = (ImageView) findViewById(R.id.cbBreakfast);
        cbLunch = (ImageView) findViewById(R.id.cbLunch);
        cbDinner = (ImageView) findViewById(R.id.cbdinner);
        cbSnacks = (ImageView) findViewById(R.id.cbsnacks);

//        breakFastGridView = (CustomGridView) findViewById(R.id.breakfastGridView);
//        lunchGridView = (CustomGridView) findViewById(R.id.lunchGridView);
//        dinnerGridView = (CustomGridView) findViewById(R.id.dinnerGridView);
//
//        mBreakFastAdapter = new BreakFastAdapter(CateringSearchActivity.this, breakFastArrayList, language);
//        mLunchAdapter = new LunchAdapter(CateringSearchActivity.this, lunchArrayList, language);
//        mDinnerAdapter = new DinnerAdapter(CateringSearchActivity.this, dinnerArrayList, language);
//
//        breakFastGridView.setAdapter(mBreakFastAdapter);
//        lunchGridView.setAdapter(mLunchAdapter);
//        dinnerGridView.setAdapter(mDinnerAdapter);

        breakFastSelected.clear();
        lunchSelected.clear();
        dinnerSelected.clear();

        new GetFoodDetails().execute(Constants.GET_FOOD_DETAILS+userId);

        tvCheckIndate.setText(ResortSearchFragment.strCheckInDate);
        tvCheckInMonth.setText(ResortSearchFragment.strCheckInMonth);
        tvCheckInWeek.setText(ResortSearchFragment.strCheckInWeek);
        tvNumofGuests.setText(Constants.noOfGuests);

        breakFastTitle.setOnClickListener(this);
        lunchTitle.setOnClickListener(this);
        dinnerTitle.setOnClickListener(this);
        snacksTitle.setOnClickListener(this);
        tvSkip.setOnClickListener(this);
        tvSearch.setOnClickListener(this);

//        breakFastGridView.setOnItemClickListener(this);
//        lunchGridView.setOnItemClickListener(this);
//        dinnerGridView.setOnItemClickListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        switch (adapterView.getId()){
//            case R.id.breakfastGridView:
//                isBreakFastSelected = true;
//                cbBreakfast.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
//                if(breakFastSelected.contains(breakFastArrayList.get(i).getFoodName())){
//                    breakFastSelected.remove(breakFastArrayList.get(i).getFoodName());
//                }
//                else{
//                    breakFastSelected.add(breakFastArrayList.get(i).getFoodName());
//                }
//                mBreakFastAdapter.notifyDataSetChanged();
//                break;
//
//            case R.id.lunchGridView:
//                isLunchSelected = true;
//                cbLunch.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
//                if(lunchSelected.contains(lunchArrayList.get(i).getFoodName())){
//                    lunchSelected.remove(lunchArrayList.get(i).getFoodName());
//                }
//                else{
//                    lunchSelected.add(lunchArrayList.get(i).getFoodName());
//                }
//                mLunchAdapter.notifyDataSetChanged();
//                break;
//
//            case R.id.dinnerGridView:
//                isDinnerSelected = true;
//                cbDinner.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
//                if(dinnerSelected.contains(dinnerArrayList.get(i).getFoodName())){
//                    dinnerSelected.remove(dinnerArrayList.get(i).getFoodName());
//                }
//                else{
//                    dinnerSelected.add(dinnerArrayList.get(i).getFoodName());
//                }
//                mDinnerAdapter.notifyDataSetChanged();
//                break;
//        }
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_skip:
//                showBookingDialog();
                if(is48HrsSelected){
//                    Intent intent = new Intent(CateringSearchActivity.this, OrderTrackingActivity.class);
//                    intent.putExtra("array",resortDetailsArrayList);
//                    intent.putExtra("position",position);
//                    startActivity(intent);

                    setResult(RESULT_OK);
                    finish();
                }
                else if(isBookNowSelected){
                    Intent intent = new Intent(CateringSearchActivity.this, ReservationActivity.class);
                    intent.putExtra("array",resortDetailsArrayList);
                    intent.putExtra("position",position);
                    intent.putExtra("class","booknow");
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(CateringSearchActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                    startActivity(intent, options.toBundle());
                }
                break;

            case R.id.tv_search:
                if(!isBreakFastSelected && !isLunchSelected && !isDinnerSelected && !isSnacksSelected){
//                    final iOSDialog iOSDialog = new iOSDialog(CateringSearchActivity.this);
//                    String appName, title, positive;
//                    if(language.equalsIgnoreCase("En")){
//                        appName = getResources().getString(R.string.app_name);
//                        title = "Please select atleast one food category";
//                        positive = getResources().getString(R.string.str_btn_ok);
//                    }
//                    else{
//                        appName = getResources().getString(R.string.app_name_ar);
//                        title = "يرجى تحديد فئة مطعم واحد على الأقل";
//                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                    }
//                    iOSDialog.setTitle(appName);
//                    iOSDialog.setSubtitle(title);
//                    iOSDialog.setPositiveLabel(positive);
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CateringSearchActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.app_name));
                        yes.setText(getResources().getString(R.string.str_btn_ok));
                        desc.setText("Please select atleast one food category");
                    }
                    else{
                        title.setText(getResources().getString(R.string.app_name_ar));
                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                        desc.setText("يرجى تحديد فئة مطعم واحد على الأقل");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    Intent intent = new Intent(CateringSearchActivity.this, FoodSelectionActivity.class);
                    intent.putExtra("breakFastArray", breakFastArrayList);
                    intent.putExtra("lunchArray", lunchArrayList);
                    intent.putExtra("dinnerArray", dinnerArrayList);
                    intent.putExtra("snacksArray", snacksArrayList);
                    intent.putExtra("isBreakFastSelected", isBreakFastSelected);
                    intent.putExtra("isLunchSelected", isLunchSelected);
                    intent.putExtra("isDinnerSelected", isDinnerSelected);
                    intent.putExtra("isSnacksSelected", isSnacksSelected);
                    intent.putExtra("array", resortDetailsArrayList);
                    intent.putExtra("position", position);
                    startActivityForResult(intent, FOOD_REQUEST);
                }
                break;

            case R.id.snacksTitle:
                if(isSnacksSelected){
                    cbSnacks.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                    isSnacksSelected = false;
                }
                else{
                    cbSnacks.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                    isSnacksSelected = true;
                }
                break;
            case R.id.breakfastTitle:
                if(isBreakFastSelected){
                    cbBreakfast.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                    isBreakFastSelected = false;
//                    breakFastSelected.clear();
//                    mBreakFastAdapter.notifyDataSetChanged();
                }
                else{
                    cbBreakfast.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                    isBreakFastSelected = true;
                }
                break;

            case R.id.lunchTitle:
                if(isLunchSelected){
                    cbLunch.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                    isLunchSelected = false;
//                    lunchSelected.clear();
//                    mLunchAdapter.notifyDataSetChanged();
                }
                else{
                    cbLunch.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                    isLunchSelected = true;
                }
                break;

            case R.id.dinnerTitle:
                if(isDinnerSelected){
                    cbDinner.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                    isDinnerSelected = false;
//                    dinnerSelected.clear();
//                    mDinnerAdapter.notifyDataSetChanged();
                }
                else{
                    cbDinner.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                    isDinnerSelected = true;
                }
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == FOOD_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
        else if (requestCode == FOOD_REQUEST && resultCode == RESULT_CANCELED){
//            setResult(RESULT_CANCELED);
//            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mBreakFastAdapter.notifyDataSetChanged();
//        mLunchAdapter.notifyDataSetChanged();
//        mDinnerAdapter.notifyDataSetChanged();
    }

    public class GetFoodDetails extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(CateringSearchActivity.this);
            dialog = new ACProgressFlower.Builder(CateringSearchActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(CateringSearchActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(CateringSearchActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONArray jsonArray = jo.getJSONArray("Success");

                            for (int i=0; i<jsonArray.length(); i++){

                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if(jsonObject.getBoolean("IsBreakFast")){
                                    FoodDetails foodDetails= new FoodDetails();

                                    foodDetails.setFoodId(jsonObject.getString("FoodId"));
                                    if (language.equalsIgnoreCase("En")) {
                                        foodDetails.setFoodName(jsonObject.getString("FoodName"));
                                    }
                                    else{
                                        foodDetails.setFoodName(jsonObject.getString("FoodName_Ar"));
                                    }
                                    foodDetails.setFoodDescription(jsonObject.getString("FoodDescription_Ar"));
                                    foodDetails.setFoodImage(jsonObject.getString("FoodImage"));
                                    foodDetails.setSource(jsonObject.getString("Source"));
                                    foodDetails.setFavourite(jsonObject.getBoolean("IsFavourite"));
                                    foodDetails.setRating(jsonObject.getString("Rating"));
                                    breakFastArrayList.add(foodDetails);
                                }

                                if(jsonObject.getBoolean("IsLunch")){
                                    FoodDetails foodDetails= new FoodDetails();

                                    foodDetails.setFoodId(jsonObject.getString("FoodId"));
                                    if (language.equalsIgnoreCase("En")) {
                                        foodDetails.setFoodName(jsonObject.getString("FoodName"));
                                    }
                                    else{
                                        foodDetails.setFoodName(jsonObject.getString("FoodName_Ar"));
                                    }
                                    foodDetails.setFoodDescription(jsonObject.getString("FoodDescription_Ar"));
                                    foodDetails.setFoodImage(jsonObject.getString("FoodImage"));
                                    foodDetails.setSource(jsonObject.getString("Source"));
                                    foodDetails.setFavourite(jsonObject.getBoolean("IsFavourite"));
                                    foodDetails.setRating(jsonObject.getString("Rating"));
                                    lunchArrayList.add(foodDetails);
                                }

                                if(jsonObject.getBoolean("IsDinner")){
                                    FoodDetails foodDetails= new FoodDetails();

                                    foodDetails.setFoodId(jsonObject.getString("FoodId"));
                                    if (language.equalsIgnoreCase("En")) {
                                        foodDetails.setFoodName(jsonObject.getString("FoodName"));
                                    }
                                    else{
                                        foodDetails.setFoodName(jsonObject.getString("FoodName_Ar"));
                                    }
                                    foodDetails.setFoodDescription(jsonObject.getString("FoodDescription_Ar"));
                                    foodDetails.setFoodImage(jsonObject.getString("FoodImage"));
                                    foodDetails.setSource(jsonObject.getString("Source"));
                                    foodDetails.setFavourite(jsonObject.getBoolean("IsFavourite"));
                                    foodDetails.setRating(jsonObject.getString("Rating"));
                                    dinnerArrayList.add(foodDetails);
                                }

                                try {
                                    if(jsonObject.getBoolean("IsSweetandSnacks")){
                                        FoodDetails foodDetails= new FoodDetails();

                                        foodDetails.setFoodId(jsonObject.getString("FoodId"));
                                        if (language.equalsIgnoreCase("En")) {
                                            foodDetails.setFoodName(jsonObject.getString("FoodName"));
                                        }
                                        else{
                                            foodDetails.setFoodName(jsonObject.getString("FoodName_Ar"));
                                        }
                                        foodDetails.setFoodDescription(jsonObject.getString("FoodDescription_Ar"));
                                        foodDetails.setFoodImage(jsonObject.getString("FoodImage"));
                                        foodDetails.setSource(jsonObject.getString("Source"));
                                        foodDetails.setFavourite(jsonObject.getBoolean("IsFavourite"));
                                        foodDetails.setRating(jsonObject.getString("Rating"));
                                        snacksArrayList.add(foodDetails);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

//                            mBreakFastAdapter.notifyDataSetChanged();
//                            mLunchAdapter.notifyDataSetChanged();
//                            mDinnerAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                            if(language.equalsIgnoreCase("En")) {
                                Toast.makeText(CateringSearchActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(CateringSearchActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                            finish();
                        }
                    }
                }
            } else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(CateringSearchActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(CateringSearchActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }
}
