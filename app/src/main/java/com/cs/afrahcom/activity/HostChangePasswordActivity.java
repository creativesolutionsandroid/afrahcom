package com.cs.afrahcom.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 29-12-2016.
 */
public class HostChangePasswordActivity extends AppCompatActivity {
    private String response12 = null;
    ACProgressFlower dialog;

    EditText etOldPassword, etNewPassword, etConfirmPassword;
    Button btnSubmit;
    String response, hostId, passwordStr;
    String newPwdStr, companyName, contactPerson, email, mobile, landline;
    Toolbar toolbar;
    SharedPreferences hostPrefs;
    SharedPreferences.Editor hostPrefEditor;
    SharedPreferences languagePrefs;
    String language;
    AlertDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_change_password);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_change_password_ar);
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostPrefEditor  = hostPrefs.edit();
        hostId = hostPrefs.getString("hostId", null);

        etOldPassword = (EditText) findViewById(R.id.et_reset_old_password);
        etNewPassword = (EditText) findViewById(R.id.et_reset_new_password);
        etConfirmPassword = (EditText) findViewById(R.id.et_reset_confirm_password);
        btnSubmit = (Button) findViewById(R.id.change_password_button);

        String response = hostPrefs.getString("host_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");
                passwordStr = userObjuect.getString("Password");
                companyName = userObjuect.getString("CompanyName");
                contactPerson = userObjuect.getString("ContactPerson");
                email = userObjuect.getString("Email");
                mobile = userObjuect.getString("MobileNo");
                landline = userObjuect.getString("LandlineNo");
            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPwdStr = etOldPassword.getText().toString();
                newPwdStr = etNewPassword.getText().toString();
                String confirmPwdStr = etConfirmPassword.getText().toString();
                if(oldPwdStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etOldPassword.setError("Please enter old password");
                    }
                    else{
                        etOldPassword.setError("من فضلك أدخل كلمة السر القديمة");
                    }
                }
                else if(!oldPwdStr.equals(passwordStr)){
                    if(language.equalsIgnoreCase("En")) {
                        etOldPassword.setError("Incorrect old password");
                    }
                    else{
                        etOldPassword.setError("كلمة المرور غير صحيحة ، من فضلك أعد الإدخال");
                    }
                }
                else if (newPwdStr.length() == 0) {
                    if(language.equalsIgnoreCase("En")) {
                        etNewPassword.setError(getResources().getString(R.string.str_alert_password));
                    }
                    else{
                        etNewPassword.setError(getResources().getString(R.string.str_alert_password_ar));
                    }
                } else if (newPwdStr.length() < 8) {
                    if(language.equalsIgnoreCase("En")) {
                        etNewPassword.setError(getResources().getString(R.string.str_alert_valid_password));
                    }
                    else{
                        etNewPassword.setError(getResources().getString(R.string.str_alert_valid_password_ar));
                    }
                } else if (confirmPwdStr.length() == 0) {
                    if(language.equalsIgnoreCase("En")) {
                        etConfirmPassword.setError(getResources().getString(R.string.str_alert_retype_password));
                    }
                    else{
                        etConfirmPassword.setError(getResources().getString(R.string.str_alert_retype_password_ar));
                    }
                } else if (!newPwdStr.equals(confirmPwdStr)) {
                    if(language.equalsIgnoreCase("En")) {
                        etConfirmPassword.setError(getResources().getString(R.string.str_alert_passwords_not_match));
                    }
                    else{
                        etConfirmPassword.setError(getResources().getString(R.string.str_alert_passwords_not_match_ar));
                    }
                } else {
                    new ChangePasswordResponse().execute(hostId, oldPwdStr, newPwdStr);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class ChangePasswordResponse extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        InputStream is = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(HostChangePasswordActivity.this);
            dialog = new ACProgressFlower.Builder(HostChangePasswordActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    // 2. make POST request to the given URL
//                        HttpGet httpPost = new HttpGet(Constants.CHANGE_PASSWORD_URL+arg0[0]+"?OldPsw="+arg0[1]+"&NewPsw="+arg0[2]);

                    // Making HTTP request
                    try {
                        // defaultHttpClient
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        HttpGet httpPost = new HttpGet(Constants.HOST_CHANGE_PASSWORD_URL + arg0[0] + "?oldpsw="+ arg0[1]+"&newpsw=" + arg0[2]);

                        httpPost.setHeader("Accept", "application/xml");
                        httpPost.setHeader("Content-type", "application/xml");
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        is = httpEntity.getContent();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                is, "iso-8859-1"), 8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        is.close();
                        response12 = sb.toString();
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            } else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        JSONObject jo= new JSONObject(result1);

                        try {
                            String successObj = jo.getString("Success");
                            try {
                                JSONObject parent = new JSONObject();
                                JSONObject jsonObject = new JSONObject();
                                JSONArray jsonArray = new JSONArray();
                                jsonArray.put("lv1");
                                jsonArray.put("lv2");

                                jsonObject.put("CompanyName", companyName);
                                jsonObject.put("ContactPerson", contactPerson);
                                jsonObject.put("MobileNo", mobile);
                                jsonObject.put("Email", email);
                                jsonObject.put("LandlineNo", landline);
                                jsonObject.put("Password", newPwdStr);
                                jsonObject.put("host_details", jsonArray);
                                parent.put("profile", jsonObject);
                                Log.d("output", parent.toString());
                                hostPrefEditor.putString("host_profile", parent.toString());
                                hostPrefEditor.putString("hostId", hostId);
                                hostPrefEditor.commit();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HostChangePasswordActivity.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout = R.layout.alert_dialog;
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView title = (TextView) dialogView.findViewById(R.id.title);
                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                            no.setVisibility(View.GONE);
                            vert.setVisibility(View.GONE);

                            if(language.equalsIgnoreCase("En")) {
                                title.setText(getResources().getString(R.string.app_name));
                                yes.setText(getResources().getString(R.string.str_btn_ok));
                                desc.setText("Change password successful");
                            }
                            else{
                                title.setText(getResources().getString(R.string.app_name_ar));
                                yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                desc.setText("Change password successful");
                            }

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                    finish();
                                }
                            });

                            customDialog = dialogBuilder.create();
                            customDialog.show();
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x;

                            double d = screenWidth*0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HostChangePasswordActivity.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout = R.layout.alert_dialog;
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView title = (TextView) dialogView.findViewById(R.id.title);
                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                            no.setVisibility(View.GONE);
                            vert.setVisibility(View.GONE);

                            if(language.equalsIgnoreCase("En")) {
                                title.setText(getResources().getString(R.string.app_name));
                                yes.setText(getResources().getString(R.string.str_btn_ok));
                                desc.setText("Change password unsuccessful");
                            }
                            else{
                                title.setText(getResources().getString(R.string.app_name_ar));
                                yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                desc.setText("Change password unsuccessful");
                            }

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                }
                            });

                            customDialog = dialogBuilder.create();
                            customDialog.show();
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x;

                            double d = screenWidth*0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                dialog.dismiss();
            }

            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result1);
        }
    }
}