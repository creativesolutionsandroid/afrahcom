package com.cs.afrahcom.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.ResortDetails;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 18-12-2017.
 */

public class AddRushDaysActivity extends AppCompatActivity {

    TextView tvResortName, submit;
    EditText etRate;
    ArrayList<ResortDetails> resortDetailsArrayList = new ArrayList<>();
    private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
    TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
    TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
    RelativeLayout layoutCheckin, layoutCheckout;
    Date mCheckInDate = null, mCheckOutDate = null, mTodayDate = null;
    String mCheckInDateStr = null, mCheckOutDateStr = null;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
    String hostId;
    SharedPreferences hostPrefs;
    SharedPreferences languagePrefs;
    String language;
    Boolean isEdit;
    Toolbar toolbar;
    AlertDialog customDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_add_rush_days);
        }
        else{
            setContentView(R.layout.activity_add_rush_days_ar);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostId = hostPrefs.getString("hostId", "0");

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        resortDetailsArrayList = (ArrayList<ResortDetails>) getIntent().getSerializableExtra("array");
        isEdit = getIntent().getBooleanExtra("edit", false);

        tvCheckInDate = (TextView) findViewById(R.id.tv_checkin_date);
        tvCheckInMonth = (TextView) findViewById(R.id.tv_checkin_month);
        tvCheckInWeek = (TextView) findViewById(R.id.tv_checkin_week);

        tvCheckOutDate = (TextView) findViewById(R.id.tv_checkout_date);
        tvCheckOutMonth = (TextView) findViewById(R.id.tv_checkout_month);
        tvCheckOutWeek = (TextView) findViewById(R.id.tv_checkout_week);

        layoutCheckin = (RelativeLayout) findViewById(R.id.layout_checkin);
        layoutCheckout = (RelativeLayout) findViewById(R.id.layout_checkout);

        etRate = (EditText) findViewById(R.id.tvRate);
        submit = (TextView) findViewById(R.id.tv_submit);
        tvResortName = (TextView) findViewById(R.id.resort_type_name);

        if(language.equalsIgnoreCase("En")) {
            tvResortName.setText(resortDetailsArrayList.get(0).getResortName());
        }
        else{
            tvResortName.setText(resortDetailsArrayList.get(0).getResortNameAr());
        }
        etRate.setSelection(etRate.length());

        final Calendar c = Calendar.getInstance();
        mCheckInYear = c.get(Calendar.YEAR);
        mCheckInMonth = c.get(Calendar.MONTH);
        mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

        if((c.get(Calendar.DATE)) <10) {
            tvCheckInDate.setText("0" + c.get(Calendar.DATE));
        }
        else{
            tvCheckInDate.setText("" + c.get(Calendar.DATE));
        }
        tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

        c.add(Calendar.DATE, 1);
        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

        if((c.get(Calendar.DATE)) <10) {
            tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
        }
        else{
            tvCheckOutDate.setText("" + c.get(Calendar.DATE));
        }
        tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

        String checkInDateString = mCheckInYear+"-"+(mCheckInMonth+1)+"-"+mCheckInDay;
        String checkOutDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(checkInDateString);
            mCheckOutDate = sdf.parse(checkOutDateString);
            mTodayDate = sdf.parse(checkInDateString);

            mCheckInDateStr = sdf.format(mCheckInDate);
            mCheckOutDateStr = sdf.format(mCheckOutDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        layoutCheckin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInDatePicker();
            }
        });

        layoutCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float rate;
                if(etRate.getText().toString().length() != 0){
                    rate = Float.parseFloat(etRate.getText().toString());
                    if(rate > 5 || rate <= 1){
                        showError();
                    }
                    else{
                        if(!isEdit) {
                            JSONObject parentObj = new JSONObject();
                            JSONObject rushObj = new JSONObject();

                            try {
                                rushObj.put("ResortId", resortDetailsArrayList.get(0).getResortId());
                                rushObj.put("HostId", hostId);
                                rushObj.put("StartDate", Constants.convertToArabic(mCheckInDateStr));
                                rushObj.put("EndDate", Constants.convertToArabic(mCheckOutDateStr));
                                rushObj.put("Rate", Constants.convertToArabic(Float.toString(rate)));
                                parentObj.put("InsertRush", rushObj);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Log.i("TAG",""+parentObj.toString());
                            new InsertRushDays().execute(parentObj.toString());

                        }
                    }
                }
                else{
                    showError();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void checkInDatePicker(){
        final Calendar c = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(AddRushDaysActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckInYear = year;
                        mCheckInDay = dayOfMonth;
                        mCheckInMonth = monthOfYear;

                        if(mCheckInDay<10) {
                            tvCheckInDate.setText("0" + mCheckInDay);
                        }
                        else{
                            tvCheckInDate.setText("" + mCheckInDay);
                        }
                        tvCheckInMonth.setText(MONTHS[mCheckInMonth]);

                        checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                    }
                }, mCheckInYear, mCheckInMonth, mCheckInDay);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
        datePickerDialog.setCancelable(false);
        String msg;
        if(language.equalsIgnoreCase("En")){
            msg = "Select Start Date";
        }
        else{
            msg = "إختر تاريخ البداية";
        }
        datePickerDialog.setMessage(msg);
        datePickerDialog.show();

    }

    public void checkOutDatePicker(int year, int date, int month){
        final Calendar c = Calendar.getInstance();
        long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
        String givenDateString = year+"-"+(month)+"-"+date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(givenDateString);
            mCheckInDateStr = sdf.format(mCheckInDate);
            selectedMillis = mCheckInDate.getTime();
            c.setTime(mCheckInDate); // yourdate is an object of type Date
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            tvCheckInWeek.setText(WEEKS[(dayOfWeek-1)]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mCheckOutYear = mCheckInYear;
        mCheckOutMonth = mCheckInMonth;
        mCheckOutDay = mCheckInDay;

        DatePickerDialog datePickerDialog = new DatePickerDialog(AddRushDaysActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckOutYear = year;
                        mCheckOutDay = dayOfMonth;
                        mCheckOutMonth = monthOfYear;

                        if(mCheckOutDay<10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay);
                        }
                        else{
                            tvCheckOutDate.setText("" + mCheckOutDay);
                        }
                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);

                        String givenDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf.parse(givenDateString);
                            mCheckOutDateStr = sdf.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek-1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
        datePickerDialog.getDatePicker().setMinDate(selectedMillis);
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(selectedMillis+max);
        datePickerDialog.setInverseBackgroundForced(true);
        String msg;
        if(language.equalsIgnoreCase("En")){
            msg = "Select End Date";
        }
        else{
            msg = "إختر تاريخ النهاية";
        }
        datePickerDialog.setMessage(msg);
        datePickerDialog.show();
    }

    public void showError(){
//        final iOSDialog iOSDialog = new iOSDialog(AddRushDaysActivity.this);
//        String appName, title, positive;
//        if(language.equalsIgnoreCase("En")){
//            appName = getResources().getString(R.string.app_name);
//            title = "Rate should be between 1.0x and 5.0x";
//            positive = getResources().getString(R.string.str_btn_ok);
//        }
//        else{
//            appName = getResources().getString(R.string.app_name_ar);
//            title = "";
//            positive = getResources().getString(R.string.str_btn_ok_ar);
//        }
//        iOSDialog.setTitle(appName);
//        iOSDialog.setSubtitle(title);
//        iOSDialog.setPositiveLabel(positive);
//        iOSDialog.setBoldPositiveLabel(false);
//        iOSDialog.setPositiveListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                iOSDialog.dismiss();
//            }
//        });
//        iOSDialog.show();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddRushDaysActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if(language.equalsIgnoreCase("En")) {
            title.setText(getResources().getString(R.string.app_name));
            yes.setText(getResources().getString(R.string.str_btn_ok));
            desc.setText("Rate should be between 1.0x and 5.0x");
        }
        else{
            title.setText(getResources().getString(R.string.app_name_ar));
            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
            desc.setText("Rate should be between 1.0x and 5.0x");
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public class InsertRushDays extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(AddRushDaysActivity.this);
            dialog = new ACProgressFlower.Builder(AddRushDaysActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.INSERT_RUSH_DAYS_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(AddRushDaysActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(AddRushDaysActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            String success = jo.getString("Success");
//
//                            final iOSDialog iOSDialog = new iOSDialog(AddRushDaysActivity.this);
//                            String appName, title, positive;
//                            if(language.equalsIgnoreCase("En")){
//                                appName = getResources().getString(R.string.app_name);
//                                title = "Rush days added successfully.";
//                                positive = getResources().getString(R.string.str_btn_ok);
//                            }
//                            else{
//                                appName = getResources().getString(R.string.app_name_ar);
//                                title = "تمت إضافة أيام الذروة بنجاح";
//                                positive = getResources().getString(R.string.str_btn_ok_ar);
//                            }
//                            iOSDialog.setTitle(appName);
//                            iOSDialog.setSubtitle(title);
//                            iOSDialog.setPositiveLabel(positive);
//                            iOSDialog.setBoldPositiveLabel(false);
//                            iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    iOSDialog.dismiss();
//                                    setResult(RESULT_OK);
//                                    finish();
//                                }
//                            });
//                            iOSDialog.show();

                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddRushDaysActivity.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout = R.layout.alert_dialog;
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView title = (TextView) dialogView.findViewById(R.id.title);
                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                            no.setVisibility(View.GONE);
                            vert.setVisibility(View.GONE);

                            if(language.equalsIgnoreCase("En")) {
                                title.setText(getResources().getString(R.string.app_name));
                                yes.setText(getResources().getString(R.string.str_btn_ok));
                                desc.setText("Rush days added successfully.");
                            }
                            else{
                                title.setText(getResources().getString(R.string.app_name_ar));
                                yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                desc.setText("تمت إضافة أيام الذروة بنجاح");
                            }

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            });

                            customDialog = dialogBuilder.create();
                            customDialog.show();
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x;

                            double d = screenWidth*0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            try {
                                JSONObject jo= new JSONObject(result);
                                String error = jo.getString("Error");
                                if(error.equals("This Dates Cannot be Inserted")){
//                                    final iOSDialog iOSDialog = new iOSDialog(AddRushDaysActivity.this);
//                                    String appName, title, positive;
//                                    if(language.equalsIgnoreCase("En")){
//                                        appName = getResources().getString(R.string.app_name);
//                                        title = "Rush days already exists for these dates.";
//                                        positive = getResources().getString(R.string.str_btn_ok);
//                                    }
//                                    else{
//                                        appName = getResources().getString(R.string.app_name_ar);
//                                        title = "أيام الذروة موجودة هذا لتاريخ";
//                                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                                    }
//                                    iOSDialog.setTitle(appName);
//                                    iOSDialog.setSubtitle(title);
//                                    iOSDialog.setPositiveLabel(positive);
//                                    iOSDialog.setBoldPositiveLabel(false);
//                                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            iOSDialog.dismiss();
//                                        }
//                                    });
//                                    iOSDialog.show();

                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddRushDaysActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                                    no.setVisibility(View.GONE);
                                    vert.setVisibility(View.GONE);

                                    if(language.equalsIgnoreCase("En")) {
                                        title.setText(getResources().getString(R.string.app_name));
                                        yes.setText(getResources().getString(R.string.str_btn_ok));
                                        desc.setText("Rush days already exists for these dates.");
                                    }
                                    else{
                                        title.setText(getResources().getString(R.string.app_name_ar));
                                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                        desc.setText("أيام الذروة موجودة هذا لتاريخ");
                                    }

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth*0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
                                }
                                else if(error.equals("Rush days for this given property already available")){
//                                    final iOSDialog iOSDialog = new iOSDialog(AddRushDaysActivity.this);
//                                    String appName, title, positive;
//                                    if(language.equalsIgnoreCase("En")){
//                                        appName = getResources().getString(R.string.app_name);
//                                        title = "Rush days already exists for these dates.";
//                                        positive = getResources().getString(R.string.str_btn_ok);
//                                    }
//                                    else{
//                                        appName = getResources().getString(R.string.app_name_ar);
//                                        title = "أيام الذروة موجودة لهذه التواريخ";
//                                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                                    }
//                                    iOSDialog.setTitle(appName);
//                                    iOSDialog.setSubtitle(title);
//                                    iOSDialog.setPositiveLabel(positive);
//                                    iOSDialog.setBoldPositiveLabel(false);
//                                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            iOSDialog.dismiss();
//                                        }
//                                    });
//                                    iOSDialog.show();

                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddRushDaysActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                                    no.setVisibility(View.GONE);
                                    vert.setVisibility(View.GONE);

                                    if(language.equalsIgnoreCase("En")) {
                                        title.setText(getResources().getString(R.string.app_name));
                                        yes.setText(getResources().getString(R.string.str_btn_ok));
                                        desc.setText("Rush days already exists for these dates.");
                                    }
                                    else{
                                        title.setText(getResources().getString(R.string.app_name_ar));
                                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                        desc.setText("أيام الذروة موجودة هذا لتاريخ");
                                    }

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth*0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
                                }
                                else{
                                    if(language.equalsIgnoreCase("En")) {
                                        Toast.makeText(AddRushDaysActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                                    }
                                    else{
                                        Toast.makeText(AddRushDaysActivity.this, "فشل الاتصال بالخادم", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }

            }else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(AddRushDaysActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(AddRushDaysActivity.this, "فشل الاتصال بالخادم", Toast.LENGTH_SHORT).show();
                }
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
