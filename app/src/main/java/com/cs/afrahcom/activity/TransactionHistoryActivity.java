package com.cs.afrahcom.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.ResortHistoryAdapter;
import com.cs.afrahcom.models.OrderHistory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 25-10-2017.
 */

public class TransactionHistoryActivity extends AppCompatActivity {

    TextView tvHistoryCount, tvNoBooingsDone;
    ArrayList<OrderHistory> orderHistoryArrayList = new ArrayList<>();
    ListView mListView;
    ResortHistoryAdapter mAdapter;
    String resortID;
    SharedPreferences hostPrefs;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_transaction_history);
        }
        else{
            setContentView(R.layout.activity_transaction_history_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        resortID = hostPrefs.getString("resortId", "-1");

        new getOrderHistory().execute(Constants.RESORT_ORDER_HISTORY_URL+resortID);

        tvHistoryCount = (TextView) findViewById(R.id.historyCount);
        tvNoBooingsDone = (TextView) findViewById(R.id.noBookingsDone);

        mListView = (ListView) findViewById(R.id.historyListView);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class getOrderHistory extends AsyncTask<String, Integer, String> {
        String  networkStatus;
        ACProgressFlower dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(TransactionHistoryActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONArray successArray = jo.getJSONArray("Success");
                            for (int i = 0; i<successArray.length(); i++){
                                JSONObject newObj = successArray.getJSONObject(i);

                                if(newObj.getString("OrderStatus").equalsIgnoreCase("New") &&
                                        newObj.getString("PaymentStatus").equalsIgnoreCase("Pending")) {
                                    OrderHistory orderHistory = new OrderHistory();
                                    orderHistory.setBookingId(newObj.getString("BookingId"));
                                    orderHistory.setStartDate(newObj.getString("StartDate"));
                                    if(language.equalsIgnoreCase("En")) {
                                        orderHistory.setResortName(newObj.getString("ResortName"));
                                    }
                                    else{
                                        orderHistory.setResortName(newObj.getString("ResortName_Ar"));
                                    }
                                    orderHistory.setNoOfMen(newObj.getString("NoOfMen"));
                                    orderHistory.setNoOfWomen(newObj.getString("NoOfWomen"));
                                    orderHistory.setResortType(newObj.getString("ResortType"));
                                    orderHistory.setBookingStatus(newObj.getString("OrderStatus"));
                                    orderHistory.setPaymenyStatus(newObj.getString("PaymentStatus"));
                                    orderHistory.setAdvanceAmount(newObj.getString("AdvancePrice"));
                                    orderHistory.setTotalAmount(newObj.getString("TotalPrice"));
//                                    orderHistory.setPaymenyStatus(newObj.getString("PaymentStatus"));
                                    orderHistory.setReservationDate(newObj.getString("ReservationDate"));
                                    orderHistory.setUserName(newObj.getString("UserName"));
                                    orderHistory.setEndDate(newObj.getString("EndDate"));
                                    orderHistory.setIsKitchenSelected(newObj.getString("IsKitchenSelected"));
                                    orderHistory.setIscateringSelected(newObj.getString("IscateringSelected"));
                                    orderHistory.setInvoiceNumber(newObj.getString("InvoiceNo"));
                                    orderHistory.setMobile(newObj.getString("MobileNo"));
                                    orderHistoryArrayList.add(orderHistory);
                                }
                            }
//                            Collections.reverse(orderHistoryArrayList);
                            mAdapter = new ResortHistoryAdapter(TransactionHistoryActivity.this, orderHistoryArrayList, language);
                            mListView.setAdapter(mAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    tvHistoryCount.setText(""+orderHistoryArrayList.size());
                    if(orderHistoryArrayList.size() == 0){
                        tvNoBooingsDone.setVisibility(View.VISIBLE);
                        mListView.setVisibility(View.GONE);
                    }
                }
            }else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(ResortHistoryActivity.this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
        finish();
    }
}
