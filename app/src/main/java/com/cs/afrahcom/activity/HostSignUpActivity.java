package com.cs.afrahcom.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;

import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 21-09-2017.
 */

public class HostSignUpActivity extends Activity implements View.OnClickListener{

    private TextView tvForgetPassword, tvSignup;
    private EditText etCompanyName, etContactPerson, etMobile, etLandline, etEmail, etPassword;
    private String mCompanyNameStr, mContactPersonStr, mEmailStr, mMobileStr, mLandlineStr, mPasswordStr;
    public static final String[] SMS_RECIEVER = {
            android.Manifest.permission.RECEIVE_SMS
    };
    public static final int SMS_REQUEST = 1;
    public static final int VERIFICATION_REQUEST = 2;
    Context mContext;
    ImageView imgCancel;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_host_signup);
        }
        else{
            setContentView(R.layout.activity_host_signup_ar);
        }
        mContext = this;

        tvForgetPassword = (TextView) findViewById(R.id.tv_forget_Password);
        tvSignup = (TextView) findViewById(R.id.tv_signup);

        etCompanyName = (EditText) findViewById(R.id.etCompanyName);
        etContactPerson = (EditText) findViewById(R.id.etContactPerson);
        etEmail = (EditText) findViewById(R.id.et_signup_email);
        etMobile = (EditText) findViewById(R.id.et_signup_mobile);
        etLandline = (EditText) findViewById(R.id.et_signup_landline);
        etPassword = (EditText) findViewById(R.id.et_signup_password);
        imgCancel = (ImageView) findViewById(R.id.image_cancel);

//        Setting Underline for textview
        tvForgetPassword.setPaintFlags(tvForgetPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

//        Requesting permission for auto otp reading
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if(!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)){
                requestPermissions(SMS_RECIEVER, SMS_REQUEST);
            }
        }

        etMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(etMobile.getText().toString().equals("0")){
                    etMobile.setText("");
                }
            }
        });
        tvSignup.setOnClickListener(this);
        tvForgetPassword.setOnClickListener(this);
        imgCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_forget_Password:
                Intent intent = new Intent(HostSignUpActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;

            case R.id.image_cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;

            case R.id.tv_signup:
                final JSONObject parent = new JSONObject();
                mCompanyNameStr = etCompanyName.getText().toString();
                mContactPersonStr = etContactPerson.getText().toString();
                mMobileStr = etMobile.getText().toString();
                mLandlineStr = etLandline.getText().toString();
                mEmailStr = etEmail.getText().toString().replaceAll(" ","");
                mPasswordStr = etPassword.getText().toString();

                if(mCompanyNameStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etCompanyName.setError(getResources().getString(R.string.str_alert_company_name));
                    }
                    else{
                        etCompanyName.setError(getResources().getString(R.string.str_alert_company_name_ar));
                    }
                }
                else if(mContactPersonStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etContactPerson.setError(getResources().getString(R.string.str_alert_company_person_name));
                    }
                    else{
                        etContactPerson.setError(getResources().getString(R.string.str_alert_company_person_name_ar));
                    }
                }
                else if(mEmailStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etEmail.setError(getResources().getString(R.string.str_alert_email));
                    }
                    else{
                        etEmail.setError(getResources().getString(R.string.str_alert_email_ar));
                    }
                }
                else if(!isValidEmail(mEmailStr)){
                    if(language.equalsIgnoreCase("En")) {
                        etEmail.setError(getResources().getString(R.string.str_alert_valid_email));
                    }
                    else{
                        etEmail.setError(getResources().getString(R.string.str_alert_valid_email_ar));
                    }
                }
                else if(mMobileStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etMobile.setError(getResources().getString(R.string.str_alert_mobile));
                    }
                    else{
                        etMobile.setError(getResources().getString(R.string.str_alert_mobile_ar));
                    }
                }
                else if(mMobileStr.length() != 9){
                    if(language.equalsIgnoreCase("En")) {
                        etMobile.setError(getResources().getString(R.string.str_alert_valid_mobile));
                    }
                    else{
                        etMobile.setError(getResources().getString(R.string.str_alert_valid_mobile_ar));
                    }
                }
                else if(mLandlineStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etLandline.setError(getResources().getString(R.string.str_alert_landline));
                    }
                    else{
                        etLandline.setError(getResources().getString(R.string.str_alert_landline_ar));
                    }
                }
                else if(mLandlineStr.length() != 9){
                    if(language.equalsIgnoreCase("En")) {
                        etLandline.setError(getResources().getString(R.string.str_alert_valid_landline));
                    }
                    else{
                        etLandline.setError(getResources().getString(R.string.str_alert_valid_landline_ar));
                    }
                }
                else if (mPasswordStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etPassword.setError(getResources().getString(R.string.str_alert_password));
                    }
                    else{
                        etPassword.setError(getResources().getString(R.string.str_alert_password_ar));
                    }
                }
                else if(mPasswordStr.length()<8){
                    if(language.equalsIgnoreCase("En")) {
                        etPassword.setError(getResources().getString(R.string.str_alert_valid_password));
                    }
                    else{
                        etPassword.setError(getResources().getString(R.string.str_alert_valid_password_ar));
                    }
                }
                else{

                    try {
//                        JSONArray mainItem = new JSONArray();

                        JSONObject mainObj = new JSONObject();
                        mainObj.put("CompanyName",mCompanyNameStr);
                        mainObj.put("ContactPerson", mContactPersonStr);
                        mainObj.put("Language", language);
                        mainObj.put("MobileNo", "966"+mMobileStr);
                        mainObj.put("Email", mEmailStr);
                        mainObj.put("Landline", "966"+mLandlineStr);
                        mainObj.put("Password", mPasswordStr);
                        mainObj.put("DeviceToken", SplashScreenActivity.regId);
                        mainObj.put("DeviceType", "Android");
//                        mainItem.put(mainObj);

                        parent.put("InsertHost", mainObj);
                        Log.i("TAG", parent.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    Alert Dialog
//                    final iOSDialog iOSDialog = new iOSDialog(HostSignUpActivity.this);
//                    iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                    iOSDialog.setSubtitle("We will be verifying the mobile number:\n\n+966" +" " + mMobileStr + "\n\nIs this OK, or would you like to edit the number?");
//                    iOSDialog.setNegativeLabel(getResources().getString(R.string.str_btn_ok));
//                    iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_edit));
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setNegativeListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                            new GetVerificationCode().execute(Constants.VERIFY_MOBILE_HOST+ "966"+mMobileStr, parent.toString());
//                        }
//                    });
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            etMobile.requestFocus();
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HostSignUpActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setText(getResources().getString(R.string.str_btn_ok));
                    yes.setText(getResources().getString(R.string.str_btn_edit));
                    desc.setText("We will be verifying the mobile number:\n\n+966" + " " + mMobileStr + "\n\nIs this OK, or would you like to edit the number?");

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            etMobile.requestFocus();
                        }
                    });
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            new GetVerificationCode().execute(Constants.VERIFY_MOBILE_HOST+ "966"+mMobileStr+"&language="+language, parent.toString());
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;
        }
    }

    public class GetVerificationCode extends AsyncTask<String, Integer, String>{
        String  networkStatus;
        ACProgressFlower dialog;
        String response;
        String userData;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(HostSignUpActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                userData = params[1];
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                String otp = jo.getString("Success");
//                                String otp = jo1.getString("OTP");
//                                String phNo = jo1.getString("MobileNo");
//
                                Intent i = new Intent(HostSignUpActivity.this, VerifyOTPActivity.class);
                                i.putExtra("OTP", otp);
                                i.putExtra("phone_number", "966"+mMobileStr);
                                i.putExtra("user_data", userData);
                                i.putExtra("forgot",false);
                                i.putExtra("screen", "host");
                                startActivityForResult(i, VERIFICATION_REQUEST);

                            }catch (JSONException je){
                                String msg = jo.getString("Failure");
                                je.printStackTrace();

//                                final iOSDialog iOSDialog = new iOSDialog(HostSignUpActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(msg);
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HostSignUpActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText(msg);
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText(msg);
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == VERIFICATION_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }else if (requestCode == VERIFICATION_REQUEST && resultCode == RESULT_CANCELED){
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
