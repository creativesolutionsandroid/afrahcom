package com.cs.afrahcom.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 22-11-2017.
 */

public class HostEditProfileActivity extends AppCompatActivity {

    private EditText etCompanyName, etContactPerson, etEmail, etMobile, etLandline;
    private String mCompanyNameStr, mContactPersonStr, mLandlineStr, mEmailStr, mMobileStr, mPasswordStr;
    SharedPreferences.Editor hostPrefEditor;
    TextView submit;
    String hostId;
    SharedPreferences hostPrefs;
    SharedPreferences languagePrefs;
    AlertDialog customDialog;
    String language;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_host_edit_profile);
        }
        else{
            setContentView(R.layout.activity_host_edit_profile_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostPrefEditor = hostPrefs.edit();
        hostId = hostPrefs.getString("hostId", null);

        etCompanyName = (EditText) findViewById(R.id.et_first_name);
        etContactPerson = (EditText) findViewById(R.id.et_middle_name);
        etEmail = (EditText) findViewById(R.id.et_last_name);
        etMobile = (EditText) findViewById(R.id.et_mobile);
        etLandline = (EditText) findViewById(R.id.et_landline);

        etMobile.setFocusableInTouchMode(false);
        etMobile.setFocusable(false);
        submit = (TextView) findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final JSONObject parent = new JSONObject();
                mCompanyNameStr = etCompanyName.getText().toString();
                mContactPersonStr = etContactPerson.getText().toString();
                mEmailStr = etEmail.getText().toString();
                mMobileStr = etMobile.getText().toString();
                mLandlineStr = etLandline.getText().toString();
                mEmailStr = etEmail.getText().toString().replaceAll(" ","");

                if(mCompanyNameStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etCompanyName.setError(getResources().getString(R.string.str_alert_company_name));
                    }
                    else{
                        etCompanyName.setError(getResources().getString(R.string.str_alert_company_name_ar));
                    }
                }
                else if(mContactPersonStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etContactPerson.setError(getResources().getString(R.string.str_alert_company_person_name));
                    }
                    else{
                        etContactPerson.setError(getResources().getString(R.string.str_alert_company_person_name_ar));
                    }
                }
                else if(mEmailStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etEmail.setError(getResources().getString(R.string.str_alert_email));
                    }
                    else{
                        etEmail.setError(getResources().getString(R.string.str_alert_email_ar));
                    }
                }
                else if(!isValidEmail(mEmailStr)){
                    if(language.equalsIgnoreCase("En")) {
                        etEmail.setError(getResources().getString(R.string.str_alert_valid_email));
                    }
                    else{
                        etEmail.setError(getResources().getString(R.string.str_alert_valid_email_ar));
                    }
                }
                else if(mLandlineStr.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        etLandline.setError(getResources().getString(R.string.str_alert_landline));
                    }
                    else{
                        etLandline.setError(getResources().getString(R.string.str_alert_landline_ar));
                    }
                }
                else if(mLandlineStr.length() != 9){
                    if(language.equalsIgnoreCase("En")) {
                        etLandline.setError(getResources().getString(R.string.str_alert_valid_landline));
                    }
                    else{
                        etLandline.setError(getResources().getString(R.string.str_alert_valid_landline_ar));
                    }
                }
                else {

                    try {
//                        JSONArray mainItem = new JSONArray();

                        JSONObject mainObj = new JSONObject();
                        mainObj.put("Id", hostId);
                        mainObj.put("CompanyName", mCompanyNameStr);
                        mainObj.put("ContactPerson",mContactPersonStr);
                        mainObj.put("Email",mEmailStr);
                        mainObj.put("Language", "En");
                        mainObj.put("MobileNo", "966" + mMobileStr);
                        mainObj.put("Landline", "966" + mLandlineStr);
                        mainObj.put("DeviceToken", SplashScreenActivity.regId);
                        mainObj.put("DeviceType", "Android");
//                        mainItem.put(mainObj);

                        parent.put("Updatedetails", mainObj);
                        Log.i("TAG", parent.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    new UpdateProfile().execute(parent.toString());
                }
            }
        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String response = hostPrefs.getString("host_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                etCompanyName.setText(userObjuect.getString("CompanyName"));
                mCompanyNameStr = userObjuect.getString("CompanyName");
                etCompanyName.setSelection(mCompanyNameStr.length());

                etContactPerson.setText(userObjuect.getString("ContactPerson"));
                mContactPersonStr = userObjuect.getString("ContactPerson");

                mMobileStr = userObjuect.getString("MobileNo");
                mMobileStr = mMobileStr.substring(3,12);
                etMobile.setText(""+mMobileStr);

                mLandlineStr = userObjuect.getString("LandlineNo");
                mLandlineStr = mLandlineStr.substring(3,12);
                etLandline.setText(""+mLandlineStr);

                etEmail.setText(userObjuect.getString("Email"));
                mEmailStr = userObjuect.getString("Email");
                mPasswordStr = userObjuect.getString("Password");

            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public class UpdateProfile extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(HostEditProfileActivity.this);
            dialog = new ACProgressFlower.Builder(HostEditProfileActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.HOST_EDIT_PROFILE_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(HostEditProfileActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(HostEditProfileActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
//                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = jo.getJSONObject("success");
                                String companyName = jo1.getString("CompanyName");
                                String contactPerson = jo1.getString("ContactPerson");
                                String email = jo1.getString("Email");
                                String landline = jo1.getString("LandLine");
//                                String password = jo1.getString("Password");
                                String mobile = jo1.getString("Mobile");
                                String hostID = jo1.getString("Id");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("CompanyName", companyName);
                                    jsonObject.put("ContactPerson", contactPerson);
                                    jsonObject.put("MobileNo", mobile);
                                    jsonObject.put("Email", email);
                                    jsonObject.put("LandlineNo", landline);
                                    jsonObject.put("Password", mPasswordStr);
                                    jsonObject.put("host_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    hostPrefEditor.putString("host_profile", parent.toString());
                                    hostPrefEditor.putString("hostId", hostID);
                                    hostPrefEditor.commit();

                                    finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
                                je.printStackTrace();
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getApplicationContext());
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText(result);
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText(result);
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(HostEditProfileActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(HostEditProfileActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                }
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
