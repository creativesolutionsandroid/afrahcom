package com.cs.afrahcom.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.HostResortSelectionAdapter;
import com.cs.afrahcom.models.HostResorts;

import java.util.ArrayList;

/**
 * Created by CS on 15-11-2017.
 */

public class HostResortSelectionActivity extends AppCompatActivity {

    TextView tvHistoryCount, title;
    ListView mListView;
    ArrayList<HostResorts> resortsArrayList = new ArrayList<>();
    HostResortSelectionAdapter mAdapter;
    public static String resortID;
    SharedPreferences hostPrefs;
    SharedPreferences.Editor hostPrefsEditor;
    SharedPreferences languagePrefs;
    String language;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_resort_selection);
        }
        else{
            setContentView(R.layout.activity_resort_selection_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostPrefsEditor = hostPrefs.edit();

        resortsArrayList = (ArrayList<HostResorts>) getIntent().getSerializableExtra("array");
        resortID = getIntent().getStringExtra("resortid");

        tvHistoryCount = (TextView) findViewById(R.id.historyCount);
        title = (TextView) findViewById(R.id.tv_booking_history);

        mListView = (ListView) findViewById(R.id.historyListView);

        mAdapter  = new HostResortSelectionAdapter(this, resortsArrayList, language);
        mListView.setAdapter(mAdapter);

        tvHistoryCount.setText(""+resortsArrayList.size());
        if(language.equalsIgnoreCase("En")) {
            title.setText("Select Property");
        }
        else{
            title.setText("تغيير القاعة");
        }

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(!HostResortSelectionActivity.resortID.equals(resortsArrayList.get(i).getResortId())) {
                    resortID = resortsArrayList.get(i).getResortId();
                    mAdapter.notifyDataSetChanged();

                    resortID = resortsArrayList.get(i).getResortId();
                    hostPrefsEditor.putString("resortId",resortID);
                    hostPrefsEditor.commit();

                    setResult(RESULT_OK);
                    finish();
                }
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
