package com.cs.afrahcom.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.CaterersAdapter;
import com.cs.afrahcom.models.FoodDetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 26-09-2017.
 */

public class CaterersActivity extends AppCompatActivity {

    ArrayList<FoodDetails> breakFastArrayList = new ArrayList<>();
    GridView mFoodGrid;
    CaterersAdapter mAdapter;
    String userId;
    SharedPreferences userPrefs;
    LinearLayout topLayout, bottomLayout;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_food_selection);
        }
        else{
            setContentView(R.layout.activity_food_selection_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        if (userId == null) {
            userId = "0";
        }

        mFoodGrid = (GridView) findViewById(R.id.foodSelectionGrid);
        topLayout = (LinearLayout) findViewById(R.id.top_layout);
        bottomLayout = (LinearLayout) findViewById(R.id.bottom_layout);
        topLayout.setVisibility(View.GONE);
        bottomLayout.setVisibility(View.GONE);

        new GetFoodDetails().execute(Constants.GET_FOOD_DETAILS+userId);

        mAdapter = new CaterersAdapter(CaterersActivity.this, breakFastArrayList, language, CaterersActivity.this);
        mFoodGrid.setAdapter(mAdapter);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class GetFoodDetails extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(CaterersActivity.this);
            dialog = new ACProgressFlower.Builder(CaterersActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(CaterersActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(CaterersActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONArray jsonArray = jo.getJSONArray("Success");

                            for (int i=0; i<jsonArray.length(); i++){

                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                FoodDetails foodDetails= new FoodDetails();

                                foodDetails.setFoodId(jsonObject.getString("FoodId"));
                                if(language.equalsIgnoreCase("En")) {
                                    foodDetails.setFoodName(jsonObject.getString("FoodName"));
                                }
                                else{
                                    foodDetails.setFoodName(jsonObject.getString("FoodName_Ar"));
                                }
                                if(language.equalsIgnoreCase("En")) {
                                    foodDetails.setFoodDescription(jsonObject.getString("FoodDescription"));
                                }
                                else{
                                    foodDetails.setFoodDescription(jsonObject.getString("FoodDescription_Ar"));
                                }
                                foodDetails.setFoodImage(jsonObject.getString("FoodImage"));
                                foodDetails.setSource(jsonObject.getString("Source"));
                                foodDetails.setFavourite(jsonObject.getBoolean("IsFavourite"));
                                foodDetails.setRating(jsonObject.getString("Rating"));
                                breakFastArrayList.add(foodDetails);
                            }

                            mAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                            if(language.equalsIgnoreCase("En")) {
                                Toast.makeText(CaterersActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(CaterersActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                            finish();
                        }
                    }
                }
            } else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(CaterersActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(CaterersActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }
}
