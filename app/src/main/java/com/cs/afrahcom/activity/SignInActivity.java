package com.cs.afrahcom.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 15-06-2016.
 */
public class SignInActivity extends Activity {

    private static final int REGISTRATION_REQUEST = 1;
    private static final int VERIFICATION_REQUEST = 2;
    private EditText etMobileNumber, etPassword;
    TextView tvSignIn, tvForgotPassword, tvSignUp;
    Toolbar toolbar;
    String response;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    Context context;
    AlertDialog customDialog;
    ImageView imgCancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_signin);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_signin_ar);
        }
        context = this;
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        etMobileNumber = (EditText) findViewById(R.id.et_signin_mobile);
        etPassword = (EditText) findViewById(R.id.et_signin_password);
        tvSignIn = (TextView) findViewById(R.id.tv_signin);
        tvSignUp = (TextView) findViewById(R.id.tv_signup);
        tvForgotPassword = (TextView) findViewById(R.id.tv_forget_Password);
        imgCancel = (ImageView) findViewById(R.id.image_cancel);

        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNumberStr = etMobileNumber.getText().toString();
                String passwordStr = etPassword.getText().toString();
                if (mobileNumberStr.length() == 0) {
                    if(language.equalsIgnoreCase("En")) {
                        etMobileNumber.setError(getResources().getString(R.string.str_alert_mobile));
                    }
                    else{
                        etMobileNumber.setError(getResources().getString(R.string.str_alert_mobile_ar));
                    }
                    etMobileNumber.requestFocus();
                }else if(mobileNumberStr.length() != 9){
                    if(language.equalsIgnoreCase("En")) {
                        etMobileNumber.setError(getResources().getString(R.string.str_alert_valid_mobile));
                    }
                    else{
                        etMobileNumber.setError(getResources().getString(R.string.str_alert_valid_mobile_ar));
                    }
                    etMobileNumber.requestFocus();
                }else if (passwordStr.length() == 0) {
                    if(language.equalsIgnoreCase("En")) {
                        etPassword.setError(getResources().getString(R.string.str_alert_password));
                    }
                    else{
                        etPassword.setError(getResources().getString(R.string.str_alert_password_ar));
                    }
                    etPassword.requestFocus();
                } else {
                    new CheckLoginDetails().execute(Constants.LOGIN_URL+"966"+mobileNumberStr+"&password="+passwordStr
                            +"&language="+language+"&devicetoken="+SplashScreenActivity.regId);
                }
            }
        });

        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivityForResult(intent, REGISTRATION_REQUEST);
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REGISTRATION_REQUEST && resultCode == RESULT_OK){
//            Intent loginIntent = new Intent(SignInActivity.this, MainActivity.class);
//            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(loginIntent);
//            finish();
            setResult(RESULT_OK);
            finish();
        }else if(requestCode == VERIFICATION_REQUEST && resultCode == RESULT_OK){
//            Intent loginIntent = new Intent(SignInActivity.this, MainActivity.class);
//            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(loginIntent);
//            finish();
            setResult(RESULT_OK);
            finish();
        }else if (resultCode == RESULT_CANCELED){
//            finish();
//            Toast.makeText(LoginActivity.this, "Registration unseccessfull", Toast.LENGTH_SHORT).show();
        }
    }

    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
            dialog = new ACProgressFlower.Builder(SignInActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }
        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(SignInActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }else {
                        try {
                            JSONObject jo= new JSONObject(result);
                            try{
                                JSONArray ja = jo.getJSONArray("succcess");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String MiddleName = jo1.getString("MiddleName");
                                String FamilyName = jo1.getString("FamilyName");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                String password = jo1.getString("Password");
                                String cityId = jo1.getString("CityId");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("MiddleName", MiddleName);
                                    jsonObject.put("FamilyName", "");
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("CityId", cityId);
                                    jsonObject.put("Password", password);
                                    jsonObject.put("language", language);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

                                    userPrefEditor.putString("login_status","loggedin");
                                    userPrefEditor.commit();

//                                    Intent loginIntent = new Intent(SignInActivity.this, MainActivity.class);
//                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(loginIntent);
//                                    finish();

                                    setResult(RESULT_OK);
                                    finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
//                                final iOSDialog iOSDialog = new iOSDialog(SignInActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(getResources().getString(R.string.str_signin_invalid_mobile_number));
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignInActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText(getResources().getString(R.string.str_signin_invalid_mobile_number));
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText(getResources().getString(R.string.str_signin_invalid_mobile_number_ar));
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else {
                Toast.makeText(SignInActivity.this, getResources().getString(R.string.str_cannot_reach_server), Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }
}
