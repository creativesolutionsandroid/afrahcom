package com.cs.afrahcom.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.FavResortsAdapter;
import com.cs.afrahcom.fragment.ResortSearchFragment;
import com.cs.afrahcom.models.Facilities;
import com.cs.afrahcom.models.ImagesModel;
import com.cs.afrahcom.models.ResortDetailsFiltered;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

import static com.cs.afrahcom.fragment.ResortSearchFragment.currentLatitude;
import static com.cs.afrahcom.fragment.ResortSearchFragment.currentLongitude;
import static com.cs.afrahcom.fragment.ResortSearchFragment.isToday;
import static com.cs.afrahcom.fragment.ResortSearchFragment.todayWeek;

/**
 * Created by CS on 25-10-2017.
 */

public class FavoriteResortsActivity extends AppCompatActivity {

    public static String userId;
    SharedPreferences userPrefs;
    TextView tvFavCount, tvNoFavResorts;
    SwipeMenuListView favListView;
    FavResortsAdapter mAdapter;
    double destinationLatitude, destinationLongitude;
    public static ArrayList<ResortDetailsFiltered> favoriteResortsArrayList = new ArrayList<>();
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    public static String language;
    public static int bannerPos = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_favorite_resorts);
        }
        else{
            setContentView(R.layout.activity_favorite_resorts_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        new getFavResorts().execute(Constants.FAV_RESORTS_URL+userId);

        tvFavCount = (TextView) findViewById(R.id.favCount);
        tvNoFavResorts = (TextView) findViewById(R.id.nofavResorts);
        favListView = (SwipeMenuListView) findViewById(R.id.favResortListView);
        mAdapter = new FavResortsAdapter(FavoriteResortsActivity.this, favoriteResortsArrayList, language);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator2 = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menu.getViewType()) {
                    case 0:
                        // create menu of type 0
                        SwipeMenuItem delete = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        delete.setBackground(new ColorDrawable(Color.rgb(0xff,
                                0x00, 0x00)));
                        // set item width
                        delete.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            delete.setTitle("Delete");
                        }
                        else{
                            delete.setTitle("حذف");
                        }

                        // set item title fontsize
                        delete.setTitleSize(18);
                        // set item title font color
                        delete.setTitleColor(Color.WHITE);

                        menu.addMenuItem(delete);


                        break;
                }
            }

        };
        favListView.setMenuCreator(creator2);

        // step 2. listener item click event
        favListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        JSONObject parentObj = new JSONObject();
                        try {
                            JSONObject mainObj = new JSONObject();
                            mainObj.put("ResortId", favoriteResortsArrayList.get(position).getResortId());
                            mainObj.put("UserId", userId);

                            parentObj.put("FavouriteResorts", mainObj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", parentObj.toString());

                        new DeleteFavOrder().execute(parentObj.toString());
                        break;
                }
                return false;
            }
        });

        favListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FavoriteResortsActivity.bannerPos = position;
                loadPopup();
            }
        });

        Constants.isKitchenSelected = 0;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    public class getFavResorts extends AsyncTask<String, Integer, String> {
        String  networkStatus;
        ACProgressFlower dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteResortsActivity.this);
            dialog = new ACProgressFlower.Builder(FavoriteResortsActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        favoriteResortsArrayList.clear();
                        DecimalFormat priceFormat = new DecimalFormat("0");
                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONArray successArray = jo.getJSONArray("Success");
                            for (int i = 0; i<successArray.length(); i++){
                                JSONObject jsonObject = successArray.getJSONObject(i);

                                ResortDetailsFiltered resortDetails = new ResortDetailsFiltered();
                                resortDetails.setResortId(jsonObject.getString("ResortId"));
                                resortDetails.setResortTypeId(jsonObject.getString("ResortTypeId"));
                                resortDetails.setResortName(jsonObject.getString("ResortName"));
                                resortDetails.setResortNameAr(jsonObject.getString("ResortName_Ar"));
                                resortDetails.setCityId(jsonObject.getString("CityId"));
                                resortDetails.setResortAddress(jsonObject.getString("ResortAddress"));
                                resortDetails.setResortType(jsonObject.getString("ResortType"));
                                resortDetails.setDesc(jsonObject.getString("Description"));
                                resortDetails.setDescAr(jsonObject.getString("Description_Ar"));
                                resortDetails.setMinCapacity(Integer.parseInt(jsonObject.getString("MinCapacity")));
                                resortDetails.setMaxCapacity(Integer.parseInt(jsonObject.getString("MaxCapacity")));
                                resortDetails.setLatitude(jsonObject.getString("Latitude"));
                                resortDetails.setLongitude(jsonObject.getString("Longitude"));
                                resortDetails.setRating(jsonObject.getString("Rating"));
                                resortDetails.setDiscount(jsonObject.getString("Discount"));
                                resortDetails.setRate(jsonObject.getString("Rate"));
                                resortDetails.setAfrahcomAdvance(jsonObject.getString("AdvanceAmount"));
                                resortDetails.setApproved(jsonObject.getString("AfrahcomApproved"));
                                resortDetails.setVatAvailability(jsonObject.getString("VatAvailability"));

                                JSONArray imagesJSON = jsonObject.getJSONArray("ResortImage");
                                ArrayList<ImagesModel> imagesArrayList = new ArrayList<>();
                                for (int j = 0; j < imagesJSON.length(); j++) {
                                    ImagesModel imagesModel = new ImagesModel();
                                    JSONObject imagesObject = imagesJSON.getJSONObject(j);
                                    imagesModel.setImageName(imagesObject.getString("ImageName"));
                                    imagesModel.setSource(imagesObject.getString("Source"));
                                    imagesArrayList.add(imagesModel);
                                }
                                resortDetails.setResortImagesArray(imagesArrayList);

                                destinationLatitude = Double.parseDouble(jsonObject.getString("Latitude"));
                                destinationLongitude = Double.parseDouble(jsonObject.getString("Longitude"));
                                float rate = Float.parseFloat(jsonObject.getString("Rate"));

                                Location me = new Location("");
                                Location dest = new Location("");

                                me.setLatitude(currentLatitude);
                                me.setLongitude(currentLongitude);

                                dest.setLatitude(destinationLatitude);
                                dest.setLongitude(destinationLongitude);

                                final DecimalFormat distanceFormat = new DecimalFormat("0");
                                float dist = (me.distanceTo(dest)) / 1000;
                                //                                    new getTrafficTime().execute();
                                resortDetails.setDistance(Integer.toString(Math.round(dist)));

                                try {
                                    JSONArray jsonArray12 = jsonObject.getJSONArray("Price");
                                    for (int j = 0; j < jsonArray12.length(); j++) {
                                        JSONObject obj = jsonArray12.getJSONObject(j);
                                        if (obj.has("Saturday")) {
                                            JSONObject satObj = obj.getJSONObject("Saturday");
                                            resortDetails.setSatMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            resortDetails.setSatWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                            resortDetails.setSatKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                            resortDetails.setSatFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                            if(todayWeek.equalsIgnoreCase("Saturday")) {
                                                resortDetails.setDiscountedPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            }
                                        } else if (obj.has("Sunday")) {
                                            JSONObject satObj = obj.getJSONObject("Sunday");
                                            resortDetails.setSunMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            resortDetails.setSunWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                            resortDetails.setSunKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                            resortDetails.setSunFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                            if(todayWeek.equalsIgnoreCase("Sunday")) {
                                                resortDetails.setDiscountedPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            }
                                        } else if (obj.has("Monday")) {
                                            JSONObject satObj = obj.getJSONObject("Monday");
                                            resortDetails.setMonMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            resortDetails.setMonWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                            resortDetails.setMonKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                            resortDetails.setMonFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                            if(todayWeek.equalsIgnoreCase("Monday")) {
                                                resortDetails.setDiscountedPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            }
                                        } else if (obj.has("Tuesday")) {
                                            JSONObject satObj = obj.getJSONObject("Tuesday");
                                            resortDetails.setTueMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            resortDetails.setTueWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                            resortDetails.setTueKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                            resortDetails.setTueFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                            if(todayWeek.equalsIgnoreCase("Tuesday")) {
                                                resortDetails.setDiscountedPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            }
                                        } else if (obj.has("Wednesday")) {
                                            JSONObject satObj = obj.getJSONObject("Wednesday");
                                            resortDetails.setWedMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            resortDetails.setWedWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                            resortDetails.setWedKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                            resortDetails.setWedFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                            if(todayWeek.equalsIgnoreCase("Wednesday")) {
                                                resortDetails.setDiscountedPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            }
                                        } else if (obj.has("Thursday")) {
                                            JSONObject satObj = obj.getJSONObject("Thursday");
                                            resortDetails.setThursMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            resortDetails.setThursWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                            resortDetails.setThursKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                            resortDetails.setThursFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                            if(todayWeek.equalsIgnoreCase("Thursday")) {
                                                resortDetails.setDiscountedPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            }
                                        } else if (obj.has("Friday")) {
                                            JSONObject satObj = obj.getJSONObject("Friday");
                                            resortDetails.setFriMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            resortDetails.setFriWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                            resortDetails.setFriKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                            resortDetails.setFriFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                            if(todayWeek.equalsIgnoreCase("Friday")) {
                                                resortDetails.setDiscountedPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                JSONArray jsonArray4 = jsonObject.getJSONArray("Facilities");
                                ArrayList<Facilities> resortFacilities = new ArrayList<>();
                                resortFacilities.clear();
                                for (int j = 0; j < jsonArray4.length(); j++) {
                                    Facilities facilities = new Facilities();
                                    JSONObject jsonObject1 = jsonArray4.getJSONObject(j);
                                    facilities.setFacilityName(jsonObject1.getString("FacilityName"));
                                    facilities.setFacilityNameAr(jsonObject1.getString("FacilityName_Ar"));
                                    facilities.setFacilityImage(jsonObject1.getString("FacilityImage"));
                                    resortFacilities.add(facilities);
                                }
                                resortDetails.setResortFacilities(resortFacilities);

                                favoriteResortsArrayList.add(resortDetails);
                            }
                            mAdapter = new FavResortsAdapter(FavoriteResortsActivity.this, favoriteResortsArrayList, language);
                            favListView.setAdapter(mAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    tvFavCount.setText(""+favoriteResortsArrayList.size());
                    if(favoriteResortsArrayList.size() == 0){
                        tvNoFavResorts.setVisibility(View.VISIBLE);
                        favListView.setVisibility(View.GONE);
                    }
                }
            }else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                }
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteResortsActivity.this);
            dialog = new ACProgressFlower.Builder(FavoriteResortsActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.DELETE_FAVORITE_RESORT_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            Toast.makeText(getApplicationContext(), "Deleted Successfully.", Toast.LENGTH_SHORT).show();
                            new getFavResorts().execute(Constants.FAV_RESORTS_URL+userId);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(language.equalsIgnoreCase("En")) {
                                Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }

            } else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(FavoriteResortsActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            Snap sn = new Snap();
//            sn.insertfav();
            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public static class PopupBannersSearch extends DialogFragment {

        View dialogView;
        RelativeLayout layoutMan, layoutWoman, layoutFamily, layoutNumofMen, layoutNumofWomen, layoutGuest;
        RelativeLayout layoutCheckin, layoutCheckout;
        ImageView cbMan, cbWoman, cbKitchen;
        private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
        String checkInDate, checkOutDate;
        TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
        TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
        TextView tvSearch, tvResortTypeName, tvCityName, tvNumOfDays;
        TextView tvNumOfMen, tvNumOfWomen, tvKitchen, tvnumOfGuests;
        EditText etguests, etMenCapacity, etWomenCapacity;
        Boolean isManSelected = true, isWomanSelected = false, isKitchenSelected = false;
        Date mCheckInDate = null, mCheckOutDate = null, mTodayDate = null;
        String mCheckInDateStr = null, mCheckOutDateStr = null;
        long NumofDaysStr = 0;
        int capacity = 0;
        float rate, discount;
        TextView submit;
        ImageView closeDialog;
        String strResortType, strWeeks = "", todayWeek = "";
        Boolean checkingWeekFirstTime = false;
        public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};

        AlertDialog customDialog;
        static PopupBannersSearch newInstance() {
            return new PopupBannersSearch();
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            if(language.equalsIgnoreCase("En")) {
                dialogView = inflater.inflate(R.layout.popup_banners_search, container, false);
            }
            else{
                dialogView = inflater.inflate(R.layout.popup_banners_search_ar, container, false);
            }

            tvCheckInDate = (TextView) dialogView.findViewById(R.id.tv_checkin_date);
            tvCheckOutDate = (TextView) dialogView.findViewById(R.id.tv_checkout_date);

            closeDialog = (ImageView) dialogView.findViewById(R.id.close_dialog);
            submit = (TextView) dialogView.findViewById(R.id.tv_submit);
            layoutMan = (RelativeLayout) dialogView.findViewById(R.id.layout_man);
            layoutWoman = (RelativeLayout) dialogView.findViewById(R.id.layout_woman);
            layoutFamily = (RelativeLayout) dialogView.findViewById(R.id.layout_kitchen);
            layoutNumofMen = (RelativeLayout) dialogView.findViewById(R.id.layout_NoOfMen);
            layoutNumofWomen = (RelativeLayout) dialogView.findViewById(R.id.layout_numOfWomen);
            layoutGuest = (RelativeLayout) dialogView.findViewById(R.id.layout_guests);

            layoutCheckin = (RelativeLayout) dialogView.findViewById(R.id.layout_checkin);
            layoutCheckout = (RelativeLayout) dialogView.findViewById(R.id.layout_checkout);

            cbMan = (ImageView) dialogView.findViewById(R.id.cb_man);
            cbWoman = (ImageView) dialogView.findViewById(R.id.cb_woman);
            cbKitchen = (ImageView) dialogView.findViewById(R.id.cb_kitchen);

            tvCheckInDate = (TextView) dialogView.findViewById(R.id.tv_checkin_date);
            tvCheckInMonth = (TextView) dialogView.findViewById(R.id.tv_checkin_month);
            tvCheckInWeek = (TextView) dialogView.findViewById(R.id.tv_checkin_week);

            tvCheckOutDate = (TextView) dialogView.findViewById(R.id.tv_checkout_date);
            tvCheckOutMonth = (TextView) dialogView.findViewById(R.id.tv_checkout_month);
            tvCheckOutWeek = (TextView) dialogView.findViewById(R.id.tv_checkout_week);

            etguests = (EditText) dialogView.findViewById(R.id.tv_num_of_guests);
            tvNumOfDays = (TextView) dialogView.findViewById(R.id.tv_number_of_days);
            etMenCapacity = (EditText) dialogView.findViewById(R.id.tv_num_of_men);
            etWomenCapacity = (EditText) dialogView.findViewById(R.id.tv_num_of_women);

            tvNumOfMen = (TextView) dialogView.findViewById(R.id.tvNumOfMen);
            tvNumOfWomen = (TextView) dialogView.findViewById(R.id.tvNumOfWomen);
            tvKitchen = (TextView) dialogView.findViewById(R.id.tvKitchen);
            tvnumOfGuests = (TextView) dialogView.findViewById(R.id.tvnumOfGuests);
            submit = (TextView) dialogView.findViewById(R.id.tv_search);

            if(favoriteResortsArrayList.get(bannerPos).getResortType().equalsIgnoreCase("Wedding Hall")){
                tvNumOfMen.setVisibility(View.GONE);
                tvNumOfWomen.setVisibility(View.GONE);
                tvnumOfGuests.setVisibility(View.VISIBLE);
                tvKitchen.setVisibility(View.VISIBLE);
                layoutNumofMen.setVisibility(View.GONE);
                layoutNumofWomen.setVisibility(View.GONE);
                layoutGuest.setVisibility(View.VISIBLE);
                layoutFamily.setVisibility(View.VISIBLE);
            }
            else if(favoriteResortsArrayList.get(bannerPos).getResortType().equalsIgnoreCase("Banquet Hall")){
                tvNumOfMen.setVisibility(View.VISIBLE);
                tvNumOfWomen.setVisibility(View.VISIBLE);
                tvnumOfGuests.setVisibility(View.GONE);
                tvKitchen.setVisibility(View.GONE);
                layoutNumofMen.setVisibility(View.VISIBLE);
                layoutNumofWomen.setVisibility(View.VISIBLE);
                layoutGuest.setVisibility(View.GONE);
                layoutFamily.setVisibility(View.GONE);
            }
            else if(favoriteResortsArrayList.get(bannerPos).getResortType().equalsIgnoreCase("Resort")){
                tvNumOfMen.setVisibility(View.GONE);
                tvNumOfWomen.setVisibility(View.GONE);
                tvnumOfGuests.setVisibility(View.VISIBLE);
                tvKitchen.setVisibility(View.GONE);
                layoutNumofMen.setVisibility(View.GONE);
                layoutNumofWomen.setVisibility(View.GONE);
                layoutGuest.setVisibility(View.VISIBLE);
                layoutFamily.setVisibility(View.GONE);
            }

            final Calendar c = Calendar.getInstance();
            mCheckInYear = c.get(Calendar.YEAR);
            mCheckInMonth = c.get(Calendar.MONTH);
            mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckInDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckInDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

            c.add(Calendar.DATE, 1);
            mCheckOutYear = c.get(Calendar.YEAR);
            mCheckOutMonth = c.get(Calendar.MONTH);
            mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckOutDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

            String checkInDateString = mCheckInYear+"-"+(mCheckInMonth+1)+"-"+mCheckInDay;
            String checkOutDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                mCheckInDate = sdf.parse(checkInDateString);
                mCheckOutDate = sdf.parse(checkOutDateString);
                mTodayDate = sdf.parse(checkInDateString);

                mCheckInDateStr = sdf.format(mCheckInDate);
                mCheckOutDateStr = sdf.format(mCheckOutDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
            getDates();
            getWeekDays();

            layoutCheckin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkInDatePicker();
                }
            });

            layoutCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                }
            });

            layoutMan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isManSelected) {
                        isManSelected = false;
                        cbMan.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                    }
                    else{
                        isManSelected = true;
                        cbMan.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                    }
                }
            });

            layoutWoman.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isWomanSelected) {
                        isWomanSelected = false;
                        cbWoman.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                    }
                    else{
                        isWomanSelected = true;
                        cbWoman.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                    }
                }
            });

            layoutFamily.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isKitchenSelected) {
                        isKitchenSelected = false;
                        cbKitchen.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                    }
                    else{
                        isKitchenSelected = true;
                        cbKitchen.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                    }
                }
            });

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    capacityCalculation();
                    if(!isManSelected && !isWomanSelected){
//                        String appName, title, positive;
//                        String language;
//                        SharedPreferences languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//                        language = languagePrefs.getString("language", "En");
//                        if(language.equalsIgnoreCase("En")){
//                            appName = getResources().getString(R.string.app_name);
//                            title = "Please select Gender";
//                            positive = getResources().getString(R.string.str_btn_ok);
//                        }
//                        else{
//                            appName = getResources().getString(R.string.app_name_ar);
//                            title = "يرجى تحديد الجنس";
//                            positive = getResources().getString(R.string.str_btn_ok_ar);
//                        }
//                        final iOSDialog iOSDialog = new iOSDialog(getContext());
//                        iOSDialog.setTitle(appName);
//                        iOSDialog.setSubtitle(title);
//                        iOSDialog.setPositiveLabel(positive);
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                            }
//                        });
//                        iOSDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.app_name));
                            yes.setText(getResources().getString(R.string.str_btn_ok));
                            desc.setText("Please select Gender");
                        }
                        else{
                            title.setText(getResources().getString(R.string.app_name_ar));
                            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                            desc.setText("يرجى تحديد الجنس");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getActivity().getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else if(capacity > favoriteResortsArrayList.get(bannerPos).getMaxCapacity()) {
//                        String appName, title, positive;
//                        String language;
//                        SharedPreferences languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//                        language = languagePrefs.getString("language", "En");
//                        if(language.equalsIgnoreCase("En")){
//                            appName = getResources().getString(R.string.app_name);
//                            title = "Maximum Capacity of this property is "+favoriteResortsArrayList.get(bannerPos).getMaxCapacity();
//                            positive = getResources().getString(R.string.str_btn_ok);
//                        }
//                        else{
//                            appName = getResources().getString(R.string.app_name_ar);
//                            title = favoriteResortsArrayList.get(bannerPos).getMinCapacity()+"الحد الأقصى لسعة القاعة ";
//                            positive = getResources().getString(R.string.str_btn_ok_ar);
//                        }
//                        final iOSDialog iOSDialog = new iOSDialog(getContext());
//                        iOSDialog.setTitle(appName);
//                        iOSDialog.setSubtitle(title);
//                        iOSDialog.setPositiveLabel(positive);
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                            }
//                        });
//                        iOSDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.app_name));
                            yes.setText(getResources().getString(R.string.str_btn_ok));
                            desc.setText("Maximum Capacity of this property is "+favoriteResortsArrayList.get(bannerPos).getMaxCapacity());
                        }
                        else{
                            title.setText(getResources().getString(R.string.app_name_ar));
                            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                            desc.setText(favoriteResortsArrayList.get(bannerPos).getMinCapacity()+"الحد الأقصى لسعة القاعة ");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getActivity().getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else if(capacity < favoriteResortsArrayList.get(bannerPos).getMinCapacity()) {
//                        String appName, title, positive;
//                        String language;
//                        SharedPreferences languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//                        language = languagePrefs.getString("language", "En");
//                        if(language.equalsIgnoreCase("En")){
//                            appName = getResources().getString(R.string.app_name);
//                            title = "Minimum Capacity of this property is "+favoriteResortsArrayList.get(bannerPos).getMinCapacity();
//                            positive = getResources().getString(R.string.str_btn_ok);
//                        }
//                        else{
//                            appName = getResources().getString(R.string.app_name_ar);
//                            title = favoriteResortsArrayList.get(bannerPos).getMinCapacity()+"الحد الأدنى لسعة القاعة ";
//                            positive = getResources().getString(R.string.str_btn_ok_ar);
//                        }
//                        final iOSDialog iOSDialog = new iOSDialog(getContext());
//                        iOSDialog.setTitle(appName);
//                        iOSDialog.setSubtitle(title);
//                        iOSDialog.setPositiveLabel(positive);
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                            }
//                        });
//                        iOSDialog.show();
//
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.app_name));
                            yes.setText(getResources().getString(R.string.str_btn_ok));
                            desc.setText("Minimum Capacity of this property is "+favoriteResortsArrayList.get(bannerPos).getMinCapacity());
                        }
                        else{
                            title.setText(getResources().getString(R.string.app_name_ar));
                            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                            desc.setText(favoriteResortsArrayList.get(bannerPos).getMinCapacity()+"الحد الأدنى لسعة القاعة ");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getActivity().getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else{
                        if(isKitchenSelected){
                            Constants.isKitchenSelected = 1;
                        }
                        else{
                            Constants.isKitchenSelected = 0;
                        }
                        final JSONObject parent = new JSONObject();
                        try {

                            JSONObject mainObj = new JSONObject();
                            mainObj.put("ResortTypeId", favoriteResortsArrayList.get(bannerPos).getResortTypeId());
                            mainObj.put("CityId", favoriteResortsArrayList.get(bannerPos).getCityId());
                            mainObj.put("IsMan", isManSelected);
                            mainObj.put("IsWoman", isWomanSelected);
                            mainObj.put("IsKitchen", isKitchenSelected);
                            mainObj.put("NoOfCapacity", Constants.convertToArabic(Integer.toString(capacity)));
                            mainObj.put("StartDate", Constants.convertToArabic(mCheckInDateStr));
                            mainObj.put("EndDate", Constants.convertToArabic(mCheckOutDateStr));
                            mainObj.put("ResortId", favoriteResortsArrayList.get(bannerPos).getResortId());
                            if (userId == null) {
                                mainObj.put("UserId", "-1");
                            } else {
                                mainObj.put("UserId", userId);
                            }

                            parent.put("SearchDetails", mainObj);
                            Log.i("TAG", parent.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        new PopupBannersSearch.CheckAvailablity().execute(parent.toString());
                    }
                }
            });

            closeDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });

            return dialogView;
        }

        @Override
        public void onStart() {
            super.onStart();
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(getDialog().getWindow().getAttributes());
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;;
            double d = width * 0.95;
            lp.width = (int) d;

            getDialog().getWindow().setAttributes(lp);
        }

        public void checkInDatePicker(){
            final Calendar c = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            mCheckInYear = year;
                            mCheckInDay = dayOfMonth;
                            mCheckInMonth = monthOfYear;

                            if(mCheckInDay<10) {
                                tvCheckInDate.setText("0" + mCheckInDay);
                            }
                            else{
                                tvCheckInDate.setText("" + mCheckInDay);
                            }
                            tvCheckInMonth.setText(MONTHS[mCheckInMonth]);

                            checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                        }
                    }, mCheckInYear, mCheckInMonth, mCheckInDay);

            String msg;
            if(language.equalsIgnoreCase("En")){
                msg = "Select Check In Date";
            }
            else{
                msg = "تاريخ الوصول";
            }
            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//            long max = TimeUnit.DAYS.toMillis(90);
//            datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
            datePickerDialog.setCancelable(false);
            datePickerDialog.setMessage(msg);
            datePickerDialog.show();
        }

        public void checkOutDatePicker(int year, int date, int month){
            final Calendar c = Calendar.getInstance();
            long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
            String givenDateString = year+"-"+(month)+"-"+date;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                mCheckInDate = sdf.parse(givenDateString);
                mCheckInDateStr = sdf.format(mCheckInDate);
                selectedMillis = mCheckInDate.getTime();
                c.setTime(mCheckInDate); // yourdate is an object of type Date
                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                tvCheckInWeek.setText(WEEKS[(dayOfWeek-1)]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mCheckOutYear = mCheckInYear;
            mCheckOutMonth = mCheckInMonth;
            mCheckOutDay = mCheckInDay;

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            mCheckOutYear = year;
                            mCheckOutDay = dayOfMonth;
                            mCheckOutMonth = monthOfYear;

                            if(mCheckOutDay<10) {
                                tvCheckOutDate.setText("0" + mCheckOutDay);
                            }
                            else{
                                tvCheckOutDate.setText("" + mCheckOutDay);
                            }
                            tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);

                            String givenDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                mCheckOutDate = sdf.parse(givenDateString);
                                mCheckOutDateStr = sdf.format(mCheckOutDate);
                                c.setTime(mCheckOutDate); // yourdate is an object of type Date
                                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                tvCheckOutWeek.setText(WEEKS[(dayOfWeek-1)]);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            CalculateNumOfDays();

                        }
                    }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
            String msg;
            if(language.equalsIgnoreCase("En")){
                msg = "Select Check Out Date";
            }
            else{
                msg = "تاريخ المغادرة";
            }
            datePickerDialog.getDatePicker().setMinDate(selectedMillis);
//            long max = TimeUnit.DAYS.toMillis(90);
//            datePickerDialog.getDatePicker().setMaxDate(selectedMillis+max);
            datePickerDialog.setInverseBackgroundForced(true);
            datePickerDialog.setMessage(msg);
            datePickerDialog.show();
        }

        public void CalculateNumOfDays(){
            long diff = mCheckOutDate.getTime() - mCheckInDate.getTime();
            NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))+1;
            tvNumOfDays.setText(""+NumofDaysStr);
            checkDayValidity();
            getDates();
            getWeekDays();
        }

        public void checkDayValidity(){
            long diff = mCheckInDate.getTime() - mTodayDate.getTime();
            long days = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))+1;
            if(days>=1){
                isToday = false;
            }
            else{
                isToday = false;
            }
            if(days>=3){
                ResortSearchFragment.isAfter3Days = true;
            }
            else {
                ResortSearchFragment.isAfter3Days = false;
            }
        }

        public void getDates(){
            ResortSearchFragment.strCheckInDate = tvCheckInDate.getText().toString();
            ResortSearchFragment.strCheckInMonth = tvCheckInMonth.getText().toString();
            ResortSearchFragment.strCheckInWeek = tvCheckInWeek.getText().toString();
            ResortSearchFragment.strCheckoutDate = tvCheckOutDate.getText().toString();
            ResortSearchFragment.strCheckoutMonth = tvCheckOutMonth.getText().toString();
            ResortSearchFragment.strCheckoutWeek = tvCheckOutWeek.getText().toString();
            ResortSearchFragment.strNumDays = tvNumOfDays.getText().toString();
//        strNumGuests = etguests.getText().toString();
//        Constants.noOfGuests = etguests.getText().toString();
            Constants.startDate = mCheckInDateStr;
            Constants.endDate = mCheckOutDateStr;
        }

        public void getWeekDays(){
            Calendar c1 = Calendar.getInstance();
            c1.setTime(mCheckInDate);

            Calendar c2 = Calendar.getInstance();
            c2.setTime(mCheckOutDate);

            strWeeks = "";

            while (! c1.after(c2)) {
                if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ){
                    if(strWeeks.equals("")){
                        strWeeks = "Saturday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Saturday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Saturday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Sunday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Sunday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Sunday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Monday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Monday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Monday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Tuesday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Tuesday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Tuesday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Wednesday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Wednesday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Wednesday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Thursday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Thursday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Thursday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Friday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Friday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Friday";
                    }
                }

                c1.add(Calendar.DATE, 1);
            }
        }
        public void capacityCalculation(){
            getDates();
            try {
                if (favoriteResortsArrayList.get(bannerPos).getResortType().equalsIgnoreCase("banquet hall")) {
                    if (isManSelected && isWomanSelected) {
                        capacity = Integer.parseInt(etMenCapacity.getText().toString()) + Integer.parseInt(etWomenCapacity.getText().toString());
                        Constants.noOfMen = etMenCapacity.getText().toString();
                        Constants.noOfWomen = etWomenCapacity.getText().toString();
                    } else if (isManSelected) {
                        capacity = Integer.parseInt(etMenCapacity.getText().toString());
                        Constants.noOfMen = etMenCapacity.getText().toString();
                        Constants.noOfWomen = "0";
                    } else if (isWomanSelected) {
                        capacity = Integer.parseInt(etWomenCapacity.getText().toString());
                        Constants.noOfMen = "0";
                        Constants.noOfWomen = etWomenCapacity.getText().toString();
                    }
                    Constants.noOfGuests = Integer.toString(capacity);
                } else {
                    if(isManSelected && isWomanSelected){
                        Constants.noOfMen = etguests.getText().toString();
                        Constants.noOfWomen = etguests.getText().toString();
                    }
                    else if(isWomanSelected){
                        Constants.noOfMen = "0";
                        Constants.noOfWomen = etguests.getText().toString();
                    }
                    else if(isManSelected){
                        Constants.noOfMen = etguests.getText().toString();
                        Constants.noOfWomen = "0";
                    }
                    capacity = Integer.parseInt(etguests.getText().toString());
                    Constants.noOfGuests = Integer.toString(capacity);
                }
            } catch (Exception e) {
                e.printStackTrace();
//                if(etMenCapacity.getText().toString().length() == 0 || etWomenCapacity.getText().toString().length() == 0
//                        || etguests.getText().toString().length() == 0){
//                    final iOSDialog iOSDialog = new iOSDialog(getContext());
//                    iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                    iOSDialog.setSubtitle("Please enter capacity");
//                    iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();
//                }
//                else{
//                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        public void priceCalculation(){
            final DecimalFormat priceFormat = new DecimalFormat("0");
            float price = 0;
            String[] selectedWeeks = strWeeks.split(",");

            if(!favoriteResortsArrayList.get(bannerPos).getResortType().equalsIgnoreCase("banquet hall")){
                for(int a = 0; a < selectedWeeks.length; a++){

                    if(isManSelected) {
                        if(isWomanSelected && favoriteResortsArrayList.get(bannerPos).getResortType().equalsIgnoreCase("wedding hall")){
                            try {
                                if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSatFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSunFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getMonFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getTueFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getWedFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getThursFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getFriFamilyPrice());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            try {
                                if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSatMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSunMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getMonMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getTueMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getWedMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getThursMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getFriMenPrice());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if(isWomanSelected){
                        try {
                            if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSatWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSunWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getMonWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getTueWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getWedWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getThursWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getFriWomenPrice());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                if(isKitchenSelected) {
                    price = price + Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSatKitchenPrice());
                }
            }
            else {
                if(isManSelected && isWomanSelected){
                    for(int a = 0; a < selectedWeeks.length; a++){
                        try {
                            if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSatMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSatWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSunMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSunWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getMonMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getMonWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getTueMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getTueWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getWedMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getWedWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getThursMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getThursWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getFriMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getFriWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if (isManSelected){
                    for(int a = 0; a < selectedWeeks.length; a++){
                        try {
                            if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSatMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSunMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getMonMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getTueMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getWedMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getThursMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getFriMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(isWomanSelected){
                    for(int a = 0; a < selectedWeeks.length; a++){
                        try {
                            if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSatWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getSunWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getMonWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getTueWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getWedWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getThursWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                price = price + (Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getFriWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
//            rate = Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getRate());
//            discount = Float.parseFloat(favoriteResortsArrayList.get(bannerPos).getDiscount());
            favoriteResortsArrayList.get(bannerPos).setActualPrice(priceFormat.format(price * rate));
            float discPrice;
            if(discount>0) {
                float actualPrice = price * rate;
                discPrice = actualPrice - (actualPrice * (discount/100));
                favoriteResortsArrayList.get(bannerPos).setDiscountedPrice(priceFormat.format(discPrice));
            }
            else{
                favoriteResortsArrayList.get(bannerPos).setDiscountedPrice(priceFormat.format(price * rate));
            }
        }

        public class CheckAvailablity extends AsyncTask<String, Integer, String> {
            ProgressDialog pDialog;
            String networkStatus;
            ACProgressFlower dialog;
            String response;

            InputStream inputStream = null;
            @Override
            protected void onPreExecute() {
                networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                dialog = new ACProgressFlower.Builder(getContext())
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY).build();
                dialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    try {
                        try {
                            // 1. create HttpClient
                            HttpClient httpclient = new DefaultHttpClient();

                            // 2. make POST request to the given URL
                            HttpPost httpPost = new HttpPost(Constants.CHECK_AVAILABILITY_URL);

                            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                            // ObjectMapper mapper = new ObjectMapper();
                            // json = mapper.writeValueAsString(person);

                            // 5. set json to StringEntity
                            StringEntity se = new StringEntity(params[0], "UTF-8");

                            // 6. set httpPost Entity
                            httpPost.setEntity(se);

                            // 7. Set some headers to inform server about the type of the content
                            httpPost.setHeader("Accept", "application/json");
                            httpPost.setHeader("Content-type", "application/json");

                            // 8. Execute POST request to the given URL
                            HttpResponse httpResponse = httpclient.execute(httpPost);

                            // 9. receive response as inputStream
                            inputStream = httpResponse.getEntity().getContent();

                            // 10. convert inputstream to string
                            if(inputStream != null) {
                                response = convertInputStreamToString(inputStream);
                                Log.i("TAGs", "user response:" + response);
                                return response;
                            }
                        } catch (Exception e) {
                            Log.d("InputStream", e.getLocalizedMessage());
                        }
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    Log.i("TAG", "user response:" + response);
                    return response;
                }else {
                    return "no internet";
                }
            }


            @Override
            protected void onPostExecute(String result) {

                if (result != null) {
                    if(result.equalsIgnoreCase("no internet")) {
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        if(result.equals("")){
                            Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        }else {

                            try {
                                JSONObject jo= new JSONObject(result);
                                JSONArray jsonArray = jo.getJSONArray("success");
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String success = jsonObject.getString("IsAvailable");

                                if(success.equalsIgnoreCase("true")) {
                                    rate = Float.parseFloat(jsonObject.getString("Rate"));
                                    discount = Float.parseFloat(jsonObject.getString("Discount"));
                                    priceCalculation();
                                    Intent intent = new Intent(getActivity(), ResortDetailsActivity.class);
                                    intent.putExtra("array", favoriteResortsArrayList);
                                    intent.putExtra("position", bannerPos);
                                    intent.putExtra("class", "home");
                                    startActivity(intent);
                                    getDialog().dismiss();
                                }
                                else if (success.equalsIgnoreCase("false")){
//                                    final iOSDialog iOSDialog = new iOSDialog(getContext());
//                                    String appName, title, positive;
//                                    if(language.equalsIgnoreCase("En")){
//                                        appName = getResources().getString(R.string.app_name);
//                                        title = "Property is not available for these dates.";
//                                        positive = getResources().getString(R.string.str_btn_ok);
//                                    }
//                                    else{
//                                        appName = getResources().getString(R.string.app_name_ar);
//                                        title = "القاعة غير متوفرة في هذا التاريخ";
//                                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                                    }
//                                    iOSDialog.setTitle(appName);
//                                    iOSDialog.setSubtitle(title);
//                                    iOSDialog.setPositiveLabel(positive);
//                                    iOSDialog.setBoldPositiveLabel(false);
//                                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            iOSDialog.dismiss();
//                                        }
//                                    });
//                                    iOSDialog.show();

                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                                    no.setVisibility(View.GONE);
                                    vert.setVisibility(View.GONE);

                                    if(language.equalsIgnoreCase("En")) {
                                        title.setText(getResources().getString(R.string.app_name));
                                        yes.setText(getResources().getString(R.string.str_btn_ok));
                                        desc.setText("Property is not available for these dates.");
                                    }
                                    else{
                                        title.setText(getResources().getString(R.string.app_name_ar));
                                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                        desc.setText("القاعة غير متوفرة في هذا التاريخ");
                                    }

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth*0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                }else {
                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                }
                if(dialog != null) {
                    dialog.dismiss();
                }

                super.onPostExecute(result);

            }

        }

        private static String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
    }

    public void loadPopup(){
        final PopupBannersSearch newFragment = PopupBannersSearch.newInstance();
        newFragment.show(getSupportFragmentManager(), "dialog");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed
                if(newFragment!=null){
                    newFragment.dismiss();
                }
            }
        });
    }
}
