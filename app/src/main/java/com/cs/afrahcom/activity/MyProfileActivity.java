package com.cs.afrahcom.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 22-11-2017.
 */

public class MyProfileActivity extends AppCompatActivity {

    private EditText etFirstName, etMiddleName, etCity, etEmail, etMobile;
    TextView mEditProfile, mChangePassword;
    String userId;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;
    String language;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_my_profile);
        }
        else{
            setContentView(R.layout.activity_my_profile_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etMiddleName = (EditText) findViewById(R.id.et_middle_name);
        etCity = (EditText) findViewById(R.id.et_city);
        etEmail = (EditText) findViewById(R.id.et_email);
        etMobile = (EditText) findViewById(R.id.et_mobile);

        mEditProfile = (TextView) findViewById(R.id.editprofile);
        mChangePassword = (TextView) findViewById(R.id.changePassword);

        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyProfileActivity.this, EditProfileActivity.class));
            }
        });

        mChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyProfileActivity.this, ChangePasswordActivity.class));
            }
        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String response = userPrefs.getString("user_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                etFirstName.setText(userObjuect.getString("fullName"));
                etMobile.setText("+"+userObjuect.getString("mobile"));
                etEmail.setText(userObjuect.getString("email"));
                if(userObjuect.getString("MiddleName").equals("null")){
                    etMiddleName.setText("");
                }
                else{
                    etMiddleName.setText(userObjuect.getString("MiddleName"));
                }

                if(userObjuect.getString("CityId").equals("0") || userObjuect.getString("CityId").equalsIgnoreCase("null")){
                    etCity.setText("");
                }
                else{
                    int mCityPos = Integer.parseInt(userObjuect.getString("CityId"));
                    mCityPos = mCityPos - 1;
                    etCity.setText(Constants.CityArrayList.get(mCityPos).getCityName());
                }
            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }
}
