package com.cs.afrahcom.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.ResortDetails;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 18-12-2017.
 */

public class OfflineBookingActivity extends AppCompatActivity {

    TextView tvResortName, submit;
    ArrayList<ResortDetails> resortDetailsArrayList = new ArrayList<>();
    private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
    TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
    TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
    RelativeLayout layoutCheckin, layoutCheckout;
    Date mCheckInDate = null, mCheckOutDate = null, mTodayDate = null;
    String mCheckInDateStr = null, mCheckOutDateStr = null;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
    String hostId;
    SharedPreferences hostPrefs;
    SharedPreferences languagePrefs;
    String language;
    Boolean isEdit;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_offline_booking);
        }
        else{
            setContentView(R.layout.activity_offline_booking);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostId = hostPrefs.getString("hostId", "0");

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        resortDetailsArrayList = (ArrayList<ResortDetails>) getIntent().getSerializableExtra("array");
        isEdit = getIntent().getBooleanExtra("edit", false);

        tvCheckInDate = (TextView) findViewById(R.id.tv_checkin_date);
        tvCheckInMonth = (TextView) findViewById(R.id.tv_checkin_month);
        tvCheckInWeek = (TextView) findViewById(R.id.tv_checkin_week);

        tvCheckOutDate = (TextView) findViewById(R.id.tv_checkout_date);
        tvCheckOutMonth = (TextView) findViewById(R.id.tv_checkout_month);
        tvCheckOutWeek = (TextView) findViewById(R.id.tv_checkout_week);

        layoutCheckin = (RelativeLayout) findViewById(R.id.layout_checkin);
        layoutCheckout = (RelativeLayout) findViewById(R.id.layout_checkout);

        submit = (TextView) findViewById(R.id.tv_submit);
        tvResortName = (TextView) findViewById(R.id.resort_type_name);

        tvResortName.setText(resortDetailsArrayList.get(0).getResortName());

        final Calendar c = Calendar.getInstance();
        mCheckInYear = c.get(Calendar.YEAR);
        mCheckInMonth = c.get(Calendar.MONTH);
        mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

        if((c.get(Calendar.DATE)) <10) {
            tvCheckInDate.setText("0" + c.get(Calendar.DATE));
        }
        else{
            tvCheckInDate.setText("" + c.get(Calendar.DATE));
        }
        tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

        c.add(Calendar.DATE, 1);
        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

        if((c.get(Calendar.DATE)) <10) {
            tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
        }
        else{
            tvCheckOutDate.setText("" + c.get(Calendar.DATE));
        }
        tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

        String checkInDateString = mCheckInYear+"-"+(mCheckInMonth+1)+"-"+mCheckInDay;
        String checkOutDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(checkInDateString);
            mCheckOutDate = sdf.parse(checkOutDateString);
            mTodayDate = sdf.parse(checkInDateString);

            mCheckInDateStr = sdf.format(mCheckInDate);
            mCheckOutDateStr = sdf.format(mCheckOutDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        layoutCheckin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInDatePicker();
            }
        });

        layoutCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject parent = new JSONObject();
                try {
//                        Setting Reservation Date
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                    String currentTime = df3.format(c.getTime());

//                        Insert order JSON preparation
                    JSONObject mainObj = new JSONObject();
                    mainObj.put("StartDate", mCheckInDateStr);
                    mainObj.put("EndDate", mCheckOutDateStr);
                    mainObj.put("UserId", "");
                    mainObj.put("ResortId", resortDetailsArrayList.get(0).getResortId());
                    mainObj.put("BookingType", "3");
                    mainObj.put("Comments", "offline booking");
                    mainObj.put("PaymentMode", "");
                    mainObj.put("TotalPrice", "");
                    mainObj.put("AdvancePrice", "");
                    mainObj.put("OrderStatus", "offline");
                    mainObj.put("DeviceToken", "");
                    mainObj.put("ReservationDate", currentTime);
                    mainObj.put("NoOfMen", "");
                    mainObj.put("NoOfWomen", "");
                    mainObj.put("UserName", "");
                    mainObj.put("MobileNo", "");
                    mainObj.put("IsCateringsSelected", "");
                    mainObj.put("ResortTypeId", Constants.resortTypeId);
                    mainObj.put("PaymentStatus", "");
                    parent.put("insertdetails", mainObj);

//                        Empty JSON object for Inserting in Tracking table
                    JSONObject emptyObj = new JSONObject();
                    parent.put("inserttracking", emptyObj);
                    Log.i("TAG", parent.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new InsertOrder().execute(parent.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void checkInDatePicker(){
        final Calendar c = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(OfflineBookingActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckInYear = year;
                        mCheckInDay = dayOfMonth;
                        mCheckInMonth = monthOfYear;

                        if(mCheckInDay<10) {
                            tvCheckInDate.setText("0" + mCheckInDay);
                        }
                        else{
                            tvCheckInDate.setText("" + mCheckInDay);
                        }
                        tvCheckInMonth.setText(MONTHS[mCheckInMonth]);

                        checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                    }
                }, mCheckInYear, mCheckInMonth, mCheckInDay);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
        datePickerDialog.setCancelable(false);
        datePickerDialog.setMessage("Select Start Date");
        datePickerDialog.show();

    }

    public void checkOutDatePicker(int year, int date, int month){
        final Calendar c = Calendar.getInstance();
        long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
        String givenDateString = year+"-"+(month)+"-"+date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(givenDateString);
            mCheckInDateStr = sdf.format(mCheckInDate);
            selectedMillis = mCheckInDate.getTime();
            c.setTime(mCheckInDate); // yourdate is an object of type Date
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            tvCheckInWeek.setText(WEEKS[(dayOfWeek-1)]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mCheckOutYear = mCheckInYear;
        mCheckOutMonth = mCheckInMonth;
        mCheckOutDay = mCheckInDay;

        DatePickerDialog datePickerDialog = new DatePickerDialog(OfflineBookingActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckOutYear = year;
                        mCheckOutDay = dayOfMonth;
                        mCheckOutMonth = monthOfYear;

                        if(mCheckOutDay<10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay);
                        }
                        else{
                            tvCheckOutDate.setText("" + mCheckOutDay);
                        }
                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);

                        String givenDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf.parse(givenDateString);
                            mCheckOutDateStr = sdf.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek-1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
        datePickerDialog.getDatePicker().setMinDate(selectedMillis);
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(selectedMillis+max);
        datePickerDialog.setInverseBackgroundForced(true);
        datePickerDialog.setMessage("Select End Date");
        datePickerDialog.show();
    }

    public class InsertOrder extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(OfflineBookingActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            JSONArray successObj = jo.getJSONArray("success");
                            JSONObject bookingObj = successObj.getJSONObject(0);
                            String bookingID = bookingObj.getString("BookingId");
                            Toast.makeText(getApplicationContext(), "Booking Successful", Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            } else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
