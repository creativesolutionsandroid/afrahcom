package com.cs.afrahcom.activity;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.cs.afrahcom.GPSTracker;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.FacilitiesFilterAdapter;
import com.cs.afrahcom.adapter.SearchResultsAdapter;
import com.cs.afrahcom.fragment.ResortSearchFragment;
import com.cs.afrahcom.models.ResortDetails;
import com.cs.afrahcom.models.ResortDetailsFiltered;
import com.cs.afrahcom.widgets.CustomGridView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by CS on 26-09-2017.
 */

public class SearchResultActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    Toolbar toolbar;
    ArrayList<ResortDetails> resortDetailsArrayList = new ArrayList<>();
    ArrayList<ResortDetailsFiltered> filteredResortDetailsArrayList = new ArrayList<>();

    Boolean isMapVisible = false;
    TextView tvMap;
    public static TextView tvResultsCount, tvNoResults;
    LinearLayout  mFilterLayout, mSortLayout;
    RelativeLayout mapFragment, mMapLayout;
    ImageView imgMapIcon;
    ListView lvResorts;
    SearchResultsAdapter searchResultsAdapter;
    private GoogleMap map;
    MarkerOptions markerOptions;
    GPSTracker gps;
    private double lat, longi;
    String strResorttype;
    public static Boolean isGPSRefreshed = false;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;
    AlertDialog alertDialog2;
    int minimumPrice, maximumPrice, finalMinPrice, finalMaxPrice, finalMinDistance = 0, finalMaxDistance = 100;
    Boolean isFiltersSet = false;
    Boolean isSortingSet = false;
    Boolean isFirstTimeSettingFilters = true;
    int ratingSelected = 0;
    public static ArrayList<String> facilitiesSelected = new ArrayList<>();
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_resort_search_results);
        }
        else{
            setContentView(R.layout.activity_resort_search_results_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            facilitiesSelected.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        resortDetailsArrayList = (ArrayList<ResortDetails>) getIntent().getSerializableExtra("array");
        minimumPrice = getIntent().getIntExtra("min",0);
        maximumPrice = getIntent().getIntExtra("max",0);
        strResorttype = getIntent().getStringExtra("resort");

        tvNoResults = (TextView) findViewById(R.id.tv_noresults);
        tvResultsCount = (TextView) findViewById(R.id.search_result_count);
        tvMap = (TextView) findViewById(R.id.tv_map);

        imgMapIcon = (ImageView) findViewById(R.id.img_map_icon) ;

        mMapLayout = (RelativeLayout) findViewById(R.id.map_layout);
        mFilterLayout = (LinearLayout) findViewById(R.id.filter_layout);
        mSortLayout = (LinearLayout) findViewById(R.id.sort_layout);
        mapFragment = (RelativeLayout) findViewById(R.id.map_fragment);

        lvResorts = (ListView) findViewById(R.id.resorts_listview);
        searchResultsAdapter = new SearchResultsAdapter(SearchResultActivity.this, filteredResortDetailsArrayList, SearchResultActivity.this, language);
        lvResorts.setAdapter(searchResultsAdapter);
        resetDetails();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(SearchResultActivity.this);

        tvResultsCount.setText(""+filteredResortDetailsArrayList.size());

        if(resortDetailsArrayList.size() == 0){
            tvNoResults.setVisibility(View.VISIBLE);
        }
        else{
            tvNoResults.setVisibility(View.GONE);
        }

//        lvResorts.addOnItemTouchListener(
//                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//                        // TODO Handle item click
//                    }
//                })
//        );

        lvResorts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(SearchResultActivity.this, ResortDetailsActivity.class);
                intent.putExtra("array",filteredResortDetailsArrayList);
                intent.putExtra("position",i);
                intent.putExtra("class","search");
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(SearchResultActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                startActivity(intent, options.toBundle());
            }
        });

        mMapLayout.setOnClickListener(this);
        mFilterLayout.setOnClickListener(this);
        mSortLayout.setOnClickListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.map_layout:
                isSortingSet = false;
                resetDetails();
                mMapLayout.setBackgroundColor(getResources().getColor(R.color.filterSelected));
                mFilterLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));
                mSortLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));
                if(!isMapVisible) {
                    mapFragment.setVisibility(View.VISIBLE);
                    lvResorts.setVisibility(View.GONE);
                    isMapVisible = true;
                    if(language.equalsIgnoreCase("En")) {
                        tvMap.setText("List");
                    }
                    else{
                        tvMap.setText("قائمة");
                    }
                    imgMapIcon.setImageDrawable(getResources().getDrawable(R.drawable.list));
                }
                else{
                    mapFragment.setVisibility(View.GONE);
                    lvResorts.setVisibility(View.VISIBLE);
                    isMapVisible = false;
                    if(language.equalsIgnoreCase("En")) {
                        tvMap.setText("Map");
                    }
                    else{
                        tvMap.setText("الخريطة");
                    }
                    imgMapIcon.setImageDrawable(getResources().getDrawable(R.drawable.map));
                }

                break;

            case R.id.filter_layout:
                isSortingSet = false;
                showFiltersDialog();
                break;

            case R.id.sort_layout:
                if(!isSortingSet) {
                    isSortingSet = true;
                    resetDetails();
                    mMapLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));
                    mFilterLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));
                    mSortLayout.setBackgroundColor(getResources().getColor(R.color.filterSelected));

                    mapFragment.setVisibility(View.GONE);
                    lvResorts.setVisibility(View.VISIBLE);
                    isMapVisible = false;
                    if(language.equalsIgnoreCase("En")) {
                        tvMap.setText("Map");
                    }
                    else{
                        tvMap.setText("الخريطة");
                    }
                    imgMapIcon.setImageDrawable(getResources().getDrawable(R.drawable.map));
                    Collections.sort(filteredResortDetailsArrayList, ResortDetailsFiltered.priceSort);
                    Collections.reverse(filteredResortDetailsArrayList);
                    searchResultsAdapter.notifyDataSetChanged();
                }
                else{
                    isSortingSet = false;
                    resetDetails();
                    Collections.sort(filteredResortDetailsArrayList, ResortDetailsFiltered.priceSort);
                    searchResultsAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(SearchResultActivity.this, perm));
    }


    public void getGPSCoordinates(){
//        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(
//                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
        if(gps != null){
            if (gps.canGetLocation()) {

                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);

                // Show the current location in Google Map
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                // Zoom in the Google Map
                map.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
                markerOptions = new MarkerOptions();
//                new GetCurrentTime().execute();
                // \n is for new line
//            Toast.makeText(getActivity(), "Your Location is - \nLat: " + lat + "\nLong: " + longi, Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(SearchResultActivity.this);
                    try {
                        map.setMyLocationEnabled(true);
                    }catch (NullPointerException npe){

                    }
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(SearchResultActivity.this, "Location permission denied, Unable to show nearby resorts", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                gps = new GPSTracker(SearchResultActivity.this);
                try {
                    map.setMyLocationEnabled(true);
                }catch (NullPointerException npe){

                }
                getGPSCoordinates();
            }
        }else {
            gps = new GPSTracker(SearchResultActivity.this);
            try {
                map.setMyLocationEnabled(true);
            }catch (NullPointerException npe){

            }
            getGPSCoordinates();
        }

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(SearchResultActivity.this, ResortDetailsActivity.class);
                intent.putExtra("array",filteredResortDetailsArrayList);
                intent.putExtra("class","map");
                intent.putExtra("title",marker.getTitle());
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(SearchResultActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                startActivity(intent, options.toBundle());
            }
        });

        markerOptions = new MarkerOptions();
        map.clear();

        for (int i = 0; i<filteredResortDetailsArrayList.size(); i++) {
            markerOptions.position(new LatLng(Double.parseDouble(filteredResortDetailsArrayList.get(i).getLatitude()), Double.parseDouble(filteredResortDetailsArrayList.get(i).getLongitude())))
                    .title(filteredResortDetailsArrayList.get(i).getResortName())
                    .snippet(filteredResortDetailsArrayList.get(i).getResortAddress()+","+filteredResortDetailsArrayList.get(i).getCity())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
            map.addMarker(markerOptions);
        }

        try {
            LatLng latLng = new LatLng(Double.parseDouble(filteredResortDetailsArrayList.get(0).getLatitude()), Double.parseDouble(filteredResortDetailsArrayList.get(0).getLongitude()));
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            map.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showFiltersDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SearchResultActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if(language.equalsIgnoreCase("En")) {
            layout = R.layout.activity_filter;
        }
        else{
            layout = R.layout.activity_filter_ar;
        }

        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        ScrollView scrollView = (ScrollView) dialogView.findViewById(R.id.scrollView);
        TextView applyFilters = (TextView) dialogView.findViewById(R.id.apply_filters);
        ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancel_action);
        TextView reset = (TextView) dialogView.findViewById(R.id.tv_reset);
        final TextView star1 = (TextView) dialogView.findViewById(R.id.star1);
        final TextView star2 = (TextView) dialogView.findViewById(R.id.star2);
        final TextView star3 = (TextView) dialogView.findViewById(R.id.star3);
        final TextView star4 = (TextView) dialogView.findViewById(R.id.star4);
        final TextView star5 = (TextView) dialogView.findViewById(R.id.star5);

        final CrystalRangeSeekbar pricerangeSeekbar = (CrystalRangeSeekbar) dialogView.findViewById(R.id.priceSeekBar);
        final CrystalRangeSeekbar distancerangeSeekbar = (CrystalRangeSeekbar) dialogView.findViewById(R.id.distanceSeekBar);

        // get min and max text view
        final TextView tvMin = (TextView) dialogView.findViewById(R.id.textMin1);
        final TextView tvMax = (TextView) dialogView.findViewById(R.id.textMax1);
        final TextView minDistance = (TextView) dialogView.findViewById(R.id.textMinDistance);
        final TextView maxDistance = (TextView) dialogView.findViewById(R.id.textMaxDistance);

        final CustomGridView mFacilitiesGrid = (CustomGridView) dialogView.findViewById(R.id.facilitiesGridView);
        final FacilitiesFilterAdapter filterAdapter = new FacilitiesFilterAdapter(SearchResultActivity.this, ResortSearchFragment.facilitiesArray, language);
        mFacilitiesGrid.setAdapter(filterAdapter);
        if(language.equalsIgnoreCase("Ar")) {
            mFacilitiesGrid.setRotationY(180);
        }

//        isFirstTimeSettingValue = true;
        if(isFiltersSet) {
            pricerangeSeekbar.setMinValue(minimumPrice);
            pricerangeSeekbar.setMaxValue(maximumPrice);
            distancerangeSeekbar.setMinValue(0);
            distancerangeSeekbar.setMaxValue(100);
            pricerangeSeekbar.setMinStartValue(finalMinPrice);
            pricerangeSeekbar.setMaxStartValue(finalMaxPrice);
        }
        else{
            pricerangeSeekbar.setMinValue(minimumPrice);
            pricerangeSeekbar.setMaxValue(maximumPrice);
            distancerangeSeekbar.setMinValue(0);
            distancerangeSeekbar.setMaxValue(100);
        }

        // set listener
        pricerangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                    tvMin.setText(String.valueOf(minValue));
                    tvMax.setText(String.valueOf(maxValue));
            }
        });

        distancerangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minDistance.setText(String.valueOf(minValue));
                maxDistance.setText(String.valueOf(maxValue));
            }
        });

        if(ratingSelected == 1){
            star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star1.setTextColor(getResources().getColor(R.color.white));
            scrollView.fullScroll(ScrollView.FOCUS_UP);
        }
        else if(ratingSelected == 2){
            star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star2.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star3.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
            star4.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
            star5.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
            star1.setTextColor(getResources().getColor(R.color.white));
            star2.setTextColor(getResources().getColor(R.color.white));
            star3.setTextColor(getResources().getColor(R.color.colorPrimary));
            star4.setTextColor(getResources().getColor(R.color.colorPrimary));
            star5.setTextColor(getResources().getColor(R.color.colorPrimary));
            scrollView.fullScroll(ScrollView.FOCUS_UP);
        }
        else if(ratingSelected == 3){
            star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star2.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star3.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star4.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
            star5.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
            star1.setTextColor(getResources().getColor(R.color.white));
            star2.setTextColor(getResources().getColor(R.color.white));
            star3.setTextColor(getResources().getColor(R.color.white));
            star4.setTextColor(getResources().getColor(R.color.colorPrimary));
            star5.setTextColor(getResources().getColor(R.color.colorPrimary));
            scrollView.fullScroll(ScrollView.FOCUS_UP);
        }
        else if(ratingSelected == 4){
            star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star2.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star3.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star4.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star5.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
            star1.setTextColor(getResources().getColor(R.color.white));
            star2.setTextColor(getResources().getColor(R.color.white));
            star3.setTextColor(getResources().getColor(R.color.white));
            star4.setTextColor(getResources().getColor(R.color.white));
            star5.setTextColor(getResources().getColor(R.color.colorPrimary));
            scrollView.fullScroll(ScrollView.FOCUS_UP);
        }
        else if(ratingSelected == 5){
            star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star2.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star3.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star4.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star5.setBackground(getResources().getDrawable(R.drawable.rating_selected));
            star1.setTextColor(getResources().getColor(R.color.white));
            star2.setTextColor(getResources().getColor(R.color.white));
            star3.setTextColor(getResources().getColor(R.color.white));
            star4.setTextColor(getResources().getColor(R.color.white));
            star5.setTextColor(getResources().getColor(R.color.white));
            scrollView.fullScroll(ScrollView.FOCUS_UP);
        }
        else{
            scrollView.fullScroll(ScrollView.FOCUS_UP);
        }

        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star2.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star3.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star4.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star5.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star1.setTextColor(getResources().getColor(R.color.white));
                star2.setTextColor(getResources().getColor(R.color.colorPrimary));
                star3.setTextColor(getResources().getColor(R.color.colorPrimary));
                star4.setTextColor(getResources().getColor(R.color.colorPrimary));
                star5.setTextColor(getResources().getColor(R.color.colorPrimary));
                ratingSelected = 1;
            }
        });

        star2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star2.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star3.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star4.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star5.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star1.setTextColor(getResources().getColor(R.color.white));
                star2.setTextColor(getResources().getColor(R.color.white));
                star3.setTextColor(getResources().getColor(R.color.colorPrimary));
                star4.setTextColor(getResources().getColor(R.color.colorPrimary));
                star5.setTextColor(getResources().getColor(R.color.colorPrimary));
                ratingSelected = 2;
            }
        });

        star3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star2.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star3.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star4.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star5.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star1.setTextColor(getResources().getColor(R.color.white));
                star2.setTextColor(getResources().getColor(R.color.white));
                star3.setTextColor(getResources().getColor(R.color.white));
                star4.setTextColor(getResources().getColor(R.color.colorPrimary));
                star5.setTextColor(getResources().getColor(R.color.colorPrimary));
                ratingSelected = 3;
            }
        });

        star4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star2.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star3.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star4.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star5.setBackground(getResources().getDrawable(R.drawable.rating_unselected));
                star1.setTextColor(getResources().getColor(R.color.white));
                star2.setTextColor(getResources().getColor(R.color.white));
                star3.setTextColor(getResources().getColor(R.color.white));
                star4.setTextColor(getResources().getColor(R.color.white));
                star5.setTextColor(getResources().getColor(R.color.colorPrimary));
                ratingSelected = 4;
            }
        });

        star5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                star1.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star2.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star3.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star4.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star5.setBackground(getResources().getDrawable(R.drawable.rating_selected));
                star1.setTextColor(getResources().getColor(R.color.white));
                star2.setTextColor(getResources().getColor(R.color.white));
                star3.setTextColor(getResources().getColor(R.color.white));
                star4.setTextColor(getResources().getColor(R.color.white));
                star5.setTextColor(getResources().getColor(R.color.white));
                ratingSelected = 5;
            }
        });

        mFacilitiesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(facilitiesSelected.contains(ResortSearchFragment.facilitiesArray.get(i))){
                    facilitiesSelected.remove(ResortSearchFragment.facilitiesArray.get(i));
                }
                else{
                    facilitiesSelected.add(ResortSearchFragment.facilitiesArray.get(i));
                }
                filterAdapter.notifyDataSetChanged();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog2.dismiss();
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mMapLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));
                mFilterLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));
                mSortLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));
                ratingSelected = 0;
                mapFragment.setVisibility(View.GONE);
                lvResorts.setVisibility(View.VISIBLE);
                alertDialog2.dismiss();
                resetDetails();
            }
        });
        applyFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filteredResortDetailsArrayList.clear();
                for (int i = 0; i<resortDetailsArrayList.size(); i++){
                    final DecimalFormat priceFormat = new DecimalFormat("0");
                    int resortPrice = Integer.parseInt(priceFormat.format(Float.parseFloat(resortDetailsArrayList.get(i).getActualPrice())));
                    int resortDistance = Integer.parseInt(priceFormat.format(Float.parseFloat(resortDetailsArrayList.get(i).getDistance())));
                    int resortRating = Integer.parseInt(resortDetailsArrayList.get(i).getRating());

                    if(resortRating == 0){
                        resortRating = 5;
                    }
                    int ratingInt = 0;
                    if(ratingSelected == 0){
                        ratingInt = 5;
                    }
                    else{
                        ratingInt = ratingSelected;
                    }

                    finalMinPrice = Integer.parseInt(tvMin.getText().toString());
                    finalMaxPrice = Integer.parseInt(tvMax.getText().toString());
                    finalMinDistance = Integer.parseInt(minDistance.getText().toString());
                    finalMaxDistance = Integer.parseInt(maxDistance.getText().toString());
                    isFiltersSet = true;

                    if(resortPrice >= finalMinPrice && resortPrice <= finalMaxPrice && resortDistance >= finalMinDistance && resortDistance <= finalMaxDistance && resortRating <= ratingInt){
                        if(facilitiesSelected.size()>0){
                            Boolean isloopBreak = false;
                            for(int j = 0; j<facilitiesSelected.size(); j++){
                                Boolean isFacilityAvailable = false;
                                for (int k = 0; k<resortDetailsArrayList.get(i).getResortFacilities().size(); k++){
                                        if(resortDetailsArrayList.get(i).getResortFacilities().get(k).getFacilityName().equalsIgnoreCase(facilitiesSelected.get(j))){
                                            isFacilityAvailable = true;
                                        }
                                }
                                if(!isFacilityAvailable){
                                    isloopBreak = true;
                                    break;
                                }
                            }

                            if(!isloopBreak) {
                                ResortDetailsFiltered resortDetailsFiltered = new ResortDetailsFiltered();
                                resortDetailsFiltered.setResortId(resortDetailsArrayList.get(i).getResortId());
                                resortDetailsFiltered.setResortName(resortDetailsArrayList.get(i).getResortName());
                                resortDetailsFiltered.setResortNameAr(resortDetailsArrayList.get(i).getResortNameAr());
                                resortDetailsFiltered.setCity(resortDetailsArrayList.get(i).getCity());
                                resortDetailsFiltered.setResortAddress(resortDetailsArrayList.get(i).getResortAddress());
                                resortDetailsFiltered.setResortType(resortDetailsArrayList.get(i).getResortType());
                                resortDetailsFiltered.setResortImagesArray(resortDetailsArrayList.get(i).getResortImagesArray());
                                resortDetailsFiltered.setActualPrice(resortDetailsArrayList.get(i).getActualPrice());
                                resortDetailsFiltered.setDiscountedPrice(resortDetailsArrayList.get(i).getDiscountedPrice());
                                resortDetailsFiltered.setLatitude(resortDetailsArrayList.get(i).getLatitude());
                                resortDetailsFiltered.setLongitude(resortDetailsArrayList.get(i).getLongitude());
                                resortDetailsFiltered.setRating(resortDetailsArrayList.get(i).getRating());
                                resortDetailsFiltered.setIsFavorite(resortDetailsArrayList.get(i).getIsFavorite());
                                resortDetailsFiltered.setApproved(resortDetailsArrayList.get(i).getApproved());
                                resortDetailsFiltered.setDistance(resortDetailsArrayList.get(i).getDistance());
                                resortDetailsFiltered.setResortFacilities(resortDetailsArrayList.get(i).getResortFacilities());
                                resortDetailsFiltered.setDiscount(resortDetailsArrayList.get(i).getDiscount());
                                resortDetailsFiltered.setDesc(resortDetailsArrayList.get(i).getDescription());
                                resortDetailsFiltered.setDescAr(resortDetailsArrayList.get(i).getDescriptionAr());
                                resortDetailsFiltered.setAfrahcomAdvance(resortDetailsArrayList.get(i).getAdvanceAmount());
                                resortDetailsFiltered.setVatAvailability(resortDetailsArrayList.get(i).getVatAvailability());
                                filteredResortDetailsArrayList.add(resortDetailsFiltered);
                            }
                        }
                        else {
                            ResortDetailsFiltered resortDetailsFiltered = new ResortDetailsFiltered();
                            resortDetailsFiltered.setResortId(resortDetailsArrayList.get(i).getResortId());
                            resortDetailsFiltered.setResortName(resortDetailsArrayList.get(i).getResortName());
                            resortDetailsFiltered.setResortNameAr(resortDetailsArrayList.get(i).getResortNameAr());
                            resortDetailsFiltered.setCity(resortDetailsArrayList.get(i).getCity());
                            resortDetailsFiltered.setAfrahcomAdvance(resortDetailsArrayList.get(i).getAdvanceAmount());
                            resortDetailsFiltered.setResortAddress(resortDetailsArrayList.get(i).getResortAddress());
                            resortDetailsFiltered.setResortType(resortDetailsArrayList.get(i).getResortType());
                            resortDetailsFiltered.setResortImagesArray(resortDetailsArrayList.get(i).getResortImagesArray());
                            resortDetailsFiltered.setActualPrice(resortDetailsArrayList.get(i).getActualPrice());
                            resortDetailsFiltered.setDiscountedPrice(resortDetailsArrayList.get(i).getDiscountedPrice());
                            resortDetailsFiltered.setLatitude(resortDetailsArrayList.get(i).getLatitude());
                            resortDetailsFiltered.setLongitude(resortDetailsArrayList.get(i).getLongitude());
                            resortDetailsFiltered.setRating(resortDetailsArrayList.get(i).getRating());
                            resortDetailsFiltered.setIsFavorite(resortDetailsArrayList.get(i).getIsFavorite());
                            resortDetailsFiltered.setApproved(resortDetailsArrayList.get(i).getApproved());
                            resortDetailsFiltered.setDistance(resortDetailsArrayList.get(i).getDistance());
                            resortDetailsFiltered.setResortFacilities(resortDetailsArrayList.get(i).getResortFacilities());
                            resortDetailsFiltered.setDiscount(resortDetailsArrayList.get(i).getDiscount());
                            resortDetailsFiltered.setDesc(resortDetailsArrayList.get(i).getDescription());
                            resortDetailsFiltered.setDescAr(resortDetailsArrayList.get(i).getDescriptionAr());
                            resortDetailsFiltered.setVatAvailability(resortDetailsArrayList.get(i).getVatAvailability());
                            filteredResortDetailsArrayList.add(resortDetailsFiltered);
                        }
                    }
                }
                mMapLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));
                mFilterLayout.setBackgroundColor(getResources().getColor(R.color.filterSelected));
                mSortLayout.setBackgroundColor(getResources().getColor(R.color.filterUnselected));

                mapFragment.setVisibility(View.GONE);
                lvResorts.setVisibility(View.VISIBLE);
                isMapVisible = false;
                if(language.equalsIgnoreCase("En")) {
                    tvMap.setText("Map");
                }
                else{
                    tvMap.setText("الخريطة");
                }
                imgMapIcon.setImageDrawable(getResources().getDrawable(R.drawable.map));
                searchResultsAdapter.notifyDataSetChanged();

                tvResultsCount.setText(""+filteredResortDetailsArrayList.size());

                if(filteredResortDetailsArrayList.size() == 0){
                    tvNoResults.setVisibility(View.VISIBLE);
                }
                else{
                    tvNoResults.setVisibility(View.GONE);
                }
                alertDialog2.dismiss();
            }
        });
        alertDialog2 = dialogBuilder.create();
//        alertDialog2.getWindow().setGravity(Gravity.BOTTOM);
//        alertDialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        alertDialog2.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog2.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
    }

    public void resetDetails(){
        facilitiesSelected.clear();
        filteredResortDetailsArrayList.clear();
        isFiltersSet = false;
        for (int i = 0; i<resortDetailsArrayList.size(); i++) {
            ResortDetailsFiltered resortDetailsFiltered = new ResortDetailsFiltered();
            resortDetailsFiltered.setResortId(resortDetailsArrayList.get(i).getResortId());
            resortDetailsFiltered.setResortName(resortDetailsArrayList.get(i).getResortName());
            resortDetailsFiltered.setResortNameAr(resortDetailsArrayList.get(i).getResortNameAr());
            resortDetailsFiltered.setCity(resortDetailsArrayList.get(i).getCity());
            resortDetailsFiltered.setAfrahcomAdvance(resortDetailsArrayList.get(i).getAdvanceAmount());
            resortDetailsFiltered.setResortAddress(resortDetailsArrayList.get(i).getResortAddress());
            resortDetailsFiltered.setResortType(resortDetailsArrayList.get(i).getResortType());
            resortDetailsFiltered.setResortImagesArray(resortDetailsArrayList.get(i).getResortImagesArray());
            resortDetailsFiltered.setActualPrice(resortDetailsArrayList.get(i).getActualPrice());
            resortDetailsFiltered.setDiscountedPrice(resortDetailsArrayList.get(i).getDiscountedPrice());
            resortDetailsFiltered.setLatitude(resortDetailsArrayList.get(i).getLatitude());
            resortDetailsFiltered.setLongitude(resortDetailsArrayList.get(i).getLongitude());
            resortDetailsFiltered.setRating(resortDetailsArrayList.get(i).getRating());
            resortDetailsFiltered.setIsFavorite(resortDetailsArrayList.get(i).getIsFavorite());
            resortDetailsFiltered.setApproved(resortDetailsArrayList.get(i).getApproved());
            resortDetailsFiltered.setDistance(resortDetailsArrayList.get(i).getDistance());
            resortDetailsFiltered.setResortFacilities(resortDetailsArrayList.get(i).getResortFacilities());
            resortDetailsFiltered.setDiscount(resortDetailsArrayList.get(i).getDiscount());
            resortDetailsFiltered.setDesc(resortDetailsArrayList.get(i).getDescription());
            resortDetailsFiltered.setDescAr(resortDetailsArrayList.get(i).getDescriptionAr());
            resortDetailsFiltered.setVatAvailability(resortDetailsArrayList.get(i).getVatAvailability());
            filteredResortDetailsArrayList.add(resortDetailsFiltered);
        }
        Collections.sort(filteredResortDetailsArrayList, ResortDetailsFiltered.distanceSort);
        searchResultsAdapter.notifyDataSetChanged();
        tvResultsCount.setText(""+filteredResortDetailsArrayList.size());

        if(filteredResortDetailsArrayList.size() == 0){
            tvNoResults.setVisibility(View.VISIBLE);
        }
        else{
            tvNoResults.setVisibility(View.GONE);
        }

        if(map!=null) {
            map.clear();

            for (int i = 0; i < filteredResortDetailsArrayList.size(); i++) {
                markerOptions.position(new LatLng(Double.parseDouble(filteredResortDetailsArrayList.get(i).getLatitude()), Double.parseDouble(filteredResortDetailsArrayList.get(i).getLongitude())))
                        .title(filteredResortDetailsArrayList.get(i).getResortName())
                        .snippet(filteredResortDetailsArrayList.get(i).getResortAddress() + "," + filteredResortDetailsArrayList.get(i).getCity())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
                map.addMarker(markerOptions);
            }
        }
    }
}
