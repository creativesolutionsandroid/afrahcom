package com.cs.afrahcom.activity;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.NotificationsAdapter;
import com.cs.afrahcom.models.Notifications;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 09-07-2016.
 */
public class HostNotificationsActivity extends AppCompatActivity {
    private ArrayList<Notifications> messageList = new ArrayList<>();
    private String response12 = null;
    ListView messagesListView;
    TextView emptyView, title;
    NotificationsAdapter mAdapter;
    Toolbar toolbar;
    String hostId;
    SharedPreferences hostPrefs;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        setContentView(R.layout.activity_notifications);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostId = hostPrefs.getString("hostId", null);

        new GetMessages().execute(Constants.HOST_NOTIFICATIONS_URL+hostId);

        MainActivity.hostNotificationCount = 0;
        Intent in = new Intent("data_action");
        sendBroadcast(in);

        emptyView = (TextView) findViewById(R.id.noBookingsDone);
        title = (TextView) findViewById(R.id.tv_booking_history);
        if(language.equalsIgnoreCase("Ar")){
            title.setText("إشعارات");
            emptyView.setText("لم يتم العثور على إشعارات");
        }

        messagesListView = (ListView) findViewById(R.id.historyListView);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class GetMessages extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(HostNotificationsActivity.this);
            dialog = new ACProgressFlower.Builder(HostNotificationsActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
            messageList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            messageList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    Notifications msg = new Notifications();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    msg.setTitle(jo1.getString("PushNotificationType"));
                                    msg.setMessage(jo1.getString("PushMessage"));
                                    msg.setDate(jo1.getString("SentDate"));

                                    messageList.add(msg);
                                }
                            }catch (Exception je){
//                                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }else {
                Toast.makeText(HostNotificationsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            if(messageList.size()>0){
                mAdapter = new NotificationsAdapter(HostNotificationsActivity.this, messageList, language);
                messagesListView.setAdapter(mAdapter);
            }
            else{
                emptyView.setVisibility(View.VISIBLE);
                messagesListView.setVisibility(View.GONE);
            }
            super.onPostExecute(result);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
    }
}
