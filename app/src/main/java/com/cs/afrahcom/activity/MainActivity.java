package com.cs.afrahcom.activity;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.BannersNewAdapter;
import com.cs.afrahcom.adapter.DrawerListAdapter;
import com.cs.afrahcom.fragment.HostDashboardFragment;
import com.cs.afrahcom.fragment.ResortRegistrationFragment;
import com.cs.afrahcom.fragment.ResortSearchFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

import static com.cs.afrahcom.adapter.BannersNewAdapter.bannerPos;
import static com.cs.afrahcom.adapter.BannersNewAdapter.galleryList;
import static com.cs.afrahcom.fragment.ResortSearchFragment.isToday;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    DrawerListAdapter adapter;
    private String[] mSidemenuTitles, mSidemenuTitles1;
    private ListView mDrawerList;
    String host;
    public static String userId;
    public static int userNotificationCount = 0;
    public static int hostNotificationCount = 0;
    //    TextView tv_signin;
    RelativeLayout mDrawerLinear;
    SharedPreferences userPrefs, hostPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences.Editor hostPrefEditor;
    boolean doubleBackToExitPressedOnce = false;
    private static final int LOGIN_REQUEST = 1;
    private static final int HISTORY_REQUEST = 2;
    private static final int FAV_RESORTS_REQUEST = 4;
    private static final int PROFILE_REQUEST = 5;
    private static final int NOTIFICATIONS_REQUEST = 6;
    private static final int LANGUAGE_REQUEST = 7;
    private static final int HOST_LOGIN_REQUEST = 3;
    private static final int SITE_VISIT_REQUEST = 8;
    FragmentManager fragmentManager = getSupportFragmentManager();
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    public static String language;
    AlertDialog customDialog;
    String hostId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerLinear = (RelativeLayout) findViewById(R.id.left_drawer);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        hostPrefEditor = hostPrefs.edit();
        userId = userPrefs.getString("userId", "0");
        hostId = hostPrefs.getString("hostId", "0");

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        configureToolbar();
        configureDrawer();

        // Add code to print out the key hash
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.cs.afrahcom",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

        if(userPrefs.getString("mode","").equals("")){
            Fragment fragment = new ResortSearchFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            setUserSideMenu();
        }
        else if(userPrefs.getString("mode","").equals("user")){
            Fragment fragment = new ResortSearchFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            setUserSideMenu();
        }
        else if(userPrefs.getString("mode","").equals("host")) {
            setHostSideMenu();
            int start = getIntent().getIntExtra("startwith", -1);
            if (start != -1) {
                selectItem(start);
            } else {
                Fragment fragment = new HostDashboardFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            }
        }

        Constants.isKitchenSelected = 0;
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            selectItem(position);
        }
    }

    /**
     * Swaps Activities in the main content view
     */
    private void selectItem(int position) {

        switch (position) {
            case 0:
                Log.i("TAG","click in activity");
//                language = languagePrefs.getString("language", "En");
//                if(language.equalsIgnoreCase("En")) {
//                    languagePrefsEditor.putString("language", "Ar");
//                    languagePrefsEditor.commit();
//                    language = "Ar";
//                }
//                else{
//                    languagePrefsEditor.putString("language", "En");
//                    languagePrefsEditor.commit();
//                    language = "En";
//                }
//                new ChangeLanguage().execute(Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language="+language);

                break;
            case 1:
                if(userPrefs.getString("mode","").equals("user")){
                    Fragment mainFragment = new ResortSearchFragment();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, mainFragment).commit();
                    setUserSideMenu();
                }
                else if(userPrefs.getString("mode","").equals("host")){
                    Fragment dashboardFragment = new HostDashboardFragment();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, dashboardFragment).commit();
                    setHostSideMenu();
                }
                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 2:
                if(userPrefs.getString("mode","").equals("user")) {
                    setUserSideMenu();
                    if (userPrefs.getString("login_status", "").contains("loggedin")) {
//                        startActivity(new Intent(MainActivity.this, FavoriteResortsActivity.class));
                        Intent intent = new Intent(MainActivity.this, FavoriteResortsActivity.class);
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                        startActivity(intent, options.toBundle());
                    } else {
                        Intent i = new Intent(MainActivity.this, SignInActivity.class);
                        startActivityForResult(i, FAV_RESORTS_REQUEST);
                    }
                }
                else if(userPrefs.getString("mode","").equals("host")){
                    Fragment dashboardFragment = new ResortRegistrationFragment();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, dashboardFragment).commit();
                    setHostSideMenu();
                }
                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 3:
                if(userPrefs.getString("mode","").equals("user")) {
                    if (userPrefs.getString("login_status", "").contains("loggedin")) {
                        Intent intent = new Intent(MainActivity.this, OrderHistoryActivity.class);
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                        startActivity(intent, options.toBundle());
                    } else {
                        Intent i = new Intent(MainActivity.this, SignInActivity.class);
                        startActivityForResult(i, HISTORY_REQUEST);
                    }
                }
                else if(userPrefs.getString("mode","").equals("host")){
//                    startActivity(new Intent(MainActivity.this, ResortHistoryActivity.class));
                    Intent intent = new Intent(MainActivity.this, HostHistoryActivity.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                    startActivity(intent, options.toBundle());
                }
                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 4:
                if (userPrefs.getString("mode", "").equals("user")) {
                    if (userPrefs.getString("login_status", "").contains("loggedin")) {
//                        startActivity(new Intent(MainActivity.this, OrderPendingUserActivity.class));
                        Intent intent = new Intent(MainActivity.this, OrderPendingUserActivity.class);
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                        startActivity(intent, options.toBundle());
                    } else {
                        Intent i = new Intent(MainActivity.this, SignInActivity.class);
                        startActivityForResult(i, SITE_VISIT_REQUEST);
                    }
                } else if (userPrefs.getString("mode", "").equals("host")) {
                    userPrefEditor.putString("mode", "user");
                    userPrefEditor.commit();
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                    finish();
                }
                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 5:
                if(userPrefs.getString("mode","").equals("user")) {
                    if(hostPrefs.getString("login_status", "").contains("loggedin")) {
                        userPrefEditor.putString("mode", "host");
                        userPrefEditor.commit();
                        startActivity(new Intent(MainActivity.this, MainActivity.class));
                        finish();
                    }
                    else {
                        Intent i = new Intent(MainActivity.this, HostSignInActivity.class);
                        startActivityForResult(i, HOST_LOGIN_REQUEST);
                    }
                }
                else if(userPrefs.getString("mode","").equals("host")){
//                    startActivity(new Intent(MainActivity.this, HostProfileActivity.class));
                    Intent intent = new Intent(MainActivity.this, HostProfileActivity.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                    startActivity(intent, options.toBundle());
                }
                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 6:
                if(userPrefs.getString("mode","").equals("user")) {
                    if (userPrefs.getString("login_status", "").contains("loggedin")) {
                        language = languagePrefs.getString("language", "En");
                        Intent intent = new Intent(MainActivity.this, MyProfileActivity.class);
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                        startActivity(intent, options.toBundle());
//                        Intent intent = new Intent(MainActivity.this, MyProfileActivity.class);
//                        ActivityOptions options =
//                                ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
//                        startActivity(intent, options.toBundle());
                    } else {
                        Intent i = new Intent(MainActivity.this, SignInActivity.class);
                        startActivityForResult(i, PROFILE_REQUEST);
                    }
//                    Intent intent = new Intent(MainActivity.this, CaterersActivity.class);
//                    ActivityOptions options =
//                            ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
//                    startActivity(intent, options.toBundle());
                }
                else if(userPrefs.getString("mode","").equals("host")){
                    language = languagePrefs.getString("language", "En");
                    Intent i = new Intent(MainActivity.this, MoreActivity.class);
                    startActivityForResult(i, LANGUAGE_REQUEST);
                    mDrawerLayout.closeDrawer(mDrawerLinear);
                }
                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 7:
                if(userPrefs.getString("mode","").equals("user")) {
                    Intent i = new Intent(MainActivity.this, MoreActivity.class);
                    startActivityForResult(i, LANGUAGE_REQUEST);
                    mDrawerLayout.closeDrawer(mDrawerLinear);
                }
                else if(userPrefs.getString("mode","").equals("host")){
//                    startActivity(new Intent(MainActivity.this, HostNotificationsActivity.class));
//                    mDrawerLayout.closeDrawer(mDrawerLinear);
                    Intent intent = new Intent(MainActivity.this, HostNotificationsActivity.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                    startActivity(intent, options.toBundle());
                }
                mDrawerLayout.closeDrawer(mDrawerLinear);
                break;

            case 8:
                if(userPrefs.getString("mode","").equals("user")) {
                    Intent intent = new Intent(MainActivity.this, UserNotificationsActivity.class);
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                        startActivity(intent, options.toBundle());
//                    language = languagePrefs.getString("language", "En");
//                    Intent i = new Intent(MainActivity.this, MoreActivity.class);
//                    startActivityForResult(i, LANGUAGE_REQUEST);
                    mDrawerLayout.closeDrawer(mDrawerLinear);
                }
                else if(userPrefs.getString("mode","").equals("host")){
                    language = languagePrefs.getString("language", "En");
                    if(language.equalsIgnoreCase("En")) {
                        languagePrefsEditor.putString("language", "Ar");
                        languagePrefsEditor.commit();
                        language = "Ar";
                    }
                    else{
                        languagePrefsEditor.putString("language", "En");
                        languagePrefsEditor.commit();
                        language = "En";
                    }
                    new ChangeLanguage().execute(Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language="+language);
                }

                break;

            case 9:
                if(userPrefs.getString("mode","").equals("user")) {
                    language = languagePrefs.getString("language", "En");
                    if(language.equalsIgnoreCase("En")) {
                        languagePrefsEditor.putString("language", "Ar");
                        languagePrefsEditor.commit();
                        language = "Ar";
                    }
                    else{
                        languagePrefsEditor.putString("language", "En");
                        languagePrefsEditor.commit();
                        language = "En";
                    }
                    new ChangeLanguage().execute(Constants.CHANGE_LANGUAGE_URL+userId+"&HostId="+hostId+"&DeviceToken="+SplashScreenActivity.regId+"&Language="+language);

                    mDrawerLayout.closeDrawer(mDrawerLinear);
                }
                else if(userPrefs.getString("mode","").equals("host")){
                    hostPrefEditor.clear();
                    hostPrefEditor.commit();
                    userPrefEditor.putString("mode", "user");
                    userPrefEditor.commit();
                    hostNotificationCount = 0;
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                    finish();
                }
                break;

            case 10:
                if(userPrefs.getString("mode","").equals("user")) {
                    if (mSidemenuTitles1[10].contains("Sign in")) {
                        Intent i = new Intent(MainActivity.this, SignInActivity.class);
                        startActivityForResult(i, LOGIN_REQUEST);
                        mDrawerLayout.closeDrawer(mDrawerLinear);
                        userNotificationCount = 0;
                        if(language.equalsIgnoreCase("En")) {
                            adapter = new DrawerListAdapter(MainActivity.this, R.layout.drawer_list_item,
                                    mSidemenuTitles);
                        }
                        else{
                            adapter = new DrawerListAdapter(MainActivity.this, R.layout.drawer_list_item_ar,
                                    mSidemenuTitles);
                        }
                        mDrawerList.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        userPrefEditor.clear();
                        userPrefEditor.commit();
                        userPrefEditor.putString("mode","user");
                        userPrefEditor.commit();
                        mDrawerLayout.closeDrawer(mDrawerLinear);
                        setUserSideMenu();
                    }
                }
                break;

            default:
                Log.i("TAG","click in default activity");
                break;
        }
    }

    private void configureToolbar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mainToolbar);
        getSupportActionBar().setTitle("");

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);

                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    public void configureDrawer() {
        // Configure drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_closed) {

            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == LOGIN_REQUEST && resultCode == RESULT_OK) {
            setUserSideMenu();
        }
        else if (requestCode == HISTORY_REQUEST && resultCode == RESULT_OK) {
            setUserSideMenu();
//            startActivity(new Intent(MainActivity.this, OrderHistoryActivity.class));
            Intent intent = new Intent(MainActivity.this, OrderHistoryActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
            startActivity(intent, options.toBundle());
        }
        else if (requestCode == SITE_VISIT_REQUEST && resultCode == RESULT_OK) {
            setUserSideMenu();
//            startActivity(new Intent(MainActivity.this, OrderHistoryActivity.class));
            Intent intent = new Intent(MainActivity.this, OrderPendingUserActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
            startActivity(intent, options.toBundle());
        }
        else if (requestCode == FAV_RESORTS_REQUEST && resultCode == RESULT_OK) {
            setUserSideMenu();
//            startActivity(new Intent(MainActivity.this, FavoriteResortsActivity.class));
            Intent intent = new Intent(MainActivity.this, FavoriteResortsActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
            startActivity(intent, options.toBundle());
        }
        else if (requestCode == PROFILE_REQUEST && resultCode == RESULT_OK) {
            setUserSideMenu();
//            startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
            Intent intent = new Intent(MainActivity.this, MyProfileActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
            startActivity(intent, options.toBundle());
        }
        else if (requestCode == NOTIFICATIONS_REQUEST && resultCode == RESULT_OK) {
            setUserSideMenu();
//            startActivity(new Intent(MainActivity.this, UserNotificationsActivity.class));
            Intent intent = new Intent(MainActivity.this, UserNotificationsActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(MainActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
            startActivity(intent, options.toBundle());
        }
        else if (requestCode == LANGUAGE_REQUEST && resultCode == RESULT_OK) {
//            setUserSideMenu();
            startActivity(new Intent(MainActivity.this, MainActivity.class));
            finish();
        }
        else if (requestCode == HOST_LOGIN_REQUEST && resultCode == RESULT_OK) {
            userPrefEditor.putString("mode", "host");
            userPrefEditor.commit();
            startActivity(new Intent(MainActivity.this, MainActivity.class));
            finish();
        }
        else if (requestCode == HOST_LOGIN_REQUEST && resultCode == RESULT_CANCELED) {
            setUserSideMenu();
        }
        else if (resultCode == RESULT_CANCELED) {
            setUserSideMenu();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,"Press BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }
    }

    public void setUserSideMenu(){
        if(hostPrefs.getString("login_status", "").contains("loggedin")) {
            if(language.equalsIgnoreCase("En")) {
                host = "Host";
            }
            else{
                host = "مضيف";
            }
        }
        else{
            if(language.equalsIgnoreCase("En")) {
                host = "Host Registration";
            }
            else{
                host = "أصحاب القاعات";
            }
        }
//        SideMenu with caterers
//        if(userPrefs.getString("login_status", "").contains("loggedin")){
//            mSidemenuTitles1 = new String[]{"", "Home", "Favorite",
//                    "Bookings", "Pending Reservations", host, "Caterers", "My Profile", "More", "Notifications", "Sign out"};
//            if(language.equalsIgnoreCase("En")) {
//                mSidemenuTitles = new String[]{"", "Home", "Favorite",
//                        "Bookings", "Pending Reservations", host, "Caterers", "My Profile", "More", "Notifications", "Sign out"};
//            }
//            else{
//                mSidemenuTitles = new String[]{"", "الرئيسية", "المفضلة",
//                        "حجوزاتي", "تأكيد الحجوزات", host, "متعهدي المطاعم للحفلات", "صفحتي", "المزيد","اشعار", "تسجيل الخروج"};
//            }
//            adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                    mSidemenuTitles);
//            mDrawerList.setAdapter(adapter);
//        }
//        else {
//            mSidemenuTitles1 = new String[]{"", "Home", "Favorite",
//                    "Bookings", "Pending Reservations", host, "Caterers", "My Profile", "More", "Notifications", "Sign in"};
//            if(language.equalsIgnoreCase("En")) {
//                mSidemenuTitles = new String[]{"", "Home", "Favorite",
//                        "Bookings", "Pending Reservations", host, "Caterers", "My Profile", "More", "Notifications", "Sign in"};
//            }
//            else{
//                mSidemenuTitles = new String[]{"", "الرئيسية", "المفضلة",
//                        "حجوزاتي", "تأكيد الحجوزات", host, "متعهدي المطاعم للحفلات", "صفحتي", "المزيد","اشعار", "تسجيل الدخول"};
//            }
//            adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
//                    mSidemenuTitles);
//            mDrawerList.setAdapter(adapter);
//        }

        if(userPrefs.getString("login_status", "").contains("loggedin")){
            mSidemenuTitles1 = new String[]{"", "Home", "Favorite",
                    "Bookings", "Site Visit Request", host, "My Profile", "More", "Notifications", "Language", "Sign out"};
            if(language.equalsIgnoreCase("En")) {
                mSidemenuTitles = new String[]{"", "Home", "Favorite",
                        "Bookings", "Site Visit Request", host, "My Profile", "More", "Notifications", "Language","Sign out"};
            }
            else{
                mSidemenuTitles = new String[]{"", "الرئيسية", "المفضلة",
                        "حجوزاتي", getResources().getString(R.string.str_pending_reservations_ar), host, "صفحتي", "المزيد",
                        "إشعارات", "اللغة","تسجيل الخروج"};
            }

            if(language.equalsIgnoreCase("En")) {
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                        mSidemenuTitles);
            }
            else{
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_ar,
                        mSidemenuTitles);
            }
            mDrawerList.setAdapter(adapter);
        }
        else {
            mSidemenuTitles1 = new String[]{"", "Home", "Favorite",
                    "Bookings", "Site Visit Request", host, "My Profile", "More", "Notifications", "Language","Sign in"};
            if(language.equalsIgnoreCase("En")) {
                mSidemenuTitles = new String[]{"", "Home", "Favorite",
                        "Bookings", "Site Visit Request", host, "My Profile", "More", "Notifications", "Language","Sign in"};
            }
            else{
                mSidemenuTitles = new String[]{"", "الرئيسية", "المفضلة",
                        "حجوزاتي", getResources().getString(R.string.str_pending_reservations_ar), host, "صفحتي", "المزيد",
                        "إشعارات","اللغة", "تسجيل الدخول"};
            }
            if(language.equalsIgnoreCase("En")) {
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                        mSidemenuTitles);
            }
            else{
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_ar,
                        mSidemenuTitles);
            }
            mDrawerList.setAdapter(adapter);
        }
    }

    public void setHostSideMenu(){
        if(hostPrefs.getString("login_status", "").contains("loggedin")){
            mSidemenuTitles1 = new String[]{"", "Dashboard", "Property Registration",
                    "Bookings", "User", "Host Profile", "More", "Notifications", "Language", "Sign out"};
            if(language.equalsIgnoreCase("En")) {
                mSidemenuTitles = new String[]{"", "Dashboard", "Property Registration",
                        "Bookings", "User", "Host Profile", "More", "Notifications", "Language", "Sign out"};
            }
            else{
                mSidemenuTitles = new String[]{"", "الصفحه الرئيسية", "تسجيل القاعة",
                        "حجوزاتي", "صفحة البحث", "معلوماتي", "المزيد","إشعارات","اللغة", "تسجيل الخروج"};
            }
            if(language.equalsIgnoreCase("En")) {
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                        mSidemenuTitles);
            }
            else{
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_ar,
                        mSidemenuTitles);
            }
            mDrawerList.setAdapter(adapter);
        }
        else {
            mSidemenuTitles1 = new String[]{"", "Dashboard", "Property Registration",
                    "Bookings", "User", "Host Profile", "More", "Notifications", "Language", "Sign in"};
            if(language.equalsIgnoreCase("En")) {
                mSidemenuTitles = new String[]{"", "Dashboard", "Property Registration",
                        "Bookings", "User", "Host Profile", "More", "Notifications", "Language", "Sign in"};
            }
            else{
                mSidemenuTitles = new String[]{"", "الصفحه الرئيسية", "تسجيل القاعة",
                        "حجوزاتي", "صفحة البحث", "معلوماتي", "المزيد","إشعارات","اللغة", "تسجيل الدخول"};
            }
            if(language.equalsIgnoreCase("En")) {
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                        mSidemenuTitles);
            }
            else{
                adapter = new DrawerListAdapter(this, R.layout.drawer_list_item_ar,
                        mSidemenuTitles);
            }
            mDrawerList.setAdapter(adapter);
        }
    }

    public class ChangeLanguage extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            dialog = new ACProgressFlower.Builder(MainActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                String response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if(dialog != null) {
                dialog.dismiss();
            }
            startActivity(new Intent(MainActivity.this, MainActivity.class));
            finish();
            super.onPostExecute(result);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
        registerReceiver(mReceiverLocation, new IntentFilter("data_action"));
        if(userPrefs.getString("mode","").equals("")){
            userPrefEditor.putString("mode","user");
            userPrefEditor.commit();
            setUserSideMenu();
        }
        else if(userPrefs.getString("mode","").equals("user")){
            setUserSideMenu();
        }
        else if(userPrefs.getString("mode","").equals("host")){
            setHostSideMenu();
        }

//        try {
//            if(!language.equalsIgnoreCase(languagePrefs.getString("language","En"))){
//                startActivity(new Intent(MainActivity.this, MainActivity.class));
//                finish();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

//    @Override
//    public void onPause() {
//        unregisterReceiver(mReceiverLocation);
//        super.onPause();
//    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mReceiverLocation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private BroadcastReceiver mReceiverLocation = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("TAG","broadcast received");
            if(language.equalsIgnoreCase("En")) {
                adapter = new DrawerListAdapter(MainActivity.this, R.layout.drawer_list_item,
                        mSidemenuTitles);
            }
            else{
                adapter = new DrawerListAdapter(MainActivity.this, R.layout.drawer_list_item_ar,
                        mSidemenuTitles);
            }
            mDrawerList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    };

    public void loadPopup(){
        final PopupBannersSearch newFragment = PopupBannersSearch.newInstance();
        newFragment.show(getSupportFragmentManager(), "dialog");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed
                if(newFragment!=null){
                    newFragment.dismiss();
                }
            }
        });
    }

    public static class PopupBannersSearch extends DialogFragment{

        View dialogView;
        RelativeLayout layoutMan, layoutWoman, layoutFamily, layoutNumofMen, layoutNumofWomen, layoutGuest;
        RelativeLayout layoutCheckin, layoutCheckout;
        ImageView cbMan, cbWoman, cbKitchen;
        private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
        String checkInDate, checkOutDate;
        TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
        TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
        TextView tvSearch, tvResortTypeName, tvCityName, tvNumOfDays;
        TextView tvNumOfMen, tvNumOfWomen, tvKitchen, tvnumOfGuests;
        EditText etguests, etMenCapacity, etWomenCapacity;
        Boolean isManSelected = true, isWomanSelected = false, isKitchenSelected = false;
        Date mCheckInDate = null, mCheckOutDate = null, mTodayDate = null;
        String mCheckInDateStr = null, mCheckOutDateStr = null;
        long NumofDaysStr = 0;
        int capacity = 0;
        float rate, discount;
        TextView submit;
        ImageView closeDialog;
        AlertDialog customDialog;
        String strResortType, strWeeks = "", todayWeek = "";
        Boolean checkingWeekFirstTime = false;
        public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};

        static PopupBannersSearch newInstance() {
            return new PopupBannersSearch();
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            if(language.equalsIgnoreCase("En")) {
                dialogView = inflater.inflate(R.layout.popup_banners_search, container, false);
            }
            else{
                dialogView = inflater.inflate(R.layout.popup_banners_search_ar, container, false);
            }

            tvCheckInDate = (TextView) dialogView.findViewById(R.id.tv_checkin_date);
            tvCheckOutDate = (TextView) dialogView.findViewById(R.id.tv_checkout_date);

            closeDialog = (ImageView) dialogView.findViewById(R.id.close_dialog);
            submit = (TextView) dialogView.findViewById(R.id.tv_submit);
            layoutMan = (RelativeLayout) dialogView.findViewById(R.id.layout_man);
            layoutWoman = (RelativeLayout) dialogView.findViewById(R.id.layout_woman);
            layoutFamily = (RelativeLayout) dialogView.findViewById(R.id.layout_kitchen);
            layoutNumofMen = (RelativeLayout) dialogView.findViewById(R.id.layout_NoOfMen);
            layoutNumofWomen = (RelativeLayout) dialogView.findViewById(R.id.layout_numOfWomen);
            layoutGuest = (RelativeLayout) dialogView.findViewById(R.id.layout_guests);

            layoutCheckin = (RelativeLayout) dialogView.findViewById(R.id.layout_checkin);
            layoutCheckout = (RelativeLayout) dialogView.findViewById(R.id.layout_checkout);

            cbMan = (ImageView) dialogView.findViewById(R.id.cb_man);
            cbWoman = (ImageView) dialogView.findViewById(R.id.cb_woman);
            cbKitchen = (ImageView) dialogView.findViewById(R.id.cb_kitchen);

            tvCheckInDate = (TextView) dialogView.findViewById(R.id.tv_checkin_date);
            tvCheckInMonth = (TextView) dialogView.findViewById(R.id.tv_checkin_month);
            tvCheckInWeek = (TextView) dialogView.findViewById(R.id.tv_checkin_week);

            tvCheckOutDate = (TextView) dialogView.findViewById(R.id.tv_checkout_date);
            tvCheckOutMonth = (TextView) dialogView.findViewById(R.id.tv_checkout_month);
            tvCheckOutWeek = (TextView) dialogView.findViewById(R.id.tv_checkout_week);

            etguests = (EditText) dialogView.findViewById(R.id.tv_num_of_guests);
            tvNumOfDays = (TextView) dialogView.findViewById(R.id.tv_number_of_days);
            etMenCapacity = (EditText) dialogView.findViewById(R.id.tv_num_of_men);
            etWomenCapacity = (EditText) dialogView.findViewById(R.id.tv_num_of_women);

            tvNumOfMen = (TextView) dialogView.findViewById(R.id.tvNumOfMen);
            tvNumOfWomen = (TextView) dialogView.findViewById(R.id.tvNumOfWomen);
            tvKitchen = (TextView) dialogView.findViewById(R.id.tvKitchen);
            tvnumOfGuests = (TextView) dialogView.findViewById(R.id.tvnumOfGuests);
            submit = (TextView) dialogView.findViewById(R.id.tv_search);

            if(BannersNewAdapter.galleryList.get(bannerPos).getResortType().equalsIgnoreCase("Wedding Hall")){
                tvNumOfMen.setVisibility(View.GONE);
                tvNumOfWomen.setVisibility(View.GONE);
                tvnumOfGuests.setVisibility(View.VISIBLE);
                tvKitchen.setVisibility(View.VISIBLE);
                layoutNumofMen.setVisibility(View.GONE);
                layoutNumofWomen.setVisibility(View.GONE);
                layoutGuest.setVisibility(View.VISIBLE);
                layoutFamily.setVisibility(View.VISIBLE);
            }
            else if(BannersNewAdapter.galleryList.get(bannerPos).getResortType().equalsIgnoreCase("Banquet Hall")){
                tvNumOfMen.setVisibility(View.VISIBLE);
                tvNumOfWomen.setVisibility(View.VISIBLE);
                tvnumOfGuests.setVisibility(View.GONE);
                tvKitchen.setVisibility(View.GONE);
                layoutNumofMen.setVisibility(View.VISIBLE);
                layoutNumofWomen.setVisibility(View.VISIBLE);
                layoutGuest.setVisibility(View.GONE);
                layoutFamily.setVisibility(View.GONE);
            }
            else if(BannersNewAdapter.galleryList.get(bannerPos).getResortType().equalsIgnoreCase("Resort")){
                tvNumOfMen.setVisibility(View.GONE);
                tvNumOfWomen.setVisibility(View.GONE);
                tvnumOfGuests.setVisibility(View.VISIBLE);
                tvKitchen.setVisibility(View.GONE);
                layoutNumofMen.setVisibility(View.GONE);
                layoutNumofWomen.setVisibility(View.GONE);
                layoutGuest.setVisibility(View.VISIBLE);
                layoutFamily.setVisibility(View.GONE);
            }

            final Calendar c = Calendar.getInstance();
            mCheckInYear = c.get(Calendar.YEAR);
            mCheckInMonth = c.get(Calendar.MONTH);
            mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckInDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckInDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

            c.add(Calendar.DATE, 1);
            mCheckOutYear = c.get(Calendar.YEAR);
            mCheckOutMonth = c.get(Calendar.MONTH);
            mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckOutDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

            String checkInDateString = mCheckInYear+"-"+(mCheckInMonth+1)+"-"+mCheckInDay;
            String checkOutDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                mCheckInDate = sdf.parse(checkInDateString);
                mCheckOutDate = sdf.parse(checkOutDateString);
                mTodayDate = sdf.parse(checkInDateString);

                mCheckInDateStr = sdf.format(mCheckInDate);
                mCheckOutDateStr = sdf.format(mCheckOutDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
            getDates();
            getWeekDays();

            layoutCheckin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkInDatePicker();
                }
            });

            layoutCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                }
            });

            layoutMan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isManSelected) {
                        isManSelected = false;
                        cbMan.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                    }
                    else{
                        isManSelected = true;
                        cbMan.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                    }
                }
            });

            layoutWoman.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isWomanSelected) {
                        isWomanSelected = false;
                        cbWoman.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                    }
                    else{
                        isWomanSelected = true;
                        cbWoman.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                    }
                }
            });

            layoutFamily.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isKitchenSelected) {
                        isKitchenSelected = false;
                        cbKitchen.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                    }
                    else{
                        isKitchenSelected = true;
                        cbKitchen.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                    }
                }
            });

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    capacityCalculation();
                    if(!isManSelected && !isWomanSelected){
//                        String appName, title, positive;
                        String language;
                        SharedPreferences languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
                        language = languagePrefs.getString("language", "En");
//                        if(language.equalsIgnoreCase("En")){
//                            appName = getResources().getString(R.string.app_name);
//                            title = "Please select Gender";
//                            positive = getResources().getString(R.string.str_btn_ok);
//                        }
//                        else{
//                            appName = getResources().getString(R.string.app_name_ar);
//                            title = "يرجى تحديد الجنس";
//                            positive = getResources().getString(R.string.str_btn_ok_ar);
//                        }
//                        final iOSDialog iOSDialog = new iOSDialog(getContext());
//                        iOSDialog.setTitle(appName);
//                        iOSDialog.setSubtitle(title);
//                        iOSDialog.setPositiveLabel(positive);
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                            }
//                        });
//                        iOSDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.app_name));
                            yes.setText(getResources().getString(R.string.str_btn_ok));
                            desc.setText("Please select Gender");
                        }
                        else{
                            title.setText(getResources().getString(R.string.app_name_ar));
                            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                            desc.setText("يرجى تحديد الجنس");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getActivity().getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else if(capacity > galleryList.get(bannerPos).getMaxCapacity()) {
//                        String appName, title, positive;
//                        String language;
//                        SharedPreferences languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//                        language = languagePrefs.getString("language", "En");
//                        if(language.equalsIgnoreCase("En")){
//                            appName = getResources().getString(R.string.app_name);
//                            title = "Maximum Capacity of this property is "+galleryList.get(bannerPos).getMaxCapacity();
//                            positive = getResources().getString(R.string.str_btn_ok);
//                        }
//                        else{
//                            appName = getResources().getString(R.string.app_name_ar);
//                            title = galleryList.get(bannerPos).getMinCapacity()+"الحد الأقصى لسعة القاعة ";
//                            positive = getResources().getString(R.string.str_btn_ok_ar);
//                        }
//                        final iOSDialog iOSDialog = new iOSDialog(getContext());
//                        iOSDialog.setTitle(appName);
//                        iOSDialog.setSubtitle(title);
//                        iOSDialog.setPositiveLabel(positive);
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                            }
//                        });
//                        iOSDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.app_name));
                            yes.setText(getResources().getString(R.string.str_btn_ok));
                            desc.setText("Maximum Capacity of this property is "+galleryList.get(bannerPos).getMaxCapacity());
                        }
                        else{
                            title.setText(getResources().getString(R.string.app_name_ar));
                            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                            desc.setText(galleryList.get(bannerPos).getMinCapacity()+"الحد الأقصى لسعة القاعة ");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getActivity().getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else if(capacity < galleryList.get(bannerPos).getMinCapacity()) {
//                        String appName, title, positive;
//                        String language;
//                        SharedPreferences languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//                        language = languagePrefs.getString("language", "En");
//                        if(language.equalsIgnoreCase("En")){
//                            appName = getResources().getString(R.string.app_name);
//                            title = "Minimum Capacity of this property is "+galleryList.get(bannerPos).getMinCapacity();
//                            positive = getResources().getString(R.string.str_btn_ok);
//                        }
//                        else{
//                            appName = getResources().getString(R.string.app_name_ar);
//                            title = galleryList.get(bannerPos).getMinCapacity()+"الحد الأدنى لسعة القاعة ";
//                            positive = getResources().getString(R.string.str_btn_ok_ar);
//                        }
//                        final iOSDialog iOSDialog = new iOSDialog(getContext());
//                        iOSDialog.setTitle(appName);
//                        iOSDialog.setSubtitle(title);
//                        iOSDialog.setPositiveLabel(positive);
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                            }
//                        });
//                        iOSDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.app_name));
                            yes.setText(getResources().getString(R.string.str_btn_ok));
                            desc.setText("Minimum Capacity of this property is "+galleryList.get(bannerPos).getMinCapacity());
                        }
                        else{
                            title.setText(getResources().getString(R.string.app_name_ar));
                            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                            desc.setText(galleryList.get(bannerPos).getMinCapacity()+"الحد الأدنى لسعة القاعة ");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getActivity().getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                    else{
                        if(isKitchenSelected){
                            Constants.isKitchenSelected = 1;
                        }
                        else{
                            Constants.isKitchenSelected = 0;
                        }
                        final JSONObject parent = new JSONObject();
                        try {

                            JSONObject mainObj = new JSONObject();
                            mainObj.put("ResortTypeId", galleryList.get(bannerPos).getResortTypeId());
                            mainObj.put("CityId", galleryList.get(bannerPos).getCityId());
                            mainObj.put("IsMan", isManSelected);
                            mainObj.put("IsWoman", isWomanSelected);
                            mainObj.put("IsKitchen", isKitchenSelected);
                            mainObj.put("NoOfCapacity", Constants.convertToArabic(Integer.toString(capacity)));
                            mainObj.put("StartDate", Constants.convertToArabic(mCheckInDateStr));
                            mainObj.put("EndDate", Constants.convertToArabic(mCheckOutDateStr));
                            mainObj.put("ResortId", galleryList.get(bannerPos).getResortId());
                            if (userId == null) {
                                mainObj.put("UserId", "-1");
                            } else {
                                mainObj.put("UserId", userId);
                            }

                            parent.put("SearchDetails", mainObj);
                            Log.i("TAG", parent.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        new CheckAvailablity().execute(parent.toString());
                    }
                }
            });

            closeDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });

            return dialogView;
        }

        @Override
        public void onStart() {
            super.onStart();
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(getDialog().getWindow().getAttributes());
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;;
            double d = width * 0.95;
            lp.width = (int) d;

            getDialog().getWindow().setAttributes(lp);
        }

        public void checkInDatePicker(){
            final Calendar c = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            mCheckInYear = year;
                            mCheckInDay = dayOfMonth;
                            mCheckInMonth = monthOfYear;

                            if(mCheckInDay<10) {
                                tvCheckInDate.setText("0" + mCheckInDay);
                            }
                            else{
                                tvCheckInDate.setText("" + mCheckInDay);
                            }
                            tvCheckInMonth.setText(MONTHS[mCheckInMonth]);

                            checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                        }
                    }, mCheckInYear, mCheckInMonth, mCheckInDay);

            String msg;
            if(language.equalsIgnoreCase("En")){
                msg = "Select Check In Date";
            }
            else{
                msg = "تاريخ الوصول";
            }
            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//            long max = TimeUnit.DAYS.toMillis(90);
//            datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
            datePickerDialog.setCancelable(false);
            datePickerDialog.setMessage(msg);
            datePickerDialog.show();
        }

        public void checkOutDatePicker(int year, int date, int month){
            final Calendar c = Calendar.getInstance();
            long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
            String givenDateString = year+"-"+(month)+"-"+date;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                mCheckInDate = sdf.parse(givenDateString);
                mCheckInDateStr = sdf.format(mCheckInDate);
                selectedMillis = mCheckInDate.getTime();
                c.setTime(mCheckInDate); // yourdate is an object of type Date
                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                tvCheckInWeek.setText(WEEKS[(dayOfWeek-1)]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mCheckOutYear = mCheckInYear;
            mCheckOutMonth = mCheckInMonth;
            mCheckOutDay = mCheckInDay;

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            mCheckOutYear = year;
                            mCheckOutDay = dayOfMonth;
                            mCheckOutMonth = monthOfYear;

                            if(mCheckOutDay<10) {
                                tvCheckOutDate.setText("0" + mCheckOutDay);
                            }
                            else{
                                tvCheckOutDate.setText("" + mCheckOutDay);
                            }
                            tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);

                            String givenDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                mCheckOutDate = sdf.parse(givenDateString);
                                mCheckOutDateStr = sdf.format(mCheckOutDate);
                                c.setTime(mCheckOutDate); // yourdate is an object of type Date
                                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                tvCheckOutWeek.setText(WEEKS[(dayOfWeek-1)]);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            CalculateNumOfDays();

                        }
                    }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
            String msg;
            if(language.equalsIgnoreCase("En")){
                msg = "Select Check Out Date";
            }
            else{
                msg = "تاريخ المغادرة";
            }
            datePickerDialog.getDatePicker().setMinDate(selectedMillis);
//            long max = TimeUnit.DAYS.toMillis(90);
//            datePickerDialog.getDatePicker().setMaxDate(selectedMillis+max);
            datePickerDialog.setInverseBackgroundForced(true);
            datePickerDialog.setMessage(msg);
            datePickerDialog.show();
        }

        public void CalculateNumOfDays(){
            long diff = mCheckOutDate.getTime() - mCheckInDate.getTime();
            NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))+1;
            tvNumOfDays.setText(""+NumofDaysStr);
            checkDayValidity();
            getDates();
            getWeekDays();
        }

        public void checkDayValidity(){
            long diff = mCheckInDate.getTime() - mTodayDate.getTime();
            long days = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))+1;
            if(days>=3){
                ResortSearchFragment.isAfter3Days = true;
            }
            else {
                ResortSearchFragment.isAfter3Days = false;
            }
            if(days>=1){
                isToday = false;
            }
            else{
                isToday = false;
            }
        }

        public void getDates(){
            ResortSearchFragment.strCheckInDate = tvCheckInDate.getText().toString();
            ResortSearchFragment.strCheckInMonth = tvCheckInMonth.getText().toString();
            ResortSearchFragment.strCheckInWeek = tvCheckInWeek.getText().toString();
            ResortSearchFragment.strCheckoutDate = tvCheckOutDate.getText().toString();
            ResortSearchFragment.strCheckoutMonth = tvCheckOutMonth.getText().toString();
            ResortSearchFragment.strCheckoutWeek = tvCheckOutWeek.getText().toString();
            ResortSearchFragment.strNumDays = tvNumOfDays.getText().toString();
//        strNumGuests = etguests.getText().toString();
//        Constants.noOfGuests = etguests.getText().toString();
            Constants.startDate = mCheckInDateStr;
            Constants.endDate = mCheckOutDateStr;
        }

        public void getWeekDays(){
            Calendar c1 = Calendar.getInstance();
            c1.setTime(mCheckInDate);

            Calendar c2 = Calendar.getInstance();
            c2.setTime(mCheckOutDate);

            strWeeks = "";

            while (! c1.after(c2)) {
                if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ){
                    if(strWeeks.equals("")){
                        strWeeks = "Saturday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Saturday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Saturday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Sunday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Sunday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Sunday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Monday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Monday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Monday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Tuesday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Tuesday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Tuesday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Wednesday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Wednesday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Wednesday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Thursday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Thursday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Thursday";
                    }
                }
                else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){
                    if(strWeeks.equals("")){
                        strWeeks = "Friday";
                        if(!checkingWeekFirstTime){
                            todayWeek = "Friday";
                            checkingWeekFirstTime = true;
                        }
                    }
                    else{
                        strWeeks = strWeeks + ",Friday";
                    }
                }

                c1.add(Calendar.DATE, 1);
            }
        }
        public void capacityCalculation(){
            getDates();
            try {
                if (BannersNewAdapter.galleryList.get(bannerPos).getResortType().equalsIgnoreCase("banquet hall")) {
                    if (isManSelected && isWomanSelected) {
                        capacity = Integer.parseInt(etMenCapacity.getText().toString()) + Integer.parseInt(etWomenCapacity.getText().toString());
                        Constants.noOfMen = etMenCapacity.getText().toString();
                        Constants.noOfWomen = etWomenCapacity.getText().toString();
                    } else if (isManSelected) {
                        capacity = Integer.parseInt(etMenCapacity.getText().toString());
                        Constants.noOfMen = etMenCapacity.getText().toString();
                        Constants.noOfWomen = "0";
                    } else if (isWomanSelected) {
                        capacity = Integer.parseInt(etWomenCapacity.getText().toString());
                        Constants.noOfMen = "0";
                        Constants.noOfWomen = etWomenCapacity.getText().toString();
                    }
                    Constants.noOfGuests = Integer.toString(capacity);
                } else {
                    if(isManSelected && isWomanSelected){
                        Constants.noOfMen = etguests.getText().toString();
                        Constants.noOfWomen = etguests.getText().toString();
                    }
                    else if(isWomanSelected){
                        Constants.noOfMen = "0";
                        Constants.noOfWomen = etguests.getText().toString();
                    }
                    else if(isManSelected){
                        Constants.noOfMen = etguests.getText().toString();
                        Constants.noOfWomen = "0";
                    }
                    capacity = Integer.parseInt(etguests.getText().toString());
                    Constants.noOfGuests = Integer.toString(capacity);
                }
            } catch (Exception e) {
                e.printStackTrace();
//                if(etMenCapacity.getText().toString().length() == 0 || etWomenCapacity.getText().toString().length() == 0
//                        || etguests.getText().toString().length() == 0){
//                    final iOSDialog iOSDialog = new iOSDialog(getContext());
//                    iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                    iOSDialog.setSubtitle("Please enter capacity");
//                    iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();
//                }
//                else{
//                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        public void priceCalculation(){
            final DecimalFormat priceFormat = new DecimalFormat("0");
            float price = 0;
            String[] selectedWeeks = strWeeks.split(",");

            if(!BannersNewAdapter.galleryList.get(bannerPos).getResortType().equalsIgnoreCase("banquet hall")){
                for(int a = 0; a < selectedWeeks.length; a++){

                    if(isManSelected) {
                        if(isWomanSelected && BannersNewAdapter.galleryList.get(bannerPos).getResortType().equalsIgnoreCase("wedding hall")){
                            try {
                                if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSatFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSunFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getMonFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getTueFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getWedFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getThursFamilyPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getFriFamilyPrice());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            try {
                                if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSatMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSunMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getMonMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getTueMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getWedMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getThursMenPrice());
                                } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getFriMenPrice());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if(isWomanSelected){
                        try {
                            if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSatWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSunWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getMonWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getTueWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getWedWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getThursWomenPrice());
                            } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getFriWomenPrice());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                if(isKitchenSelected) {
                    price = price + Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSatKitchenPrice());
                }
            }
            else {
                if(isManSelected && isWomanSelected){
                    for(int a = 0; a < selectedWeeks.length; a++){
                        try {
                            if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSatMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSatWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSunMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSunWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getMonMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getMonWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getTueMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getTueWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getWedMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getWedWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getThursMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getThursWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getFriMenPrice()) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getFriWomenPrice()) * Integer.parseInt(Constants.noOfWomen)));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if (isManSelected){
                    for(int a = 0; a < selectedWeeks.length; a++){
                        try {
                            if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSatMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSunMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getMonMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getTueMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getWedMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getThursMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getFriMenPrice()) * Integer.parseInt(Constants.noOfMen));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(isWomanSelected){
                    for(int a = 0; a < selectedWeeks.length; a++){
                        try {
                            if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSatWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getSunWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getMonWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getTueWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getWedWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getThursWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                            else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                price = price + (Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getFriWomenPrice()) * Integer.parseInt(Constants.noOfWomen));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
//            float rate = Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getRate());
//            float discount = Float.parseFloat(BannersNewAdapter.galleryList.get(bannerPos).getDiscount());
            BannersNewAdapter.galleryList.get(bannerPos).setActualPrice(priceFormat.format(price * rate));
            float discPrice;
            if(discount>0) {
                float actualPrice = price * rate;
                discPrice = actualPrice - (actualPrice * (discount/100));
                BannersNewAdapter.galleryList.get(bannerPos).setDiscountedPrice(priceFormat.format(discPrice));
            }
            else{
                BannersNewAdapter.galleryList.get(bannerPos).setDiscountedPrice(priceFormat.format(price * rate));
            }
        }

        public class CheckAvailablity extends AsyncTask<String, Integer, String> {
            ProgressDialog pDialog;
            String networkStatus;
            ACProgressFlower dialog;
            String response;

            InputStream inputStream = null;
            @Override
            protected void onPreExecute() {
                networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                dialog = new ACProgressFlower.Builder(getContext())
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY).build();
                dialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    try {
                        try {
                            // 1. create HttpClient
                            HttpClient httpclient = new DefaultHttpClient();

                            // 2. make POST request to the given URL
                            HttpPost httpPost = new HttpPost(Constants.CHECK_AVAILABILITY_URL);

                            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                            // ObjectMapper mapper = new ObjectMapper();
                            // json = mapper.writeValueAsString(person);

                            // 5. set json to StringEntity
                            StringEntity se = new StringEntity(params[0], "UTF-8");

                            // 6. set httpPost Entity
                            httpPost.setEntity(se);

                            // 7. Set some headers to inform server about the type of the content
                            httpPost.setHeader("Accept", "application/json");
                            httpPost.setHeader("Content-type", "application/json");

                            // 8. Execute POST request to the given URL
                            HttpResponse httpResponse = httpclient.execute(httpPost);

                            // 9. receive response as inputStream
                            inputStream = httpResponse.getEntity().getContent();

                            // 10. convert inputstream to string
                            if(inputStream != null) {
                                response = convertInputStreamToString(inputStream);
                                Log.i("TAGs", "user response:" + response);
                                return response;
                            }
                        } catch (Exception e) {
                            Log.d("InputStream", e.getLocalizedMessage());
                        }
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    Log.i("TAG", "user response:" + response);
                    return response;
                }else {
                    return "no internet";
                }
            }


            @Override
            protected void onPostExecute(String result) {

                if (result != null) {
                    if(result.equalsIgnoreCase("no internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }else{
                        if(result.equals("")){
                            Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        }else {

                            try {
                                JSONObject jo= new JSONObject(result);
                                JSONArray jsonArray = jo.getJSONArray("success");
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String success = jsonObject.getString("IsAvailable");
//                                String success = jo.getString("Success");

                                if(success.equalsIgnoreCase("true")) {
                                    rate = Float.parseFloat(jsonObject.getString("Rate"));
                                    discount = Float.parseFloat(jsonObject.getString("Discount"));
                                    priceCalculation();
                                    Intent intent = new Intent(getActivity(), ResortDetailsActivity.class);
                                    intent.putExtra("array", galleryList);
                                    intent.putExtra("position", bannerPos);
                                    intent.putExtra("class", "home");
                                    startActivity(intent);
                                    getDialog().dismiss();
                                }
                                else if (success.equalsIgnoreCase("false")){
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                                    no.setVisibility(View.GONE);
                                    vert.setVisibility(View.GONE);

                                    if(language.equalsIgnoreCase("En")) {
                                        title.setText(getResources().getString(R.string.app_name));
                                        yes.setText(getResources().getString(R.string.str_btn_ok));
                                        desc.setText("Property is not available for these dates.");
                                    }
                                    else{
                                        title.setText(getResources().getString(R.string.app_name_ar));
                                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                        desc.setText("القاعة غير متوفرة في هذا التاريخ");
                                    }

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth*0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }else {
                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                }
                if(dialog != null) {
                    dialog.dismiss();
                }
                super.onPostExecute(result);
            }
        }

        private static String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
    }

}
