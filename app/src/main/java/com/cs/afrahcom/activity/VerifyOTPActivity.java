package com.cs.afrahcom.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

/**
 * Created by cs android on 07-02-2017.
 */

public class VerifyOTPActivity extends Activity implements OTPListener {

    String mobilenumber;
    TextView mMobile_tv;
    EditText motp_et;
    Button verify_btn;
    String userOTP,serverOTP,userData;
    private String response = null;
    SharedPreferences userPrefs, hostPrefs;
    SharedPreferences.Editor userPrefEditor, hostPrefEditor;
    String mWhichClass;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_verify_otp);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_verify_otp_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostPrefEditor  = hostPrefs.edit();

        mMobile_tv = (TextView) findViewById(R.id.tv_verify_mobile_number);
        verify_btn = (Button) findViewById(R.id.btn_verify_otp);
        motp_et = (EditText) findViewById(R.id.et_otp);
        OtpReader.bind(this,"AfrahcomAD");

//        Checking for SMS read permission
        if(!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(VerifyOTPActivity.this, "SMS read permission denied, Please enter OTP manually", Toast.LENGTH_LONG).show();
        }

//        Getting intent data
        serverOTP = getIntent().getStringExtra("OTP");
        userData = getIntent().getStringExtra("user_data");
        mobilenumber = getIntent().getExtras().getString("phone_number").replace(" ","");
        mWhichClass = getIntent().getStringExtra("screen");

        mMobile_tv.setText("+"+mobilenumber);

        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userOTP = motp_et.getText().toString();
                if(userOTP.length() == 0){
                    motp_et.setError("Please enter otp");
                    motp_et.requestFocus();
                }
                else if(!serverOTP.equals(userOTP)){
                    motp_et.setError("Enter correct otp");
                    motp_et.requestFocus();
                }
                else{
                    if(getIntent().getStringExtra("screen").equalsIgnoreCase("forgot")){
                        Intent i = new Intent(VerifyOTPActivity.this, ResetPasswordActivity.class);
                        i.putExtra("mobile", mobilenumber);
                        startActivity(i);
                        finish();
                    }
                    else if(getIntent().getStringExtra("screen").equalsIgnoreCase("hostforgot")){
                        Intent i = new Intent(VerifyOTPActivity.this, HostResetPasswordActivity.class);
                        i.putExtra("mobile", mobilenumber);
                        startActivity(i);
                        finish();
                    }
                    else if(getIntent().getStringExtra("screen").equalsIgnoreCase("user")) {
                        new RegisterUser().execute(userData);
                    }else if(getIntent().getStringExtra("screen").equalsIgnoreCase("host")) {
                        Log.i("TAG","host resgistration");
                        new RegisterHost().execute(userData);
//                        setResult(RESULT_OK);
//                        finish();
                    }
                }
            }
        });
    }

    @Override
    public void otpReceived(String smsText) {
        //Do whatever you want to do with the text
        String[] str = null;
        try {
            smsText = smsText.replace("'","");
            str = smsText.split(":");
        } catch (Exception e) {
            e.printStackTrace();
        }
        motp_et.setText(""+str[1]);
        motp_et.setSelection(motp_et.length());
        if (str[1].equals(serverOTP)) {
            if(getIntent().getStringExtra("screen").equalsIgnoreCase("forgot")){
                Intent i = new Intent(VerifyOTPActivity.this, ResetPasswordActivity.class);
                i.putExtra("mobile", mobilenumber);
                startActivity(i);
                finish();
            }
            else if(getIntent().getStringExtra("screen").equalsIgnoreCase("hostforgot")){
                Intent i = new Intent(VerifyOTPActivity.this, HostResetPasswordActivity.class);
                i.putExtra("mobile", mobilenumber);
                startActivity(i);
                finish();
            }
            else if(getIntent().getStringExtra("screen").equalsIgnoreCase("user")) {
                new RegisterUser().execute(userData);
            }else if(getIntent().getStringExtra("screen").equalsIgnoreCase("host")) {
                Log.i("TAG","host resgistration");
                new RegisterHost().execute(userData);
//                        setResult(RESULT_OK);
//                        finish();
            }
        }
    }

    public class RegisterUser extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOTPActivity.this);
            dialog = new ACProgressFlower.Builder(VerifyOTPActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.REGISTRATION_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(VerifyOTPActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("succcess");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String MiddleName = jo1.getString("MiddleName");
                                String FamilyName = jo1.getString("FamilyName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                String password = jo1.getString("Password");
                                String cityId = jo1.getString("CityId");
//                                boolean isVerified = jo1.getBoolean("IsVerified");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("MiddleName", FamilyName);
                                    jsonObject.put("FamilyName", "");
                                    jsonObject.put("CityId", cityId);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("Password", password);
                                    jsonObject.put("language", language);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);
                                    userPrefEditor.commit();

                                    userPrefEditor.putString("login_status","loggedin");
                                    userPrefEditor.commit();
                                    setResult(RESULT_OK);
                                    finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
//                                final iOSDialog iOSDialog = new iOSDialog(VerifyOTPActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(result);
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VerifyOTPActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText(result);
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText(result);
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(VerifyOTPActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }
    }


    public class RegisterHost extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOTPActivity.this);
            dialog = new ACProgressFlower.Builder(VerifyOTPActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.HOST_REGISTRATION_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(VerifyOTPActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
//
                            try {
                                JSONObject jo= new JSONObject(result);

                                try {
                                    JSONArray ja = jo.getJSONArray("Success");
                                    JSONObject jo1 = ja.getJSONObject(0);
                                    String companyName = jo1.getString("CompanyName");
                                    String contactPerson = jo1.getString("ContactPerson");
                                    String email = jo1.getString("Email");
                                    String landline = jo1.getString("LandlineNo");
                                    String password = jo1.getString("Password");
                                    String mobile = jo1.getString("MobileNo");
                                    String hostID = jo1.getString("HostId");

                                    try {
                                        JSONObject parent = new JSONObject();
                                        JSONObject jsonObject = new JSONObject();
                                        JSONArray jsonArray = new JSONArray();
                                        jsonArray.put("lv1");
                                        jsonArray.put("lv2");

                                        jsonObject.put("CompanyName", companyName);
                                        jsonObject.put("ContactPerson", contactPerson);
                                        jsonObject.put("MobileNo", mobile);
                                        jsonObject.put("Email", email);
                                        jsonObject.put("LandlineNo", landline);
                                        jsonObject.put("Password", password);
                                        jsonObject.put("host_details", jsonArray);
                                        parent.put("profile", jsonObject);
                                        Log.d("output", parent.toString());
                                        hostPrefEditor.putString("host_profile", parent.toString());
                                        hostPrefEditor.putString("hostId", hostID);
                                        hostPrefEditor.commit();

                                        hostPrefEditor.putString("login_status", "loggedin");
                                        hostPrefEditor.commit();
                                        setResult(RESULT_OK);
                                        finish();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }catch (JSONException je){
//                                final iOSDialog iOSDialog = new iOSDialog(VerifyOTPActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(result);
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VerifyOTPActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                                    no.setVisibility(View.GONE);
                                    vert.setVisibility(View.GONE);

                                    if(language.equalsIgnoreCase("En")) {
                                        title.setText(getResources().getString(R.string.app_name));
                                        yes.setText(getResources().getString(R.string.str_btn_ok));
                                        desc.setText(result);
                                    }
                                    else{
                                        title.setText(getResources().getString(R.string.app_name_ar));
                                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                        desc.setText(result);
                                    }

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth*0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(VerifyOTPActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
