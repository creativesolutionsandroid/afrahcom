package com.cs.afrahcom.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.cs.afrahcom.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 22-11-2017.
 */

public class HostProfileActivity extends AppCompatActivity {

    private EditText etCompanyName, etContactPerson, etLandline, etEmail, etMobile;
    TextView mEditProfile, mChangePassword;
    String userId;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences hostPrefs;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_host_profile);
        }
        else{
            setContentView(R.layout.activity_host_profile_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
//        userId = hostPrefs.getString("userId", null);

        etCompanyName = (EditText) findViewById(R.id.et_comapny_name);
        etContactPerson = (EditText) findViewById(R.id.et_conatct_person);
        etLandline = (EditText) findViewById(R.id.et_landline);
        etEmail = (EditText) findViewById(R.id.et_email);
        etMobile = (EditText) findViewById(R.id.et_mobile);

        mEditProfile = (TextView) findViewById(R.id.editprofile);
        mChangePassword = (TextView) findViewById(R.id.changePassword);

        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HostProfileActivity.this, HostEditProfileActivity.class));
            }
        });

        mChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HostProfileActivity.this, HostChangePasswordActivity.class));
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String response = hostPrefs.getString("host_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                etCompanyName.setText(userObjuect.getString("CompanyName"));
                etContactPerson.setText(userObjuect.getString("ContactPerson"));
                etMobile.setText("+"+userObjuect.getString("MobileNo"));
                etEmail.setText(userObjuect.getString("Email"));
                etLandline.setText("+"+userObjuect.getString("LandlineNo"));
            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }
}
