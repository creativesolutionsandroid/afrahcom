package com.cs.afrahcom.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.Facilities;
import com.cs.afrahcom.models.ImagesModel;
import com.cs.afrahcom.models.ResortDetailsFiltered;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

import static com.cs.afrahcom.fragment.ResortSearchFragment.currentLatitude;
import static com.cs.afrahcom.fragment.ResortSearchFragment.currentLongitude;

/**
 * Created by CS on 16-10-2017.
 */

public class OrderTrackingActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener{

    TextView tvResortName, tvResortAddress, tvTodaydate, tvTodayMonth, tvTodayWeek, tvResortPrice, tvCountdownTitle;
    ImageView resortImage;
    Toolbar toolbar;
    ArrayList<ResortDetailsFiltered> resortDetailsArrayList = new ArrayList<>();
    int position;
    private GoogleMap map;
    TextView tvCancel, tvVisited;
    CountDownTimer countDownTimer2;
    TextView tvTimeLeft;
    ProgressBar progressBar;
    String bookingID;
    String userId;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences userPrefs;
    long remainingMillis = 60 * 2880 * 1000;
    LinearLayout propertyLayout;
    AlertDialog customDialog;

    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_pending_reservation);
        }
        else{
            setContentView(R.layout.activity_pending_reservation_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bookingID = getIntent().getStringExtra("bookingID");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        tvResortName = (TextView) findViewById(R.id.resort_name);
        tvResortAddress = (TextView) findViewById(R.id.resortAddress);
        tvResortPrice = (TextView) findViewById(R.id.resortPrice);
        tvTodaydate = (TextView) findViewById(R.id.tv_today_date);
        tvTodayMonth = (TextView) findViewById(R.id.tv_today_month);
        tvTodayWeek = (TextView) findViewById(R.id.tv_today_week);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        tvVisited = (TextView) findViewById(R.id.tv_visited);
        tvTimeLeft = (TextView) findViewById(R.id.timeLeft);
        tvCountdownTitle = (TextView) findViewById(R.id.day_countdown_text);

        propertyLayout = (LinearLayout) findViewById(R.id.propertyLayout);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        resortImage = (ImageView) findViewById(R.id.resort_image);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(OrderTrackingActivity.this);

        new TrackOrder().execute(Constants.TRACK_ORDER_URL+bookingID+"&userid="+userId);

        tvCancel.setOnClickListener(this);
        tvVisited.setOnClickListener(this);
        propertyLayout.setOnClickListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.propertyLayout:
                Intent intent12 = new Intent(OrderTrackingActivity.this, ResortDetailsActivity.class);
                intent12.putExtra("array",resortDetailsArrayList);
                intent12.putExtra("position",0);
                intent12.putExtra("class","tracking");
                startActivity(intent12);
                break;
            case R.id.tv_cancel:
//                Alert Dialog
//                final iOSDialog iOSDialog = new iOSDialog(OrderTrackingActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Do you want to cancel the Booking?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل تريد إلغاء الحجز؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        JSONObject mainObj = new JSONObject();
//                        try {
//                            JSONObject bookingObj = new JSONObject();
//                            bookingObj.put("BookingId",bookingID);
//                            mainObj.put("CancelDetails",bookingObj);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        Log.i("TAG",""+mainObj.toString());
//                        new cancelOrder().execute(mainObj.toString());
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderTrackingActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                String title, positive, negative;
                if(language.equalsIgnoreCase("En")){
                    title = "Do you want to cancel the Booking?";
                    positive = getResources().getString(R.string.str_btn_yes);
                    negative = getResources().getString(R.string.str_btn_no);
                }
                else{
                    title = "هل تريد إلغاء الحجز؟";
                    positive = getResources().getString(R.string.str_btn_yes_ar);
                    negative = getResources().getString(R.string.str_btn_no_ar);
                }

                desc.setText(title);
                yes.setText(positive);
                no.setText(negative);

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                        JSONObject mainObj = new JSONObject();
                        try {
                            JSONObject bookingObj = new JSONObject();
                            bookingObj.put("BookingId",bookingID);
                            mainObj.put("CancelDetails",bookingObj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG",""+mainObj.toString());
                        new cancelOrder().execute(mainObj.toString());
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                break;

            case R.id.tv_visited:
                Intent intent = new Intent(OrderTrackingActivity.this, ReservationActivity.class);
                intent.putExtra("array",resortDetailsArrayList);
                intent.putExtra("position",position);
                intent.putExtra("class","pending");
                startActivity(intent);
                break;
        }
    }

    public class TrackOrder extends AsyncTask<String, Integer, String> {
        String  networkStatus;
        ACProgressFlower dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(OrderTrackingActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONObject successObj = jo.getJSONObject("Success");
                            JSONObject newObj = successObj.getJSONObject("New");

                            final DecimalFormat priceFormat = new DecimalFormat("##,##,###");
                            resortDetailsArrayList.clear();
                            ResortDetailsFiltered booking = new ResortDetailsFiltered();
                            booking.setBookingID(newObj.getString("BookingId"));
                            booking.setStartDate(newObj.getString("StartDate"));
                            booking.setSubTotal((newObj.getString("SubtotalPrice")));
                            booking.setTotal((newObj.getString("Price")));
                            booking.setNetAmount((newObj.getString("TotalPrice")));
                            booking.setSubvat((newObj.getString("VatPrice")));
                            booking.setDisc((newObj.getString("DiscountPrice")));
                            booking.setEndDate(newObj.getString("EndDate"));
                            booking.setResortId(newObj.getString("ResortId"));
                            booking.setResortName(newObj.getString("ResortName"));
                            booking.setResortNameAr(newObj.getString("ResortName_Ar"));
                            booking.setDesc(newObj.getString("Description"));
                            booking.setDescAr(newObj.getString("Description_Ar"));
                            booking.setResortAddress(newObj.getString("ResortAddress"));
                            booking.setResortImage(newObj.getString("ImageName"));
                            booking.setSource(newObj.getString("Source"));
                            booking.setLatitude(newObj.getString("Latitude"));
                            booking.setLongitude(newObj.getString("Longitude"));
                            booking.setRating(newObj.getString("Rating"));
                            booking.setAfrahcomAdvance(newObj.getString("AdvanceAmount"));
                            booking.setApproved(newObj.getString("AfrahcomApproved"));

                            Location me   = new Location("");
                            Location dest = new Location("");

                            me.setLatitude(currentLatitude);
                            me.setLongitude(currentLongitude);

                            dest.setLatitude(Double.parseDouble(newObj.getString("Latitude")));
                            dest.setLongitude(Double.parseDouble(newObj.getString("Longitude")));

                            final DecimalFormat distanceFormat = new DecimalFormat("0");
                            float dist = (me.distanceTo(dest))/1000;

                            booking.setDistance(distanceFormat.format(dist));


                            JSONArray facilitiesArray = successObj.getJSONArray("Resortandfacilities");
                            JSONObject facObj = facilitiesArray.getJSONObject(0);
                            JSONArray facility = facObj.getJSONArray("FacilityDetails");
                            ArrayList<Facilities> resortFacilities = new ArrayList<>();
                            for (int j = 0; j<facility.length(); j++){
                                Facilities facilities = new Facilities();
                                JSONObject jsonObject1 = facility.getJSONObject(j);

                                facilities.setFacilityNameAr(jsonObject1.getString("FacilityName_Ar"));
                                facilities.setFacilityName(jsonObject1.getString("FacilityName"));
                                facilities.setFacilityImage(jsonObject1.getString("FacilityImage"));

                                resortFacilities.add(facilities);
                            }
                            booking.setResortFacilities(resortFacilities);

                            JSONArray ImagesArray = successObj.getJSONArray("ResortImages");
                            JSONObject imgObj = ImagesArray.getJSONObject(0);
                            JSONArray Image = imgObj.getJSONArray("Resortimage");
                            ArrayList<ImagesModel> imagesArrayList = new ArrayList<>();
                            for(int j = 0; j<Image.length(); j++){
                                ImagesModel imagesModel = new ImagesModel();
                                JSONObject imagesObject = Image.getJSONObject(j);
                                imagesModel.setImageName(imagesObject.getString("ImageName"));
                                imagesModel.setSource(imagesObject.getString("Source"));
                                imagesArrayList.add(imagesModel);
                            }
                            booking.setResortImagesArray(imagesArrayList);

                            resortDetailsArrayList.add(booking);
                            position = 0;

                            calculateMinutesLeft(newObj.getString("ReservationDate"));

                            if(language.equalsIgnoreCase("En")) {
                                tvResortName.setText(newObj.getString("ResortName"));
                            }
                            else{
                                tvResortName.setText(newObj.getString("ResortName_Ar"));
                            }
                            tvResortAddress.setText(newObj.getString("ResortAddress"));

                            if(newObj.getString("SubtotalPrice").equalsIgnoreCase("0.0")) {
                                tvResortPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(newObj.getString("Price")))));
                            }
                            else{
                                tvResortPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(newObj.getString("SubtotalPrice")))));
                            }

                            if (newObj.getString("Source").equals("1")) {
                                Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + newObj.getString("ImageName")).into(resortImage);
                            }
                            else {
                                Glide.with(getApplicationContext()).load(Constants.SOURCE2_URL + newObj.getString("ImageName")).into(resortImage);
                            }

                            if(map!=null){
                                LatLng latLng = new LatLng(Double.parseDouble(newObj.getString("Latitude")), Double.parseDouble(newObj.getString("Longitude")));
                                MarkerOptions markerOptions = new MarkerOptions();
                                map.clear();
                                // Show the current location in Google Map
                                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                // Zoom in the Google Map
                                map.animateCamera(CameraUpdateFactory.zoomTo(9.0f));

                                markerOptions.position(latLng)
                                        .title(resortDetailsArrayList.get(position).getResortName())
                                        .snippet(newObj.getString("ResortAddress"))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
                                map.addMarker(markerOptions);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }
            }else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    public class cancelOrder extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(OrderTrackingActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.CANCEL_ORDER_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            if(language.equalsIgnoreCase("En")) {
                                Toast.makeText(getApplicationContext(), "Booking Cancelled", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "تم إلغاء الحجز", Toast.LENGTH_SHORT).show();
                            }
                            Intent intent = new Intent(OrderTrackingActivity.this, OrderHistoryActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra("class", "reservation");
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            } else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public void calculateMinutesLeft(String reservationDate){
        Date bookedDate = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        String[] reservDate = reservationDate.split("T");
        String checkInDate = reservDate[0]+" "+reservDate[1];

        try {
            bookedDate = DateFormat.parse(checkInDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long startMillis = bookedDate.getTime();
        long currentMillis = calendar.getTimeInMillis();
        long endMillis = startMillis + (60 * 2880 * 1000);  /*48 hours*/
        remainingMillis = endMillis - currentMillis;
        startTimer();

//        Setting Reservation Date
        calendar.setTime(bookedDate);
        if((calendar.get(Calendar.DATE)) <10) {
            tvTodaydate.setText("0" + calendar.get(Calendar.DATE));
        }
        else{
            tvTodaydate.setText("" + calendar.get(Calendar.DATE));
        }
        tvTodayMonth.setText(MONTHS[calendar.get(Calendar.MONTH)]);
        tvTodayWeek.setText(WEEKS[(calendar.get(Calendar.DAY_OF_WEEK)-1)]);
    }

    public void startTimer(){
        countDownTimer2 = new CountDownTimer(remainingMillis, 1000) {
            // 1000 means, onTick function will be called at every 1000 milliseconds

            @Override
            public void onTick(long leftTimeInMilliseconds) {

                if(leftTimeInMilliseconds<(60*1440*1000)){
                    if(language.equalsIgnoreCase("En")) {
                        tvCountdownTitle.setText("Last Day");
                    }
                    else{
                        tvCountdownTitle.setText("اليوم الأخير");
                    }
                }

                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(leftTimeInMilliseconds),
                        TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));

                tvTimeLeft.setText(Constants.convertToArabic(hms));
            }
            @Override
            public void onFinish() {
                tvTimeLeft.setText("00:00:00");
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(OrderTrackingActivity.this, OrderPendingUserActivity.class));
        finish();
    }
}
