package com.cs.afrahcom.activity;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.GalleryAdapter;
import com.cs.afrahcom.adapter.ImagesScrollAdapter;
import com.cs.afrahcom.adapter.ResortDetailsFilterAdapter;
import com.cs.afrahcom.fragment.ResortSearchFragment;
import com.cs.afrahcom.models.ResortDetailsFiltered;
import com.cs.afrahcom.widgets.CustomGridView;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;
import com.github.demono.AutoScrollViewPager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by CS on 26-09-2017.
 */

public class ResortDetailsActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    Toolbar toolbar;
    TextView tvReserve, tvResortName, tvResortAddress, tvResortDistance, tvResortPrice;
    private String firstName, mMiddleNameStr, mMiddleName, mEmailStr, mMobileStr;
    TextView tvCheckinDate, tvCheckinMonth, tvCheckinWeek, tvCheckoutMonth, tvCheckoutDate, tvCheckoutWeek, tvNumofDays;
    ImageView imgResortCoverPic;
    RatingBar ratingBar;
    SharedPreferences userPrefs;
    String isVatApplicable;
    SharedPreferences.Editor userPrefEditor;
    private static final int LOGIN_REQUEST = 1;
    private static final int CATERING_REQUEST = 2;
    ArrayList<ResortDetailsFiltered> resortDetailsArrayList = new ArrayList<>();
    //    ArrayList<Banners> bannersArrayList = new ArrayList<>();
    int position;
    //    Boolean isBanners = false;
    LinearLayout infoLayout, mapLayout, photosLayout, reserveLayout, horizontalLayout, chatLayout;
    private GoogleMap map;
    RelativeLayout mapFragment, infoDetailsLayout;
    RelativeLayout layoutCheckin, layoutCheckout;
    CustomGridView mFacilitiesGrid;
    CustomGridView mGalleryGrid;
    RelativeLayout nameLayout;
    public static RelativeLayout imagesLayout;
    //    public static GridView mHorizontalGrid;
    GalleryAdapter mGalleryAdapter;
    ImagesScrollAdapter mHorizontalAdapter;
    ResortDetailsFilterAdapter filterAdapter;
    AlertDialog alertDialog2;
    public static Boolean is48HrsSelected = false, isBookNowSelected = false;
    CircleIndicator defaultIndicator;
    AutoScrollViewPager defaultViewpager;
    RelativeLayout approvedLayout;
    Context mContext;
    ScrollView scrollView, scrollView1;
    String userId, userName, mobileNo;
    SharedPreferences languagePrefs;
    String language;
    AlertDialog customDialog;
    private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
    TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
    TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
    String mCheckInDateStr = null, mCheckOutDateStr = null;
    Date mCheckInDate = null, mCheckOutDate = null;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
    float total = 0, subTotal = 0, subvat = 0, disc = 0, netAmount = 0;
    final DecimalFormat priceFormat = new DecimalFormat("##,##,###");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_resort_details);
        } else {
            setContentView(R.layout.activity_resort_details_ar);
        }
        mContext = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        is48HrsSelected = false;
        isBookNowSelected = false;

        if (getIntent().getStringExtra("class").equals("map")) {
            resortDetailsArrayList = (ArrayList<ResortDetailsFiltered>) getIntent().getSerializableExtra("array");
            String title = getIntent().getStringExtra("title");
            for (int i = 0; i < resortDetailsArrayList.size(); i++) {
                if (title.equals(resortDetailsArrayList.get(i).getResortName())) {
                    position = i;
                }
            }
        } else {
            position = getIntent().getIntExtra("position", 0);
            resortDetailsArrayList = (ArrayList<ResortDetailsFiltered>) getIntent().getSerializableExtra("array");
        }

        try {
            isVatApplicable = resortDetailsArrayList.get(position).getVatAvailability();
        } catch (Exception e) {
            e.printStackTrace();
            isVatApplicable = "true";
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView1 = (ScrollView) findViewById(R.id.scrollView1);
        ratingBar = (RatingBar) findViewById(R.id.rating_bar);

        tvReserve = (TextView) findViewById(R.id.tv_reserve);
        tvResortName = (TextView) findViewById(R.id.resortName);
        tvResortAddress = (TextView) findViewById(R.id.resortAddress);
        tvResortDistance = (TextView) findViewById(R.id.resortDistance);
        tvResortPrice = (TextView) findViewById(R.id.resortPrice);

        tvCheckinDate = (TextView) findViewById(R.id.tv_checkin_date);
        tvCheckinMonth = (TextView) findViewById(R.id.tv_checkin_month);
        tvCheckinWeek = (TextView) findViewById(R.id.tv_checkin_week);
        tvCheckoutDate = (TextView) findViewById(R.id.tv_checkout_date);
        tvCheckoutMonth = (TextView) findViewById(R.id.tv_checkout_month);
        tvCheckoutWeek = (TextView) findViewById(R.id.tv_checkout_week);
        tvNumofDays = (TextView) findViewById(R.id.tv_number_of_days);
        approvedLayout = (RelativeLayout) findViewById(R.id.approved_layout);
        nameLayout = (RelativeLayout) findViewById(R.id.nameLayout);

        mapFragment = (RelativeLayout) findViewById(R.id.map_fragment);
        infoDetailsLayout = (RelativeLayout) findViewById(R.id.infoDetailsLayout);
        layoutCheckin = (RelativeLayout) findViewById(R.id.layout_checkin);
        layoutCheckout = (RelativeLayout) findViewById(R.id.layout_checkout);

        infoLayout = (LinearLayout) findViewById(R.id.layoutInfo);
        mapLayout = (LinearLayout) findViewById(R.id.layoutMap);
        photosLayout = (LinearLayout) findViewById(R.id.layoutPhotos);
        reserveLayout = (LinearLayout) findViewById(R.id.reserve_layout);
        chatLayout = (LinearLayout) findViewById(R.id.layoutChat);

        imgResortCoverPic = (ImageView) findViewById(R.id.resort_image);
        imagesLayout = (RelativeLayout) findViewById(R.id.imagesLayout);

        mFacilitiesGrid = (CustomGridView) findViewById(R.id.facilitiesGridView);
        mGalleryGrid = (CustomGridView) findViewById(R.id.photosGrid);
        defaultViewpager = (AutoScrollViewPager) findViewById(R.id.viewPager);
        defaultIndicator = (CircleIndicator) findViewById(R.id.indicator);


        if (getIntent().getStringExtra("class").equals("tracking")) {
            reserveLayout.setVisibility(View.GONE);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            String startDate = resortDetailsArrayList.get(position).getStartDate();
            String endDate = resortDetailsArrayList.get(position).getEndDate();
            String[] dateStr1 = startDate.split("T");
            String[] dateStr2 = endDate.split("T");
            String[] FinaldateStr1 = dateStr1[0].split("-");
            String[] FinaldateStr2 = dateStr2[0].split("-");
            Date date1 = null;
            Date date2 = null;
            try {
                date1 = dateFormat.parse(dateStr1[0]);
                date2 = dateFormat.parse(dateStr2[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            tvCheckinDate.setText(FinaldateStr1[2]);
            tvCheckoutDate.setText(FinaldateStr2[2]);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);
            tvCheckinMonth.setText(MONTHS[calendar.get(Calendar.MONTH)]);
            tvCheckinWeek.setText(WEEKS[(calendar.get(Calendar.DAY_OF_WEEK) - 1)]);

            calendar.setTime(date2);
            tvCheckoutMonth.setText(MONTHS[calendar.get(Calendar.MONTH)]);
            tvCheckoutWeek.setText(WEEKS[(calendar.get(Calendar.DAY_OF_WEEK) - 1)]);

            long diff = date2.getTime() - date1.getTime();
            long NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)) + 1;
            tvNumofDays.setText("" + NumofDaysStr);

            try {
                if (resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("1")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
                } else if (resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("2")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE2_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(position).getResortImage())
                        .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
            }
            tvResortPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getTotal())))));
        } else {
            tvCheckinDate.setText(ResortSearchFragment.strCheckInDate);
            tvCheckinMonth.setText(ResortSearchFragment.strCheckInMonth);
            tvCheckinWeek.setText(ResortSearchFragment.strCheckInWeek);
            tvCheckoutDate.setText(ResortSearchFragment.strCheckoutDate);
            tvCheckoutMonth.setText(ResortSearchFragment.strCheckoutMonth);
            tvCheckoutWeek.setText(ResortSearchFragment.strCheckoutWeek);
            tvNumofDays.setText(ResortSearchFragment.strNumDays);
            tvResortPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getDiscountedPrice())))));
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(ResortDetailsActivity.this);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getApplicationContext().getResources().getColor(R.color.ratingBarColorindetails), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(getApplicationContext().getResources().getColor(R.color.ratingBarColorindetails), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(getApplicationContext().getResources().getColor(R.color.ratingBarColorindetails), PorterDuff.Mode.SRC_ATOP);

        if (language.equalsIgnoreCase("En")) {
            tvResortName.setText(resortDetailsArrayList.get(position).getResortName());
        } else {
            tvResortName.setText(resortDetailsArrayList.get(position).getResortNameAr());
        }
        tvResortAddress.setText(resortDetailsArrayList.get(position).getResortAddress());
        tvResortDistance.setText(resortDetailsArrayList.get(position).getDistance() + " KM");

        if (resortDetailsArrayList.get(position).getRating().equals("0")) {
            ratingBar.setRating(5);
        } else {
            ratingBar.setRating(Float.parseFloat(resortDetailsArrayList.get(position).getRating()));
        }

        try {
            if (resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("1")) {
                Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                        .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
            } else if (resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("2")) {
                Glide.with(getApplicationContext()).load(Constants.SOURCE2_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                        .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        filterAdapter = new ResortDetailsFilterAdapter(ResortDetailsActivity.this, language, resortDetailsArrayList.get(position).getResortFacilities());
        mFacilitiesGrid.setAdapter(filterAdapter);

        if (language.equalsIgnoreCase("Ar")){
            mFacilitiesGrid.setRotationY(180);
        }

        mGalleryAdapter = new GalleryAdapter(ResortDetailsActivity.this,resortDetailsArrayList.get(position).getResortImagesArray());
        mGalleryGrid.setAdapter(mGalleryAdapter);

        mHorizontalAdapter = new ImagesScrollAdapter(ResortDetailsActivity.this,resortDetailsArrayList.get(position).getResortImagesArray());
        defaultViewpager.setAdapter(mHorizontalAdapter);

        try {
            if (resortDetailsArrayList.get(position).getApproved().equals("true")){
                approvedLayout.setVisibility(View.VISIBLE);
            }
            else{
                approvedLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mGalleryGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                imagesLayout.setVisibility(View.VISIBLE);
                defaultViewpager.setCurrentItem(i);
            }
        });

        scrollView1.fullScroll(ScrollView.FOCUS_UP);
        nameLayout.setOnClickListener(this);
        tvReserve.setOnClickListener(this);
        mapLayout.setOnClickListener(this);
        infoLayout.setOnClickListener(this);
        photosLayout.setOnClickListener(this);
        chatLayout.setOnClickListener(this);
        layoutCheckin.setOnClickListener(this);
        layoutCheckout.setOnClickListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_checkin:
//                checkInDatePicker();
                break;

            case R.id.layout_checkout:
//                checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                break;

            case R.id.layoutChat:
                if(userPrefs.getString("login_status", "").contains("loggedin")) {
                    FreshchatConfig freshchatConfig = new FreshchatConfig("52210f92-7d7c-4193-8ef5-e91e15fec6b6", "8cf1a49c-234b-4132-9e76-d79d7d1972e4");
                    freshchatConfig.setCameraCaptureEnabled(false);
                    freshchatConfig.setGallerySelectionEnabled(false);
                    Freshchat.getInstance(getApplicationContext()).init(freshchatConfig);
                    FreshchatUser freshUser = Freshchat.getInstance(getApplicationContext()).getUser();
                    freshUser.setFirstName(firstName);
                    freshUser.setLastName(mMiddleName);
                    freshUser.setEmail(mEmailStr);
                    freshUser.setPhone("+966", mMobileStr);

                    //Call setUser so that the user information is synced with Freshchat's servers
                    Freshchat.getInstance(getApplicationContext()).setUser(freshUser);
                    Freshchat.showConversations(getApplicationContext());
                }
                else {
                    startActivityForResult(new Intent(ResortDetailsActivity.this, SignInActivity.class), LOGIN_REQUEST);
                }
                break;
            case R.id.layoutInfo:
                infoLayout.setBackgroundColor(getResources().getColor(R.color.reservationselected));
                mapLayout.setBackgroundColor(getResources().getColor(R.color.reservationunselected));
                photosLayout.setBackgroundColor(getResources().getColor(R.color.reservationunselected));
                infoDetailsLayout.setVisibility(View.VISIBLE);
                mapFragment.setVisibility(View.GONE);
                mGalleryGrid.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                scrollView1.setVisibility(View.VISIBLE);
                scrollView1.fullScroll(ScrollView.FOCUS_UP);
                break;

            case R.id.nameLayout:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ResortDetailsActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                int layout = R.layout.dialog_caterer_description;
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(true);

                TextView title = (TextView) dialogView.findViewById(R.id.name);
                TextView desc = (TextView) dialogView.findViewById(R.id.foodDescription);
                ImageView cancel = (ImageView) dialogView.findViewById(R.id.image_cancel);

                String titleStr, descStr;
                if(language.equalsIgnoreCase("En")){
                    titleStr = resortDetailsArrayList.get(position).getResortName();
                    descStr = resortDetailsArrayList.get(position).getDesc();
                }
                else{
                    titleStr = resortDetailsArrayList.get(position).getResortNameAr();
                    descStr = resortDetailsArrayList.get(position).getDescAr();
                }
                title.setText(titleStr);
                desc.setText(descStr);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog2.dismiss();
                    }
                });

                alertDialog2 = dialogBuilder.create();
                alertDialog2.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog2.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                break;

            case R.id.layoutMap:
                mapLayout.setBackgroundColor(getResources().getColor(R.color.reservationselected));
                infoLayout.setBackgroundColor(getResources().getColor(R.color.reservationunselected));
                photosLayout.setBackgroundColor(getResources().getColor(R.color.reservationunselected));
                infoDetailsLayout.setVisibility(View.GONE);
                mapFragment.setVisibility(View.VISIBLE);
                mGalleryGrid.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                scrollView1.setVisibility(View.GONE);
                break;

            case R.id.layoutPhotos:
                mapLayout.setBackgroundColor(getResources().getColor(R.color.reservationunselected));
                infoLayout.setBackgroundColor(getResources().getColor(R.color.reservationunselected));
                photosLayout.setBackgroundColor(getResources().getColor(R.color.reservationselected));
                infoDetailsLayout.setVisibility(View.GONE);
                mapFragment.setVisibility(View.GONE);
                mGalleryGrid.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
                scrollView1.setVisibility(View.GONE);
                scrollView.fullScroll(View.FOCUS_UP);
                break;

            case R.id.tv_reserve:
                if(userPrefs.getString("login_status", "").contains("loggedin")) {
                    showBookingDialog();
                }
                else{
                    startActivityForResult(new Intent(ResortDetailsActivity.this, SignInActivity.class), LOGIN_REQUEST);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == LOGIN_REQUEST && resultCode == RESULT_OK){
            showBookingDialog();
        }
        else if(requestCode == CATERING_REQUEST && resultCode == RESULT_OK){
            insertData();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

//        if(!isBanners){
        final LatLng latLng = new LatLng(Double.parseDouble(resortDetailsArrayList.get(position).getLatitude()), Double.parseDouble(resortDetailsArrayList.get(position).getLongitude()));

        // Show the current location in Google Map
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        // Zoom in the Google Map
        map.animateCamera(CameraUpdateFactory.zoomTo(13.0f));

        MarkerOptions markerOptions = new MarkerOptions();
        map.clear();
        String name;
        if(language.equalsIgnoreCase("En")){
            name = resortDetailsArrayList.get(position).getResortName();
        }
        else{
            name = resortDetailsArrayList.get(position).getResortNameAr();
        }
        markerOptions.position(new LatLng(Double.parseDouble(resortDetailsArrayList.get(position).getLatitude()), Double.parseDouble(resortDetailsArrayList.get(position).getLongitude())))
                .title(name)
                .snippet(resortDetailsArrayList.get(position).getResortAddress())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
        Marker mymarker = map.addMarker(markerOptions);
        mymarker.showInfoWindow();

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+ResortSearchFragment.currentLatitude+","+ResortSearchFragment.currentLongitude+"&daddr="
                                +Double.parseDouble(resortDetailsArrayList.get(position).getLatitude())+"," +Double.parseDouble(resortDetailsArrayList.get(position).getLongitude())));
                startActivity(intent);
            }
        });
    }

    public void checkInDatePicker(){
        final Calendar c = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckInYear = year;
                        mCheckInDay = dayOfMonth;
                        mCheckInMonth = monthOfYear;

                        if(mCheckInDay<10) {
                            tvCheckInDate.setText("0" + mCheckInDay);
                        }
                        else{
                            tvCheckInDate.setText("" + mCheckInDay);
                        }
                        tvCheckInMonth.setText(MONTHS[mCheckInMonth]);

                        checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                    }
                }, mCheckInYear, mCheckInMonth, mCheckInDay);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
        datePickerDialog.setCancelable(false);
        datePickerDialog.setMessage("Select Check In Date");
        datePickerDialog.show();
    }

    public void checkOutDatePicker(int year, int date, int month){
        final Calendar c = Calendar.getInstance();
        long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
        String givenDateString = year+"-"+(month)+"-"+date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(givenDateString);
            mCheckInDateStr = sdf.format(mCheckInDate);
            selectedMillis = mCheckInDate.getTime();
            c.setTime(mCheckInDate); // yourdate is an object of type Date
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            tvCheckInWeek.setText(WEEKS[(dayOfWeek-1)]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mCheckOutYear = mCheckInYear;
        mCheckOutMonth = mCheckInMonth;
        mCheckOutDay = mCheckInDay;

        DatePickerDialog datePickerDialog = new DatePickerDialog(ResortDetailsActivity.this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckOutYear = year;
                        mCheckOutDay = dayOfMonth;
                        mCheckOutMonth = monthOfYear;

                        if(mCheckOutDay<10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay);
                        }
                        else{
                            tvCheckOutDate.setText("" + mCheckOutDay);
                        }
                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);

                        String givenDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf.parse(givenDateString);
                            mCheckOutDateStr = sdf.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek-1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        CalculateNumOfDays();

                    }
                }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
        datePickerDialog.getDatePicker().setMinDate(selectedMillis);
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(selectedMillis+max);
        datePickerDialog.setInverseBackgroundForced(true);
        datePickerDialog.setMessage("Select Check Out Date");
        datePickerDialog.show();
    }

    public void showBookingDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ResortDetailsActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if(language.equalsIgnoreCase("En")) {
            layout = R.layout.dialog_booking_type;
        }
        else{
            layout = R.layout.dialog_booking_type_ar;
        }

        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        RelativeLayout pendingLayout = (RelativeLayout) dialogView.findViewById(R.id.pendingReservationLayout);
        RelativeLayout bookNowLayout = (RelativeLayout) dialogView.findViewById(R.id.bookNowLayout);
        TextView tv48hrs = (TextView) dialogView.findViewById(R.id.pendingReservationText);
        TextView tvContinue = (TextView) dialogView.findViewById(R.id.tv_continue);
        ImageView cancel = (ImageView) dialogView.findViewById(R.id.image_cancel);
        final ImageView cb48hrs = (ImageView) dialogView.findViewById(R.id.cbPendingReservation);
        final ImageView cbBookNow = (ImageView) dialogView.findViewById(R.id.cbBookNow);

        if(!ResortSearchFragment.isAfter3Days){
            pendingLayout.setAlpha(0.5f);
        }
        if(is48HrsSelected){
            cb48hrs.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
            cbBookNow.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
            Constants.bookingType = "1";
        }
        else if(isBookNowSelected){
            cbBookNow.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
            cb48hrs.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
            Constants.bookingType = "2";
        }

        if(language.equalsIgnoreCase("En")) {
            tv48hrs.setText(Html.fromHtml(String.format("<body><font size=18>Do you want to visit the location before booking</font><font size=12>(you can visit within 48hrs, if not reservation will cancel)</font></body>")));
        }
        else{
            tv48hrs.setText(Html.fromHtml(String.format("هل تريد زيارة الموقع قبل حجزك في غضون 48 ساعه ، (حجزك الآن مبدئي)")));
        }

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is48HrsSelected & !isBookNowSelected){
//                    String appName, title, positive;
//                    if(language.equalsIgnoreCase("En")){
//                        appName = "Afrahcom";
//                        title = "Please select booking type";
//                        positive = getResources().getString(R.string.str_btn_ok);
//                    }
//                    else{
//                        appName = "افراحكم";
//                        title = "الرجاء إختيار نوع الحجز";
//                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                    }
//                    final iOSDialog iOSDialog = new iOSDialog(ResortDetailsActivity.this);
//                    iOSDialog.setTitle(appName);
//                    iOSDialog.setSubtitle(title);
//                    iOSDialog.setPositiveLabel(positive);
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ResortDetailsActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.app_name));
                        yes.setText(getResources().getString(R.string.str_btn_ok));
                        desc.setText("Please select booking type");
                    }
                    else{
                        title.setText(getResources().getString(R.string.app_name_ar));
                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                        desc.setText("الرجاء إختيار نوع الحجز");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else{
                    alertDialog2.cancel();

                    if(is48HrsSelected){
                        Constants.isCateringSelected = 0;
                        insertData();
                    }
                    else if(isBookNowSelected){
                        Intent intent = new Intent(ResortDetailsActivity.this, ReservationActivity.class);
                        intent.putExtra("array",resortDetailsArrayList);
                        intent.putExtra("position",position);
                        intent.putExtra("class","booknow");
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(ResortDetailsActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                        startActivity(intent, options.toBundle());
                    }

//                    Catering AlertDialog
//                    String appName, title, positive, negative;
//                    if(language.equalsIgnoreCase("En")){
//                        appName = "Afrahcom";
//                        title = "Do you want to add catering?";
//                        positive = getResources().getString(R.string.str_btn_yes);
//                        negative = getResources().getString(R.string.str_btn_no);
//                    }
//                    else{
//                        appName = "افراحكم";
//                        title = "هل تريد إضافة متعهد مطاعم";
//                        positive = getResources().getString(R.string.str_btn_yes_ar);
//                        negative = getResources().getString(R.string.str_btn_no_ar);
//                    }
//                    final iOSDialog iOSDialog = new iOSDialog(ResortDetailsActivity.this);
//                    iOSDialog.setTitle(appName);
//                    iOSDialog.setSubtitle(title);
//                    iOSDialog.setPositiveLabel(positive);
//                    iOSDialog.setNegativeLabel(negative);
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                            Intent intent = new Intent(ResortDetailsActivity.this, CateringSearchActivity.class);
//                            intent.putExtra("array",resortDetailsArrayList);
//                            intent.putExtra("position",position);
//                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
//                            startActivityForResult(intent, CATERING_REQUEST);
//                        }
//                    });
//                    iOSDialog.setNegativeListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            iOSDialog.dismiss();
//                            if(is48HrsSelected){
//                                Constants.bookingType = "1";
//                                Constants.isCateringSelected = 0;
//                                insertData();
//                            }
//                            else if(isBookNowSelected){
//                                Constants.bookingType = "2";
//                                Intent intent = new Intent(ResortDetailsActivity.this, ReservationActivity.class);
//                                intent.putExtra("array",resortDetailsArrayList);
//                                intent.putExtra("position",position);
//                                intent.putExtra("class","booknow");
//                                ActivityOptions options =
//                                        ActivityOptions.makeCustomAnimation(ResortDetailsActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
//                                startActivity(intent, options.toBundle());
//                            }
//                        }
//                    });

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ResortDetailsActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    String title, positive, negative;
                    if(language.equalsIgnoreCase("En")){
                        title = "Do you want to add catering?";
                        positive = getResources().getString(R.string.str_btn_yes);
                        negative = getResources().getString(R.string.str_btn_no);
                    }
                    else{
                        title = "هل تريد إضافة متعهد مطاعم؟";
                        positive = getResources().getString(R.string.str_btn_yes_ar);
                        negative = getResources().getString(R.string.str_btn_no_ar);
                    }

                    desc.setText(title);
                    yes.setText(positive);
                    no.setText(negative);

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent intent = new Intent(ResortDetailsActivity.this, CateringSearchActivity.class);
                            intent.putExtra("array",resortDetailsArrayList);
                            intent.putExtra("position",position);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            startActivityForResult(intent, CATERING_REQUEST);
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent intent = new Intent(ResortDetailsActivity.this, ReservationActivity.class);
                            intent.putExtra("array",resortDetailsArrayList);
                            intent.putExtra("position",position);
                            intent.putExtra("class","booknow");
                            ActivityOptions options =
                                    ActivityOptions.makeCustomAnimation(ResortDetailsActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                            startActivity(intent, options.toBundle());
                        }
                    });

                    customDialog = dialogBuilder.create();
//                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
//                    iOSDialog.show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog2.dismiss();
            }
        });

        pendingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ResortSearchFragment.isAfter3Days) {
                    cb48hrs.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                    cbBookNow.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                    is48HrsSelected = true;
                    isBookNowSelected = false;
                }
            }
        });

        bookNowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cbBookNow.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                cb48hrs.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                isBookNowSelected = true;
                is48HrsSelected = false;
            }
        });

        alertDialog2 = dialogBuilder.create();
//        alertDialog2.getWindow().setGravity(Gravity.BOTTOM);
//        alertDialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        alertDialog2.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog2.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void insertData(){
        try {
            if(Float.parseFloat(resortDetailsArrayList.get(position).getDiscount())>0){

                total = Float.parseFloat(resortDetailsArrayList.get(position).getActualPrice());
                subTotal = Float.parseFloat(resortDetailsArrayList.get(position).getDiscountedPrice());
                if(isVatApplicable.equalsIgnoreCase("true")) {
                    subvat = subTotal * (Constants.VAT / 100);
                }
                else{
                    subvat = (float)0.00;
                }
                disc = total - subTotal;
                netAmount = subTotal + subvat ;
            }
            else{
                total = Float.parseFloat(resortDetailsArrayList.get(position).getDiscountedPrice());
                if(isVatApplicable.equalsIgnoreCase("true")) {
                    subvat = total * (Constants.VAT / 100);
                }
                else{
                    subvat = (float)0.00;
                }
                netAmount = total + subvat;
            }
        } catch (Exception e) {
            e.printStackTrace();
            total = Float.parseFloat(resortDetailsArrayList.get(position).getDiscountedPrice());
            subvat = total* (Constants.VAT/100);
            netAmount = total + subvat;
        }
        final DecimalFormat priceFormat1 = new DecimalFormat("0");
        final DecimalFormat priceFormat = new DecimalFormat("0");
        JSONObject parent = new JSONObject();
        try {
//           Fetching Version Name
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

//           Setting Reservation Date
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            String currentTime = df3.format(c.getTime());
            String advance = resortDetailsArrayList.get(position).getAfrahcomAdvance();
            if(advance.equalsIgnoreCase("null") || advance == null){
                advance = "500";
            }

//           Insert order JSON preparation
            JSONObject mainObj = new JSONObject();
            mainObj.put("StartDate", Constants.convertToArabic(Constants.startDate));
            mainObj.put("EndDate", Constants.convertToArabic(Constants.endDate));
            mainObj.put("UserId", userId);
            mainObj.put("ResortId", resortDetailsArrayList.get(position).getResortId());
            mainObj.put("BookingType", "1");
            mainObj.put("Comments", "Android v" + version);
            mainObj.put("PaymentMode", "1");
            mainObj.put("TotalPrice", Constants.convertToArabic(priceFormat1.format(netAmount)));
            mainObj.put("Price", Constants.convertToArabic(priceFormat1.format(total)));
            mainObj.put("DiscountPrice", Constants.convertToArabic(priceFormat1.format(disc)));
            mainObj.put("SubtotalPrice", Constants.convertToArabic(priceFormat1.format(subTotal)));
            mainObj.put("vatPrice", Constants.convertToArabic(priceFormat1.format(subvat)));
            mainObj.put("AdvancePrice", advance);
            mainObj.put("OrderStatus", "Pending");
            mainObj.put("DeviceToken", SplashScreenActivity.regId);
            mainObj.put("ReservationDate", currentTime);
            mainObj.put("NoOfMen", Constants.noOfMen);
            mainObj.put("NoOfWomen", Constants.noOfWomen);
            mainObj.put("UserName", userName);
            mainObj.put("MobileNo", mobileNo);
            mainObj.put("IsCateringsSelected", Constants.isCateringSelected);
            mainObj.put("IsKitchenSelected", Constants.isKitchenSelected);
            mainObj.put("ResortTypeId", Constants.resortTypeId);
            mainObj.put("PaymentStatus", "Pending");
            parent.put("insertdetails", mainObj);

//            Empty JSON object for Inserting in Tracking table
            JSONObject emptyObj = new JSONObject();
            parent.put("inserttracking", emptyObj);

            if(Constants.isCateringSelected==1) {
                try {
                    String foodIds = "";
                    for (int i = 0; i < Constants.SelectedFoodIds.size(); i++) {
                        if (i == 0) {
                            foodIds = Constants.SelectedFoodIds.get(i);
                        } else {
                            foodIds = foodIds + "," + Constants.SelectedFoodIds.get(i);
                        }
                    }

                    JSONObject cateringObj = new JSONObject();
                    cateringObj.put("Breakfast", Constants.isBreakfastSelected);
                    cateringObj.put("Lunch", Constants.isLunchSelected);
                    cateringObj.put("Dinner", Constants.isDinnerSelected);
                    cateringObj.put("SweetandSnacks", Constants.isSnacksSelected);
                    cateringObj.put("BreakfastId", foodIds);
                    parent.put("InsertCatering", cateringObj);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                JSONObject cateringObj = new JSONObject();
                parent.put("InsertCatering", cateringObj);
            }
            Log.i("TAG", parent.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        new InsertOrder().execute(parent.toString());
    }

    public class InsertOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(mContext);
            dialog = new ACProgressFlower.Builder(ResortDetailsActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(mContext, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(mContext, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            JSONArray successArray = jo.getJSONArray("success");
                            JSONObject jo1 = successArray.getJSONObject(0);
                            String bookingID = jo1.getString("BookingId");

                            if(language.equalsIgnoreCase("En")) {
                                Toast.makeText(mContext, "Booking Successful", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(mContext, "تم الحجز بنجاح", Toast.LENGTH_SHORT).show();
                            }
                            Intent intent = new Intent(ResortDetailsActivity.this, OrderTrackingActivity.class);
                            intent.putExtra("array",resortDetailsArrayList);
                            intent.putExtra("position",position);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra("bookingID",bookingID);
                            startActivity(intent);
                            if (dialog != null) {
                                dialog.dismiss();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "Cannot reach server", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            } else {
                Toast.makeText(mContext, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    protected void onResume() {
        super.onResume();
        String response = userPrefs.getString("user_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                firstName = userObjuect.getString("fullName");
                mMobileStr = userObjuect.getString("mobile");
                mMobileStr = mMobileStr.substring(3,12);
                mEmailStr = userObjuect.getString("email");
                if(userObjuect.getString("MiddleName").equals("null")){
                    mMiddleName = "";
                }
                else{
                    mMiddleName = userObjuect.getString("MiddleName");
                }
                userName = firstName + " "+ mMiddleName;
                mobileNo = (userObjuect.getString("mobile"));
            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }
}
