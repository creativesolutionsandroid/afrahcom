package com.cs.afrahcom.activity;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.fragment.ResortSearchFragment;
import com.cs.afrahcom.models.Banners;
import com.cs.afrahcom.models.ResortDetailsFiltered;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 17-10-2017.
 */

public class ReservationActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextView tvReserve, tvResortName, tvResortAddress, tvResortDistance, tvResortPrice;
    TextView tvCheckinDate, tvCheckinMonth, tvCheckinWeek, tvCheckoutMonth, tvCheckoutDate, tvCheckoutWeek, tvNumofDays;
    TextView tvTotalPrice, tvVAT, tvNetAmount, tvAdvance;
    TextView tvDiscountTitle, tvDiscount, tvSubTitle, tvSubTotal;
    View lineDiscount;
    String advanceAmount;
    ImageView imgResortCoverPic;
    RatingBar ratingBar;
    SharedPreferences userPrefs;
    private static final int LOGIN_REQUEST = 1;
    ArrayList<ResortDetailsFiltered> resortDetailsArrayList = new ArrayList<>();
    ArrayList<Banners> bannersArrayList = new ArrayList<>();
    int position;
    int paymentMode = 2;
    String isVatApplicable;
    LinearLayout mFacilitylayout, menuLayout;
    RelativeLayout mAdvanceLayout;
    RelativeLayout approvedLayout;
    Context mContext;
    float netAmount;
    String startDate1 = "", endDate1 = "";
    String userId, userName, mobileNo, mWhichClass = "";
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
    SharedPreferences languagePrefs;
    String language;
    ImageView cardRadio, cashRadio;
    LinearLayout cardLayout, cashLayout;
    float total = 0, subTotal = 0, subvat = 0, disc = 0;
    AlertDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_reservation);
        }
        else{
            setContentView(R.layout.activity_reservation_ar);
        }
        mContext = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        position = getIntent().getIntExtra("position",0);
        resortDetailsArrayList = (ArrayList<ResortDetailsFiltered>) getIntent().getSerializableExtra("array");
        try {
            mWhichClass = getIntent().getStringExtra("class");
        } catch (Exception e) {
            e.printStackTrace();
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        getUserDetails();

        ratingBar = (RatingBar) findViewById(R.id.rating_bar);

        tvReserve = (TextView) findViewById(R.id.tv_reserve);
        tvResortName = (TextView) findViewById(R.id.resortName);
        tvResortAddress = (TextView) findViewById(R.id.resortAddress);
        tvResortDistance = (TextView) findViewById(R.id.resortDistance);
        tvResortPrice = (TextView) findViewById(R.id.resortPrice);
        tvCheckinDate = (TextView) findViewById(R.id.tv_checkin_date);
        tvCheckinMonth = (TextView) findViewById(R.id.tv_checkin_month);
        tvCheckinWeek = (TextView) findViewById(R.id.tv_checkin_week);
        tvCheckoutDate = (TextView) findViewById(R.id.tv_checkout_date);
        tvCheckoutMonth = (TextView) findViewById(R.id.tv_checkout_month);
        tvCheckoutWeek = (TextView) findViewById(R.id.tv_checkout_week);
        tvNumofDays = (TextView) findViewById(R.id.tv_number_of_days);
        approvedLayout = (RelativeLayout) findViewById(R.id.approved_layout);

        cardRadio = (ImageView) findViewById(R.id.cardRadio);
        cashRadio = (ImageView) findViewById(R.id.cashRadio);
        cardLayout = (LinearLayout) findViewById(R.id.cardPayment);
        cashLayout = (LinearLayout) findViewById(R.id.cashPayment);

        tvTotalPrice = (TextView) findViewById(R.id.tv_price);
        tvVAT = (TextView) findViewById(R.id.tv_vat);
        tvNetAmount = (TextView) findViewById(R.id.tv_net);
        tvAdvance = (TextView) findViewById(R.id.advanceAmount);

        tvDiscountTitle= (TextView) findViewById(R.id.tv_discount_title);
        tvDiscount = (TextView) findViewById(R.id.tv_discount_price);
        tvSubTitle = (TextView) findViewById(R.id.tv_subTotal_title);
        tvSubTotal = (TextView) findViewById(R.id.tv_subTotal);
        lineDiscount = (View) findViewById(R.id.lineDiscount);

        mAdvanceLayout = (RelativeLayout) findViewById(R.id.advance_layout);

        mFacilitylayout = (LinearLayout) findViewById(R.id.facility_layout);
        menuLayout = (LinearLayout) findViewById(R.id.menuLayout);
        menuLayout.setVisibility(View.GONE);

        imgResortCoverPic = (ImageView) findViewById(R.id.resort_image);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getApplicationContext().getResources().getColor(R.color.ratingBarColorindetails), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(getApplicationContext().getResources().getColor(R.color.ratingBarColorindetails), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(getApplicationContext().getResources().getColor(R.color.ratingBarColorindetails), PorterDuff.Mode.SRC_ATOP);

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###");
//        final DecimalFormat priceFormat1 = new DecimalFormat("##,##,###.00");
        if(language.equalsIgnoreCase("En")) {
            tvResortName.setText(resortDetailsArrayList.get(position).getResortName());
        }
        else{
            tvResortName.setText(resortDetailsArrayList.get(position).getResortNameAr());
        }
        tvResortAddress.setText(resortDetailsArrayList.get(position).getResortAddress());
        tvResortDistance.setText(resortDetailsArrayList.get(position).getDistance() + " KM");
        if(language.equalsIgnoreCase("En")) {
            tvReserve.setText("Confirm Reserve");
        }
        else{
            tvReserve.setText("تأكيد الحجز");
        }

        try {
            isVatApplicable = resortDetailsArrayList.get(position).getVatAvailability();
        } catch (Exception e) {
            e.printStackTrace();
            isVatApplicable = "true";
        }

        try {
            if (resortDetailsArrayList.get(position).getApproved().equals("true")){
                approvedLayout.setVisibility(View.VISIBLE);
            }
            else{
                approvedLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            tvResortPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getDiscountedPrice())))));
        } catch (Exception e) {
            e.printStackTrace();
//            tvResortPrice.setText(Constants.convertToArabic(resortDetailsArrayList.get(position).getDiscountedPrice()));
        }

        advanceAmount = Constants.convertToArabic(resortDetailsArrayList.get(position).getAfrahcomAdvance());
        try {
            if(advanceAmount.equalsIgnoreCase("null")){
               advanceAmount = "400";
            }
        } catch (Exception e) {
            e.printStackTrace();
            advanceAmount = "400";
        }

        try {
            if (resortDetailsArrayList.get(position).getRating().equals("0")) {
                ratingBar.setRating(5);
            } else {
                ratingBar.setRating(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getRating())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(!mWhichClass.equals("pending")) {
            tvCheckinDate.setText(ResortSearchFragment.strCheckInDate);
            tvCheckinMonth.setText(ResortSearchFragment.strCheckInMonth);
            tvCheckinWeek.setText(ResortSearchFragment.strCheckInWeek);
            tvCheckoutDate.setText(ResortSearchFragment.strCheckoutDate);
            tvCheckoutMonth.setText(ResortSearchFragment.strCheckoutMonth);
            tvCheckoutWeek.setText(ResortSearchFragment.strCheckoutWeek);
            tvNumofDays.setText(ResortSearchFragment.strNumDays);

            Float discount = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getDiscount()));
            if(discount != 0){
                tvDiscountTitle.setVisibility(View.VISIBLE);
                tvDiscount.setVisibility(View.VISIBLE);
                tvSubTitle.setVisibility(View.VISIBLE);
                tvSubTotal.setVisibility(View.VISIBLE);
                lineDiscount.setVisibility(View.VISIBLE);

                total = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getActualPrice()));
                subTotal = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getDiscountedPrice()));
                if(isVatApplicable.equalsIgnoreCase("true")) {
                    subvat = subTotal * (Constants.VAT / 100);
                    tvVAT.setText(Constants.convertToArabic(priceFormat.format(subvat)));
                }
                else{
                    subvat = (float)0.00;
                    tvVAT.setText("0");
                }
                disc = total - subTotal;
                netAmount = subTotal + subvat ;

                tvTotalPrice.setText(Constants.convertToArabic(priceFormat.format(total)));
                tvVAT.setText(Constants.convertToArabic(priceFormat.format(subvat)));
                tvDiscount.setText(Constants.convertToArabic(priceFormat.format(disc)));
                tvSubTotal.setText(Constants.convertToArabic(priceFormat.format(subTotal)));
                tvNetAmount.setText(Constants.convertToArabic(priceFormat.format(netAmount)));
            }
            else{
                total = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getDiscountedPrice()));
                if(isVatApplicable.equalsIgnoreCase("true")) {
                    subvat = total* (Constants.VAT/100);
                    tvVAT.setText(Constants.convertToArabic(priceFormat.format(subvat)));
                }
                else{
                    subvat = (float)0.00;
                    tvVAT.setText("0");
                }
                netAmount = total + subvat;
                tvTotalPrice.setText(Constants.convertToArabic(priceFormat.format(total)));
                tvNetAmount.setText(Constants.convertToArabic(priceFormat.format(netAmount)));
                if(Float.parseFloat(advanceAmount) > netAmount){
                    tvAdvance.setText(""+(int) total);
                    advanceAmount = Float.toString(total);
                }
                else{
                    tvAdvance.setText(""+(int) Float.parseFloat(advanceAmount));
                }
            }

            try {
                if(resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("1")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
                }
                else if(resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("2")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE2_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(ResortSearchFragment.isToday){
                cashLayout.setAlpha(0.2f);
            }
        }
        else{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            String startDate = resortDetailsArrayList.get(position).getStartDate();
            String endDate = resortDetailsArrayList.get(position).getEndDate();
            String[] dateStr1 = startDate.split("T");
            String[] dateStr2 = endDate.split("T");
            String[] FinaldateStr1 = dateStr1[0].split("-");
            String[] FinaldateStr2 = dateStr2[0].split("-");
            Date date1 = null;
            Date date2 = null;
            try {
                date1 = dateFormat.parse(dateStr1[0]);
                date2 = dateFormat.parse(dateStr2[0]);

                startDate1 = dateFormat.format(date1);
                endDate1 = dateFormat.format(date2);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Float discount = Float.parseFloat(resortDetailsArrayList.get(position).getDisc());
            if(discount!=0){
                tvDiscountTitle.setVisibility(View.VISIBLE);
                tvDiscount.setVisibility(View.VISIBLE);
                tvSubTitle.setVisibility(View.VISIBLE);
                tvSubTotal.setVisibility(View.VISIBLE);
                lineDiscount.setVisibility(View.VISIBLE);

                try {
                    tvResortPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getSubTotal())))));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                total = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getTotal()));
                subTotal = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getSubTotal()));
                subvat = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getSubvat()));
                disc = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getDisc()));
                netAmount = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getNetAmount()));

                tvTotalPrice.setText(Constants.convertToArabic(priceFormat.format(total)));
                if(subvat == 0) {
                    tvVAT.setText("0");
                }
                else{
                    tvVAT.setText(Constants.convertToArabic(priceFormat.format(subvat)));
                }
                tvDiscount.setText(Constants.convertToArabic(priceFormat.format(disc)));
                tvSubTotal.setText(Constants.convertToArabic(priceFormat.format(subTotal)));
                tvNetAmount.setText(Constants.convertToArabic(priceFormat.format(netAmount)));
            }
            else{
                try {
                    tvResortPrice.setText(Constants.convertToArabic(priceFormat.format(Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getTotal())))));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                total = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getTotal()));
                subvat = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getSubvat()));
                netAmount = Float.parseFloat(Constants.convertToArabic(resortDetailsArrayList.get(position).getNetAmount()));
                tvTotalPrice.setText(Constants.convertToArabic(priceFormat.format(total)));
                if(subvat == 0) {
                    tvVAT.setText("0");
                }
                else{
                    tvVAT.setText(Constants.convertToArabic(priceFormat.format(subvat)));
                }
                tvNetAmount.setText(Constants.convertToArabic(priceFormat.format(netAmount)));
                if(Float.parseFloat(advanceAmount) > netAmount){
                    tvAdvance.setText(""+(int) total);
                    advanceAmount = Float.toString(total);
                }
                else{
                    tvAdvance.setText(""+(int) Float.parseFloat(advanceAmount));
                }
            }
            tvCheckinDate.setText(FinaldateStr1[2]);
            tvCheckoutDate.setText(FinaldateStr2[2]);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);
            tvCheckinMonth.setText(MONTHS[calendar.get(Calendar.MONTH)]);
            tvCheckinWeek.setText(WEEKS[(calendar.get(Calendar.DAY_OF_WEEK)-1)]);

            calendar.setTime(date2);
            tvCheckoutMonth.setText(MONTHS[calendar.get(Calendar.MONTH)]);
            tvCheckoutWeek.setText(WEEKS[(calendar.get(Calendar.DAY_OF_WEEK)-1)]);

            long diff = date2.getTime() - date1.getTime();
            long NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))+1;
            tvNumofDays.setText(""+NumofDaysStr);

            try {
                if(resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("1")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
                }
                else if(resortDetailsArrayList.get(position).getResortImagesArray().get(0).getSource().equals("2")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE2_URL + resortDetailsArrayList.get(position).getResortImagesArray().get(0).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(position).getResortImage())
                        .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(imgResortCoverPic);
            }
        }

        tvReserve.setOnClickListener(this);
        cashLayout.setOnClickListener(this);
        cardLayout.setOnClickListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.cardPayment:
                cardRadio.setImageResource(R.drawable.radio_on);
                cashRadio.setImageResource(R.drawable.radio_off);
                paymentMode = 2;
                break;

            case R.id.cashPayment:
                if(!ResortSearchFragment.isToday && paymentMode!=1) {
//                final iOSDialog iOSDialog = new iOSDialog(ReservationActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Pay the advance with in 24hrs.";
//                    positive = "Accept";
//                    negative = "Reject";
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "الرجاء دفع العربون في غضون ل 24 ساعة";
//                    positive = "قبول";
//                    negative = "رفض";
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        cardRadio.setImageResource(R.drawable.radio_off);
//                        cashRadio.setImageResource(R.drawable.radio_on);
//                        paymentMode = 1;
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ReservationActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    String title, positive, negative;
                    if(language.equalsIgnoreCase("En")){
                        title = "Pay the advance with in 24hrs.";
                        positive = "Accept";
                        negative = "Reject";
                    }
                    else{
                        title = "الرجاء دفع العربون في غضون ل 24 ساعة";
                        positive = "قبول";
                        negative = "رفض";
                    }

                    desc.setText(title);
                    yes.setText(positive);
                    no.setText(negative);

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                       customDialog.dismiss();
                       cardRadio.setImageResource(R.drawable.radio_off);
                       cashRadio.setImageResource(R.drawable.radio_on);
                       paymentMode = 1;
                        }
                    });
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;

            case R.id.tv_reserve:
                if(paymentMode!=1){
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ReservationActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.app_name));
                        yes.setText(getResources().getString(R.string.str_btn_ok));
                        desc.setText("Please select payment mode");
                    }
                    else{
                        title.setText(getResources().getString(R.string.app_name_ar));
                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                        desc.setText("يرجى تحديد طريقة الدفع");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    String paymentStatus = "";
                    if (paymentMode == 1) {
                        paymentStatus = "Pending";
                    } else {
                        paymentStatus = "Paid";
                    }
                    final DecimalFormat priceFormat1 = new DecimalFormat("0");
                    final DecimalFormat priceFormat = new DecimalFormat("0");
                    if (!mWhichClass.equals("pending")) {
                        JSONObject parent = new JSONObject();
                        try {
//                        Fetching Version Name
                            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                            String version = pInfo.versionName;

//                        Setting Reservation Date
                            Calendar c = Calendar.getInstance();
                            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                            String currentTime = df3.format(c.getTime());

//                        Insert order JSON preparation
                            JSONObject mainObj = new JSONObject();
                            mainObj.put("StartDate", Constants.convertToArabic(Constants.startDate));
                            mainObj.put("EndDate", Constants.convertToArabic(Constants.endDate));
                            mainObj.put("UserId", userId);
                            mainObj.put("ResortId", resortDetailsArrayList.get(position).getResortId());
                            mainObj.put("BookingType", "2");
                            mainObj.put("Comments", "Android v" + version);
                            mainObj.put("PaymentMode", Integer.toString(paymentMode));
                            mainObj.put("TotalPrice", Constants.convertToArabic(priceFormat1.format(netAmount)));
                            mainObj.put("Price", Constants.convertToArabic(priceFormat1.format(total)));
                            mainObj.put("DiscountPrice", Constants.convertToArabic(priceFormat1.format(disc)));
                            mainObj.put("SubtotalPrice", Constants.convertToArabic(priceFormat1.format(subTotal)));
                            mainObj.put("vatPrice", Constants.convertToArabic(priceFormat1.format(subvat)));
                            mainObj.put("AdvancePrice", advanceAmount);
                            mainObj.put("OrderStatus", "New");
                            mainObj.put("DeviceToken", SplashScreenActivity.regId);
                            mainObj.put("ReservationDate", currentTime);
                            mainObj.put("NoOfMen", Constants.noOfMen);
                            mainObj.put("NoOfWomen", Constants.noOfWomen);
                            mainObj.put("UserName", userName);
                            mainObj.put("MobileNo", mobileNo);
                            mainObj.put("IsCateringsSelected", Constants.isCateringSelected);
                            mainObj.put("IsKitchenSelected", Constants.isKitchenSelected);
                            mainObj.put("ResortTypeId", Constants.resortTypeId);
                            mainObj.put("PaymentStatus", paymentStatus);
                            parent.put("insertdetails", mainObj);

//                        Empty JSON object for Inserting in Tracking table
                            JSONObject emptyObj = new JSONObject();
                            parent.put("inserttracking", emptyObj);

                            if (Constants.isCateringSelected == 1) {
                                try {
                                    String foodIds = "";
                                    for (int i = 0; i < Constants.SelectedFoodIds.size(); i++) {
                                        if (i == 0) {
                                            foodIds = Constants.SelectedFoodIds.get(i);
                                        } else {
                                            foodIds = foodIds + "," + Constants.SelectedFoodIds.get(i);
                                        }
                                    }

                                    JSONObject cateringObj = new JSONObject();
                                    cateringObj.put("Breakfast", Constants.isBreakfastSelected);
                                    cateringObj.put("Lunch", Constants.isLunchSelected);
                                    cateringObj.put("Dinner", Constants.isDinnerSelected);
                                    cateringObj.put("SweetandSnacks", Constants.isSnacksSelected);
                                    cateringObj.put("BreakfastId", foodIds);
                                    parent.put("InsertCatering", cateringObj);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                JSONObject cateringObj = new JSONObject();
                                parent.put("InsertCatering", cateringObj);
                            }

                            Log.i("TAG", parent.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        new InsertOrder().execute(parent.toString());
                    } else {
                        if (startDate1 == null || endDate1 == null) {
                            Toast.makeText(ReservationActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            JSONObject parent = new JSONObject();
                            try {
//                        Update order JSON preparation
                                JSONObject mainObj = new JSONObject();
                                mainObj.put("BookingId", resortDetailsArrayList.get(position).getBookingID());
                                mainObj.put("OrderStatus", "New");
                                mainObj.put("PaymentMode", Integer.toString(paymentMode));
                                mainObj.put("TotalPrice", priceFormat1.format(netAmount));
                                mainObj.put("AdvancePrice", advanceAmount);
                                mainObj.put("StartDate", startDate1);
                                mainObj.put("EndDate", endDate1);
                                mainObj.put("PaymentStatus", paymentStatus);
                                parent.put("UpdateDetails", mainObj);

//                        Empty JSON object for Inserting in Tracking table
                                JSONObject emptyObj = new JSONObject();
                                parent.put("inserttracking", emptyObj);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Log.i("TAG", parent.toString());
                            new UpdateOrderStatus().execute(parent.toString());
                        }
                    }
                }
                break;
        }
    }

    public class InsertOrder extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(mContext);
            dialog = new ACProgressFlower.Builder(ReservationActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(mContext, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(mContext, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            JSONArray successObj = jo.getJSONArray("success");
                            JSONObject bookingObj = successObj.getJSONObject(0);
                            String bookingID = bookingObj.getString("BookingId");

//                            if(Constants.isCateringSelected==1) {
//                                JSONObject parent = new JSONObject();
//                                try {
//                                    String foodIds = "";
//                                    for (int i = 0; i < Constants.SelectedFoodIds.size(); i++){
//                                        if(i == 0){
//                                            foodIds = Constants.SelectedFoodIds.get(i);
//                                        }
//                                        else {
//                                            foodIds = foodIds + "," +Constants.SelectedFoodIds.get(i);
//                                        }
//                                    }
//
//                                    JSONObject mainObj = new JSONObject();
//                                    mainObj.put("BookingId", bookingID);
//                                    mainObj.put("Breakfast", Constants.isBreakfastSelected);
//                                    mainObj.put("Lunch", Constants.isLunchSelected);
//                                    mainObj.put("Dinner", Constants.isDinnerSelected);
//                                    mainObj.put("BreakfastId", foodIds);
//                                    parent.put("InsertCatering", mainObj);
//                                    new InsertCatering().execute(parent.toString());
//
//                                    Log.i("TAG", parent.toString());
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }

//                            }
//                            else {
                                if(language.equalsIgnoreCase("En")) {
                                    Toast.makeText(mContext, "Booking Successful", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(mContext, "تم الحجز بنجاح", Toast.LENGTH_SHORT).show();
                                }
                                Intent intent = new Intent(ReservationActivity.this, OrderHistoryActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("class", "reservation");
                            ActivityOptions options =
                                    ActivityOptions.makeCustomAnimation(ReservationActivity.this, R.anim.enter_from_right , R.anim.exit_to_left);
                            startActivity(intent, options.toBundle());
//                            }
                        } catch (Exception e) {
//                            e.printStackTrace();
                            try {
                                JSONObject jo = new JSONObject(result);
                                String failure = jo.getString("Failure");
                                if(failure.equalsIgnoreCase("This Propery has been booked by some else")){
//                                    final iOSDialog iOSDialog = new iOSDialog(ReservationActivity.this);
//                                    String appName, title, positive;
//                                    if(language.equalsIgnoreCase("En")){
//                                        appName = getResources().getString(R.string.app_name);
//                                        title = "Property is not available for these dates.";
//                                        positive = getResources().getString(R.string.str_btn_ok);
//                                    }
//                                    else{
//                                        appName = getResources().getString(R.string.app_name_ar);
//                                        title = "القاعة غير متوفرة في هذا التاريخ";
//                                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                                    }
//                                    iOSDialog.setTitle(appName);
//                                    iOSDialog.setSubtitle(title);
//                                    iOSDialog.setPositiveLabel(positive);
//                                    iOSDialog.setBoldPositiveLabel(false);
//                                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            iOSDialog.dismiss();
//                                        }
//                                    });
//                                    iOSDialog.show();

                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ReservationActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                                    no.setVisibility(View.GONE);
                                    vert.setVisibility(View.GONE);

                                    if(language.equalsIgnoreCase("En")) {
                                        title.setText(getResources().getString(R.string.app_name));
                                        yes.setText(getResources().getString(R.string.str_btn_ok));
                                        desc.setText("Property is not available for these dates.");
                                    }
                                    else{
                                        title.setText(getResources().getString(R.string.app_name_ar));
                                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                        desc.setText("القاعة غير متوفرة في هذا التاريخ");
                                    }

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth*0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                                Toast.makeText(mContext, "Cannot reach server", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            } else {
                Toast.makeText(mContext, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }

    public class InsertCatering extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(mContext);
            dialog = new ACProgressFlower.Builder(ReservationActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.INSERT_CATERING_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }
        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(mContext, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(mContext, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String successObj = jo.getString("Success");

                            Toast.makeText(mContext, "Booking Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ReservationActivity.this, OrderHistoryActivity.class);
                            intent.putExtra("class", "reservation");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "Cannot reach server", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            } else {
                Toast.makeText(mContext, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            Snap sn = new Snap();
//            sn.insertfav();
            super.onPostExecute(result);

        }
    }


    public class UpdateOrderStatus extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(mContext);
            dialog = new ACProgressFlower.Builder(ReservationActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    // 1. create HttpClient
                    HttpClient httpclient = new DefaultHttpClient();

                    // 2. make POST request to the given URL
                    HttpPost httpPost = new HttpPost(Constants.UPDATE_ORDER_STATUS_URL);

                    // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                    // ObjectMapper mapper = new ObjectMapper();
                    // json = mapper.writeValueAsString(person);

                    // 5. set json to StringEntity
                    StringEntity se = new StringEntity(params[0], "UTF-8");

                    // 6. set httpPost Entity
                    httpPost.setEntity(se);

                    // 7. Set some headers to inform server about the type of the content
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");

                    // 8. Execute POST request to the given URL
                    HttpResponse httpResponse = httpclient.execute(httpPost);

                    // 9. receive response as inputStream
                    inputStream = httpResponse.getEntity().getContent();

                    // 10. convert inputstream to string
                    if(inputStream != null) {
                        response = convertInputStreamToString(inputStream);
                        Log.i("TAGs", "user response:" + response);
                        return response;
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "fav response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(mContext, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(mContext, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String success = jo.getString("Success");
                            Intent intent = new Intent(ReservationActivity.this, OrderHistoryActivity.class);
                            intent.putExtra("class", "reservation");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                JSONObject jo = new JSONObject(result);
                                String success = jo.getString("Failure");
                                if(success.equalsIgnoreCase("Update UnSuccessfully")) {
//                                    final iOSDialog iOSDialog = new iOSDialog(ReservationActivity.this);
//                                    String appName, title, positive;
//                                    if (language.equalsIgnoreCase("En")) {
//                                        appName = getResources().getString(R.string.app_name);
//                                        title = "Property is not available for these dates.";
//                                        positive = getResources().getString(R.string.str_btn_ok);
//                                    } else {
//                                        appName = getResources().getString(R.string.app_name_ar);
//                                        title = "القاعة غير متوفرة في هذا التاريخ";
//                                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                                    }
//                                    iOSDialog.setTitle(appName);
//                                    iOSDialog.setSubtitle(title);
//                                    iOSDialog.setPositiveLabel(positive);
//                                    iOSDialog.setBoldPositiveLabel(false);
//                                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            iOSDialog.dismiss();
//                                            Intent intent = new Intent(ReservationActivity.this, OrderHistoryActivity.class);
//                                            intent.putExtra("class", "reservation");
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                            startActivity(intent);
//                                        }
//                                    });
//                                    iOSDialog.show();

                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ReservationActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                                    no.setVisibility(View.GONE);
                                    vert.setVisibility(View.GONE);

                                    if(language.equalsIgnoreCase("En")) {
                                        title.setText(getResources().getString(R.string.app_name));
                                        yes.setText(getResources().getString(R.string.str_btn_ok));
                                        desc.setText("Property is not available for these dates.");
                                    }
                                    else{
                                        title.setText(getResources().getString(R.string.app_name_ar));
                                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                        desc.setText("القاعة غير متوفرة في هذا التاريخ");
                                    }

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                            Intent intent = new Intent(ReservationActivity.this, OrderHistoryActivity.class);
                                            intent.putExtra("class", "reservation");
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth*0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
                                }
                                else{
                                    Toast.makeText(mContext, "Cannot reach server", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                                Toast.makeText(mContext, "Cannot reach server", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

            } else {
                Toast.makeText(mContext, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            Snap sn = new Snap();
//            sn.insertfav();
            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public void getUserDetails(){
        String response = userPrefs.getString("user_profile", null);
        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                String firstName = userObjuect.getString("fullName");
                String mMiddleName;
                if(userObjuect.getString("MiddleName").equals("null")){
                    mMiddleName = "";
                }
                else{
                    mMiddleName = userObjuect.getString("MiddleName");
                }
                userName = firstName + " "+ mMiddleName;
                mobileNo = (userObjuect.getString("mobile"));
            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }
}