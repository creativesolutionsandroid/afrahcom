package com.cs.afrahcom.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.models.ResortDetails;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 17-11-2017.
 */

public class UpdatePhotosActivity extends AppCompatActivity {

    ImageView cancel1,cancel2,cancel3,cancel4,cancel5,cancel6,cancel7,cancel8;
    ImageView resortImage1,resortImage2,resortImage3,resortImage4,resortImage5,resortImage6,resortImage7,resortImage8,addMoreImages;
    ArrayList<ResortDetails> resortDetailsArrayList = new ArrayList<>();
    String galleryID;
    String imageName1 = "", imageName2 = "", imageName3 = "", imageName4 = "", imageName5 = "", imageName6 = "", imageName7 = ""
            , imageName8 = "";
    TextView updatePhotos;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int STORAGE_REQUEST = 5;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    int imageSelected = 0;
    Bitmap thumbnail1 = null, thumbnail2 = null, thumbnail3 = null, thumbnail4 = null, thumbnail5 = null, thumbnail6 = null
            , thumbnail7 = null, thumbnail8 = null, documentthumbnail = null;
    String selectedPath1, selectedPath2, selectedPath3, selectedPath4, selectedPath5, selectedPath6, selectedPath7, selectedPath8;
    String imageResponse;
    private DefaultHttpClient mHttpClient11;
    private String original_imagePath1 = null;
    private String original_imagePath2 = null;
    private String original_imagePath3 = null;
    private String original_imagePath4 = null;
    private String original_imagePath5 = null;
    private String original_imagePath6 = null;
    private String original_imagePath7 = null;
    private String original_imagePath8 = null;
    LinearLayout photosLayout3, photosLayout4;
    SharedPreferences hostPrefs;
    SharedPreferences.Editor hostPrefsEditor;
    ArrayList<String> selectedFacilitiesId = new ArrayList<>();
    String hostId;
    SharedPreferences languagePrefs;
    String language;
    Toolbar toolbar;
    AlertDialog customDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_update_photos);
        }
        else{
            setContentView(R.layout.activity_update_photos_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostPrefsEditor = hostPrefs.edit();
        hostId = hostPrefs.getString("hostId", "0");

        photosLayout3 = (LinearLayout) findViewById(R.id.photos3);
        photosLayout4 = (LinearLayout) findViewById(R.id.photos4);

        resortImage1 = (ImageView) findViewById(R.id.resortImage1);
        resortImage2 = (ImageView) findViewById(R.id.resortImage2);
        resortImage3 = (ImageView) findViewById(R.id.resortImage3);
        resortImage4 = (ImageView) findViewById(R.id.resortImage4);
        resortImage5 = (ImageView) findViewById(R.id.resortImage5);
        resortImage7 = (ImageView) findViewById(R.id.resortImage7);
        resortImage6 = (ImageView) findViewById(R.id.resortImage6);
        resortImage8 = (ImageView) findViewById(R.id.resortImage8);

        cancel1 = (ImageView) findViewById(R.id.image1Cancel);
        cancel2 = (ImageView) findViewById(R.id.image2Cancel);
        cancel3 = (ImageView) findViewById(R.id.image3Cancel);
        cancel4 = (ImageView) findViewById(R.id.image4Cancel);
        cancel5 = (ImageView) findViewById(R.id.image5Cancel);
        cancel6 = (ImageView) findViewById(R.id.image6Cancel);
        cancel7 = (ImageView) findViewById(R.id.image7Cancel);
        cancel8 = (ImageView) findViewById(R.id.image8Cancel);
        addMoreImages = (ImageView) findViewById(R.id.addmoreimages);

        photosLayout3.setVisibility(View.VISIBLE);
        photosLayout4.setVisibility(View.VISIBLE);
        addMoreImages.setVisibility(View.INVISIBLE);

        updatePhotos = (TextView) findViewById(R.id.tv_submit);

        resortDetailsArrayList = (ArrayList<ResortDetails>) getIntent().getSerializableExtra("array");

        for (int i = 0; i< resortDetailsArrayList.get(0).getResortImagesArray().size(); i++) {
            if(i == 0) {
                    if (resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("1")) {
                        Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                                .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage1);
                    } else if (resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("2")) {
                        Glide.with(getApplicationContext()).load(Constants.SOURCE2_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                                .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage1);
                    }
                    imageName1 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
                    cancel1.setVisibility(View.VISIBLE);
            }
            else if(i == 1) {
                    if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("1")) {
                        Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                                .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage2);
                    }
                    else if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("2")) {
                        Glide.with(getApplicationContext()).load(Constants.SOURCE2_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                                .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage2);
                    }
                    imageName2 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
                    cancel2.setVisibility(View.VISIBLE);
            }
            else if(i == 2) {
                    if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("1")) {
                        Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                                .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage3);
                    }
                    else if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("2")) {
                        Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                                .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage3);
                    }
                    imageName3 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
                    cancel3.setVisibility(View.VISIBLE);
            }
            else if(i == 3) {
                    if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("1")) {
                        Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                                .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage4);
                    }
                    else if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("2")) {
                        Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                                .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage4);
                    }
                    imageName4 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
                    cancel4.setVisibility(View.VISIBLE);
            }
            else if(i == 4) {
                if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("1")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage5);
                }
                else if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("2")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage5);
                }
                imageName5 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
                cancel5.setVisibility(View.VISIBLE);
            }
            else if(i == 5) {
                if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("1")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage6);
                }
                else if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("2")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage6);
                }
                imageName6 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
                cancel6.setVisibility(View.VISIBLE);
            }
            else if(i == 6) {
                if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("1")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage7);
                }
                else if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("2")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage7);
                }
                imageName7 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
                cancel7.setVisibility(View.VISIBLE);
            }
            else if(i == 7) {
                if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("1")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage8);
                }
                else if(resortDetailsArrayList.get(0).getResortImagesArray().get(i).getSource().equals("2")) {
                    Glide.with(getApplicationContext()).load(Constants.SOURCE1_URL + resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName())
                            .placeholder(getResources().getDrawable(R.drawable.empty_photo)).into(resortImage8);
                }
                imageName8 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
                cancel8.setVisibility(View.VISIBLE);
            }
        }

        addMoreImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photosLayout3.setVisibility(View.VISIBLE);
                photosLayout4.setVisibility(View.VISIBLE);
                addMoreImages.setVisibility(View.INVISIBLE);
            }
        });

        resortImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageName1.equals("")) {
                    imageSelected = 1;
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {

                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }
            }
        });

        resortImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageName2.equals("")) {
                    imageSelected = 2;
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {

                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }
            }
        });

        resortImage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("TAG","imageName3 "+imageName3);
                if(imageName3.equals("")) {
                    imageSelected = 3;

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {

                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }
            }
        });

        resortImage4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageName4.equals("")) {
                    imageSelected = 4;

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {

                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }
            }
        });

        resortImage5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageName5.equals("")) {
                    imageSelected = 5;

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {

                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }
            }
        });

        resortImage6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageName6.equals("")) {
                    imageSelected = 6;

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {

                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }
            }
        });

        resortImage7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageName7.equals("")) {
                    imageSelected = 7;

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {

                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }
            }
        });

        resortImage8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageName8.equals("")) {
                    imageSelected = 8;

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {

                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }
            }
        });

        cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Are you sure to delete the image?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل أنت متأكد من حذف الصوره؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        resortImage1.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
//                        imageName1 = "";
//                        cancel1.setVisibility(View.GONE);
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();

                deleteImagePopup(0);
            }
        });

        cancel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Are you sure to delete the image?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل أنت متأكد من حذف الصوره؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        resortImage2.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
//                        imageName2 = "";
//                        cancel2.setVisibility(View.GONE);
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                deleteImagePopup(1);
            }
        });

        cancel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Are you sure to delete the image?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل أنت متأكد من حذف الصوره؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        resortImage3.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
//                        imageName3 = "";
//                        cancel3.setVisibility(View.GONE);
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                deleteImagePopup(2);
            }
        });

        cancel4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Are you sure to delete the image?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل أنت متأكد من حذف الصوره؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        resortImage4.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
//                        imageName4 = "";
//                        cancel4.setVisibility(View.GONE);
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                deleteImagePopup(3);
            }
        });

        cancel5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Are you sure to delete the image?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل أنت متأكد من حذف الصوره؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        resortImage5.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
//                        imageName5 = "";
//                        cancel5.setVisibility(View.GONE);
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                deleteImagePopup(4);
            }
        });

        cancel6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Are you sure to delete the image?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل أنت متأكد من حذف الصوره؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        resortImage6.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
//                        imageName6 = "";
//                        cancel6.setVisibility(View.GONE);
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                deleteImagePopup(5);
            }
        });

        cancel7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Are you sure to delete the image?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل أنت متأكد من حذف الصوره؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        resortImage7.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
//                        imageName7 = "";
//                        cancel7.setVisibility(View.GONE);
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                deleteImagePopup(6);
            }
        });

        cancel8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                String appName, title, positive, negative;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Are you sure to delete the image?";
//                    positive = getResources().getString(R.string.str_btn_yes);
//                    negative = getResources().getString(R.string.str_btn_no);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "هل أنت متأكد من حذف الصوره؟";
//                    positive = getResources().getString(R.string.str_btn_yes_ar);
//                    negative = getResources().getString(R.string.str_btn_no_ar);
//                }
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setNegativeLabel(negative);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                        resortImage8.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
//                        imageName8 = "";
//                        cancel8.setVisibility(View.GONE);
//                    }
//                });
//                iOSDialog.setNegativeListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                deleteImagePopup(7);
            }
        });

        updatePhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageName1.equals("") && imageName2.equals("") && imageName3.equals("") && imageName4.equals("")
                        && imageName5.equals("") && imageName6.equals("") && imageName7.equals("") && imageName8.equals("")){
//                    final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                    iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                    iOSDialog.setSubtitle("Please upload atleast one Photo");
//                    iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpdatePhotosActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.app_name));
                        yes.setText(getResources().getString(R.string.str_btn_ok));
                        desc.setText("Please upload atleast one Photo");
                    }
                    else{
                        title.setText(getResources().getString(R.string.app_name_ar));
                        yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                        desc.setText("يرجى تحميل صورة واحدة على الأقل");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    jsonPreparation();
//                    Toast.makeText(getApplicationContext(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(UpdatePhotosActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:
                if (canAccessStorage()) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Storage permission denied, Unable to select pictures", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE_FROM_GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();

                    try {
                        if (imageSelected == 1) {
                            thumbnail1 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            selectedPath1 = getPath(uri);
                        } else if (imageSelected == 2) {
                            thumbnail2 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            selectedPath2 = getPath(uri);
                        } else if (imageSelected == 3) {
                            thumbnail3 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            selectedPath3 = getPath(uri);
                        } else if (imageSelected == 4) {
                            thumbnail4 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            selectedPath4 = getPath(uri);
                        } else if (imageSelected == 5) {
                            thumbnail5 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            selectedPath5 = getPath(uri);
                        } else if (imageSelected == 6) {
                            thumbnail6 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            selectedPath6 = getPath(uri);
                        } else if (imageSelected == 7) {
                            thumbnail7 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            selectedPath7 = getPath(uri);
                        } else if (imageSelected == 8) {
                            thumbnail8 = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            selectedPath8 = getPath(uri);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    convertPixel();
                }
                break;
        }
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void convertPixel() {

        if(imageSelected == 1) {
            imageName1 = "";
            thumbnail1 = Bitmap.createScaledBitmap(thumbnail1, 700, 500,
                    false);
            thumbnail1 = codec(thumbnail1, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            resortImage1.setImageDrawable(drawable);
            cancel1.setVisibility(View.VISIBLE);

            new Dobackground().execute();
        }
        else if(imageSelected == 2) {
            imageName2 = "";
            thumbnail2 = Bitmap.createScaledBitmap(thumbnail2, 700, 500,
                    false);
            thumbnail2 = codec(thumbnail2, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            resortImage2.setImageDrawable(drawable);
            cancel2.setVisibility(View.VISIBLE);

            new Dobackground().execute();
        }
        else if(imageSelected == 3) {
            imageName3 = "";
            thumbnail3 = Bitmap.createScaledBitmap(thumbnail3, 700, 500,
                    false);
            thumbnail3 = codec(thumbnail3, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            resortImage3.setImageDrawable(drawable);
            cancel3.setVisibility(View.VISIBLE);

            new Dobackground().execute();
        }
        else if(imageSelected == 4) {
            imageName4 = "";
            thumbnail4 = Bitmap.createScaledBitmap(thumbnail4, 700, 500,
                    false);
            thumbnail4 = codec(thumbnail4, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            resortImage4.setImageDrawable(drawable);
            cancel4.setVisibility(View.VISIBLE);

            new Dobackground().execute();
        }
        else if(imageSelected == 5) {
            imageName5 = "";
            thumbnail5 = Bitmap.createScaledBitmap(thumbnail5, 700, 500,
                    false);
            thumbnail5 = codec(thumbnail5, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail5);
            resortImage5.setImageDrawable(drawable);
            cancel5.setVisibility(View.VISIBLE);

            new Dobackground().execute();
        }
        else if(imageSelected == 6) {
            imageName6 = "";
            thumbnail6 = Bitmap.createScaledBitmap(thumbnail6, 700, 500,
                    false);
            thumbnail6 = codec(thumbnail6, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail6);
            resortImage6.setImageDrawable(drawable);
            cancel6.setVisibility(View.VISIBLE);

            new Dobackground().execute();
        }
        else if(imageSelected == 7) {
            imageName7 = "";
            thumbnail7 = Bitmap.createScaledBitmap(thumbnail7, 700, 500,
                    false);
            thumbnail7 = codec(thumbnail7, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail7);
            resortImage7.setImageDrawable(drawable);
            cancel7.setVisibility(View.VISIBLE);

            new Dobackground().execute();
        }
        else if(imageSelected == 8) {
            imageName8 = "";
            thumbnail8 = Bitmap.createScaledBitmap(thumbnail8, 700, 500,
                    false);
            thumbnail8 = codec(thumbnail8, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail8);
            resortImage8.setImageDrawable(drawable);
            cancel8.setVisibility(View.VISIBLE);

            new Dobackground().execute();
        }
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    public void jsonPreparation(){
        final JSONObject parent = new JSONObject();
        String cityID = resortDetailsArrayList.get(0).getCityId();
        String minCap = resortDetailsArrayList.get(0).getMinCapacity();
        String maxCap = resortDetailsArrayList.get(0).getMaxCapacity();
        String selectedCategory = resortDetailsArrayList.get(0).getResortType();
        String strAddress = resortDetailsArrayList.get(0).getResortAddress();
        String resortId = resortDetailsArrayList.get(0).getResortId();
        String resortName = resortDetailsArrayList.get(0).getResortName();
        String resortDesc = resortDetailsArrayList.get(0).getDescription();
        String resortNameAr = resortDetailsArrayList.get(0).getResortNameAr();
        String resortDescAr = resortDetailsArrayList.get(0).getDescriptionAr();
        double resortLatitude = Double.parseDouble(resortDetailsArrayList.get(0).getLatitude());
        double resortLongitude = Double.parseDouble(resortDetailsArrayList.get(0).getLongitude());

        String sundayMenPrice = resortDetailsArrayList.get(0).getSundayMenPrice();
        String mondayMenPrice = resortDetailsArrayList.get(0).getMondayMenPrice();
        String tuesdayMenPrice = resortDetailsArrayList.get(0).getTuesdayMenPrice();
        String wednesdayMenPrice = resortDetailsArrayList.get(0).getWednesdayMenPrice();
        String thursdayMenPrice = resortDetailsArrayList.get(0).getThursadayMenPrice();
        String fridayMenPrice = resortDetailsArrayList.get(0).getFridayMenPrice();
        String saturdayMenPrice = resortDetailsArrayList.get(0).getSaturdayMenPrice();

        String sundayWomenPrice = resortDetailsArrayList.get(0).getSundayWomenPrice();
        String mondayWomenPrice = resortDetailsArrayList.get(0).getMondayWomenPrice();
        String tuesdayWomenPrice = resortDetailsArrayList.get(0).getTuesdayWomenPrice();
        String wednesdayWomenPrice = resortDetailsArrayList.get(0).getWednesdayWomenPrice();
        String thursdayWomenPrice = resortDetailsArrayList.get(0).getThursdayWomenPrice();
        String fridayWomenPrice = resortDetailsArrayList.get(0).getFridayWomenPrice();
        String saturdayWomenPrice = resortDetailsArrayList.get(0).getSaturdayWomenPrice();

        String sundayFamilyPrice = resortDetailsArrayList.get(0).getSundayFamilyPrice();
        String mondayFamilyPrice = resortDetailsArrayList.get(0).getMondayFamilyPrice();
        String tuesdayFamilyPrice = resortDetailsArrayList.get(0).getTuesdayFamilyPrice();
        String wednesdayFamilyPrice = resortDetailsArrayList.get(0).getWednesdayFamilyPrice();
        String thursdayFamilyPrice = resortDetailsArrayList.get(0).getThursdayFamilyPrice();
        String fridayFamilyPrice = resortDetailsArrayList.get(0).getFridayFamilyPrice();
        String saturdayFamilyPrice = resortDetailsArrayList.get(0).getSaturdayFamilyPrice();
        String advanceAmount = resortDetailsArrayList.get(0).getAdvanceAmount();
        String vatNumb = resortDetailsArrayList.get(0).getVatNumber();
        String bankNam = resortDetailsArrayList.get(0).getBankName();
        String iban = resortDetailsArrayList.get(0).getIbanNumber();
        int vat = 1;
        if(resortDetailsArrayList.get(0).getVatAvailability().equalsIgnoreCase("false")){
            vat = 0;
        }

        String kitchenPrice = resortDetailsArrayList.get(0).getKitchenPrice();

        for (int i = 0; i< resortDetailsArrayList.get(0).getResortFacilities().size(); i++){
            selectedFacilitiesId.add(resortDetailsArrayList.get(0).getResortFacilities().get(i).getFacilityId());
        }

        if(selectedCategory.contains("Banquet Hall")) {

                try {
                    JSONObject mainObj = new JSONObject();
                    mainObj.put("ResortId", resortId);
                    mainObj.put("ResortTypeId", "3");
                    mainObj.put("CityId", cityID);
                    mainObj.put("IsManAvailability", 1);
                    mainObj.put("IsWomanAvailability", 1);
                    mainObj.put("IsKitchenavailability", 1);
                    mainObj.put("MinCapacity", Integer.parseInt(minCap));
                    mainObj.put("MaxCapacity", Integer.parseInt(maxCap));
                    mainObj.put("ResortName", resortName);
                    mainObj.put("ResortName_Ar", resortNameAr);
                    mainObj.put("Description_Ar", resortDescAr);
                    mainObj.put("ResortAddress", strAddress);
                    mainObj.put("Latitude", resortLatitude);
                    mainObj.put("Longitude", resortLongitude);
                    mainObj.put("Description", resortDesc);
                    mainObj.put("HostId", hostId);
                    mainObj.put("VatNumber", vatNumb);
                    mainObj.put("BankName", bankNam);
                    mainObj.put("IbanNumber", iban);
                    mainObj.put("vatavailability", vat);
                    mainObj.put("AdvanceAmount", advanceAmount);

//                    Prices Array
                    JSONArray pricingArray = new JSONArray();
                    JSONObject saturdayObj = new JSONObject();
                    saturdayObj.put("Did", "1");
                    saturdayObj.put("MenPrice", saturdayMenPrice);
                    saturdayObj.put("WomenPrice", saturdayWomenPrice);
                    saturdayObj.put("FamilyPrice", saturdayFamilyPrice);
                    saturdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(saturdayObj);

                    JSONObject sundayObj = new JSONObject();
                    sundayObj.put("Did", "2");
                    sundayObj.put("MenPrice", sundayMenPrice);
                    sundayObj.put("WomenPrice", sundayWomenPrice);
                    sundayObj.put("FamilyPrice", sundayFamilyPrice);
                    sundayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(sundayObj);

                    JSONObject mondayObj = new JSONObject();
                    mondayObj.put("Did", "3");
                    mondayObj.put("MenPrice", mondayMenPrice);
                    mondayObj.put("WomenPrice", mondayWomenPrice);
                    mondayObj.put("FamilyPrice", mondayFamilyPrice);
                    mondayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(mondayObj);

                    JSONObject tuesdayObj = new JSONObject();
                    tuesdayObj.put("Did", "4");
                    tuesdayObj.put("MenPrice", tuesdayMenPrice);
                    tuesdayObj.put("WomenPrice", tuesdayWomenPrice);
                    tuesdayObj.put("FamilyPrice", tuesdayFamilyPrice);
                    tuesdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(tuesdayObj);

                    JSONObject wednesdayObj = new JSONObject();
                    wednesdayObj.put("Did", "5");
                    wednesdayObj.put("MenPrice", wednesdayMenPrice);
                    wednesdayObj.put("WomenPrice", wednesdayWomenPrice);
                    wednesdayObj.put("FamilyPrice", wednesdayFamilyPrice);
                    wednesdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(wednesdayObj);

                    JSONObject thursdayObj = new JSONObject();
                    thursdayObj.put("Did", "6");
                    thursdayObj.put("MenPrice", thursdayMenPrice);
                    thursdayObj.put("WomenPrice", thursdayWomenPrice);
                    thursdayObj.put("FamilyPrice", thursdayFamilyPrice);
                    thursdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(thursdayObj);

                    JSONObject fridayObj = new JSONObject();
                    fridayObj.put("Did", "7");
                    fridayObj.put("MenPrice", fridayMenPrice);
                    fridayObj.put("WomenPrice", fridayWomenPrice);
                    fridayObj.put("FamilyPrice", fridayFamilyPrice);
                    fridayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(fridayObj);

                    JSONArray imagesArray = new JSONArray();
                    if (imageName1 != null && !imageName1.equals("")) {
                        JSONObject image1 = new JSONObject();
                        image1.put("ImageName", imageName1);
                        imagesArray.put(image1);
                    }
                    if (imageName2!= null && !imageName2.equals("")) {
                        JSONObject image2 = new JSONObject();
                        image2.put("ImageName", imageName2);
                        imagesArray.put(image2);
                    }
                    if (imageName3!= null && !imageName3.equals("")) {
                        JSONObject image3 = new JSONObject();
                        image3.put("ImageName", imageName3);
                        imagesArray.put(image3);
                    }
                    if (imageName4!= null && !imageName4.equals("")) {
                        JSONObject image4 = new JSONObject();
                        image4.put("ImageName", imageName4);
                        imagesArray.put(image4);
                    }
                    if (imageName5!= null && !imageName5.equals("")) {
                        JSONObject image5 = new JSONObject();
                        image5.put("ImageName", imageName5);
                        imagesArray.put(image5);
                    }
                    if (imageName6!= null && !imageName6.equals("")) {
                        JSONObject image6 = new JSONObject();
                        image6.put("ImageName", imageName6);
                        imagesArray.put(image6);
                    }
                    if (imageName7!= null && !imageName7.equals("")) {
                        JSONObject image7 = new JSONObject();
                        image7.put("ImageName", imageName7);
                        imagesArray.put(image7);
                    }
                    if (imageName8!= null && !imageName8.equals("")) {
                        JSONObject image8 = new JSONObject();
                        image8.put("ImageName", imageName8);
                        imagesArray.put(image8);
                    }

                    JSONArray facilitiesArray = new JSONArray();
                    for (int i = 0; i < selectedFacilitiesId.size(); i++) {
                        JSONObject faciltyObj = new JSONObject();
                        faciltyObj.put("FacilityId", selectedFacilitiesId.get(i));
                        facilitiesArray.put(faciltyObj);
                    }

                    parent.put("UpdateResort", mainObj);
                    parent.put("UpdatePrices", pricingArray);
                    parent.put("UpdateResortImages", imagesArray);
                    parent.put("UpdateResortFacility", facilitiesArray);
                    Log.i("TAG", parent.toString());
                    new UpdateResort().execute(parent.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        else if(selectedCategory.contains("Wedding Hall")) {

                try {

                    JSONObject mainObj = new JSONObject();
                    mainObj.put("ResortId", resortId);
                    mainObj.put("ResortTypeId", "1");
                    mainObj.put("CityId", cityID);
                    mainObj.put("IsManAvailability", 1);
                    mainObj.put("IsWomanAvailability", 1);
                    mainObj.put("IsKitchenavailability", 1);
                    mainObj.put("MinCapacity", Integer.parseInt(minCap));
                    mainObj.put("MaxCapacity", Integer.parseInt(maxCap));
                    mainObj.put("ResortName", resortName);
                    mainObj.put("ResortName_Ar", resortNameAr);
                    mainObj.put("Description_Ar", resortDescAr);
                    mainObj.put("ResortAddress", strAddress);
                    mainObj.put("Latitude", resortLatitude);
                    mainObj.put("Longitude", resortLongitude);
                    mainObj.put("Description", resortDesc);
                    mainObj.put("HostId", hostId);
                    mainObj.put("VatNumber", vatNumb);
                    mainObj.put("BankName", bankNam);
                    mainObj.put("IbanNumber", iban);
                    mainObj.put("vatavailability", vat);
                    mainObj.put("AdvanceAmount", advanceAmount);

//                    Prices Array
                    JSONArray pricingArray = new JSONArray();
                    JSONObject saturdayObj = new JSONObject();
                    saturdayObj.put("Did", "1");
                    saturdayObj.put("MenPrice", saturdayMenPrice);
                    saturdayObj.put("WomenPrice", saturdayWomenPrice);
                    saturdayObj.put("FamilyPrice", saturdayFamilyPrice);
                    saturdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(saturdayObj);

                    JSONObject sundayObj = new JSONObject();
                    sundayObj.put("Did", "2");
                    sundayObj.put("MenPrice", sundayMenPrice);
                    sundayObj.put("WomenPrice", sundayWomenPrice);
                    sundayObj.put("FamilyPrice", sundayFamilyPrice);
                    sundayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(sundayObj);

                    JSONObject mondayObj = new JSONObject();
                    mondayObj.put("Did", "3");
                    mondayObj.put("MenPrice", mondayMenPrice);
                    mondayObj.put("WomenPrice", mondayWomenPrice);
                    mondayObj.put("FamilyPrice", mondayFamilyPrice);
                    mondayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(mondayObj);

                    JSONObject tuesdayObj = new JSONObject();
                    tuesdayObj.put("Did", "4");
                    tuesdayObj.put("MenPrice", tuesdayMenPrice);
                    tuesdayObj.put("WomenPrice", tuesdayWomenPrice);
                    tuesdayObj.put("FamilyPrice", tuesdayFamilyPrice);
                    tuesdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(tuesdayObj);

                    JSONObject wednesdayObj = new JSONObject();
                    wednesdayObj.put("Did", "5");
                    wednesdayObj.put("MenPrice", wednesdayMenPrice);
                    wednesdayObj.put("WomenPrice", wednesdayWomenPrice);
                    wednesdayObj.put("FamilyPrice", wednesdayFamilyPrice);
                    wednesdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(wednesdayObj);

                    JSONObject thursdayObj = new JSONObject();
                    thursdayObj.put("Did", "6");
                    thursdayObj.put("MenPrice", thursdayMenPrice);
                    thursdayObj.put("WomenPrice", thursdayWomenPrice);
                    thursdayObj.put("FamilyPrice", thursdayFamilyPrice);
                    thursdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(thursdayObj);

                    JSONObject fridayObj = new JSONObject();
                    fridayObj.put("Did", "7");
                    fridayObj.put("MenPrice", fridayMenPrice);
                    fridayObj.put("WomenPrice", fridayWomenPrice);
                    fridayObj.put("FamilyPrice", fridayFamilyPrice);
                    fridayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(fridayObj);

                    JSONArray imagesArray = new JSONArray();

                    if (imageName1 != null && !imageName1.equals("")) {
                        JSONObject image1 = new JSONObject();
                        image1.put("ImageName", imageName1);
                        imagesArray.put(image1);
                    }
                    if (imageName2!= null && !imageName2.equals("")) {
                        JSONObject image2 = new JSONObject();
                        image2.put("ImageName", imageName2);
                        imagesArray.put(image2);
                    }
                    if (imageName3!= null && !imageName3.equals("")) {
                        JSONObject image3 = new JSONObject();
                        image3.put("ImageName", imageName3);
                        imagesArray.put(image3);
                    }
                    if (imageName4!= null && !imageName4.equals("")) {
                        JSONObject image4 = new JSONObject();
                        image4.put("ImageName", imageName4);
                        imagesArray.put(image4);
                    }
                    if (imageName5!= null && !imageName5.equals("")) {
                        JSONObject image5 = new JSONObject();
                        image5.put("ImageName", imageName5);
                        imagesArray.put(image5);
                    }
                    if (imageName6!= null && !imageName6.equals("")) {
                        JSONObject image6 = new JSONObject();
                        image6.put("ImageName", imageName6);
                        imagesArray.put(image6);
                    }
                    if (imageName7!= null && !imageName7.equals("")) {
                        JSONObject image7 = new JSONObject();
                        image7.put("ImageName", imageName7);
                        imagesArray.put(image7);
                    }
                    if (imageName8!= null && !imageName8.equals("")) {
                        JSONObject image8 = new JSONObject();
                        image8.put("ImageName", imageName8);
                        imagesArray.put(image8);
                    }


                    JSONArray facilitiesArray = new JSONArray();
                    for (int i = 0; i < selectedFacilitiesId.size(); i++) {
                        JSONObject faciltyObj = new JSONObject();
                        faciltyObj.put("FacilityId", selectedFacilitiesId.get(i));
                        facilitiesArray.put(faciltyObj);
                    }

                    parent.put("UpdateResort", mainObj);
                    parent.put("UpdatePrices", pricingArray);
                    parent.put("UpdateResortImages", imagesArray);
                    parent.put("UpdateResortFacility", facilitiesArray);

                    Log.i("TAG", parent.toString());
                    new UpdateResort().execute(parent.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        else if(selectedCategory.contains("Resort")) {
                try {
                    JSONObject mainObj = new JSONObject();
                    mainObj.put("ResortId", resortId);
                    mainObj.put("ResortTypeId", "2");
                    mainObj.put("CityId", cityID);
                    mainObj.put("IsManAvailability", 1);
                    mainObj.put("IsWomanAvailability", 1);
                    mainObj.put("IsKitchenavailability", 1);
                    mainObj.put("MinCapacity", Integer.parseInt(minCap));
                    mainObj.put("MaxCapacity", Integer.parseInt(maxCap));
                    mainObj.put("ResortName", resortName);
                    mainObj.put("ResortName_Ar", resortNameAr);
                    mainObj.put("Description_Ar", resortDescAr);
                    mainObj.put("ResortAddress", strAddress);
                    mainObj.put("Latitude", resortLatitude);
                    mainObj.put("Longitude", resortLongitude);
                    mainObj.put("Description", resortDesc);
                    mainObj.put("HostId", hostId);
                    mainObj.put("VatNumber", vatNumb);
                    mainObj.put("BankName", bankNam);
                    mainObj.put("IbanNumber", iban);
                    mainObj.put("vatavailability", vat);
                    mainObj.put("AdvanceAmount", advanceAmount);

//                    Prices Array
                    JSONArray pricingArray = new JSONArray();
                    JSONObject saturdayObj = new JSONObject();
                    saturdayObj.put("Did", "1");
                    saturdayObj.put("MenPrice", saturdayMenPrice);
                    saturdayObj.put("WomenPrice", saturdayWomenPrice);
                    saturdayObj.put("FamilyPrice", saturdayFamilyPrice);
                    saturdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(saturdayObj);

                    JSONObject sundayObj = new JSONObject();
                    sundayObj.put("Did", "2");
                    sundayObj.put("MenPrice", sundayMenPrice);
                    sundayObj.put("WomenPrice", sundayWomenPrice);
                    sundayObj.put("FamilyPrice", sundayFamilyPrice);
                    sundayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(sundayObj);

                    JSONObject mondayObj = new JSONObject();
                    mondayObj.put("Did", "3");
                    mondayObj.put("MenPrice", mondayMenPrice);
                    mondayObj.put("WomenPrice", mondayWomenPrice);
                    mondayObj.put("FamilyPrice", mondayFamilyPrice);
                    mondayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(mondayObj);

                    JSONObject tuesdayObj = new JSONObject();
                    tuesdayObj.put("Did", "4");
                    tuesdayObj.put("MenPrice", tuesdayMenPrice);
                    tuesdayObj.put("WomenPrice", tuesdayWomenPrice);
                    tuesdayObj.put("FamilyPrice", tuesdayFamilyPrice);
                    tuesdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(tuesdayObj);

                    JSONObject wednesdayObj = new JSONObject();
                    wednesdayObj.put("Did", "5");
                    wednesdayObj.put("MenPrice", wednesdayMenPrice);
                    wednesdayObj.put("WomenPrice", wednesdayWomenPrice);
                    wednesdayObj.put("FamilyPrice", wednesdayFamilyPrice);
                    wednesdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(wednesdayObj);

                    JSONObject thursdayObj = new JSONObject();
                    thursdayObj.put("Did", "6");
                    thursdayObj.put("MenPrice", thursdayMenPrice);
                    thursdayObj.put("WomenPrice", thursdayWomenPrice);
                    thursdayObj.put("FamilyPrice", thursdayFamilyPrice);
                    thursdayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(thursdayObj);

                    JSONObject fridayObj = new JSONObject();
                    fridayObj.put("Did", "7");
                    fridayObj.put("MenPrice", fridayMenPrice);
                    fridayObj.put("WomenPrice", fridayWomenPrice);
                    fridayObj.put("FamilyPrice", fridayFamilyPrice);
                    fridayObj.put("kitchenprice", kitchenPrice);
                    pricingArray.put(fridayObj);

                    JSONArray imagesArray = new JSONArray();
                    if (imageName1 != null && !imageName1.equals("")) {
                        JSONObject image1 = new JSONObject();
                        image1.put("ImageName", imageName1);
                        imagesArray.put(image1);
                    }
                    if (imageName2!= null && !imageName2.equals("")) {
                        JSONObject image2 = new JSONObject();
                        image2.put("ImageName", imageName2);
                        imagesArray.put(image2);
                    }
                    if (imageName3!= null && !imageName3.equals("")) {
                        JSONObject image3 = new JSONObject();
                        image3.put("ImageName", imageName3);
                        imagesArray.put(image3);
                    }
                    if (imageName4!= null && !imageName4.equals("")) {
                        JSONObject image4 = new JSONObject();
                        image4.put("ImageName", imageName4);
                        imagesArray.put(image4);
                    }
                    if (imageName5!= null && !imageName5.equals("")) {
                        JSONObject image5 = new JSONObject();
                        image5.put("ImageName", imageName5);
                        imagesArray.put(image5);
                    }
                    if (imageName6!= null && !imageName6.equals("")) {
                        JSONObject image6 = new JSONObject();
                        image6.put("ImageName", imageName6);
                        imagesArray.put(image6);
                    }
                    if (imageName7!= null && !imageName7.equals("")) {
                        JSONObject image7 = new JSONObject();
                        image7.put("ImageName", imageName7);
                        imagesArray.put(image7);
                    }
                    if (imageName8!= null && !imageName8.equals("")) {
                        JSONObject image8 = new JSONObject();
                        image8.put("ImageName", imageName8);
                        imagesArray.put(image8);
                    }

                    JSONArray facilitiesArray = new JSONArray();
                    for (int i = 0; i < selectedFacilitiesId.size(); i++) {
                        JSONObject faciltyObj = new JSONObject();
                        faciltyObj.put("FacilityId", selectedFacilitiesId.get(i));
                        facilitiesArray.put(faciltyObj);
                    }

                    parent.put("UpdateResort", mainObj);
                    parent.put("UpdatePrices", pricingArray);
                    parent.put("UpdateResortImages", imagesArray);
                    parent.put("UpdateResortFacility", facilitiesArray);
                    Log.i("TAG", parent.toString());
                    new UpdateResort().execute(parent.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    public class UpdateResort extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(UpdatePhotosActivity.this);
            dialog = new ACProgressFlower.Builder(UpdatePhotosActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.RESORT_UPDATE_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(UpdatePhotosActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(UpdatePhotosActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
//
                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject ja = jo.getJSONObject("Success");

//                                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                                String appName, title, positive, negative;
//                                if(language.equalsIgnoreCase("En")){
//                                    appName = getResources().getString(R.string.app_name);
//                                    title = "Update successful.";
//                                    positive = getResources().getString(R.string.str_btn_ok);
//                                    negative = getResources().getString(R.string.str_btn_no);
//                                }
//                                else{
//                                    appName = getResources().getString(R.string.app_name_ar);
//                                    title = "تم التحديث بنجاح";
//                                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                                    negative = getResources().getString(R.string.str_btn_no_ar);
//                                }
//                                iOSDialog.setTitle(appName);
//                                iOSDialog.setSubtitle(title);
//                                iOSDialog.setPositiveLabel(positive);
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        Intent intent = new Intent(UpdatePhotosActivity.this, MainActivity.class);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        startActivity(intent);
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpdatePhotosActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText("Update successful.");
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText("تم التحديث بنجاح");
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
//
//                                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle("Update successful.");
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        Intent intent = new Intent(UpdatePhotosActivity.this, MainActivity.class);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        startActivity(intent);
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                            }catch (JSONException je){
//                                final iOSDialog iOSDialog = new iOSDialog(UpdatePhotosActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(result);
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpdatePhotosActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText(result);
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText(result);
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(UpdatePhotosActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    class Dobackground extends AsyncTask<Void, Void, Void> {
        ACProgressFlower dialog;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(UpdatePhotosActivity.this);
            dialog = new ACProgressFlower.Builder(UpdatePhotosActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params1) {
            // TODO Auto-generated method stub
//            submit();

            File file1=null;
            HttpResponse httpResponse = null;

            if (imageSelected == 1) {
                if(thumbnail1!=null){
                    original_imagePath1=StoreByteImage(thumbnail1, false);
                }
            } else if (imageSelected == 2) {
                if(thumbnail2!=null){
                    original_imagePath1=StoreByteImage(thumbnail2, false);
                }
            } else if (imageSelected == 3) {
                if(thumbnail3!=null){
                    original_imagePath1=StoreByteImage(thumbnail3, false);
                }
            } else if (imageSelected == 4) {
                if(thumbnail4!=null){
                    original_imagePath1=StoreByteImage(thumbnail4, false);
                }
            }
            else if (imageSelected == 5) {
                if(thumbnail5!=null){
                    original_imagePath1=StoreByteImage(thumbnail5, false);
                }
            }
            else if (imageSelected == 6) {
                if(thumbnail6!=null){
                    original_imagePath1=StoreByteImage(thumbnail6, false);
                }
            }
            else if (imageSelected == 7) {
                if(thumbnail7!=null){
                    original_imagePath1=StoreByteImage(thumbnail7, false);
                }
            }
            else if (imageSelected == 8) {
                if(thumbnail8!=null){
                    original_imagePath1=StoreByteImage(thumbnail8, false);
                }
            }

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            mHttpClient11 = new DefaultHttpClient(params);

            try{
                HttpPost httppost = new HttpPost(Constants.UPLOAD_IMAGE_URL);

                if(original_imagePath1!=null){
                    file1 = new File(original_imagePath1);
                }

                MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                if(null!=file1&& file1.exists()){
                    multipartEntity.addPart("image1", new FileBody(file1));
                }

                httppost.setEntity(multipartEntity);

                httpResponse=  mHttpClient11.execute(httppost);

	      /* String responseString = EntityUtils.toString(response.getEntity());
	        Log.d("UPLOAD", "------------------------------"+responseString);*/

            }catch(Exception e){
                e.printStackTrace();
            }

            // key and value pair


            InputStream entity = null;
            try {
                entity = httpResponse.getEntity().getContent();
                if(entity != null) {
                    imageResponse = convertInputStreamToString(entity);
                    Log.i("TAG", "user response:" + imageResponse);
                }
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Log.i("TAG","-----------------response-----------");

            // data = parser.getDocument(entity);
            return null;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            if(imageResponse!=null && imageResponse.length()>0) {
                try {
                    JSONArray resultArray = new JSONArray(imageResponse);
                    for (int i = 0; i < resultArray.length(); i++){
                        JSONObject jsonObject = resultArray.getJSONObject(i);
                        if (imageSelected == 1) {
                            imageName1 = jsonObject.getString("Name");
                        } else if (imageSelected == 2) {
                            imageName2 = jsonObject.getString("Name");
                        } else if (imageSelected == 3) {
                            imageName3 = jsonObject.getString("Name");
                        } else if (imageSelected == 4) {
                            imageName4 = jsonObject.getString("Name");
                        }else if (imageSelected == 5) {
                            imageName5 = jsonObject.getString("Name");
                        } else if (imageSelected == 6) {
                            imageName6 = jsonObject.getString("Name");
                        } else if (imageSelected == 7) {
                            imageName7 = jsonObject.getString("Name");
                        } else if (imageSelected == 8) {
                            imageName8 = jsonObject.getString("Name");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(UpdatePhotosActivity.this, "Images upload failed.Please try again", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(UpdatePhotosActivity.this, "خطأ في تحميل الصور ، الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                }
            }
            if(dialog!=null){
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    public String StoreByteImage(Bitmap bitmap, boolean isDocument) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        String pictureName = "";
        pictureName = "IMG_" + hostId + "_" + timeStamp + ".jpg";

        File sdImageMainDirectory = new File("/sdcard/"+ pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

			/*Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,

			imageData.length,options);*/
            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();

    }

    public void deleteImagePopup(final int position){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpdatePhotosActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        String title, positive, negative;
        if(language.equalsIgnoreCase("En")){
            title = "Are you sure to delete the image?";
            positive = getResources().getString(R.string.str_btn_yes);
            negative = getResources().getString(R.string.str_btn_no);
        }
        else{
            title = "هل أنت متأكد من حذف الصوره؟";
            positive = getResources().getString(R.string.str_btn_yes_ar);
            negative = getResources().getString(R.string.str_btn_no_ar);
        }

        desc.setText(title);
        yes.setText(positive);
        no.setText(negative);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                if(position == 0){
                    resortImage1.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                    imageName1 = "";
                    cancel1.setVisibility(View.GONE);
                }
                else if(position == 1){
                    resortImage2.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                    imageName2 = "";
                    cancel2.setVisibility(View.GONE);
                }
                else if(position == 2){
                    resortImage3.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                    imageName3 = "";
                    cancel3.setVisibility(View.GONE);
                }
                else if(position == 3){
                    resortImage4.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                    imageName4 = "";
                    cancel4.setVisibility(View.GONE);
                }
                else if(position == 4){
                    resortImage5.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                    imageName5 = "";
                    cancel5.setVisibility(View.GONE);
                }
                else if(position == 5){
                    resortImage6.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                    imageName6 = "";
                    cancel6.setVisibility(View.GONE);
                }
                else if(position == 6){
                    resortImage7.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                    imageName7 = "";
                    cancel7.setVisibility(View.GONE);
                }
                else if(position == 7){
                    resortImage8.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                    imageName8 = "";
                    cancel8.setVisibility(View.GONE);
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
