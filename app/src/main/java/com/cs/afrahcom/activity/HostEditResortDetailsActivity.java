package com.cs.afrahcom.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.adapter.HostEditFacilitiesAdapter;
import com.cs.afrahcom.adapter.HostFacilitiesAdapter;
import com.cs.afrahcom.models.Facilities;
import com.cs.afrahcom.models.ResortDetails;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by CS on 15-11-2017.
 */

public class HostEditResortDetailsActivity extends AppCompatActivity {

    ArrayList<ResortDetails> resortDetailsArrayList = new ArrayList<>();
    ArrayList<Facilities> facilitiesArrayList = new ArrayList<>();
    EditText etResortName, etResortNameAr, etResortAddress, etResortDescription, etResortDescriptionAr;
    TextView tvCityName;
    ImageView map;
    String cityName = "";
    GridView mFacilityGrid;
    HostFacilitiesAdapter mHostFacilitiesAdapter;
    public static ArrayList<String> selectedFacilities = new ArrayList<>();
    public static ArrayList<String> selectedFacilitiesId = new ArrayList<>();
    ArrayList<String> facilitiesArray = new ArrayList<>();
    HostEditFacilitiesAdapter mAdapter;
    LinearLayout kitchenLayout;
    String selectedCategory;
    TextView resortType;
    EditText etMinCapacity, etMaxCapacity;
    public static String minCap, maxCap;
    EditText sundayMen, mondayMen, tuesdayMen, wednesdayMen, thursdayMen, fridayMen, saturdayMen, kitchenPrice, AdvanceAmount;
    EditText sundayWomen, mondayWomen, tuesdayWomen, wednesdayWomen, thursdayWomen, fridayWomen, saturdayWomen;
    EditText sundayFamily, mondayFamily, tuesdayFamily, wednesdayFamily, thursdayFamily, fridayFamily, saturdayFamily;
    String imageName1, imageName2, imageName3, imageName4, imageName5, imageName6, imageName7, imageName8;
    LinearLayout vatYesLayout, vatNoLayout;
    ImageView vatYesImage, vatNoImage;
    Boolean vatApplicable = true;
    EditText vatNumber, bankName, ibanNumber;
    String hostId, resortId, resortTypeID, cityID = "1";
    Double resortLatitude, resortLongitude;
    private static final int PLACE_PICKER_REQUEST = 1;
    TextView submit;
    ScrollView scrollView;
    String strAddress = "";
    SharedPreferences hostPrefs;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences.Editor hostPrefsEditor;
    AlertDialog customDialog;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_edit_resort);
        }
        else{
            setContentView(R.layout.activity_edit_resort_ar);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostPrefs = getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostPrefsEditor = hostPrefs.edit();
        hostId = hostPrefs.getString("hostId", "0");

        resortDetailsArrayList = (ArrayList<ResortDetails>) getIntent().getSerializableExtra("array");

        etResortName = (EditText) findViewById(R.id.et_ResortName);
        etResortNameAr = (EditText) findViewById(R.id.et_ResortNameAr);
        etResortAddress = (EditText) findViewById(R.id.et_ResortAddress);
        etResortDescription = (EditText) findViewById(R.id.et_ResortDescsription);
        etResortDescriptionAr = (EditText) findViewById(R.id.et_ResortDescsriptionAr);

        mFacilityGrid = (GridView) findViewById(R.id.facilitiesGridView);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        sundayMen = (EditText) findViewById(R.id.sundayMenPrice);
        mondayMen = (EditText) findViewById(R.id.mondayMenPrice);
        tuesdayMen = (EditText) findViewById(R.id.tuesdayMenPrice);
        wednesdayMen = (EditText) findViewById(R.id.wednesdayMenPrice);
        thursdayMen = (EditText) findViewById(R.id.thursdayMenPrice);
        fridayMen = (EditText) findViewById(R.id.fridayMenPrice);
        saturdayMen = (EditText) findViewById(R.id.saturdayMenPrice);
        kitchenPrice = (EditText) findViewById(R.id.kitchenPrice);
        etMinCapacity = (EditText) findViewById(R.id.minCapacity);
        etMaxCapacity = (EditText) findViewById(R.id.maxCapacity);
        resortType = (TextView) findViewById(R.id.categoryName);
        submit = (TextView) findViewById(R.id.tv_submit);
        AdvanceAmount = (EditText) findViewById(R.id.AdvancePrice);

        map = (ImageView) findViewById(R.id.map);

        kitchenLayout = (LinearLayout) findViewById(R.id.kitchenLayout);

        sundayWomen = (EditText) findViewById(R.id.sundayWomenPrice);
        mondayWomen = (EditText) findViewById(R.id.mondayWomenPrice);
        tuesdayWomen = (EditText) findViewById(R.id.tuesdayWomenPrice);
        wednesdayWomen = (EditText) findViewById(R.id.wednesdayWomenPrice);
        thursdayWomen = (EditText) findViewById(R.id.thursdayWomenPrice);
        fridayWomen = (EditText) findViewById(R.id.fridayWomenPrice);
        saturdayWomen = (EditText) findViewById(R.id.saturdayWomenPrice);

        sundayFamily = (EditText) findViewById(R.id.sundayFamilyPrice);
        mondayFamily = (EditText) findViewById(R.id.mondayFamilyPrice);
        tuesdayFamily = (EditText) findViewById(R.id.tuesdayFamilyPrice);
        wednesdayFamily = (EditText) findViewById(R.id.wednesdayFamilyPrice);
        thursdayFamily = (EditText) findViewById(R.id.thursdayFamiyPrice);
        fridayFamily = (EditText) findViewById(R.id.fridayFamilyPrice);
        saturdayFamily = (EditText) findViewById(R.id.saturdayFamilyPrice);

        vatNumber = (EditText) findViewById(R.id.vatNumber);
        bankName = (EditText) findViewById(R.id.bankName);
        ibanNumber = (EditText) findViewById(R.id.ibanNumber);

        vatYesLayout = (LinearLayout) findViewById(R.id.vatYes);
        vatNoLayout = (LinearLayout) findViewById(R.id.vatNo);

        vatYesImage = (ImageView) findViewById(R.id.vatYesImage);
        vatNoImage = (ImageView) findViewById(R.id.vatNoImage);
        tvCityName = (TextView) findViewById(R.id.tv_city_name);

        etResortName.setText(resortDetailsArrayList.get(0).getResortName());
        etResortNameAr.setText(resortDetailsArrayList.get(0).getResortNameAr());
        etResortAddress.setText(resortDetailsArrayList.get(0).getResortAddress());
        etResortDescription.setText(resortDetailsArrayList.get(0).getDescription());
        etResortDescriptionAr.setText(resortDetailsArrayList.get(0).getDescriptionAr());
        etMinCapacity.setText(resortDetailsArrayList.get(0).getMinCapacity());
        etMaxCapacity.setText(resortDetailsArrayList.get(0).getMaxCapacity());
        vatNumber.setText(resortDetailsArrayList.get(0).getVatNumber());
        bankName.setText(resortDetailsArrayList.get(0).getBankName());
        ibanNumber.setText(resortDetailsArrayList.get(0).getIbanNumber());
        tvCityName.setText(resortDetailsArrayList.get(0).getCity());
        cityID = resortDetailsArrayList.get(0).getCityId();

        if(resortDetailsArrayList.get(0).getVatAvailability().equalsIgnoreCase("false")){
            vatNoImage.setImageResource(R.drawable.radio_on);
            vatYesImage.setImageResource(R.drawable.radio_off);
            vatApplicable = false;
        }
        else{
            vatNoImage.setImageResource(R.drawable.radio_off);
            vatYesImage.setImageResource(R.drawable.radio_on);
            vatApplicable = true;
        }

        vatYesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vatYesImage.setImageResource(R.drawable.radio_on);
                vatNoImage.setImageResource(R.drawable.radio_off);
                vatApplicable = true;
            }
        });

        vatNoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vatNoImage.setImageResource(R.drawable.radio_on);
                vatYesImage.setImageResource(R.drawable.radio_off);
                vatApplicable = false;
            }
        });

        new GetResortsDetails().execute(Constants.GET_BANNERS_URL+"0&UserType=1");
        changePriceFields();
        getFacilities();

//        mFacilityGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.i("TAG","fac "+resortDetailsArrayList.get(0).getResortFacilities().get(i).getFacilityName());
//                if(selectedFacilities.contains(resortDetailsArrayList.get(0).getResortFacilities().get(i).getFacilityName())){
//                    selectedFacilities.remove(resortDetailsArrayList.get(0).getResortFacilities().get(i).getFacilityName());
//                }
//                else{
//                    selectedFacilities.add(resortDetailsArrayList.get(0).getResortFacilities().get(i).getFacilityName());
//                }1
//                mAdapter.notifyDataSetChanged();
//            }
//        });

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAddress();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fieldValidations();
//                Toast.makeText(getApplicationContext(), "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode, Intent data) {
        switch(requestCode){

            case PLACE_PICKER_REQUEST:
                if(resultCode == RESULT_OK){
                    final Place place = PlacePicker.getPlace(HostEditResortDetailsActivity.this, data);

                    if ("".equals(place.getAddress())) {
//                Alert Dialog
                        strAddress = "";
//                        final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                        iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                        iOSDialog.setSubtitle("Sorry! we couldn\'t detect your location. Please place the pin on your exact location.");
//                        iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                            }
//                        });
//                        iOSDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HostEditResortDetailsActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.app_name));
                            yes.setText(getResources().getString(R.string.str_btn_ok));
                            desc.setText("Sorry! we couldn't detect your location. Please place the pin on your exact location.");
                        }
                        else{
                            title.setText(getResources().getString(R.string.app_name_ar));
                            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                            desc.setText("Sorry! we couldn't detect your location. Please place the pin on your exact location.");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                    } else {
                        etResortAddress.setText(place.getAddress());
                        strAddress = place.getAddress().toString();

                        resortLatitude = place.getLatLng().latitude;
                        resortLongitude = place.getLatLng().longitude;

                        String Map_url = "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=130x100&markers=color:red%7C" +
                                resortLatitude + "," + resortLongitude;
                        Glide.with(getApplicationContext()).load(Map_url).into(map);

                        Geocoder geocoder = new Geocoder(HostEditResortDetailsActivity.this, Locale.getDefault());
                        try
                        {
                            List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude,place.getLatLng().longitude, 1);
//                            cityName = addresses.get(0).getAddressLine(1);
                            cityName = addresses.get(0).getLocality();

                            if(cityName == null){
                                cityName = addresses.get(0).getAddressLine(1);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", "cityName "+cityName);
                    }
                }
                break;
        }
    }

    public class GetResortsDetails extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            dialog = new ACProgressFlower.Builder(HostEditResortDetailsActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if(language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(HostEditResortDetailsActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(HostEditResortDetailsActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONObject ja = jo.getJSONObject("Success");

                            JSONArray jsonArray4 = ja.getJSONArray("Facilities");
                            for (int i = 0; i<jsonArray4.length(); i++){
                                Facilities facilities = new Facilities();
                                JSONObject jsonObject1 = jsonArray4.getJSONObject(i);

                                facilities.setFacilityId(jsonObject1.getString("FacilityId"));
                                facilities.setFacilityName(jsonObject1.getString("FacilityName"));
                                facilities.setFacilityNameAr(jsonObject1.getString("FacilityName_Ar"));

                                facilitiesArrayList.add(facilities);
                            }
                        } catch (Exception e) {
                            if(language.equalsIgnoreCase("En")) {
                                Toast.makeText(HostEditResortDetailsActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(HostEditResortDetailsActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                            e.printStackTrace();
                        }

                        mAdapter = new HostEditFacilitiesAdapter(HostEditResortDetailsActivity.this, facilitiesArrayList, language);
                        mFacilityGrid.setAdapter(mAdapter);
                    }
                }
            } else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(HostEditResortDetailsActivity.this, R.string.str_cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(HostEditResortDetailsActivity.this, R.string.str_cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                }
            }
//            getAddress();
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public void changePriceFields(){
        selectedCategory = resortDetailsArrayList.get(0).getResortType();
        if(language.equalsIgnoreCase("En")) {
            resortType.setText(selectedCategory);
        }
        else{
            if(selectedCategory.equalsIgnoreCase("wedding hall")){
                resortType.setText("قصور الافراح");
            }
            else if(selectedCategory.equalsIgnoreCase("resort")){
                resortType.setText("الشاليهات");
            }
            else if(selectedCategory.equalsIgnoreCase("banquet hall")){
                resortType.setText("قاعات الفنادق");
            }
        }
        strAddress = resortDetailsArrayList.get(0).getResortAddress();
        resortId = resortDetailsArrayList.get(0).getResortId();
        resortLatitude = Double.parseDouble(resortDetailsArrayList.get(0).getLatitude());
        resortLongitude = Double.parseDouble(resortDetailsArrayList.get(0).getLongitude());

        for (int i = 0; i< resortDetailsArrayList.get(0).getResortImagesArray().size(); i++) {
            if(i == 0) {
                imageName1 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
            }
            else if(i == 1) {
                    imageName2 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
            }
            else if(i == 2) {
                    imageName3 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
            }
            else if(i == 3) {
                    imageName4 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
            }
            else if(i == 4) {
                imageName5 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
            }
            else if(i == 5) {
                imageName6 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
            }
            else if(i == 6) {
                imageName7 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
            }
            else if(i == 7) {
                imageName8 = resortDetailsArrayList.get(0).getResortImagesArray().get(i).getImageName();
            }
        }
        AdvanceAmount.setText(resortDetailsArrayList.get(0).getAdvanceAmount());
        if(selectedCategory.contains("Banquet Hall")){
            resortTypeID = "3";
            sundayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSundayMenPrice()));
            mondayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getMondayMenPrice()));
            tuesdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getTuesdayMenPrice()));
            wednesdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayMenPrice()));
            thursdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayMenPrice()));
            fridayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getFridayMenPrice()));
            saturdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSaturdayMenPrice()));

            sundayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSundayWomenPrice()));
            mondayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getMondayWomenPrice()));
            tuesdayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getTuesdayWomenPrice()));
            wednesdayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayWomenPrice()));
            thursdayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getThursdayWomenPrice()));
            fridayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getFridayWomenPrice()));
            saturdayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSaturdayWomenPrice()));

            kitchenLayout.setVisibility(View.GONE);

            sundayWomen.setVisibility(View.VISIBLE);
            mondayWomen.setVisibility(View.VISIBLE);
            tuesdayWomen.setVisibility(View.VISIBLE);
            wednesdayWomen.setVisibility(View.VISIBLE);
            thursdayWomen.setVisibility(View.VISIBLE);
            fridayWomen.setVisibility(View.VISIBLE);
            saturdayWomen.setVisibility(View.VISIBLE);

            sundayFamily.setVisibility(View.INVISIBLE);
            mondayFamily.setVisibility(View.INVISIBLE);
            tuesdayFamily.setVisibility(View.INVISIBLE);
            wednesdayFamily.setVisibility(View.INVISIBLE);
            thursdayFamily.setVisibility(View.INVISIBLE);
            fridayFamily.setVisibility(View.INVISIBLE);
            saturdayFamily.setVisibility(View.INVISIBLE);

            if(language.equalsIgnoreCase("En")) {
                sundayMen.setHint(R.string.str_men_price_hint);
                mondayMen.setHint(R.string.str_men_price_hint);
                tuesdayMen.setHint(R.string.str_men_price_hint);
                wednesdayMen.setHint(R.string.str_men_price_hint);
                thursdayMen.setHint(R.string.str_men_price_hint);
                fridayMen.setHint(R.string.str_men_price_hint);
                saturdayMen.setHint(R.string.str_men_price_hint);

                sundayWomen.setHint(R.string.str_women_price_hint);
                mondayWomen.setHint(R.string.str_women_price_hint);
                tuesdayWomen.setHint(R.string.str_women_price_hint);
                wednesdayWomen.setHint(R.string.str_women_price_hint);
                thursdayWomen.setHint(R.string.str_women_price_hint);
                fridayWomen.setHint(R.string.str_women_price_hint);
                saturdayWomen.setHint(R.string.str_women_price_hint);
                AdvanceAmount.setHint(R.string.str_advance);
            }
            else{
                sundayMen.setHint(R.string.str_men_price_hint_ar);
                mondayMen.setHint(R.string.str_men_price_hint_ar);
                tuesdayMen.setHint(R.string.str_men_price_hint_ar);
                wednesdayMen.setHint(R.string.str_men_price_hint_ar);
                thursdayMen.setHint(R.string.str_men_price_hint_ar);
                fridayMen.setHint(R.string.str_men_price_hint_ar);
                saturdayMen.setHint(R.string.str_men_price_hint_ar);

                sundayWomen.setHint(R.string.str_women_price_hint_ar);
                mondayWomen.setHint(R.string.str_women_price_hint_ar);
                tuesdayWomen.setHint(R.string.str_women_price_hint_ar);
                wednesdayWomen.setHint(R.string.str_women_price_hint_ar);
                thursdayWomen.setHint(R.string.str_women_price_hint_ar);
                fridayWomen.setHint(R.string.str_women_price_hint_ar);
                saturdayWomen.setHint(R.string.str_women_price_hint_ar);
                AdvanceAmount.setHint(R.string.str_advance);
            }
        }
        else if(selectedCategory.contains("Wedding Hall")){
            resortTypeID = "1";
            sundayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSundayMenPrice()));
            mondayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getMondayMenPrice()));
            tuesdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getTuesdayMenPrice()));
            wednesdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayMenPrice()));
            thursdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayMenPrice()));
            fridayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getFridayMenPrice()));
            saturdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSaturdayMenPrice()));

            sundayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSundayWomenPrice()));
            mondayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getMondayWomenPrice()));
            tuesdayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getTuesdayWomenPrice()));
            wednesdayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayWomenPrice()));
            thursdayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getThursdayWomenPrice()));
            fridayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getFridayWomenPrice()));
            saturdayWomen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSaturdayWomenPrice()));
            kitchenPrice.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getKitchenPrice()));

            sundayFamily.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSundayFamilyPrice()));
            mondayFamily.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getMondayFamilyPrice()));
            tuesdayFamily.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getTuesdayFamilyPrice()));
            wednesdayFamily.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayFamilyPrice()));
            thursdayFamily.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getThursdayFamilyPrice()));
            fridayFamily.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getFridayFamilyPrice()));
            saturdayFamily.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSaturdayFamilyPrice()));

            kitchenLayout.setVisibility(View.VISIBLE);

            sundayWomen.setVisibility(View.VISIBLE);
            mondayWomen.setVisibility(View.VISIBLE);
            tuesdayWomen.setVisibility(View.VISIBLE);
            wednesdayWomen.setVisibility(View.VISIBLE);
            thursdayWomen.setVisibility(View.VISIBLE);
            fridayWomen.setVisibility(View.VISIBLE);
            saturdayWomen.setVisibility(View.VISIBLE);

            if(language.equalsIgnoreCase("En")) {
                sundayMen.setHint(R.string.str_man_price_hint);
                mondayMen.setHint(R.string.str_man_price_hint);
                tuesdayMen.setHint(R.string.str_man_price_hint);
                wednesdayMen.setHint(R.string.str_man_price_hint);
                thursdayMen.setHint(R.string.str_man_price_hint);
                fridayMen.setHint(R.string.str_man_price_hint);
                saturdayMen.setHint(R.string.str_man_price_hint);

                sundayWomen.setHint(R.string.str_woman_price_hint);
                mondayWomen.setHint(R.string.str_woman_price_hint);
                tuesdayWomen.setHint(R.string.str_woman_price_hint);
                wednesdayWomen.setHint(R.string.str_woman_price_hint);
                thursdayWomen.setHint(R.string.str_woman_price_hint);
                fridayWomen.setHint(R.string.str_woman_price_hint);
                saturdayWomen.setHint(R.string.str_woman_price_hint);

                sundayFamily.setHint(R.string.str_family_price_hint);
                mondayFamily.setHint(R.string.str_family_price_hint);
                tuesdayFamily.setHint(R.string.str_family_price_hint);
                wednesdayFamily.setHint(R.string.str_family_price_hint);
                thursdayFamily.setHint(R.string.str_family_price_hint);
                fridayFamily.setHint(R.string.str_family_price_hint);
                saturdayFamily.setHint(R.string.str_family_price_hint);
                AdvanceAmount.setHint(R.string.str_advance);
            }
            else{
                sundayMen.setHint(R.string.str_man_price_hint_ar);
                mondayMen.setHint(R.string.str_man_price_hint_ar);
                tuesdayMen.setHint(R.string.str_man_price_hint_ar);
                wednesdayMen.setHint(R.string.str_man_price_hint_ar);
                thursdayMen.setHint(R.string.str_man_price_hint_ar);
                fridayMen.setHint(R.string.str_man_price_hint_ar);
                saturdayMen.setHint(R.string.str_man_price_hint_ar);

                sundayWomen.setHint(R.string.str_woman_price_hint_ar);
                mondayWomen.setHint(R.string.str_woman_price_hint_ar);
                tuesdayWomen.setHint(R.string.str_woman_price_hint_ar);
                wednesdayWomen.setHint(R.string.str_woman_price_hint_ar);
                thursdayWomen.setHint(R.string.str_woman_price_hint_ar);
                fridayWomen.setHint(R.string.str_woman_price_hint_ar);
                saturdayWomen.setHint(R.string.str_woman_price_hint_ar);

                sundayFamily.setHint(R.string.str_family_price_hint_ar);
                mondayFamily.setHint(R.string.str_family_price_hint_ar);
                tuesdayFamily.setHint(R.string.str_family_price_hint_ar);
                wednesdayFamily.setHint(R.string.str_family_price_hint_ar);
                thursdayFamily.setHint(R.string.str_family_price_hint_ar);
                fridayFamily.setHint(R.string.str_family_price_hint_ar);
                saturdayFamily.setHint(R.string.str_family_price_hint_ar);
                AdvanceAmount.setHint(R.string.str_advance_ar);
            }
        }
        else if(selectedCategory.contains("Resort")){
            resortTypeID = "2";
            sundayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSundayMenPrice()));
            mondayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getMondayMenPrice()));
            tuesdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getTuesdayMenPrice()));
            wednesdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayMenPrice()));
            thursdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getWednesdayMenPrice()));
            fridayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getFridayMenPrice()));
            saturdayMen.setText(Constants.convertToArabic(resortDetailsArrayList.get(0).getSaturdayMenPrice()));

            kitchenLayout.setVisibility(View.GONE);

            sundayWomen.setVisibility(View.INVISIBLE);
            mondayWomen.setVisibility(View.INVISIBLE);
            tuesdayWomen.setVisibility(View.INVISIBLE);
            wednesdayWomen.setVisibility(View.INVISIBLE);
            thursdayWomen.setVisibility(View.INVISIBLE);
            fridayWomen.setVisibility(View.INVISIBLE);
            saturdayWomen.setVisibility(View.INVISIBLE);

            sundayFamily.setVisibility(View.INVISIBLE);
            mondayFamily.setVisibility(View.INVISIBLE);
            tuesdayFamily.setVisibility(View.INVISIBLE);
            wednesdayFamily.setVisibility(View.INVISIBLE);
            thursdayFamily.setVisibility(View.INVISIBLE);
            fridayFamily.setVisibility(View.INVISIBLE);
            saturdayFamily.setVisibility(View.INVISIBLE);

            if(language.equalsIgnoreCase("En")) {
                sundayMen.setHint(R.string.str_price_hint);
                mondayMen.setHint(R.string.str_price_hint);
                tuesdayMen.setHint(R.string.str_price_hint);
                wednesdayMen.setHint(R.string.str_price_hint);
                thursdayMen.setHint(R.string.str_price_hint);
                fridayMen.setHint(R.string.str_price_hint);
                saturdayMen.setHint(R.string.str_price_hint);
                AdvanceAmount.setHint(R.string.str_advance);
            }
            else{
                sundayMen.setHint(R.string.str_price_hint_ar);
                mondayMen.setHint(R.string.str_price_hint_ar);
                tuesdayMen.setHint(R.string.str_price_hint_ar);
                wednesdayMen.setHint(R.string.str_price_hint_ar);
                thursdayMen.setHint(R.string.str_price_hint_ar);
                fridayMen.setHint(R.string.str_price_hint_ar);
                saturdayMen.setHint(R.string.str_price_hint_ar);
                AdvanceAmount.setHint(R.string.str_advance_ar);
            }
        }
    }

    public void getFacilities(){
        selectedFacilities.clear();
        selectedFacilitiesId.clear();
        for (int i = 0; i< resortDetailsArrayList.get(0).getResortFacilities().size(); i++){
            selectedFacilities.add(resortDetailsArrayList.get(0).getResortFacilities().get(i).getFacilityName());
            selectedFacilitiesId.add(resortDetailsArrayList.get(0).getResortFacilities().get(i).getFacilityId());
        }
//        mAdapter.notifyDataSetChanged();
    }

    public void fieldValidations(){
        final JSONObject parent = new JSONObject();
        minCap = etMinCapacity.getText().toString();
        maxCap = etMaxCapacity.getText().toString();
        int vat = 1;
        String vatNumb = null, bankNam= null, iban= null;
        if(vatApplicable){
            vat = 1;
            vatNumb = vatNumber.getText().toString();
            bankNam = bankName.getText().toString();
            iban = ibanNumber.getText().toString();
        }
        else{
            vat = 0;
            if(vatNumber.getText().toString().length() == 0) {
                vatNumb = "";
            }
            else{
                vatNumb = vatNumber.getText().toString();
            }
            if(bankName.getText().toString().length() == 0) {
                bankNam = "";
            }
            else{
                bankNam = bankName.getText().toString();
            }
            if(ibanNumber.getText().toString().length() == 0) {
                iban = "";
            }
            else{
                iban = ibanNumber.getText().toString();
            }
        }
//        for(int i = 0; i < selectedFacilities.size(); i++){
//            for(int j = 0; j < facilitiesArrayList.size(); j++){
//                if(facilitiesArrayList.get(j).getFacilityName().equals(selectedFacilities.get(i))){
//                    selectedFacilitiesId.add(facilitiesArrayList.get(j).getFacilityId());
//                    return;
//                }
//            }
//        }

        if(selectedCategory.contains("Banquet Hall")) {
            if (strAddress.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select Address";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحديد العنوان";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();

                showDialog("Please select Address", "يرجى تحديد العنوان");
            } else if (minCap.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Minimum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إدخال الحد الأدنى من السعة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Minimum Capacity", "يرجى إدخال الحد الأدنى من السعة");
            } else if (maxCap.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Maximum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "الرجاء إدخال السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Maximum Capacity", "الرجاء إدخال السعة القصوى");
            }
            else if (Integer.parseInt(minCap)>Integer.parseInt(maxCap)) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Minimum capacity should be less than maximum capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يجب أن يكون الحد الأدنى أقل من السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Minimum capacity should be less than maximum capacity", "يجب أن يكون الحد الأدنى أقل من السعة القصوى");
            }
            else if (selectedFacilities.size() == 0) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select facilities";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجي اختيار الخدمات";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select facilities", "يرجي اختيار الخدمات");
            } else if (etResortName.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortName.setError("Please enter property Name");
                }
                else{
                    etResortName.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (etResortNameAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortNameAr.setError("Please enter property Name");
                }
                else{
                    etResortNameAr.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
            else if (AdvanceAmount.getText().toString().length() == 0 || Integer.parseInt(AdvanceAmount.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    AdvanceAmount.setError("Please enter Advance Amount");
                }
                else{
                    AdvanceAmount.setError("الرجاء إدخال السعر");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }else if (etResortDescription.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescription.setError("Please enter property Description");
                }
                else{
                    etResortDescription.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (etResortDescriptionAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescriptionAr.setError("Please enter property Description");
                }
                else{
                    etResortDescriptionAr.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (saturdayMen.getText().toString().length() == 0 || Integer.parseInt(saturdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayMen.setError("Please enter Price");
                }
                else{
                    saturdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayMen.getText().toString().length() == 0 || Integer.parseInt(sundayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayMen.setError("Please enter Price");
                }
                else{
                    sundayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayMen.getBottom());
            } else if (mondayMen.getText().toString().length() == 0 || Integer.parseInt(mondayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayMen.setError("Please enter Price");
                }
                else{
                    mondayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayMen.getBottom());
            } else if (tuesdayMen.getText().toString().length() == 0 || Integer.parseInt(tuesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayMen.setError("Please enter Price");
                }
                else{
                    tuesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayMen.getBottom());
            } else if (wednesdayMen.getText().toString().length() == 0 || Integer.parseInt(wednesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayMen.setError("Please enter Price");
                }
                else{
                    wednesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayMen.getBottom());
            } else if (thursdayMen.getText().toString().length() == 0 || Integer.parseInt(thursdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayMen.setError("Please enter Price");
                }
                else{
                    thursdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayMen.getBottom());
            } else if (fridayMen.getText().toString().length() == 0 || Integer.parseInt(fridayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayMen.setError("Please enter Price");
                }
                else{
                    fridayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayMen.getBottom());
            } else if (saturdayWomen.getText().toString().length() == 0 || Integer.parseInt(saturdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayWomen.setError("Please enter Price");
                }
                else{
                    saturdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayWomen.getText().toString().length() == 0 || Integer.parseInt(sundayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayWomen.setError("Please enter Price");
                }
                else{
                    sundayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayWomen.getBottom());
            } else if (mondayWomen.getText().toString().length() == 0 || Integer.parseInt(mondayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayWomen.setError("Please enter Price");
                }
                else{
                    mondayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayWomen.getBottom());
            } else if (tuesdayWomen.getText().toString().length() == 0 || Integer.parseInt(tuesdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayWomen.setError("Please enter Price");
                }
                else{
                    tuesdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayWomen.getBottom());
            } else if (wednesdayWomen.getText().toString().length() == 0 || Integer.parseInt(wednesdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayWomen.setError("Please enter Price");
                }
                else{
                    wednesdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayWomen.getBottom());
            } else if (thursdayWomen.getText().toString().length() == 0 || Integer.parseInt(thursdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayWomen.setError("Please enter Price");
                }
                else{
                    thursdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayWomen.getBottom());
            } else if (fridayWomen.getText().toString().length() == 0 || Integer.parseInt(fridayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayWomen.setError("Please enter Price");
                }
                else{
                    fridayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayWomen.getBottom());
            } else if(vatApplicable && vatNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    vatNumber.setError("Please enter vat number");
                }
                else{
                    vatNumber.setError("الرجاء إدخال رقم ضريبة القيمة المضافة");
                }
            } else if(vatApplicable && bankName.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    bankName.setError("Please enter bank name");
                }
                else{
                    bankName.setError("الرجاء إدخال إسم البنك");
                }
            } else if(vatApplicable && ibanNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    ibanNumber.setError("Please enter iban number");
                }
                else{
                    ibanNumber.setError("الرجاء إدخال رقم الآيبان");
                }
            }  else if (AdvanceAmount.getText().toString().length() == 0 || Integer.parseInt(AdvanceAmount.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    AdvanceAmount.setError("Please enter Advance Amount");
                }
                else{
                    AdvanceAmount.setError("الرجاء إدخال السعر");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }else {
                try {
                    JSONObject mainObj = new JSONObject();
                    mainObj.put("ResortId", resortId);
                    mainObj.put("ResortTypeId", resortTypeID);
                    mainObj.put("CityId", cityID);
                    mainObj.put("IsManAvailability", 1);
                    mainObj.put("IsWomanAvailability", 1);
                    mainObj.put("IsKitchenavailability", 1);
                    mainObj.put("MinCapacity", Integer.parseInt(minCap));
                    mainObj.put("MaxCapacity", Integer.parseInt(maxCap));
                    mainObj.put("ResortName", etResortName.getText().toString());
                    mainObj.put("ResortName_Ar", etResortNameAr.getText().toString());
                    mainObj.put("Description_Ar", etResortDescriptionAr.getText().toString());
                    mainObj.put("ResortAddress", etResortAddress.getText().toString());
                    mainObj.put("Latitude", resortLatitude);
                    mainObj.put("Longitude", resortLongitude);
                    mainObj.put("Description", etResortDescription.getText().toString());
                    mainObj.put("HostId", hostId);
                    mainObj.put("VatNumber", vatNumb);
                    mainObj.put("BankName", bankNam);
                    mainObj.put("IbanNumber", iban);
                    mainObj.put("vatavailability", vat);
                    mainObj.put("AdvanceAmount", AdvanceAmount.getText().toString());

//                    Prices Array
                    JSONArray pricingArray = new JSONArray();
                    JSONObject saturdayObj = new JSONObject();
                    saturdayObj.put("Did", "1");
                    saturdayObj.put("MenPrice", Constants.convertToArabic(saturdayMen.getText().toString()));
                    saturdayObj.put("WomenPrice", Constants.convertToArabic(saturdayWomen.getText().toString()));
                    saturdayObj.put("FamilyPrice", "0");
                    saturdayObj.put("kitchenprice", "0");
                    pricingArray.put(saturdayObj);

                    JSONObject sundayObj = new JSONObject();
                    sundayObj.put("Did", "2");
                    sundayObj.put("MenPrice", Constants.convertToArabic(sundayMen.getText().toString()));
                    sundayObj.put("WomenPrice", Constants.convertToArabic(sundayWomen.getText().toString()));
                    sundayObj.put("FamilyPrice", "0");
                    sundayObj.put("kitchenprice", "0");
                    pricingArray.put(sundayObj);

                    JSONObject mondayObj = new JSONObject();
                    mondayObj.put("Did", "3");
                    mondayObj.put("MenPrice", Constants.convertToArabic(mondayMen.getText().toString()));
                    mondayObj.put("WomenPrice", Constants.convertToArabic(mondayWomen.getText().toString()));
                    mondayObj.put("FamilyPrice", "0");
                    mondayObj.put("kitchenprice", "0");
                    pricingArray.put(mondayObj);

                    JSONObject tuesdayObj = new JSONObject();
                    tuesdayObj.put("Did", "4");
                    tuesdayObj.put("MenPrice", Constants.convertToArabic(tuesdayMen.getText().toString()));
                    tuesdayObj.put("WomenPrice", Constants.convertToArabic(tuesdayWomen.getText().toString()));
                    tuesdayObj.put("FamilyPrice", "0");
                    tuesdayObj.put("kitchenprice", "0");
                    pricingArray.put(tuesdayObj);

                    JSONObject wednesdayObj = new JSONObject();
                    wednesdayObj.put("Did", "5");
                    wednesdayObj.put("MenPrice", Constants.convertToArabic(wednesdayMen.getText().toString()));
                    wednesdayObj.put("WomenPrice", Constants.convertToArabic(wednesdayWomen.getText().toString()));
                    wednesdayObj.put("FamilyPrice", "0");
                    wednesdayObj.put("kitchenprice", "0");
                    pricingArray.put(wednesdayObj);

                    JSONObject thursdayObj = new JSONObject();
                    thursdayObj.put("Did", "6");
                    thursdayObj.put("MenPrice", Constants.convertToArabic(thursdayMen.getText().toString()));
                    thursdayObj.put("WomenPrice", Constants.convertToArabic(thursdayWomen.getText().toString()));
                    thursdayObj.put("FamilyPrice", "0");
                    thursdayObj.put("kitchenprice", "0");
                    pricingArray.put(thursdayObj);

                    JSONObject fridayObj = new JSONObject();
                    fridayObj.put("Did", "7");
                    fridayObj.put("MenPrice", Constants.convertToArabic(fridayMen.getText().toString()));
                    fridayObj.put("WomenPrice", Constants.convertToArabic(fridayWomen.getText().toString()));
                    fridayObj.put("FamilyPrice", "0");
                    fridayObj.put("kitchenprice", "0");
                    pricingArray.put(fridayObj);

                    JSONArray imagesArray = new JSONArray();
                    if (imageName1 != null) {
                        JSONObject image1 = new JSONObject();
                        image1.put("ImageName", imageName1);
                        imagesArray.put(image1);
                    }
                    if (imageName2!= null) {
                        JSONObject image2 = new JSONObject();
                        image2.put("ImageName", imageName2);
                        imagesArray.put(image2);
                    }
                    if (imageName3!= null) {
                        JSONObject image3 = new JSONObject();
                        image3.put("ImageName", imageName3);
                        imagesArray.put(image3);
                    }
                    if (imageName4!= null) {
                        JSONObject image4 = new JSONObject();
                        image4.put("ImageName", imageName4);
                        imagesArray.put(image4);
                    }
                    if (imageName5!= null) {
                        JSONObject image5 = new JSONObject();
                        image5.put("ImageName", imageName5);
                        imagesArray.put(image5);
                    }
                    if (imageName6!= null) {
                        JSONObject image6 = new JSONObject();
                        image6.put("ImageName", imageName6);
                        imagesArray.put(image6);
                    }
                    if (imageName7!= null) {
                        JSONObject image7 = new JSONObject();
                        image7.put("ImageName", imageName7);
                        imagesArray.put(image7);
                    }
                    if (imageName8!= null) {
                        JSONObject image8 = new JSONObject();
                        image8.put("ImageName", imageName8);
                        imagesArray.put(image8);
                    }

                    JSONArray facilitiesArray = new JSONArray();
                    for (int i = 0; i < selectedFacilitiesId.size(); i++) {
                        JSONObject faciltyObj = new JSONObject();
                        faciltyObj.put("FacilityId", selectedFacilitiesId.get(i));
                        facilitiesArray.put(faciltyObj);
                    }

                    parent.put("UpdateResort", mainObj);
                    parent.put("UpdatePrices", pricingArray);
                    parent.put("UpdateResortImages", imagesArray);
                    parent.put("UpdateResortFacility", facilitiesArray);
                    Log.i("TAG", parent.toString());
                    new UpdateResort().execute(parent.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else if(selectedCategory.contains("Wedding Hall")) {
            if (strAddress.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select Address";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحديد العنوان";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();

                showDialog("Please select Address", "يرجى تحديد العنوان");
            } else if (minCap.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Minimum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إدخال الحد الأدنى من السعة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Minimum Capacity", "يرجى إدخال الحد الأدنى من السعة");
            } else if (maxCap.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Maximum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "الرجاء إدخال السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Maximum Capacity", "الرجاء إدخال السعة القصوى");
            }
            else if (Integer.parseInt(minCap)>Integer.parseInt(maxCap)) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Minimum capacity should be less than maximum capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يجب أن يكون الحد الأدنى أقل من السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Minimum capacity should be less than maximum capacity", "يجب أن يكون الحد الأدنى أقل من السعة القصوى");
            }
            else if (selectedFacilities.size() == 0) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select facilities";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجي اختيار الخدمات";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select facilities", "يرجي اختيار الخدمات");
            } else if (etResortName.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortName.setError("Please enter property Name");
                }
                else{
                    etResortName.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }  else if (etResortNameAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortNameAr.setError("Please enter property Name");
                }
                else{
                    etResortNameAr.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
            else if (AdvanceAmount.getText().toString().length() == 0 || Integer.parseInt(AdvanceAmount.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    AdvanceAmount.setError("Please enter Advance Amount");
                }
                else{
                    AdvanceAmount.setError("الرجاء إدخال السعر");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }else if (etResortDescription.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescription.setError("Please enter property Description");
                }
                else{
                    etResortDescription.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (etResortDescriptionAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescriptionAr.setError("Please enter property Description");
                }
                else{
                    etResortDescriptionAr.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (saturdayMen.getText().toString().length() == 0 || Integer.parseInt(saturdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayMen.setError("Please enter Price");
                }
                else{
                    saturdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayMen.getText().toString().length() == 0 || Integer.parseInt(sundayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayMen.setError("Please enter Price");
                }
                else{
                    sundayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayMen.getBottom());
            } else if (mondayMen.getText().toString().length() == 0 || Integer.parseInt(mondayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayMen.setError("Please enter Price");
                }
                else{
                    mondayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayMen.getBottom());
            } else if (tuesdayMen.getText().toString().length() == 0 || Integer.parseInt(tuesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayMen.setError("Please enter Price");
                }
                else{
                    tuesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayMen.getBottom());
            } else if (wednesdayMen.getText().toString().length() == 0 || Integer.parseInt(wednesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayMen.setError("Please enter Price");
                }
                else{
                    wednesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayMen.getBottom());
            } else if (thursdayMen.getText().toString().length() == 0 || Integer.parseInt(thursdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayMen.setError("Please enter Price");
                }
                else{
                    thursdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayMen.getBottom());
            } else if (fridayMen.getText().toString().length() == 0 || Integer.parseInt(fridayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayMen.setError("Please enter Price");
                }
                else{
                    fridayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayMen.getBottom());
            } else if (kitchenPrice.getText().toString().length() == 0 || Integer.parseInt(kitchenPrice.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    kitchenPrice.setError("Please enter Price");
                }
                else{
                    kitchenPrice.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, kitchenPrice.getBottom());
            } else if (saturdayWomen.getText().toString().length() == 0 || Integer.parseInt(saturdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayWomen.setError("Please enter Price");
                }
                else{
                    saturdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayWomen.getText().toString().length() == 0 || Integer.parseInt(sundayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayWomen.setError("Please enter Price");
                }
                else{
                    sundayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayWomen.getBottom());
            } else if (mondayWomen.getText().toString().length() == 0 || Integer.parseInt(mondayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayWomen.setError("Please enter Price");
                }
                else{
                    mondayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayWomen.getBottom());
            } else if (tuesdayWomen.getText().toString().length() == 0 || Integer.parseInt(tuesdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayWomen.setError("Please enter Price");
                }
                else{
                    tuesdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayWomen.getBottom());
            } else if (wednesdayWomen.getText().toString().length() == 0 || Integer.parseInt(wednesdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayWomen.setError("Please enter Price");
                }
                else{
                    wednesdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayWomen.getBottom());
            } else if (thursdayWomen.getText().toString().length() == 0 || Integer.parseInt(thursdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayWomen.setError("Please enter Price");
                }
                else{
                    thursdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayWomen.getBottom());
            } else if (fridayWomen.getText().toString().length() == 0 || Integer.parseInt(fridayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayWomen.setError("Please enter Price");
                }
                else{
                    fridayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayWomen.getBottom());
            }
            else if (saturdayFamily.getText().toString().length() == 0 || Integer.parseInt(saturdayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayFamily.setError("Please enter Price");
                }
                else{
                    saturdayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayFamily.getText().toString().length() == 0 || Integer.parseInt(sundayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayFamily.setError("Please enter Price");
                }
                else{
                    sundayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayWomen.getBottom());
            } else if (mondayFamily.getText().toString().length() == 0 || Integer.parseInt(mondayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayFamily.setError("Please enter Price");
                }
                else{
                    mondayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayWomen.getBottom());
            } else if (tuesdayFamily.getText().toString().length() == 0 || Integer.parseInt(tuesdayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayFamily.setError("Please enter Price");
                }
                else{
                    tuesdayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayWomen.getBottom());
            } else if (wednesdayFamily.getText().toString().length() == 0 || Integer.parseInt(wednesdayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayFamily.setError("Please enter Price");
                }
                else{
                    wednesdayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayWomen.getBottom());
            } else if (thursdayFamily.getText().toString().length() == 0 || Integer.parseInt(thursdayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayFamily.setError("Please enter Price");
                }
                else{
                    thursdayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayWomen.getBottom());
            } else if (fridayFamily.getText().toString().length() == 0 || Integer.parseInt(fridayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayFamily.setError("Please enter Price");
                }
                else{
                    fridayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayWomen.getBottom());
            } else if(vatApplicable && vatNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    vatNumber.setError("Please enter vat number");
                }
                else{
                    vatNumber.setError("الرجاء إدخال رقم ضريبة القيمة المضافة");
                }
            } else if(vatApplicable && bankName.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    bankName.setError("Please enter bank name");
                }
                else{
                    bankName.setError("الرجاء إدخال إسم البنك");
                }
            } else if(vatApplicable && ibanNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    ibanNumber.setError("Please enter iban number");
                }
                else{
                    ibanNumber.setError("الرجاء إدخال رقم الآيبان");
                }
            }
            else {

                try {
                    JSONObject mainObj = new JSONObject();
                    mainObj.put("ResortId", resortId);
                    mainObj.put("ResortTypeId", resortTypeID);
                    mainObj.put("CityId", cityID);
                    mainObj.put("IsManAvailability", 1);
                    mainObj.put("IsWomanAvailability", 1);
                    mainObj.put("IsKitchenavailability", 1);
                    mainObj.put("MinCapacity", Integer.parseInt(minCap));
                    mainObj.put("MaxCapacity", Integer.parseInt(maxCap));
                    mainObj.put("ResortName", etResortName.getText().toString());
                    mainObj.put("ResortName_Ar", etResortNameAr.getText().toString());
                    mainObj.put("Description_Ar", etResortDescriptionAr.getText().toString());
                    mainObj.put("ResortAddress", etResortAddress.getText().toString());
                    mainObj.put("Latitude", resortLatitude);
                    mainObj.put("Longitude", resortLongitude);
                    mainObj.put("Description", etResortDescription.getText().toString());
                    mainObj.put("HostId", hostId);
                    mainObj.put("VatNumber", vatNumb);
                    mainObj.put("BankName", bankNam);
                    mainObj.put("IbanNumber", iban);
                    mainObj.put("vatavailability", vat);
                    mainObj.put("AdvanceAmount", AdvanceAmount.getText().toString());

//                    Prices Array
                    JSONArray pricingArray = new JSONArray();
                    JSONObject saturdayObj = new JSONObject();
                    saturdayObj.put("Did", "1");
                    saturdayObj.put("MenPrice", Constants.convertToArabic(saturdayMen.getText().toString()));
                    saturdayObj.put("WomenPrice", Constants.convertToArabic(saturdayWomen.getText().toString()));
                    saturdayObj.put("FamilyPrice", Constants.convertToArabic(saturdayFamily.getText().toString()));
                    saturdayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                    pricingArray.put(saturdayObj);

                    JSONObject sundayObj = new JSONObject();
                    sundayObj.put("Did", "2");
                    sundayObj.put("MenPrice", Constants.convertToArabic(sundayMen.getText().toString()));
                    sundayObj.put("WomenPrice", Constants.convertToArabic(sundayWomen.getText().toString()));
                    sundayObj.put("FamilyPrice", Constants.convertToArabic(sundayFamily.getText().toString()));
                    sundayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                    pricingArray.put(sundayObj);

                    JSONObject mondayObj = new JSONObject();
                    mondayObj.put("Did", "3");
                    mondayObj.put("MenPrice", Constants.convertToArabic(mondayMen.getText().toString()));
                    mondayObj.put("WomenPrice", Constants.convertToArabic(mondayWomen.getText().toString()));
                    mondayObj.put("FamilyPrice", Constants.convertToArabic(mondayFamily.getText().toString()));
                    mondayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                    pricingArray.put(mondayObj);

                    JSONObject tuesdayObj = new JSONObject();
                    tuesdayObj.put("Did", "4");
                    tuesdayObj.put("MenPrice", Constants.convertToArabic(tuesdayMen.getText().toString()));
                    tuesdayObj.put("WomenPrice", Constants.convertToArabic(tuesdayWomen.getText().toString()));
                    tuesdayObj.put("FamilyPrice", Constants.convertToArabic(tuesdayFamily.getText().toString()));
                    tuesdayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                    pricingArray.put(tuesdayObj);

                    JSONObject wednesdayObj = new JSONObject();
                    wednesdayObj.put("Did", "5");
                    wednesdayObj.put("MenPrice", Constants.convertToArabic(wednesdayMen.getText().toString()));
                    wednesdayObj.put("WomenPrice", Constants.convertToArabic(wednesdayWomen.getText().toString()));
                    wednesdayObj.put("FamilyPrice", Constants.convertToArabic(wednesdayFamily.getText().toString()));
                    wednesdayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                    pricingArray.put(wednesdayObj);

                    JSONObject thursdayObj = new JSONObject();
                    thursdayObj.put("Did", "6");
                    thursdayObj.put("MenPrice", Constants.convertToArabic(thursdayMen.getText().toString()));
                    thursdayObj.put("WomenPrice", Constants.convertToArabic(thursdayWomen.getText().toString()));
                    thursdayObj.put("FamilyPrice", Constants.convertToArabic(thursdayFamily.getText().toString()));
                    thursdayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                    pricingArray.put(thursdayObj);

                    JSONObject fridayObj = new JSONObject();
                    fridayObj.put("Did", "7");
                    fridayObj.put("MenPrice", Constants.convertToArabic(fridayMen.getText().toString()));
                    fridayObj.put("WomenPrice", Constants.convertToArabic(fridayWomen.getText().toString()));
                    fridayObj.put("FamilyPrice", Constants.convertToArabic(fridayWomen.getText().toString()));
                    fridayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                    pricingArray.put(fridayObj);

                    JSONArray imagesArray = new JSONArray();

                    if (imageName1 != null) {
                        JSONObject image1 = new JSONObject();
                        image1.put("ImageName", imageName1);
                        imagesArray.put(image1);
                    }
                    if (imageName2!= null) {
                        JSONObject image2 = new JSONObject();
                        image2.put("ImageName", imageName2);
                        imagesArray.put(image2);
                    }
                    if (imageName3!= null) {
                        JSONObject image3 = new JSONObject();
                        image3.put("ImageName", imageName3);
                        imagesArray.put(image3);
                    }
                    if (imageName4!= null) {
                        JSONObject image4 = new JSONObject();
                        image4.put("ImageName", imageName4);
                        imagesArray.put(image4);
                    }
                    if (imageName5!= null) {
                        JSONObject image5 = new JSONObject();
                        image5.put("ImageName", imageName5);
                        imagesArray.put(image5);
                    }
                    if (imageName6!= null) {
                        JSONObject image6 = new JSONObject();
                        image6.put("ImageName", imageName6);
                        imagesArray.put(image6);
                    }
                    if (imageName7!= null) {
                        JSONObject image7 = new JSONObject();
                        image7.put("ImageName", imageName7);
                        imagesArray.put(image7);
                    }
                    if (imageName8!= null) {
                        JSONObject image8 = new JSONObject();
                        image8.put("ImageName", imageName8);
                        imagesArray.put(image8);
                    }

                    JSONArray facilitiesArray = new JSONArray();
                    for (int i = 0; i < selectedFacilitiesId.size(); i++) {
                        JSONObject faciltyObj = new JSONObject();
                        faciltyObj.put("FacilityId", selectedFacilitiesId.get(i));
                        facilitiesArray.put(faciltyObj);
                    }

                    parent.put("UpdateResort", mainObj);
                    parent.put("UpdatePrices", pricingArray);
                    parent.put("UpdateResortImages", imagesArray);
                    parent.put("UpdateResortFacility", facilitiesArray);

                    Log.i("TAG", parent.toString());
                    new UpdateResort().execute(parent.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else if(selectedCategory.contains("Resort")) {
            if (strAddress.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select Address";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحديد العنوان";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();

                showDialog("Please select Address", "يرجى تحديد العنوان");
            } else if (minCap.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Minimum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إدخال الحد الأدنى من السعة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Minimum Capacity", "يرجى إدخال الحد الأدنى من السعة");
            } else if (maxCap.equals("")) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Maximum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "الرجاء إدخال السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Maximum Capacity", "الرجاء إدخال السعة القصوى");
            }
            else if (Integer.parseInt(minCap)>Integer.parseInt(maxCap)) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Minimum capacity should be less than maximum capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يجب أن يكون الحد الأدنى أقل من السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Minimum capacity should be less than maximum capacity", "يجب أن يكون الحد الأدنى أقل من السعة القصوى");
            }
            else if (selectedFacilities.size() == 0) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select facilities";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجي اختيار الخدمات";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select facilities", "يرجي اختيار الخدمات");
            } else if (etResortName.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortName.setError("Please enter property Name");
                }
                else{
                    etResortName.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }  else if (etResortNameAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortNameAr.setError("Please enter property Name");
                }
                else{
                    etResortNameAr.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
            else if (AdvanceAmount.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    AdvanceAmount.setError("Please enter Advance Amount");
                }
                else{
                    AdvanceAmount.setError("الرجاء إدخال السعر");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }else if (etResortDescription.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescription.setError("Please enter property Description");
                }
                else{
                    etResortDescription.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (etResortDescriptionAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescriptionAr.setError("Please enter property Description");
                }
                else{
                    etResortDescriptionAr.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (saturdayMen.getText().toString().length() == 0 || Integer.parseInt(saturdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayMen.setError("Please enter Price");
                }
                else{
                    saturdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayMen.getText().toString().length() == 0 || Integer.parseInt(sundayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayMen.setError("Please enter Price");
                }
                else{
                    sundayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayMen.getBottom());
            } else if (mondayMen.getText().toString().length() == 0 || Integer.parseInt(mondayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayMen.setError("Please enter Price");
                }
                else{
                    mondayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayMen.getBottom());
            } else if (tuesdayMen.getText().toString().length() == 0 || Integer.parseInt(tuesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayMen.setError("Please enter Price");
                }
                else{
                    tuesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayMen.getBottom());
            } else if (wednesdayMen.getText().toString().length() == 0 || Integer.parseInt(wednesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayMen.setError("Please enter Price");
                }
                else{
                    wednesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayMen.getBottom());
            } else if (thursdayMen.getText().toString().length() == 0 || Integer.parseInt(thursdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayMen.setError("Please enter Price");
                }
                else{
                    thursdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayMen.getBottom());
            } else if (fridayMen.getText().toString().length() == 0 || Integer.parseInt(fridayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayMen.setError("Please enter Price");
                }
                else{
                    fridayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayMen.getBottom());
            } else if(vatApplicable && vatNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    vatNumber.setError("Please enter vat number");
                }
                else{
                    vatNumber.setError("الرجاء إدخال رقم ضريبة القيمة المضافة");
                }
            } else if(vatApplicable && bankName.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    bankName.setError("Please enter bank name");
                }
                else{
                    bankName.setError("الرجاء إدخال إسم البنك");
                }
            } else if(vatApplicable && ibanNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    ibanNumber.setError("Please enter iban number");
                }
                else{
                    ibanNumber.setError("الرجاء إدخال رقم الآيبان");
                }
            } else {
                try {
                    JSONObject mainObj = new JSONObject();
                    mainObj.put("ResortId", resortId);
                    mainObj.put("ResortTypeId", resortTypeID);
                    mainObj.put("CityId", cityID);
                    mainObj.put("IsManAvailability", 1);
                    mainObj.put("IsWomanAvailability", 1);
                    mainObj.put("IsKitchenavailability", 1);
                    mainObj.put("MinCapacity", Integer.parseInt(minCap));
                    mainObj.put("MaxCapacity", Integer.parseInt(maxCap));
                    mainObj.put("ResortName", etResortName.getText().toString());
                    mainObj.put("ResortName_Ar", etResortNameAr.getText().toString());
                    mainObj.put("Description_Ar", etResortDescriptionAr.getText().toString());
                    mainObj.put("ResortAddress", etResortAddress.getText().toString());
                    mainObj.put("Latitude", resortLatitude);
                    mainObj.put("Longitude", resortLongitude);
                    mainObj.put("Description", etResortDescription.getText().toString());
                    mainObj.put("HostId", hostId);
                    mainObj.put("VatNumber", vatNumb);
                    mainObj.put("BankName", bankNam);
                    mainObj.put("IbanNumber", iban);
                    mainObj.put("vatavailability", vat);
                    mainObj.put("AdvanceAmount", AdvanceAmount.getText().toString());

//                    Prices Array
                    JSONArray pricingArray = new JSONArray();
                    JSONObject saturdayObj = new JSONObject();
                    saturdayObj.put("Did", "1");
                    saturdayObj.put("MenPrice", Constants.convertToArabic(saturdayMen.getText().toString()));
                    saturdayObj.put("WomenPrice", Constants.convertToArabic(saturdayMen.getText().toString()));
                    saturdayObj.put("FamilyPrice", "0");
                    saturdayObj.put("kitchenprice", "0");
                    pricingArray.put(saturdayObj);

                    JSONObject sundayObj = new JSONObject();
                    sundayObj.put("Did", "2");
                    sundayObj.put("MenPrice", Constants.convertToArabic(sundayMen.getText().toString()));
                    sundayObj.put("WomenPrice", Constants.convertToArabic(sundayMen.getText().toString()));
                    sundayObj.put("FamilyPrice", "0");
                    sundayObj.put("kitchenprice", "0");
                    pricingArray.put(sundayObj);

                    JSONObject mondayObj = new JSONObject();
                    mondayObj.put("Did", "3");
                    mondayObj.put("MenPrice", Constants.convertToArabic(mondayMen.getText().toString()));
                    mondayObj.put("WomenPrice", Constants.convertToArabic(mondayMen.getText().toString()));
                    mondayObj.put("FamilyPrice", "0");
                    mondayObj.put("kitchenprice", "0");
                    pricingArray.put(mondayObj);

                    JSONObject tuesdayObj = new JSONObject();
                    tuesdayObj.put("Did", "4");
                    tuesdayObj.put("MenPrice", Constants.convertToArabic(tuesdayMen.getText().toString()));
                    tuesdayObj.put("WomenPrice", Constants.convertToArabic(tuesdayMen.getText().toString()));
                    tuesdayObj.put("FamilyPrice", "0");
                    tuesdayObj.put("kitchenprice", "0");
                    pricingArray.put(tuesdayObj);

                    JSONObject wednesdayObj = new JSONObject();
                    wednesdayObj.put("Did", "5");
                    wednesdayObj.put("MenPrice", Constants.convertToArabic(wednesdayMen.getText().toString()));
                    wednesdayObj.put("WomenPrice", Constants.convertToArabic(wednesdayMen.getText().toString()));
                    wednesdayObj.put("FamilyPrice", "0");
                    wednesdayObj.put("kitchenprice", "0");
                    pricingArray.put(wednesdayObj);

                    JSONObject thursdayObj = new JSONObject();
                    thursdayObj.put("Did", "6");
                    thursdayObj.put("MenPrice", Constants.convertToArabic(thursdayMen.getText().toString()));
                    thursdayObj.put("WomenPrice", Constants.convertToArabic(thursdayMen.getText().toString()));
                    thursdayObj.put("FamilyPrice", "0");
                    thursdayObj.put("kitchenprice", "0");
                    pricingArray.put(thursdayObj);

                    JSONObject fridayObj = new JSONObject();
                    fridayObj.put("Did", "7");
                    fridayObj.put("MenPrice", Constants.convertToArabic(fridayMen.getText().toString()));
                    fridayObj.put("WomenPrice", Constants.convertToArabic(fridayMen.getText().toString()));
                    fridayObj.put("FamilyPrice", "0");
                    fridayObj.put("kitchenprice", "0");
                    pricingArray.put(fridayObj);

                    JSONArray imagesArray = new JSONArray();
                    if (imageName1 != null) {
                        JSONObject image1 = new JSONObject();
                        image1.put("ImageName", imageName1);
                        imagesArray.put(image1);
                    }
                    if (imageName2 != null) {
                        JSONObject image2 = new JSONObject();
                        image2.put("ImageName", imageName2);
                        imagesArray.put(image2);
                    }
                    if (imageName3 != null) {
                        JSONObject image3 = new JSONObject();
                        image3.put("ImageName", imageName3);
                        imagesArray.put(image3);
                    }
                    if (imageName4 != null) {
                        JSONObject image4 = new JSONObject();
                        image4.put("ImageName", imageName4);
                        imagesArray.put(image4);
                    }
                    if (imageName5!= null) {
                        JSONObject image5 = new JSONObject();
                        image5.put("ImageName", imageName5);
                        imagesArray.put(image5);
                    }
                    if (imageName6!= null) {
                        JSONObject image6 = new JSONObject();
                        image6.put("ImageName", imageName6);
                        imagesArray.put(image6);
                    }
                    if (imageName7!= null) {
                        JSONObject image7 = new JSONObject();
                        image7.put("ImageName", imageName7);
                        imagesArray.put(image7);
                    }
                    if (imageName8!= null) {
                        JSONObject image8 = new JSONObject();
                        image8.put("ImageName", imageName8);
                        imagesArray.put(image8);
                    }

                    JSONArray facilitiesArray = new JSONArray();
                    for (int i = 0; i < selectedFacilitiesId.size(); i++) {
                        JSONObject faciltyObj = new JSONObject();
                        faciltyObj.put("FacilityId", selectedFacilitiesId.get(i));
                        facilitiesArray.put(faciltyObj);
                    }

                    parent.put("UpdateResort", mainObj);
                    parent.put("UpdatePrices", pricingArray);
                    parent.put("UpdateResortImages", imagesArray);
                    parent.put("UpdateResortFacility", facilitiesArray);
                    Log.i("TAG", parent.toString());
                    new UpdateResort().execute(parent.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class UpdateResort extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(HostEditResortDetailsActivity.this);
            dialog = new ACProgressFlower.Builder(HostEditResortDetailsActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.RESORT_UPDATE_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HostEditResortDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(HostEditResortDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
                        try {
                            JSONObject jo= new JSONObject(result);
                            try{
                                JSONObject ja = jo.getJSONObject("Success");

//                                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle("Update successful.");
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        Intent intent = new Intent(HostEditResortDetailsActivity.this, MainActivity.class);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        startActivity(intent);
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HostEditResortDetailsActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText("Update successful.");
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText("Update successful.");
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                        Intent intent = new Intent(HostEditResortDetailsActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);

                            }catch (JSONException je){
                                String failure = jo.getString("Failure");
//                                final iOSDialog iOSDialog = new iOSDialog(HostEditResortDetailsActivity.this);
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(failure);
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HostEditResortDetailsActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText(failure);
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText(failure);
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else {
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public void getAddress(){
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            Intent intent = intentBuilder.build(HostEditResortDetailsActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void showDialog(String description, String description_ar){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HostEditResortDetailsActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if(language.equalsIgnoreCase("En")) {
            title.setText(getResources().getString(R.string.app_name));
            yes.setText(getResources().getString(R.string.str_btn_ok));
            desc.setText(description);
        }
        else{
            title.setText(getResources().getString(R.string.app_name_ar));
            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
            desc.setText(description_ar);
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
