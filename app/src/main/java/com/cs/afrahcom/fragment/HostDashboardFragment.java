package com.cs.afrahcom.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.DiscountsActivity;
import com.cs.afrahcom.activity.HostEditResortDetailsActivity;
import com.cs.afrahcom.activity.ResortHistoryActivity;
import com.cs.afrahcom.activity.HostPendingReservationsActivity;
import com.cs.afrahcom.activity.HostResortSelectionActivity;
import com.cs.afrahcom.activity.MainActivity;
import com.cs.afrahcom.activity.RushDaysActivity;
import com.cs.afrahcom.activity.TransactionHistoryActivity;
import com.cs.afrahcom.activity.UpdatePhotosActivity;
import com.cs.afrahcom.adapter.DashboardImagesAdapter;
import com.cs.afrahcom.models.Discounts;
import com.cs.afrahcom.models.Facilities;
import com.cs.afrahcom.models.HostResorts;
import com.cs.afrahcom.models.ImagesModel;
import com.cs.afrahcom.models.ResortDetails;
import com.cs.afrahcom.models.RushDays;
import com.github.demono.AutoScrollViewPager;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import me.relex.circleindicator.CircleIndicator;

import static android.app.Activity.RESULT_OK;

/**
 * Created by CS on 30-10-2017.
 */

public class HostDashboardFragment extends Fragment {

    TextView tvHistoryCount, tvPendingCount, tvTransactionCount, tvResortName, tvResortType, discounts;
    ImageView calenderImage, downArrow;
    RelativeLayout calendarLayout;
    LinearLayout historyLayout, pendingLayout, transactionHistoryLayout;
    CompactCalendarView calendarView;
    Boolean isCalendarVisible = false;
    String resortID = "";
    ArrayList<String> bookingStartDates = new ArrayList<>();
    ArrayList<String> bookingEndDates = new ArrayList<>();
    ArrayList<String> confirmedBookingId = new ArrayList<>();
    ArrayList<String> confirmedPaymentStatus = new ArrayList<>();
    ArrayList<String> confirmedAdvancePrice = new ArrayList<>();
    ArrayList<String> offlineBookingStartDates = new ArrayList<>();
    ArrayList<String> offlineBookingEndDates = new ArrayList<>();
    ArrayList<String> offlineBookingId = new ArrayList<>();
    public static ArrayList<ResortDetails> resortDetailsArrayList = new ArrayList<>();
    public static Date dateSelected = null;
    public static Date bookingStartDate = null;
    public static Date bookingEndDate = null;
    Date FirstDayofMonth = null;
    public static String bookingId = null, payStatus, advance;
    ArrayList<HostResorts> hostResortsArrayList = new ArrayList<>();
    SharedPreferences hostPrefs;
    SharedPreferences.Editor hostPrefsEditor;
    CircleIndicator defaultIndicator;
    AutoScrollViewPager defaultViewpager;
    DashboardImagesAdapter mGalleryAdapter;
    LinearLayout moreLayout;
    ImageView more;
    TextView editResort, changeResort, rushDays, noProperties;
    ScrollView scrollView;
    Boolean isMoreOptionsVisible = false;
    TextView updatePhotos;
    String hostId;
    TextView month;
    AlertDialog alertDialog2;
    ArrayList<Date> allOfflineDates = new ArrayList<>();
    ArrayList<Date> allConfirmedDates = new ArrayList<>();
    ArrayList<Date> pendingConfirmedDates = new ArrayList<>();
    private static int SELECT_RESORT = 1;
    public static final String[] MONTHS = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    View rootView;
    SharedPreferences languagePrefs;
    public static String language;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.activity_host_dashboard, container, false);
        }
        else{
            rootView = inflater.inflate(R.layout.activity_host_dashboard_ar, container, false);
        }

        hostPrefs = getContext().getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostPrefsEditor = hostPrefs.edit();
        hostId = hostPrefs.getString("hostId", "0");
//        hostId = "1";

        tvHistoryCount = (TextView) rootView.findViewById(R.id.historyCount);
        tvTransactionCount = (TextView) rootView.findViewById(R.id.transactionCount);
        tvPendingCount = (TextView) rootView.findViewById(R.id.pendingCount);
        tvResortName = (TextView) rootView.findViewById(R.id.resortName);
        tvResortType = (TextView) rootView.findViewById(R.id.resortType);
        discounts = (TextView) rootView.findViewById(R.id.discounts);
        editResort = (TextView) rootView.findViewById(R.id.editResort);
        rushDays = (TextView) rootView.findViewById(R.id.rushDays);
        changeResort = (TextView) rootView.findViewById(R.id.changeResort);
        noProperties = (TextView) rootView.findViewById(R.id.noProperties);
        updatePhotos = (TextView) rootView.findViewById(R.id.tv_updatePhoto);
        month = (TextView) rootView.findViewById(R.id.month);

        defaultViewpager = (AutoScrollViewPager) rootView.findViewById(R.id.viewPager);
        defaultIndicator = (CircleIndicator) rootView.findViewById(R.id.indicator);

//        resortImage1 = (ImageView) rootView.findViewById(R.id.resortImage1);
        calenderImage = (ImageView) rootView.findViewById(R.id.calenderImage);
        downArrow = (ImageView) rootView.findViewById(R.id.arrow);
        more = (ImageView) rootView.findViewById(R.id.more);

        calendarView = (CompactCalendarView) rootView.findViewById(R.id.calnedarView);

        calendarLayout = (RelativeLayout) rootView.findViewById(R.id.layout1);
        historyLayout = (LinearLayout) rootView.findViewById(R.id.bookingHistoryLayout);
        pendingLayout = (LinearLayout) rootView.findViewById(R.id.pendingReservationLayout);
        transactionHistoryLayout = (LinearLayout) rootView.findViewById(R.id.transactionLayout);
//        notificationLayout = (LinearLayout) rootView.findViewById(R.id.notificationLayout);
        moreLayout = (LinearLayout) rootView.findViewById(R.id.moreLayout);

        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);

        new GetHostResorts().execute(Constants.HOST_RESORTS_URL+hostId+"&UserId="+hostId+"&UserType=2");

        calendarView.setVisibility(View.GONE);

        calendarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isCalendarVisible) {
                    downArrow.setVisibility(View.VISIBLE);
                    calendarView.setVisibility(View.VISIBLE);
                    month.setVisibility(View.VISIBLE);
                    calenderImage.setVisibility(View.INVISIBLE);
                    isCalendarVisible = true;
                }
                else{
                    downArrow.setVisibility(View.GONE);
                    calendarView.setVisibility(View.GONE);
                    month.setVisibility(View.GONE);
                    calenderImage.setVisibility(View.VISIBLE);
                    isCalendarVisible = false;
                }
            }
        });

        historyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getContext(), ResortHistoryActivity.class));

//                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                downArrow.setVisibility(View.GONE);
                calendarView.setVisibility(View.GONE);
                month.setVisibility(View.GONE);
                calenderImage.setVisibility(View.VISIBLE);
                isCalendarVisible = false;
            }
        });

        transactionHistoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getContext(), TransactionHistoryActivity.class));

//                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                downArrow.setVisibility(View.GONE);
                calendarView.setVisibility(View.GONE);
                month.setVisibility(View.GONE);
                calenderImage.setVisibility(View.VISIBLE);
                isCalendarVisible = false;
            }
        });

        pendingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getContext(), HostPendingReservationsActivity.class));

//                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                downArrow.setVisibility(View.GONE);
                calendarView.setVisibility(View.GONE);
                month.setVisibility(View.GONE);
                calenderImage.setVisibility(View.VISIBLE);
                isCalendarVisible = false;
            }
        });

//        notificationLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().startActivity(new Intent(getContext(), HostNotificationsActivity.class));
//
////                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                downArrow.setVisibility(View.GONE);
//                calendarView.setVisibility(View.GONE);
//                month.setVisibility(View.GONE);
//                calenderImage.setVisibility(View.VISIBLE);
//                isCalendarVisible = false;
//            }
//        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isMoreOptionsVisible){
                    isMoreOptionsVisible = true;
                    moreLayout.setVisibility(View.VISIBLE);
                }
                else{
                    isMoreOptionsVisible = false;
                    moreLayout.setVisibility(View.GONE);
                }
            }
        });

        editResort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HostEditResortDetailsActivity.class);
                intent.putExtra("array", resortDetailsArrayList);
                startActivityForResult(intent, SELECT_RESORT);
                moreLayout.setVisibility(View.GONE);
                isMoreOptionsVisible = false;

                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                downArrow.setVisibility(View.GONE);
                calendarView.setVisibility(View.GONE);
                month.setVisibility(View.GONE);
                calenderImage.setVisibility(View.VISIBLE);
                isCalendarVisible = false;
            }
        });

        discounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DiscountsActivity.class);
                intent.putExtra("array", resortDetailsArrayList);
                startActivity(intent);
                moreLayout.setVisibility(View.GONE);

                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                downArrow.setVisibility(View.GONE);
                calendarView.setVisibility(View.GONE);
                month.setVisibility(View.GONE);
                calenderImage.setVisibility(View.VISIBLE);
                isCalendarVisible = false;
            }
        });

        rushDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RushDaysActivity.class);
                intent.putExtra("array", resortDetailsArrayList);
                startActivity(intent);
                moreLayout.setVisibility(View.GONE);

                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                downArrow.setVisibility(View.GONE);
                calendarView.setVisibility(View.GONE);
                month.setVisibility(View.GONE);
                calenderImage.setVisibility(View.VISIBLE);
                isCalendarVisible = false;
            }
        });

        changeResort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HostResortSelectionActivity.class);
                intent.putExtra("array", hostResortsArrayList);
                intent.putExtra("resortid", resortID);
                startActivityForResult(intent, SELECT_RESORT);
                moreLayout.setVisibility(View.GONE);
                isMoreOptionsVisible = false;

                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                downArrow.setVisibility(View.GONE);
                calendarView.setVisibility(View.GONE);
                month.setVisibility(View.GONE);
                calenderImage.setVisibility(View.VISIBLE);
                isCalendarVisible = false;
            }
        });

        updatePhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UpdatePhotosActivity.class);
                intent.putExtra("array", resortDetailsArrayList);
                startActivityForResult(intent, SELECT_RESORT);

                calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                downArrow.setVisibility(View.GONE);
                calendarView.setVisibility(View.GONE);
                month.setVisibility(View.GONE);
                calenderImage.setVisibility(View.VISIBLE);
                isCalendarVisible = false;
            }
        });

        calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                dateSelected = dateClicked;
                if(!allConfirmedDates.contains(dateClicked) && !allOfflineDates.contains(dateClicked)) {
                    calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    final Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, -1);
                    if(c.getTime().before(dateSelected)) {
                        final OfflineBooking newFragment = OfflineBooking.newInstance();
                        newFragment.show(getChildFragmentManager(), "dialog");

                        getChildFragmentManager().executePendingTransactions();
                        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                //do whatever you want when dialog is dismissed
                                resortID = hostPrefs.getString("resortId", "-1");
                                new GetResortDetails().execute(Constants.RESORT_DASHBOARD_URL+resortID+
                                        "&UserId=0&UserType=2");
                                if(newFragment!=null){
                                    newFragment.dismiss();
                                }
                            }
                        });
                    }
                    else{
                        if(language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), "No Bookings Found", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(getContext(), "لا يوجد حجوزات سابقة لديك", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else if(allOfflineDates.contains(dateClicked)){
                    calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#FF0000"));
                    OfflineDatesCheck();

                    final CancelOfflineBooking newFragment = CancelOfflineBooking.newInstance();
                    newFragment.show(getChildFragmentManager(), "dialog");

                    getChildFragmentManager().executePendingTransactions();
                    newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            //do whatever you want when dialog is dismissed
                            resortID = hostPrefs.getString("resortId", "-1");
                            new GetResortDetails().execute(Constants.RESORT_DASHBOARD_URL+resortID+
                                    "&UserId=0&UserType=2");
                            if(newFragment!=null){
                                newFragment.dismiss();
                            }
                        }
                    });
                }
                else if(allConfirmedDates.contains(dateClicked)){
                    if(pendingConfirmedDates.contains(dateClicked)){
                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#ffd100"));
                    }
                    else {
                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#35A357"));
                    }
                    ConfirmedDatesCheck();

                    final BookingDetailsPopup newFragment = BookingDetailsPopup.newInstance();
                    newFragment.show(getChildFragmentManager(), "dialog");

                    getChildFragmentManager().executePendingTransactions();
                    newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            //do whatever you want when dialog is dismissed
                            resortID = hostPrefs.getString("resortId", "-1");
                            new GetResortDetails().execute(Constants.RESORT_DASHBOARD_URL+resortID+
                                    "&UserId=0&UserType=2");
                            if(newFragment!=null){
                                newFragment.dismiss();
                            }
                        }
                    });
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(firstDayOfNewMonth);
                FirstDayofMonth = firstDayOfNewMonth;
                month.setText(MONTHS[calendar.get(Calendar.MONTH)]+" "+calendar.get(Calendar.YEAR));
                if(!allConfirmedDates.contains(firstDayOfNewMonth) && !allOfflineDates.contains(firstDayOfNewMonth)) {
                    calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
                else if(allOfflineDates.contains(firstDayOfNewMonth)){
                    calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#FF0000"));
                }
                else if(allConfirmedDates.contains(firstDayOfNewMonth)){
                    if(pendingConfirmedDates.contains(firstDayOfNewMonth)){
                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#ffd100"));
                    }
                    else {
                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#35A357"));
                    }
                }

//                Calendar todayCalendar = Calendar.getInstance();
//                Date todayDate = todayCalendar.getTime();
//                if(calendar.get(Calendar.MONTH) == todayCalendar.get(Calendar.MONTH)){
//                    Log.i("TAG","month matched");
//                    if(!allConfirmedDates.contains(todayDate) && !allOfflineDates.contains(todayDate)) {
//                        calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                        calendarView.setCurrentDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                    }
//                    else if(allOfflineDates.contains(todayDate)){
//                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#FF0000"));
//                        calendarView.setCurrentDayBackgroundColor(Color.parseColor("#FF0000"));
//                    }
//                    else if(allConfirmedDates.contains(todayDate)){
//                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#35A357"));
//                        calendarView.setCurrentDayBackgroundColor(Color.parseColor("#35A357"));
//                    }
//                }
            }
        });

        return rootView;
    }
//    @Override
//    public void onResume() {
//        super.onResume();
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SELECT_RESORT && resultCode == RESULT_OK){
            resortID = hostPrefs.getString("resortId", "-1");
            new GetResortDetails().execute(Constants.RESORT_DASHBOARD_URL+resortID+
                    "&UserId=0&UserType=2");
        }
    }

    public class GetResortDetails extends AsyncTask<String, Integer, String> {
        String response;
        String networkStatus;
        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            resortDetailsArrayList.clear();
                            JSONObject jo= new JSONObject(result);
                            JSONObject successObj = jo.getJSONObject("Success");

                            int historyCount = Integer.parseInt(successObj.getString("BookingHistory"));
                            int pendingCOunt = Integer.parseInt(successObj.getString("PendingReservation"));
                            int transactionCount = Integer.parseInt(successObj.getString("TrackingHistory"));
                            tvHistoryCount.setText(""+historyCount);
                            tvPendingCount.setText(""+pendingCOunt);
                            tvTransactionCount.setText(""+transactionCount);

                            JSONArray datesArray = successObj.getJSONArray("BookingDates");
                            bookingStartDates.clear();
                            bookingEndDates.clear();
                            confirmedBookingId.clear();
                            confirmedPaymentStatus.clear();
                            confirmedAdvancePrice.clear();
                            offlineBookingId.clear();
                            offlineBookingStartDates.clear();
                            offlineBookingEndDates.clear();
                            calendarView.setCurrentDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            for (int i = 0; i < datesArray.length(); i++) {
                                JSONObject datesObject = datesArray.getJSONObject(i);
                                if(datesObject.getString("orderstatus").equalsIgnoreCase("New") || datesObject.getString("orderstatus").equalsIgnoreCase("Close")) {
                                    bookingStartDates.add(datesObject.getString("StartDate"));
                                    bookingEndDates.add(datesObject.getString("EndDate"));
                                    confirmedBookingId.add(datesObject.getString("BookingId"));
                                    confirmedPaymentStatus.add(datesObject.getString("PaymentStatus"));
                                    confirmedAdvancePrice.add(datesObject.getString("AdvancePrice"));
                                }
                                else if(datesObject.getString("orderstatus").equalsIgnoreCase("offline")){
                                    offlineBookingStartDates.add(datesObject.getString("StartDate"));
                                    offlineBookingEndDates.add(datesObject.getString("EndDate"));
                                    offlineBookingId.add(datesObject.getString("BookingId"));
                                }
                            }
                            final DecimalFormat priceFormat = new DecimalFormat("0");

                            JSONArray resortArray = successObj.getJSONArray("ResortDetails");
                            JSONObject jsonObject = resortArray.getJSONObject(0);
                            ResortDetails resortDetails = new ResortDetails();

                            resortDetails.setResortId(jsonObject.getString("ResortId"));
                            resortDetails.setResortNameAr(jsonObject.getString("ResortName_Ar"));
                            resortDetails.setResortName(jsonObject.getString("ResortName"));
                            resortDetails.setCityId(jsonObject.getString("CityId"));
                            if(language.equalsIgnoreCase("En")) {
                                resortDetails.setCity(jsonObject.getString("City"));
                            }
                            else{
                                resortDetails.setCity(jsonObject.getString("City_Ar"));
                            }
                            resortDetails.setDescriptionAr(jsonObject.getString("Description_Ar"));
                            resortDetails.setDescription(jsonObject.getString("Description"));
                            resortDetails.setResortAddress(jsonObject.getString("ResortAddress"));
                            resortDetails.setResortType(jsonObject.getString("ResortType"));
                            resortDetails.setMinCapacity(jsonObject.getString("MinCapacity"));
                            resortDetails.setMaxCapacity(jsonObject.getString("MaxCapacity"));
                            resortDetails.setLatitude(jsonObject.getString("Latitude"));
                            resortDetails.setLongitude(jsonObject.getString("Longitude"));
                            resortDetails.setRating(jsonObject.getString("Rating"));
                            resortDetails.setApproved(jsonObject.getString("Approved"));
                            try {
                                resortDetails.setAdvanceAmount(priceFormat.format(Float.parseFloat(jsonObject.getString("AdvanceAmount"))));
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                resortDetails.setAdvanceAmount("");
                                e.printStackTrace();
                            }
                            resortDetails.setVatAvailability(jsonObject.getString("VatAvailability"));
                            resortDetails.setBankName(jsonObject.getString("BankName"));
                            resortDetails.setIbanNumber(jsonObject.getString("IbanNumber"));
                            resortDetails.setVatNumber(jsonObject.getString("VatNumber"));

                            JSONArray imagesJSON = jsonObject.getJSONArray("ResortImage");
                            ArrayList<ImagesModel> imagesArrayList = new ArrayList<>();
                            for(int j = 0; j<imagesJSON.length(); j++){
                                JSONObject imagesObject = imagesJSON.getJSONObject(j);
                                ImagesModel imagesModel = new ImagesModel();
                                imagesModel.setImageName(imagesObject.getString("ImageName"));
                                imagesModel.setSource(imagesObject.getString("Source"));
                                imagesArrayList.add(imagesModel);
                            }
                            resortDetails.setResortImagesArray(imagesArrayList);
                            mGalleryAdapter = new DashboardImagesAdapter(getContext(),imagesArrayList);
                            defaultViewpager.setAdapter(mGalleryAdapter);
                            defaultIndicator.setViewPager(defaultViewpager);
                            defaultViewpager.startAutoScroll();
                            defaultViewpager.setCycle(true);


                            JSONArray jsonArray4 = jsonObject.getJSONArray("Facililty");
                            ArrayList<Facilities> resortFacilities = new ArrayList<>();
                            for (int j = 0; j<jsonArray4.length(); j++){
                                Facilities facilities = new Facilities();
                                JSONObject jsonObject1 = jsonArray4.getJSONObject(j);

                                facilities.setFacilityId(jsonObject1.getString("FacilityId"));
                                facilities.setFacilityNameAr(jsonObject1.getString("FacilityName_Ar"));
                                facilities.setFacilityName(jsonObject1.getString("FacilityName"));
                                facilities.setFacilityImage(jsonObject1.getString("FacilityImage"));

                                resortFacilities.add(facilities);
                            }
                            resortDetails.setResortFacilities(resortFacilities);

                            try {
                                JSONArray jsonArray = jsonObject.getJSONArray("Prices");
                                for(int i = 0; i < jsonArray.length(); i++ ){
                                    JSONObject jobj = jsonArray.getJSONObject(i);
                                    if(jobj.has("Saturday")){
                                        JSONObject obj = jobj.getJSONObject("Saturday");
                                        resortDetails.setSaturdayMenPrice(priceFormat.format(Float.parseFloat(obj.getString("MenPrice"))));
                                        resortDetails.setSaturdayWomenPrice(priceFormat.format(Float.parseFloat(obj.getString("WomanPrice"))));
                                        resortDetails.setSaturdayFamilyPrice(priceFormat.format(Float.parseFloat(obj.getString("FamilyPrice"))));
                                        resortDetails.setKitchenPrice(priceFormat.format(Float.parseFloat(obj.getString("KitchenPrice"))));
                                    }
                                    else if(jobj.has("Sunday")){
                                        JSONObject obj = jobj.getJSONObject("Sunday");
                                        resortDetails.setSundayMenPrice(priceFormat.format(Float.parseFloat(obj.getString("MenPrice"))));
                                        resortDetails.setSundayWomenPrice(priceFormat.format(Float.parseFloat(obj.getString("WomanPrice"))));
                                        resortDetails.setSundayFamilyPrice(priceFormat.format(Float.parseFloat(obj.getString("FamilyPrice"))));
                                    }
                                    else if(jobj.has("Monday")){
                                        JSONObject obj = jobj.getJSONObject("Monday");
                                        resortDetails.setMondayMenPrice(priceFormat.format(Float.parseFloat(obj.getString("MenPrice"))));
                                        resortDetails.setMondayWomenPrice(priceFormat.format(Float.parseFloat(obj.getString("WomanPrice"))));
                                        resortDetails.setMondayFamilyPrice(priceFormat.format(Float.parseFloat(obj.getString("FamilyPrice"))));
                                    }
                                    else if(jobj.has("Tuesday")){
                                        JSONObject obj = jobj.getJSONObject("Tuesday");
                                        resortDetails.setTuesdayMenPrice(priceFormat.format(Float.parseFloat(obj.getString("MenPrice"))));
                                        resortDetails.setTuesdayWomenPrice(priceFormat.format(Float.parseFloat(obj.getString("WomanPrice"))));
                                        resortDetails.setTuesdayFamilyPrice(priceFormat.format(Float.parseFloat(obj.getString("FamilyPrice"))));
                                    }
                                    else if(jobj.has("Wednesday")){
                                        JSONObject obj = jobj.getJSONObject("Wednesday");
                                        resortDetails.setWednesdayMenPrice(priceFormat.format(Float.parseFloat(obj.getString("MenPrice"))));
                                        resortDetails.setWednesdayWomenPrice(priceFormat.format(Float.parseFloat(obj.getString("WomanPrice"))));
                                        resortDetails.setWednesdayFamilyPrice(priceFormat.format(Float.parseFloat(obj.getString("FamilyPrice"))));
                                    }
                                    else if(jobj.has("Thursday")){
                                        JSONObject obj = jobj.getJSONObject("Thursday");
                                        resortDetails.setThursadayMenPrice(priceFormat.format(Float.parseFloat(obj.getString("MenPrice"))));
                                        resortDetails.setThursdayWomenPrice(priceFormat.format(Float.parseFloat(obj.getString("WomanPrice"))));
                                        resortDetails.setThursdayFamilyPrice(priceFormat.format(Float.parseFloat(obj.getString("FamilyPrice"))));
                                    }
                                    else if(jobj.has("Friday")){
                                        JSONObject obj = jobj.getJSONObject("Friday");
                                        resortDetails.setFridayMenPrice(priceFormat.format(Float.parseFloat(obj.getString("MenPrice"))));
                                        resortDetails.setFridayWomenPrice(priceFormat.format(Float.parseFloat(obj.getString("WomanPrice"))));
                                        resortDetails.setFridayFamilyPrice(priceFormat.format(Float.parseFloat(obj.getString("FamilyPrice"))));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            ArrayList<RushDays> rushDaysArrayList = new ArrayList<>();
                            JSONArray rushArray = jsonObject.getJSONArray("RushDays");
                            for (int i = 0; i < rushArray.length(); i++){
                                JSONObject obj = rushArray.getJSONObject(i);
                                RushDays rushDays = new RushDays();
                                rushDays.setRushId(obj.getString("RushId"));
                                rushDays.setStartDate(obj.getString("StartDate"));
                                rushDays.setEndDate(obj.getString("EndDate"));
                                rushDays.setRate(obj.getString("Rate"));
                                rushDaysArrayList.add(rushDays);
                            }
                            resortDetails.setRushDays(rushDaysArrayList);

                            ArrayList<Discounts> discountsArrayList = new ArrayList<>();
                            JSONArray discountArray = jsonObject.getJSONArray("Discounts");
                            for (int i = 0; i < discountArray.length(); i++){
                                JSONObject obj = discountArray.getJSONObject(i);
                                Discounts rushDays = new Discounts();
                                rushDays.setDiscountId(obj.getString("DiscountId"));
                                rushDays.setStartDate(obj.getString("StartDate"));
                                rushDays.setEndDate(obj.getString("EndDate"));
                                rushDays.setDiscount(obj.getString("Percentage"));
                                discountsArrayList.add(rushDays);
                            }
                            resortDetails.setDiscounts(discountsArrayList);

                            resortDetailsArrayList.add(resortDetails);

                            resortID = jsonObject.getString("ResortId");
                            if(language.equalsIgnoreCase("En")) {
                                tvResortName.setText(jsonObject.getString("ResortName"));
                                tvResortType.setText(jsonObject.getString("ResortType"));
                            }
                            else{
                                tvResortName.setText(jsonObject.getString("ResortName_Ar"));
                                if(jsonObject.getString("ResortType").equalsIgnoreCase("Wedding Hall")) {
                                    tvResortType.setText("قصور الافراح");
                                }
                                else if(jsonObject.getString("ResortType").equalsIgnoreCase("Banquet Hall")) {
                                    tvResortType.setText("الفنادق");
                                }
                                else if(jsonObject.getString("ResortType").equalsIgnoreCase("Resort")) {
                                    tvResortType.setText("الشاليهات");
                                }
                            }
                            calendarView.removeAllEvents();
                            setEventsOnCalnedar();
                            setOfflineEventsOnCalnedar();

                        } catch (Exception e) {
                            updatePhotos.setFocusable(false);
                            updatePhotos.setFocusableInTouchMode(false);
//                            noProperties.setVisibility(View.VISIBLE);
//                            scrollView.setVisibility(View.GONE);
                            more.setFocusable(false);
                            more.setFocusableInTouchMode(false);
                            Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public class GetHostResorts extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
                    updatePhotos.setFocusable(false);
                    updatePhotos.setFocusableInTouchMode(false);
                    noProperties.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                    more.setFocusable(false);
                    more.setFocusableInTouchMode(false);
                } else {
                    if (result.equals("")) {
                        Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        updatePhotos.setFocusable(false);
                        updatePhotos.setFocusableInTouchMode(false);
                        noProperties.setVisibility(View.VISIBLE);
                        scrollView.setVisibility(View.GONE);
                        more.setFocusable(false);
                        more.setFocusableInTouchMode(false);
                    } else {
                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONObject successObj = jo.getJSONObject("Success");
                            MainActivity.hostNotificationCount = successObj.getInt("UnReadPushNotificationCount");
//                            MainActivity.adapter.notifyDataSetChanged();
                            Intent in = new Intent("data_action");
                            getContext().sendBroadcast(in);
                            JSONArray successArray = successObj.getJSONArray("PropertyDetais");
                            for(int i = 0; i < successArray.length(); i++){
                                JSONObject obj = successArray.getJSONObject(i);
                                HostResorts hostResorts = new HostResorts();
                                hostResorts.setResortId(obj.getString("ResortId"));
                                hostResorts.setResortnameAr(obj.getString("ResortName_Ar"));
                                hostResorts.setResortname(obj.getString("ResortName"));
                                hostResorts.setResorttype(obj.getString("ResortType"));
                                hostResortsArrayList.add(hostResorts);
                            }

                            resortID = hostPrefs.getString("resortId", "-1");
                            if(resortID.equals("-1")) {
                                resortID = hostResortsArrayList.get(0).getResortId();
                                hostPrefsEditor.putString("resortId",resortID);
                                hostPrefsEditor.commit();
                            }
                            new GetResortDetails().execute(Constants.RESORT_DASHBOARD_URL+resortID+
                                    "&UserId=0&UserType=2");

                        } catch (Exception e) {
                            try {
                                JSONObject jo= new JSONObject(result);
                                String failure = jo.getString("Failure");
                                if(failure.equalsIgnoreCase("No Record Found")) {
//                                    Toast.makeText(getContext(), "No Resorts Found.", Toast.LENGTH_SHORT).show();
                                    noProperties.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.GONE);
                                    more.setFocusable(false);
                                    more.setFocusableInTouchMode(false);
                                    updatePhotos.setFocusable(false);
                                    updatePhotos.setFocusableInTouchMode(false);
                                    Intent intent = new Intent(getContext(), MainActivity.class);
                                    intent.putExtra("startwith",2);
                                    getActivity().startActivity(intent);
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                                noProperties.setVisibility(View.VISIBLE);
                                scrollView.setVisibility(View.GONE);
                                more.setFocusable(false);
                                more.setFocusableInTouchMode(false);
                                updatePhotos.setFocusable(false);
                                updatePhotos.setFocusableInTouchMode(false);
                                Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            }
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                updatePhotos.setFocusable(false);
                updatePhotos.setFocusableInTouchMode(false);
                noProperties.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.GONE);
                more.setFocusable(false);
                more.setFocusableInTouchMode(false);
                Toast.makeText(getContext(), "cannot reach server1", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public void setEventsOnCalnedar(){
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Calendar calendar = Calendar.getInstance();
        if(FirstDayofMonth != null) {
            calendar.setTime(FirstDayofMonth);
        }
        calendarView.setCurrentDate(calendar.getTime());
        month.setText(MONTHS[calendar.get(Calendar.MONTH)]+" "+calendar.get(Calendar.YEAR));

        ArrayList<Date> allDates = new ArrayList<>();
        ArrayList<String> payStatus = new ArrayList<>();

        for(int i = 0; i < bookingStartDates.size(); i++){
            Date mCheckInDate = null, mCheckOutDate = null;
            String dateStr = bookingStartDates.get(i);
            String dateStr1 = bookingEndDates.get(i);

            String[] reservDate = dateStr.split("T");
            String checkInDate = reservDate[0]+" "+reservDate[1];
            String[] reservDate1 = dateStr1.split("T");
            String checkOutDate = reservDate1[0]+" "+reservDate1[1];

            try {
                mCheckInDate = DateFormat.parse(checkInDate);
                mCheckOutDate = DateFormat.parse(checkOutDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar c1 = Calendar.getInstance();
            c1.setTime(mCheckInDate);

            Calendar c2 = Calendar.getInstance();
            c2.setTime(mCheckOutDate);

            if(c1.equals(c2)){
                allDates.add(c1.getTime());
                payStatus.add(confirmedPaymentStatus.get(i));
            }
            else {
                while (!c1.after(c2)) {
                    if (!allDates.contains(c1.getTime())) {
                        allDates.add(c1.getTime());
                        payStatus.add(confirmedPaymentStatus.get(i));
                    }
                    c1.add(Calendar.DATE, 1);
                }
            }
        }
        allConfirmedDates = allDates;

            for (int i = 0; i < allDates.size(); i++) {
                Calendar todaycalendar = Calendar.getInstance();
                SimpleDateFormat DateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                Date todayDate = todaycalendar.getTime();
                Date otherDate = null;
                String todaydateParsed = DateFormat1.format(todayDate);
                String dateParsed = DateFormat1.format(allDates.get(i).getTime());
                try {
                    todayDate = DateFormat1.parse(todaydateParsed);
                    otherDate = DateFormat1.parse(dateParsed);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (todayDate.equals(otherDate)) {
                    if(payStatus.get(i).equalsIgnoreCase("pending")) {
                        calendarView.setCurrentDayBackgroundColor(Color.parseColor("#ffd100"));
                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#ffd100"));
                    }
                    else{
                        calendarView.setCurrentDayBackgroundColor(Color.parseColor("#35A357"));
                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#35A357"));
                    }
                }

                long startMillis = allDates.get(i).getTime();
                if(payStatus.get(i).equalsIgnoreCase("pending")) {
                    pendingConfirmedDates.add(allDates.get(i));
                    Event ev1 = new Event(Color.parseColor("#ffd100"), startMillis);
                    calendarView.addEvent(ev1);
                }
                else {
                    Event ev1 = new Event(Color.parseColor("#35A357"), startMillis);
                    calendarView.addEvent(ev1);
                }
            }
    }

    public void setOfflineEventsOnCalnedar(){
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        ArrayList<Date> allDates = new ArrayList<>();

        for(int i = 0; i < offlineBookingStartDates.size(); i++){
            Date mCheckInDate = null, mCheckOutDate = null;
            String dateStr = offlineBookingStartDates.get(i);
            String dateStr1 = offlineBookingEndDates.get(i);

            String[] reservDate = dateStr.split("T");
            String checkInDate = reservDate[0]+" "+reservDate[1];
            String[] reservDate1 = dateStr1.split("T");
            String checkOutDate = reservDate1[0]+" "+reservDate1[1];

            try {
                mCheckInDate = DateFormat.parse(checkInDate);
                mCheckOutDate = DateFormat.parse(checkOutDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar c1 = Calendar.getInstance();
            c1.setTime(mCheckInDate);

            Calendar c2 = Calendar.getInstance();
            c2.setTime(mCheckOutDate);

            if(c1.equals(c2)){
                allDates.add(c1.getTime());
            }
            else {
                while (!c1.after(c2)) {
                    if (!allDates.contains(c1.getTime())) {
                        allDates.add(c1.getTime());
                    }
                    c1.add(Calendar.DATE, 1);
                }
            }
        }

        allOfflineDates = allDates;
        for(int i = 0; i < allDates.size(); i++){
            SimpleDateFormat DateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Calendar calendar = Calendar.getInstance();
            Date todayDate = calendar.getTime();
            Date otherDate = null;
            String todaydateParsed = DateFormat1.format(todayDate);
            String dateParsed = DateFormat1.format(allDates.get(i).getTime());
            try {
                todayDate = DateFormat1.parse(todaydateParsed);
                otherDate = DateFormat1.parse(dateParsed);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(todayDate.equals(otherDate)){
                calendarView.setCurrentDayBackgroundColor(Color.parseColor("#ff0000"));
                calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#ff0000"));
            }

            long startMillis = allDates.get(i).getTime();
            Event ev1 = new Event(Color.parseColor("#ff0000"), startMillis);
            calendarView.addEvent(ev1);

            try {
                if(FirstDayofMonth!=null) {
                    if (!allConfirmedDates.contains(FirstDayofMonth) && !allOfflineDates.contains(FirstDayofMonth)) {
                        calendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    } else if (allOfflineDates.contains(FirstDayofMonth)) {
                        calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#FF0000"));
                    } else if (allConfirmedDates.contains(FirstDayofMonth)) {
                        if(pendingConfirmedDates.contains(FirstDayofMonth)){
                            calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#ffd100"));
                        }
                        else {
                            calendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#35A357"));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void ConfirmedDatesCheck(){
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        for(int i = 0; i < bookingStartDates.size(); i++){
            Date mCheckInDate = null, mCheckOutDate = null;
            String dateStr = bookingStartDates.get(i);
            String dateStr1 = bookingEndDates.get(i);

            String[] reservDate = dateStr.split("T");
            String checkInDate = reservDate[0]+" "+reservDate[1];
            String[] reservDate1 = dateStr1.split("T");
            String checkOutDate = reservDate1[0]+" "+reservDate1[1];

            try {
                mCheckInDate = DateFormat.parse(checkInDate);
                mCheckOutDate = DateFormat.parse(checkOutDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar c = Calendar.getInstance();
            c.setTime(dateSelected);

            Calendar c1 = Calendar.getInstance();
            c1.setTime(mCheckInDate);

            Calendar c2 = Calendar.getInstance();
            c2.setTime(mCheckOutDate);

            if(c1.equals(c2)){
                if(c.equals(c1)){
                    bookingStartDate = mCheckInDate;
                    bookingEndDate = mCheckOutDate;
                    bookingId = confirmedBookingId.get(i);
                    payStatus = confirmedPaymentStatus.get(i);
                    advance = confirmedAdvancePrice.get(i);
                }
            }
            else {
                while (!c1.after(c2)) {
                    if(c.equals(c1)){
                        bookingStartDate = mCheckInDate;
                        bookingEndDate = mCheckOutDate;
                        bookingId = confirmedBookingId.get(i);
                        payStatus = confirmedPaymentStatus.get(i);
                        advance = confirmedAdvancePrice.get(i);
                        break;
                    }
                    c1.add(Calendar.DATE, 1);
                }
            }
        }
    }

    public void OfflineDatesCheck(){
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        for(int i = 0; i < offlineBookingStartDates.size(); i++){
            Date mCheckInDate = null, mCheckOutDate = null;
            String dateStr = offlineBookingStartDates.get(i);
            String dateStr1 = offlineBookingEndDates.get(i);

            String[] reservDate = dateStr.split("T");
            String checkInDate = reservDate[0]+" "+reservDate[1];
            String[] reservDate1 = dateStr1.split("T");
            String checkOutDate = reservDate1[0]+" "+reservDate1[1];

            try {
                mCheckInDate = DateFormat.parse(checkInDate);
                mCheckOutDate = DateFormat.parse(checkOutDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar c = Calendar.getInstance();
            c.setTime(dateSelected);

            Calendar c1 = Calendar.getInstance();
            c1.setTime(mCheckInDate);

            Calendar c2 = Calendar.getInstance();
            c2.setTime(mCheckOutDate);

            if(c1.equals(c2)){
                if(c.equals(c1)){
                    bookingStartDate = mCheckInDate;
                    bookingEndDate = mCheckOutDate;
                    bookingId = offlineBookingId.get(i);
                }
            }
            else {
                while (!c1.after(c2)) {
                    if(c.equals(c1)){
                        bookingStartDate = mCheckInDate;
                        bookingEndDate = mCheckOutDate;
                        bookingId = offlineBookingId.get(i);
                        break;
                    }
                    c1.add(Calendar.DATE, 1);
                }
            }
        }

    }

    public static class OfflineBooking extends DialogFragment {

        View dialogView;
        private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
        TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek, resortNameTV, submit;
        TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
        ImageView closeDialog;
        RelativeLayout layoutCheckin, layoutCheckout;
        Date mCheckInDate = null, mCheckOutDate = null, mTodayDate = null;
        String mCheckInDateStr = null, mCheckOutDateStr = null;
        public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
        SharedPreferences languagePrefs;
        String language;

        static OfflineBooking newInstance() {
            return new OfflineBooking();
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
                dialogView = inflater.inflate(R.layout.activity_offline_booking, container, false);
            }
            else{
                dialogView = inflater.inflate(R.layout.activity_offline_booking_ar, container, false);
            }

            tvCheckInDate = (TextView) dialogView.findViewById(R.id.tv_checkin_date);
            tvCheckInMonth = (TextView) dialogView.findViewById(R.id.tv_checkin_month);
            tvCheckInWeek = (TextView) dialogView.findViewById(R.id.tv_checkin_week);

            tvCheckOutDate = (TextView) dialogView.findViewById(R.id.tv_checkout_date);
            tvCheckOutMonth = (TextView) dialogView.findViewById(R.id.tv_checkout_month);
            tvCheckOutWeek = (TextView) dialogView.findViewById(R.id.tv_checkout_week);

            layoutCheckin = (RelativeLayout) dialogView.findViewById(R.id.layout_checkin);
            layoutCheckout = (RelativeLayout) dialogView.findViewById(R.id.layout_checkout);

            closeDialog = (ImageView) dialogView.findViewById(R.id.close_dialog);
            submit = (TextView) dialogView.findViewById(R.id.tv_submit);
            resortNameTV = (TextView) dialogView.findViewById(R.id.resort_type_name);

            if(language.equalsIgnoreCase("En")) {
                resortNameTV.setText(resortDetailsArrayList.get(0).getResortName());
            }
            else{
                resortNameTV.setText(resortDetailsArrayList.get(0).getResortNameAr());
            }

            final Calendar c = Calendar.getInstance();
            c.setTime(dateSelected);
            mCheckInYear = c.get(Calendar.YEAR);
            mCheckInMonth = c.get(Calendar.MONTH);
            mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckInDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckInDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

//            c.add(Calendar.DATE, 1);
            mCheckOutYear = c.get(Calendar.YEAR);
            mCheckOutMonth = c.get(Calendar.MONTH);
            mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckOutDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

            String checkInDateString = mCheckInYear+"-"+(mCheckInMonth+1)+"-"+mCheckInDay;
            String checkOutDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                mCheckInDate = sdf.parse(checkInDateString);
                mCheckOutDate = sdf.parse(checkOutDateString);
                mTodayDate = sdf.parse(checkInDateString);

                mCheckInDateStr = sdf.format(mCheckInDate);
                mCheckOutDateStr = sdf.format(mCheckOutDate);
            } catch (Exception e) {
                e.printStackTrace();
            }

            closeDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });

            layoutCheckin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkInDatePicker();
                }
            });

            layoutCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                }
            });

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    JSONObject parent = new JSONObject();
                    try {
//                        Setting Reservation Date
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                        String currentTime = df3.format(c.getTime());

//                        Insert order JSON preparation
                        JSONObject mainObj = new JSONObject();
                        mainObj.put("StartDate", mCheckInDateStr);
                        mainObj.put("EndDate", mCheckOutDateStr);
                        mainObj.put("UserId", "");
                        mainObj.put("ResortId", resortDetailsArrayList.get(0).getResortId());
                        mainObj.put("BookingType", "3");
                        mainObj.put("Comments", "Offline booking from Android");
                        mainObj.put("PaymentMode", "");
                        mainObj.put("TotalPrice", "");
                        mainObj.put("AdvancePrice", "");
                        mainObj.put("OrderStatus", "Offline");
                        mainObj.put("DeviceToken", "");
                        mainObj.put("ReservationDate", currentTime);
                        mainObj.put("NoOfMen", "");
                        mainObj.put("NoOfWomen", "");
                        mainObj.put("UserName", "");
                        mainObj.put("MobileNo", "");
                        mainObj.put("IsCateringsSelected", "");
                        mainObj.put("ResortTypeId", "");
                        mainObj.put("PaymentStatus", "");
                        parent.put("insertdetails", mainObj);

//                        Empty JSON object for Inserting in Tracking table
                        JSONObject emptyObj = new JSONObject();
                        parent.put("inserttracking", emptyObj);

                        JSONObject cateringObj = new JSONObject();
                        parent.put("InsertCatering", cateringObj);
                        Log.i("TAG", parent.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    new InsertOrder().execute(parent.toString());
                }
            });
            return dialogView;
        }

        public void checkInDatePicker(){
            final Calendar c = Calendar.getInstance();

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            mCheckInYear = year;
                            mCheckInDay = dayOfMonth;
                            mCheckInMonth = monthOfYear;

                            if(mCheckInDay<10) {
                                tvCheckInDate.setText("0" + mCheckInDay);
                            }
                            else{
                                tvCheckInDate.setText("" + mCheckInDay);
                            }
                            tvCheckInMonth.setText(MONTHS[mCheckInMonth]);

                            checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                        }
                    }, mCheckInYear, mCheckInMonth, mCheckInDay);

            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//            long max = TimeUnit.DAYS.toMillis(90);
//            datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
            datePickerDialog.setCancelable(false);
            String msg;
            if(language.equalsIgnoreCase("En")){
                msg = "Select Start Date";
            }
            else{
                msg = "إختر تاريخ البداية";
            }
            datePickerDialog.setMessage(msg);
            datePickerDialog.show();
        }

        public void checkOutDatePicker(int year, int date, int month){
            final Calendar c = Calendar.getInstance();
            long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
            String givenDateString = year+"-"+(month)+"-"+date;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                mCheckInDate = sdf.parse(givenDateString);
                mCheckInDateStr = sdf.format(mCheckInDate);
                selectedMillis = mCheckInDate.getTime();
                c.setTime(mCheckInDate); // yourdate is an object of type Date
                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                tvCheckInWeek.setText(WEEKS[(dayOfWeek-1)]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mCheckOutYear = mCheckInYear;
            mCheckOutMonth = mCheckInMonth;
            mCheckOutDay = mCheckInDay;

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            mCheckOutYear = year;
                            mCheckOutDay = dayOfMonth;
                            mCheckOutMonth = monthOfYear;

                            if(mCheckOutDay<10) {
                                tvCheckOutDate.setText("0" + mCheckOutDay);
                            }
                            else{
                                tvCheckOutDate.setText("" + mCheckOutDay);
                            }
                            tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);

                            String givenDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                mCheckOutDate = sdf.parse(givenDateString);
                                mCheckOutDateStr = sdf.format(mCheckOutDate);
                                c.setTime(mCheckOutDate); // yourdate is an object of type Date
                                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                tvCheckOutWeek.setText(WEEKS[(dayOfWeek-1)]);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
            datePickerDialog.getDatePicker().setMinDate(selectedMillis);
//            long max = TimeUnit.DAYS.toMillis(90);
//            datePickerDialog.getDatePicker().setMaxDate(selectedMillis+max);
            datePickerDialog.setInverseBackgroundForced(true);
            String msg;
            if(language.equalsIgnoreCase("En")){
                msg = "Select End Date";
            }
            else{
                msg = "إختر تاريخ النهاية";
            }
            datePickerDialog.setMessage(msg);
            datePickerDialog.show();
        }

        public class InsertOrder extends AsyncTask<String, Integer, String> {
            String networkStatus;
            ACProgressFlower dialog;
            String response;
            InputStream inputStream = null;

            @Override
            protected void onPreExecute() {
                networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                dialog = new ACProgressFlower.Builder(getContext())
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY).build();
                dialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    Log.i("TAG", "fav response:" + response);
                    return response;
                } else {
                    return "no internet";
                }

            }

            @Override
            protected void onPostExecute(String result) {

                if (result != null) {
                    if (result.equalsIgnoreCase("no internet")) {
                        Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                    } else {
                        if (result.equals("")) {
                            Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {

                            try {
                                JSONObject jo = new JSONObject(result);
                                JSONArray successObj = jo.getJSONArray("success");
                                JSONObject bookingObj = successObj.getJSONObject(0);
                                String bookingID = bookingObj.getString("BookingId");
                                if(language.equalsIgnoreCase("En")) {
                                    Toast.makeText(getContext(), "Booking Successful", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(getContext(), "تم الحجز بنجاح", Toast.LENGTH_SHORT).show();
                                }
                                getDialog().dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } else {
                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

                super.onPostExecute(result);

            }
        }
        private static String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
    }

    public static class CancelOfflineBooking extends DialogFragment {

        View dialogView;
        TextView tvCheckInDate, resortNameTV, submit;
        TextView tvCheckOutDate;
        ImageView closeDialog;
        private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
        TextView tvCheckInMonth, tvCheckInWeek;
        TextView tvCheckOutMonth, tvCheckOutWeek;
        public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};

        static CancelOfflineBooking newInstance() {
            return new CancelOfflineBooking();
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            if(language.equalsIgnoreCase("En")) {
                dialogView = inflater.inflate(R.layout.activity_cancel_offline_booking, container, false);
            }
            else {
                dialogView = inflater.inflate(R.layout.activity_cancel_offline_booking_ar, container, false);
            }

            tvCheckInDate = (TextView) dialogView.findViewById(R.id.tv_checkin_date);
            tvCheckInMonth = (TextView) dialogView.findViewById(R.id.tv_checkin_month);
            tvCheckInWeek = (TextView) dialogView.findViewById(R.id.tv_checkin_week);

            tvCheckOutDate = (TextView) dialogView.findViewById(R.id.tv_checkout_date);
            tvCheckOutMonth = (TextView) dialogView.findViewById(R.id.tv_checkout_month);
            tvCheckOutWeek = (TextView) dialogView.findViewById(R.id.tv_checkout_week);

            closeDialog = (ImageView) dialogView.findViewById(R.id.close_dialog);
            submit = (TextView) dialogView.findViewById(R.id.tv_submit);
            resortNameTV = (TextView) dialogView.findViewById(R.id.resort_type_name);

            if(language.equalsIgnoreCase("En")) {
                resortNameTV.setText(resortDetailsArrayList.get(0).getResortName());
            }
            else{
                resortNameTV.setText(resortDetailsArrayList.get(0).getResortNameAr());
            }

            Calendar c = Calendar.getInstance();
            c.setTime(bookingStartDate);
            mCheckInYear = c.get(Calendar.YEAR);
            mCheckInMonth = c.get(Calendar.MONTH);
            mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckInDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckInDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

            c.setTime(bookingEndDate);
            mCheckInYear = c.get(Calendar.YEAR);
            mCheckInMonth = c.get(Calendar.MONTH);
            mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckOutDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

//            SimpleDateFormat DateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
//            String startDate = DateFormat.format(bookingStartDate);
//            String endDate = DateFormat.format(bookingEndDate);
//
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            Date todayDate = calendar.getTime();
//
//            tvCheckInDate.setText(startDate);
//            tvCheckOutDate.setText(endDate);

            if(todayDate.after(bookingEndDate)){
                submit.setVisibility(View.GONE);
            }

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new OfflineBookingCancel().execute(Constants.CANCEL_OFFLINE_BOOKING_URL+bookingId);
                }
            });

            closeDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });

            return dialogView;
        }

        public class OfflineBookingCancel extends AsyncTask<String, Integer, String> {

            String response;
            String networkStatus;
            ACProgressFlower dialog;

            @Override
            protected void onPreExecute() {
                response = null;
                networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                dialog = new ACProgressFlower.Builder(getContext())
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY).build();
                dialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.i("TAG", "items response:" + response);
                    return response;
                } else {
                    return "no internet";
                }
            }

            @Override
            protected void onPostExecute(String result) {
                if (result != null) {
                    if (result.equalsIgnoreCase("no internet")) {
                        Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        if (result.equals("")) {
                            Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                JSONObject jo= new JSONObject(result);
                                getDialog().dismiss();
                                if(language.equalsIgnoreCase("En")) {
                                    Toast.makeText(getContext(), "Booking Cancelled", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(getContext(), "تم إلغاء الحجز", Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception e) {
                                try {
                                    JSONObject jo= new JSONObject(result);
                                    String failure = jo.getString("Failure");
                                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                                }
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "cannot reach server1", Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        }
    }

    public static class BookingDetailsPopup extends DialogFragment {

        View dialogView;
        TextView tvCheckInDate,  resortNameTV, submit, advanceAmount, paymentStatus;
        TextView tvCheckOutDate;
        ImageView closeDialog;
        private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
        TextView tvCheckInMonth, tvCheckInWeek;
        TextView tvCheckOutMonth, tvCheckOutWeek;
        public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        public static final String[] WEEKS =  {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
        AlertDialog customDialog;
        static BookingDetailsPopup newInstance() {
            return new BookingDetailsPopup();
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            if(language.equalsIgnoreCase("En")) {
                dialogView = inflater.inflate(R.layout.activity_booking_details, container, false);
            }
            else{
                dialogView = inflater.inflate(R.layout.activity_booking_details_ar, container, false);
            }

            tvCheckInDate = (TextView) dialogView.findViewById(R.id.tv_checkin_date);
            tvCheckInMonth = (TextView) dialogView.findViewById(R.id.tv_checkin_month);
            tvCheckInWeek = (TextView) dialogView.findViewById(R.id.tv_checkin_week);

            tvCheckOutDate = (TextView) dialogView.findViewById(R.id.tv_checkout_date);
            tvCheckOutMonth = (TextView) dialogView.findViewById(R.id.tv_checkout_month);
            tvCheckOutWeek = (TextView) dialogView.findViewById(R.id.tv_checkout_week);
            advanceAmount = (TextView) dialogView.findViewById(R.id.advanceAmount);
            paymentStatus = (TextView) dialogView.findViewById(R.id.paymentStatus);

            closeDialog = (ImageView) dialogView.findViewById(R.id.close_dialog);
            submit = (TextView) dialogView.findViewById(R.id.tv_submit);
            resortNameTV = (TextView) dialogView.findViewById(R.id.resort_type_name);

            if(language.equalsIgnoreCase("En")) {
                resortNameTV.setText(resortDetailsArrayList.get(0).getResortName());
            }
            else{
                resortNameTV.setText(resortDetailsArrayList.get(0).getResortNameAr());
            }

            Calendar c = Calendar.getInstance();
            c.setTime(bookingStartDate);
            mCheckInYear = c.get(Calendar.YEAR);
            mCheckInMonth = c.get(Calendar.MONTH);
            mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckInDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckInDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

            c.setTime(bookingEndDate);
            mCheckInYear = c.get(Calendar.YEAR);
            mCheckInMonth = c.get(Calendar.MONTH);
            mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

            if((c.get(Calendar.DATE)) <10) {
                tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
            }
            else{
                tvCheckOutDate.setText("" + c.get(Calendar.DATE));
            }
            tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
            tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

//            SimpleDateFormat DateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
//            String startDate = DateFormat.format(bookingStartDate);
//            String endDate = DateFormat.format(bookingEndDate);

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            Date todayDate = calendar.getTime();

//            tvCheckInDate.setText(startDate);
//            tvCheckOutDate.setText(endDate);
            advanceAmount.setText(""+(int) Float.parseFloat(advance));
            if(payStatus.equalsIgnoreCase("pending")){
                if(language.equalsIgnoreCase("En")) {
                    paymentStatus.setText("Pending");
                }
                else{
                    paymentStatus.setText("انتظار الدفع");
                }
            }
            else{
                if(language.equalsIgnoreCase("En")) {
                    paymentStatus.setText("Done");
                }
                else{
                    paymentStatus.setText("تم الدفع");
                }
            }

            if (todayDate.after(bookingEndDate) || !payStatus.equalsIgnoreCase("pending")) {
                submit.setVisibility(View.GONE);
            }

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    final iOSDialog iOSDialog = new iOSDialog(getContext());
//                    String appName, title, positive, negative;
//                    if(language.equalsIgnoreCase("En")){
//                        appName = getResources().getString(R.string.app_name);
//                        title = "Did you receive advance payment?";
//                        positive = getResources().getString(R.string.str_btn_yes);
//                        negative = getResources().getString(R.string.str_btn_no);
//                    }
//                    else{
//                        appName = getResources().getString(R.string.app_name_ar);
//                        title = "هل تم إستلام العربون ؟";
//                        positive = getResources().getString(R.string.str_btn_yes_ar);
//                        negative = getResources().getString(R.string.str_btn_no_ar);
//                    }
//                    iOSDialog.setTitle(appName);
//                    iOSDialog.setSubtitle(title);
//                    iOSDialog.setPositiveLabel(positive);
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setNegativeLabel(negative);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                            new UpdatePaymentStatus().execute(Constants.UPDATE_PAYMENT_STATUS_URL + bookingId + "&amount="
//                                    + advance +"&orderstatus=New");
//                        }
//                    });
//                    iOSDialog.setNegativeListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            JSONObject mainObj = new JSONObject();
//                            try {
//                                JSONObject bookingObj = new JSONObject();
//                                bookingObj.put("BookingId",bookingId);
//                                mainObj.put("CancelDetails",bookingObj);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            Log.i("TAG",""+mainObj.toString());
//                            new cancelOrder().execute(mainObj.toString());
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getContext().getResources().getString(R.string.app_name));
                        yes.setText(getContext().getResources().getString(R.string.str_btn_yes));
                        no.setText(getContext().getResources().getString(R.string.str_btn_no));
                        desc.setText("Did you receive advance payment?");
                    }
                    else{
                        title.setText(getContext().getResources().getString(R.string.app_name_ar));
                        yes.setText(getContext().getResources().getString(R.string.str_btn_yes_ar));
                        no.setText(getContext().getResources().getString(R.string.str_btn_no_ar));
                        desc.setText("هل تم إستلام العربون ؟");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            new UpdatePaymentStatus().execute(Constants.UPDATE_PAYMENT_STATUS_URL + bookingId + "&amount="
                                    + advance +"&orderstatus=New");
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            customDialog.dismiss();
                            JSONObject mainObj = new JSONObject();
                            try {
                                JSONObject bookingObj = new JSONObject();
                                bookingObj.put("BookingId",bookingId);
                                mainObj.put("CancelDetails",bookingObj);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Log.i("TAG",""+mainObj.toString());
                            new cancelOrder().execute(mainObj.toString());
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
            });

            closeDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });

            return dialogView;
        }

        public class cancelOrder extends AsyncTask<String, Integer, String> {
            String networkStatus;
            ACProgressFlower dialog;
            String response;
            InputStream inputStream = null;

            @Override
            protected void onPreExecute() {
                networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                dialog = new ACProgressFlower.Builder(getContext())
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY).build();
                dialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.CANCEL_ORDER_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    Log.i("TAG", "fav response:" + response);
                    return response;
                } else {
                    return "no internet";
                }
            }

            @Override
            protected void onPostExecute(String result) {

                if (result != null) {
                    if (result.equalsIgnoreCase("no internet")) {
                        Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        if (result.equals("")) {
                            Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {

                            try {
                                JSONObject jo = new JSONObject(result);
                                String s = jo.getString("Success");
                                if(language.equalsIgnoreCase("En")) {
                                    Toast.makeText(getContext(), "Booking Cancelled", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(getContext(), "تم إلغاء الحجز", Toast.LENGTH_SHORT).show();
                                }
                                getDialog().dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

                super.onPostExecute(result);

            }
        }

        private static String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }

        public class UpdatePaymentStatus extends AsyncTask<String, Integer, String> {

            String response;
            String networkStatus;
            ACProgressFlower dialog;

            @Override
            protected void onPreExecute() {
                response = null;
                networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                dialog = new ACProgressFlower.Builder(getContext())
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY).build();
                dialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.i("TAG", ""+ response);
                    return response;
                } else {
                    return "no internet";
                }
            }

            @Override
            protected void onPostExecute(String result) {
                if (result != null) {
                    if (result.equalsIgnoreCase("no internet")) {
                        Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        if (result.equals("")) {
                            Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                JSONObject jo= new JSONObject(result);
                                String success = jo.getString("Success");
                                submit.setVisibility(View.GONE);
                                if(language.equalsIgnoreCase("En")) {
                                    paymentStatus.setText("Done");
                                }
                                else{
                                    paymentStatus.setText("تم الدفع");
                                }

                            } catch (Exception e) {
                                try {
                                    JSONObject jo= new JSONObject(result);
                                    String failure = jo.getString("Failure");
                                    Toast.makeText(getContext(), failure, Toast.LENGTH_SHORT).show();
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                                }
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        }
    }
}
