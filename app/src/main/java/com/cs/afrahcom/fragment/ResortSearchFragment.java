package com.cs.afrahcom.fragment;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.afrahcom.Constants;
import com.cs.afrahcom.GPSTracker;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.MainActivity;
import com.cs.afrahcom.activity.RatingActivity;
import com.cs.afrahcom.activity.SearchResultActivity;
import com.cs.afrahcom.adapter.BannersNewAdapter;
import com.cs.afrahcom.models.Cities;
import com.cs.afrahcom.models.Facilities;
import com.cs.afrahcom.models.ImagesModel;
import com.cs.afrahcom.models.ResortDetails;
import com.cs.afrahcom.models.ResortDetailsFiltered;
import com.cs.afrahcom.models.ResortType;
import com.github.demono.AutoScrollViewPager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by CS on 26-09-2017.
 */

public class ResortSearchFragment extends Fragment implements View.OnClickListener{

    RelativeLayout layoutMan, layoutWoman, layoutFamily, layoutNumofMen, layoutNumofWomen, layoutGuest;
    RelativeLayout layoutCheckin, layoutCheckout;
    ImageView cbMan, cbWoman, cbKitchen;
    private int mCheckInYear, mCheckInMonth, mCheckInDay, mCheckOutYear, mCheckOutMonth, mCheckOutDay;
    String checkInDate, checkOutDate;
    TextView tvCheckInDate, tvCheckInMonth, tvCheckInWeek;
    TextView tvCheckOutDate, tvCheckOutMonth, tvCheckOutWeek;
    TextView tvSearch, tvResortTypeName, tvCityName, tvNumOfDays;
    TextView tvNumOfMen, tvNumOfWomen, tvKitchen, tvnumOfGuests;
    EditText etguests, etMenCapacity, etWomenCapacity;
    Boolean isManSelected = true, isWomanSelected = false, isKitchenSelected = false;
    Date mCheckInDate = null, mCheckOutDate = null, mTodayDate = null;
    public static Boolean isAfter3Days = false, isToday = false;
    String mCheckInDateStr = null, mCheckOutDateStr = null;
    long NumofDaysStr = 0;
    int mResortPos = 0, mCityPos = 0;
    String userId;
    SharedPreferences userPrefs;
    int NumofDays = 2;
    String strResortType, strWeeks = "";
    Boolean checkingWeekFirstTime = false;
    public static String todayWeek = "";
    int count = 0;
    RatingBar ratingBar;
    AlertDialog customDialog;

    CircleIndicator defaultIndicator;
    AutoScrollViewPager defaultViewpager;
    BannersNewAdapter mBannersAdapter;
    public static double currentLatitude, currentLongitude, destinationLatitude, destinationLongitude;
    String distance = "0";
    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;

    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};

    ArrayList<ResortDetails> resortDetailsArrayList = new ArrayList<>();
    ArrayList<ResortType> resortTypeArrayList = new ArrayList<>();
    ArrayList<ResortDetailsFiltered> bannersArrayList = new ArrayList<>();
    ArrayList<Cities> citiesArrayList = new ArrayList<>();
    ArrayList<String> citiesSpinnerArray = new ArrayList<>();
    ArrayList<String> resortSpinnerArray = new ArrayList<>();
    //    ArrayList<String> numberofGuestsArray = new ArrayList<>();
    public static ArrayList<String> facilitiesArray = new ArrayList<>();
    ArrayAdapter<String> resortAdapter;
    ArrayAdapter<String> cityAdapter;
    //    ArrayAdapter<String> guestsAdapter;
//    ArrayAdapter<String> menAdapter;
//    ArrayAdapter<String> womenAdapter;
    Spinner resortTypeSpinner, citySpinner;
    //    guestsSpinner, menSpinner, womenSpinner;
    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    int minimumPrice, maximumPrice;
    RelativeLayout rateLayout;
    TextView resortName1;
    TextView skipRating;
    public static String strCheckInDate, strCheckInMonth, strCheckInWeek, strCheckoutDate, strCheckoutMonth, strCheckoutWeek, strNumDays;
    SharedPreferences languagePrefs;
    String language;
    View rootView;
    private FusedLocationProviderClient mFusedLocationClient;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.activity_resort_search, container, false);
        }
        else{
            rootView = inflater.inflate(R.layout.activity_resort_search_ar, container, false);
        }

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        layoutMan = rootView.findViewById(R.id.layout_man);
        layoutWoman = rootView.findViewById(R.id.layout_woman);
        layoutFamily = rootView.findViewById(R.id.layout_kitchen);
        layoutNumofMen = rootView.findViewById(R.id.layout_NoOfMen);
        layoutNumofWomen = rootView.findViewById(R.id.layout_numOfWomen);
        layoutGuest = rootView.findViewById(R.id.layout_guests);
        rateLayout = rootView.findViewById(R.id.rate_layout);
        resortName1 = rootView.findViewById(R.id.dialog_title);

        layoutCheckin = rootView.findViewById(R.id.layout_checkin);
        layoutCheckout = rootView.findViewById(R.id.layout_checkout);

        ratingBar = rootView.findViewById(R.id.ratingBar1);

        cbMan = rootView.findViewById(R.id.cb_man);
        cbWoman = rootView.findViewById(R.id.cb_woman);
        cbKitchen = rootView.findViewById(R.id.cb_kitchen);

        tvCheckInDate = rootView.findViewById(R.id.tv_checkin_date);
        tvCheckInMonth = rootView.findViewById(R.id.tv_checkin_month);
        tvCheckInWeek = rootView.findViewById(R.id.tv_checkin_week);
        skipRating = rootView.findViewById(R.id.skip_rating);

        tvCheckOutDate = rootView.findViewById(R.id.tv_checkout_date);
        tvCheckOutMonth = rootView.findViewById(R.id.tv_checkout_month);
        tvCheckOutWeek = rootView.findViewById(R.id.tv_checkout_week);

        tvSearch = rootView.findViewById(R.id.tv_search);
        tvResortTypeName = rootView.findViewById(R.id.resort_type_name);
        tvCityName = rootView.findViewById(R.id.tv_city_name);
        etguests = rootView.findViewById(R.id.tv_num_of_guests);
        tvNumOfDays = rootView.findViewById(R.id.tv_number_of_days);
        etMenCapacity = rootView.findViewById(R.id.tv_num_of_men);
        etWomenCapacity = rootView.findViewById(R.id.tv_num_of_women);

        tvNumOfMen = rootView.findViewById(R.id.tvNumOfMen);
        tvNumOfWomen = rootView.findViewById(R.id.tvNumOfWomen);
        tvKitchen = rootView.findViewById(R.id.tvKitchen);
        tvnumOfGuests = rootView.findViewById(R.id.tvnumOfGuests);

        resortTypeSpinner = rootView.findViewById(R.id.resort_spinner);
        citySpinner = rootView.findViewById(R.id.city_spinner);
//        guestsSpinner = (Spinner) rootView.findViewById(R.id.guests_spinner);
//        menSpinner = (Spinner) rootView.findViewById(R.id.men_spinner);
//        womenSpinner = (Spinner) rootView.findViewById(R.id.women_spinner);

        defaultViewpager = rootView.findViewById(R.id.viewPager);
        defaultIndicator = rootView.findViewById(R.id.indicator);

        isAfter3Days = false;

        resortAdapter = new ArrayAdapter<String>(getContext(), R.layout.list_spinner, resortSpinnerArray) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(18);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.colorSignupText));

                return v;
            }
        };

        cityAdapter = new ArrayAdapter<String>(getContext(), R.layout.list_spinner, citiesSpinnerArray) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(18);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.colorSignupText));

                return v;
            }
        };

        tvNumOfMen.setVisibility(View.GONE);
        tvNumOfWomen.setVisibility(View.GONE);
        tvnumOfGuests.setVisibility(View.VISIBLE);
        tvKitchen.setVisibility(View.VISIBLE);
        layoutNumofMen.setVisibility(View.GONE);
        layoutNumofWomen.setVisibility(View.GONE);
        layoutGuest.setVisibility(View.VISIBLE);
        layoutFamily.setVisibility(View.VISIBLE);

        resortTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                tvResortTypeName.setText(resortSpinnerArray.get(position));
                strResortType = resortSpinnerArray.get(position);
                mResortPos = position;
                Constants.isKitchenSelected = 0;

                if(resortTypeArrayList.get(position).getResortTypeId().equalsIgnoreCase("1")){
                    tvNumOfMen.setVisibility(View.GONE);
                    tvNumOfWomen.setVisibility(View.GONE);
                    tvnumOfGuests.setVisibility(View.VISIBLE);
                    tvKitchen.setVisibility(View.VISIBLE);
                    layoutNumofMen.setVisibility(View.GONE);
                    layoutNumofWomen.setVisibility(View.GONE);
                    layoutGuest.setVisibility(View.VISIBLE);
                    layoutFamily.setVisibility(View.VISIBLE);
                }
                else if(resortTypeArrayList.get(position).getResortTypeId().equalsIgnoreCase("3")){
                    tvNumOfMen.setVisibility(View.VISIBLE);
                    tvNumOfWomen.setVisibility(View.VISIBLE);
                    tvnumOfGuests.setVisibility(View.GONE);
                    tvKitchen.setVisibility(View.GONE);
                    layoutNumofMen.setVisibility(View.VISIBLE);
                    layoutNumofWomen.setVisibility(View.VISIBLE);
                    layoutGuest.setVisibility(View.GONE);
                    layoutFamily.setVisibility(View.GONE);
//                    tvNumOfMen.setText("0");
//                    tvNumOfWomen.setText("0");
                }
                else if(resortTypeArrayList.get(position).getResortTypeId().equalsIgnoreCase("2")){
                    tvNumOfMen.setVisibility(View.GONE);
                    tvNumOfWomen.setVisibility(View.GONE);
                    tvnumOfGuests.setVisibility(View.VISIBLE);
                    tvKitchen.setVisibility(View.GONE);
                    layoutNumofMen.setVisibility(View.GONE);
                    layoutNumofWomen.setVisibility(View.GONE);
                    layoutGuest.setVisibility(View.VISIBLE);
                    layoutFamily.setVisibility(View.GONE);
                }
//                if(resortSpinnerArray.get(position).equalsIgnoreCase("wedding hall")){
//                    tvNumOfMen.setVisibility(View.GONE);
//                    tvNumOfWomen.setVisibility(View.GONE);
//                    tvnumOfGuests.setVisibility(View.VISIBLE);
//                    tvKitchen.setVisibility(View.VISIBLE);
//                    layoutNumofMen.setVisibility(View.GONE);
//                    layoutNumofWomen.setVisibility(View.GONE);
//                    layoutGuest.setVisibility(View.VISIBLE);
//                    layoutFamily.setVisibility(View.VISIBLE);
//                }
//                else if(resortSpinnerArray.get(position).equalsIgnoreCase("Banquet Hall")){
//                    tvNumOfMen.setVisibility(View.VISIBLE);
//                    tvNumOfWomen.setVisibility(View.VISIBLE);
//                    tvnumOfGuests.setVisibility(View.GONE);
//                    tvKitchen.setVisibility(View.GONE);
//                    layoutNumofMen.setVisibility(View.VISIBLE);
//                    layoutNumofWomen.setVisibility(View.VISIBLE);
//                    layoutGuest.setVisibility(View.GONE);
//                    layoutFamily.setVisibility(View.GONE);
//                }
//                else if(resortSpinnerArray.get(position).equalsIgnoreCase("resort")){
//                    tvNumOfMen.setVisibility(View.GONE);
//                    tvNumOfWomen.setVisibility(View.GONE);
//                    tvnumOfGuests.setVisibility(View.VISIBLE);
//                    tvKitchen.setVisibility(View.GONE);
//                    layoutNumofMen.setVisibility(View.GONE);
//                    layoutNumofWomen.setVisibility(View.GONE);
//                    layoutGuest.setVisibility(View.VISIBLE);
//                    layoutFamily.setVisibility(View.GONE);
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvCityName.setText(citiesSpinnerArray.get(i));
                mCityPos = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        skipRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateLayout.setVisibility(View.GONE);
            }
        });

//        guestsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                etguests.setText(numberofGuestsArray.get(i));
//                mCapacityPos = i;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        menSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                etMenCapacity.setText(numberofGuestsArray.get(i));
//                mMenPos = i;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//        womenSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                etWomenCapacity.setText(numberofGuestsArray.get(i));
//                mWomenPos = i;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        final Calendar c = Calendar.getInstance();
        mCheckInYear = c.get(Calendar.YEAR);
        mCheckInMonth = c.get(Calendar.MONTH);
        mCheckInDay = c.get(Calendar.DAY_OF_MONTH);

        if((c.get(Calendar.DATE)) <10) {
            tvCheckInDate.setText("0" + c.get(Calendar.DATE));
        }
        else{
            tvCheckInDate.setText("" + c.get(Calendar.DATE));
        }
        tvCheckInMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckInWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

        c.add(Calendar.DATE, 1);
        mCheckOutYear = c.get(Calendar.YEAR);
        mCheckOutMonth = c.get(Calendar.MONTH);
        mCheckOutDay = c.get(Calendar.DAY_OF_MONTH);

        if((c.get(Calendar.DATE)) <10) {
            tvCheckOutDate.setText("0" + c.get(Calendar.DATE));
        }
        else{
            tvCheckOutDate.setText("" + c.get(Calendar.DATE));
        }
        tvCheckOutMonth.setText(MONTHS[c.get(Calendar.MONTH)]);
        tvCheckOutWeek.setText(WEEKS[(c.get(Calendar.DAY_OF_WEEK)-1)]);

        String checkInDateString = mCheckInYear+"-"+(mCheckInMonth+1)+"-"+mCheckInDay;
        String checkOutDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(checkInDateString);
            mCheckOutDate = sdf.parse(checkOutDateString);
            mTodayDate = sdf.parse(checkInDateString);

            mCheckInDateStr = sdf.format(mCheckInDate);
            mCheckOutDateStr = sdf.format(mCheckOutDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getDates();

        new GetBanners().execute(Constants.GET_BANNERS_URL+userId+"&UserType=1");
        getWeekDays();

        layoutMan.setOnClickListener(this);
        layoutWoman.setOnClickListener(this);
        layoutFamily.setOnClickListener(this);

        layoutCheckin.setOnClickListener(this);
        layoutCheckout.setOnClickListener(this);
        tvSearch.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_man:
                if(isManSelected) {
                    isManSelected = false;
                    cbMan.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                }
                else{
                    isManSelected = true;
                    cbMan.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                }
                break;

            case R.id.layout_woman:
                if(isWomanSelected) {
                    isWomanSelected = false;
                    cbWoman.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                }
                else{
                    isWomanSelected = true;
                    cbWoman.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                }
                break;

            case R.id.layout_kitchen:
                if(isKitchenSelected) {
                    isKitchenSelected = false;
                    cbKitchen.setImageDrawable(getResources().getDrawable(R.drawable.gender_unselected));
                }
                else{
                    isKitchenSelected = true;
                    cbKitchen.setImageDrawable(getResources().getDrawable(R.drawable.gender_selected));
                }
                break;

            case R.id.layout_checkin:
                checkInDatePicker();
                break;

            case R.id.layout_checkout:
                checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                break;

            case R.id.tv_search:
                if(!isManSelected && !isWomanSelected){
//                    String appName, title, positive;
//                    if(language.equalsIgnoreCase("En")){
//                        appName = getResources().getString(R.string.app_name);
//                        title = "Please select Gender";
//                        positive = getResources().getString(R.string.str_btn_ok);
//                    }
//                    else{
//                        appName = getResources().getString(R.string.app_name_ar);
//                        title = "يرجى تحديد الجنس";
//                        positive = getResources().getString(R.string.str_btn_ok_ar);
//                    }
//                    final iOSDialog iOSDialog = new iOSDialog(getContext());
//                    iOSDialog.setTitle(appName);
//                    iOSDialog.setSubtitle(title);
//                    iOSDialog.setPositiveLabel(positive);
//                    iOSDialog.setBoldPositiveLabel(false);
//                    iOSDialog.setPositiveListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            iOSDialog.dismiss();
//                        }
//                    });
//                    iOSDialog.show();

                    showDialog("Please select Gender", "يرجى تحديد الجنس");
                }
                else {
                    NumofDays = Integer.parseInt(tvNumOfDays.getText().toString());
                    getDates();

                    int capacity = 0;
                    try {
                        if(resortTypeArrayList.get(mResortPos).getResortTypeName().equalsIgnoreCase("banquet hall") ||
                                resortTypeArrayList.get(mResortPos).getResortTypeName().equalsIgnoreCase("قاعات الفنادق")){
                            if(isManSelected && isWomanSelected) {
                                capacity = Integer.parseInt(etMenCapacity.getText().toString()) + Integer.parseInt(etWomenCapacity.getText().toString());
                                Constants.noOfMen = etMenCapacity.getText().toString();
                                Constants.noOfWomen = etWomenCapacity.getText().toString();
                            }
                            else if(isManSelected){
                                capacity = Integer.parseInt(etMenCapacity.getText().toString());
                                Constants.noOfMen = etMenCapacity.getText().toString();
                                Constants.noOfWomen = "0";
                            }
                            else if(isWomanSelected){
                                capacity = Integer.parseInt(etWomenCapacity.getText().toString());
                                Constants.noOfMen = "0";
                                Constants.noOfWomen = etWomenCapacity.getText().toString();
                            }
                            Constants.noOfGuests = Integer.toString(capacity);
                        }
                        else {
                            if(isManSelected && isWomanSelected){
                                Constants.noOfMen = etguests.getText().toString();
                                Constants.noOfWomen = etguests.getText().toString();
                            }
                            else if(isWomanSelected){
                                Constants.noOfMen = "0";
                                Constants.noOfWomen = etguests.getText().toString();
                            }
                            else if(isManSelected){
                                Constants.noOfMen = etguests.getText().toString();
                                Constants.noOfWomen = "0";
                            }
                            capacity = Integer.parseInt(etguests.getText().toString());
                            Constants.noOfGuests = Integer.toString(capacity);
                        }

                        if(capacity!=0) {
                            if (isKitchenSelected) {
                                Constants.isKitchenSelected = 1;
                            } else {
                                Constants.isKitchenSelected = 0;
                            }
                            final JSONObject parent = new JSONObject();
                            try {
                                JSONArray mainItem = new JSONArray();

                                JSONObject mainObj = new JSONObject();
                                mainObj.put("ResortTypeId", resortTypeArrayList.get(mResortPos).getResortTypeId());
//                        mainObj.put("ResortTypeId", "1");
                                mainObj.put("CityId", citiesArrayList.get(mCityPos).getCityId());
                                mainObj.put("IsMan", isManSelected);
                                mainObj.put("IsWoman", isWomanSelected);
                                mainObj.put("IsKitchen", isKitchenSelected);
                                mainObj.put("NoOfCapacity", Integer.toString(capacity));
//                        mainObj.put("NoOfCapacity", Integer.toString(10));
                                mainObj.put("StartDate", Constants.convertToArabic(mCheckInDateStr));
                                mainObj.put("EndDate", Constants.convertToArabic(mCheckOutDateStr));
                                if (userId == null) {
                                    mainObj.put("UserId", "-1");
                                } else {
                                    mainObj.put("UserId", userId);
                                }
                                mainItem.put(mainObj);

                                parent.put("SearchDetails", mainItem);
                                Log.i("TAG", parent.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Constants.resortTypeId = resortTypeArrayList.get(mResortPos).getResortTypeId();

                            new getSearchResults().execute(parent.toString());
                        }
                        else{
                            showDialog("Please select number of guests", "يرجى تحديد عدد الضيوف");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if(etMenCapacity.getText().toString().length() == 0 || etWomenCapacity.getText().toString().length() == 0
                                || etguests.getText().toString().length() == 0){
                            showDialog("Please select capacity", "Please select capacity");
                        }
                        else {
                            new GetBanners().execute(Constants.GET_BANNERS_URL+userId+"&UserType=1");
                        }
                    }
                }
                break;
        }
    }

    public void checkInDatePicker(){
        final Calendar c = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckInYear = year;
                        mCheckInDay = dayOfMonth;
                        mCheckInMonth = monthOfYear;

                        if(mCheckInDay<10) {
                            tvCheckInDate.setText("0" + mCheckInDay);
                        }
                        else{
                            tvCheckInDate.setText("" + mCheckInDay);
                        }
                        tvCheckInMonth.setText(MONTHS[mCheckInMonth]);

                        checkOutDatePicker(mCheckInYear, mCheckInDay, (mCheckInMonth+1));
                    }
                }, mCheckInYear, mCheckInMonth, mCheckInDay);

        String msg;
        if(language.equalsIgnoreCase("En")){
            msg = "Select Check In Date";
        }
        else{
            msg = "تاريخ الوصول";
        }
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//        long max = TimeUnit.DAYS.toMillis(90);
//        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
        datePickerDialog.setCancelable(false);
        datePickerDialog.setMessage(msg);
        datePickerDialog.show();
    }

    public void checkOutDatePicker(int year, int date, int month){
        final Calendar c = Calendar.getInstance();
        long selectedMillis = c.getTimeInMillis();
//        final Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, 1);
        String givenDateString = year+"-"+(month)+"-"+date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mCheckInDate = sdf.parse(givenDateString);
            mCheckInDateStr = sdf.format(mCheckInDate);
            selectedMillis = mCheckInDate.getTime();
            c.setTime(mCheckInDate); // yourdate is an object of type Date
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            tvCheckInWeek.setText(WEEKS[(dayOfWeek-1)]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mCheckOutYear = mCheckInYear;
        mCheckOutMonth = mCheckInMonth;
        mCheckOutDay = mCheckInDay;

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        mCheckOutYear = year;
                        mCheckOutDay = dayOfMonth;
                        mCheckOutMonth = monthOfYear;

                        if(mCheckOutDay<10) {
                            tvCheckOutDate.setText("0" + mCheckOutDay);
                        }
                        else{
                            tvCheckOutDate.setText("" + mCheckOutDay);
                        }
                        tvCheckOutMonth.setText(MONTHS[mCheckOutMonth]);

                        String givenDateString = mCheckOutYear+"-"+(mCheckOutMonth+1)+"-"+mCheckOutDay;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            mCheckOutDate = sdf.parse(givenDateString);
                            mCheckOutDateStr = sdf.format(mCheckOutDate);
                            c.setTime(mCheckOutDate); // yourdate is an object of type Date
                            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                            tvCheckOutWeek.setText(WEEKS[(dayOfWeek-1)]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        CalculateNumOfDays();

                    }
                }, mCheckOutYear, mCheckOutMonth, mCheckOutDay);
        String msg;
        if(language.equalsIgnoreCase("En")){
            msg = "Select Check Out Date";
        }
        else{
            msg = "تاريخ المغادرة";
        }
        datePickerDialog.getDatePicker().setMinDate(selectedMillis);
//        long max = TimeUnit.DAYS.toMillis(90);
//        datePickerDialog.getDatePicker().setMaxDate(selectedMillis+max);
        datePickerDialog.setInverseBackgroundForced(true);
        datePickerDialog.setMessage(msg);
        datePickerDialog.show();
    }


    public class GetBanners extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        //        MaterialDialog dialog;
        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
//            dialog = new MaterialDialog.Builder(getContext())
//                    .title(R.string.app_name)
//                    .content("Please wait...")
//                    .progress(true, 0)
//                    .cancelable(false)
//                    .backgroundColor(getResources().getColor(R.color.colorPrimaryDark))
//                    .widgetColor(getResources().getColor(R.color.white))
//                    .progressIndeterminateStyle(true)
//                    .show();
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
                } else {
                    if (result.equals("")) {
                        Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
//                        new GetBanners().execute(Constants.GET_BANNERS_URL);
                    } else {
                        try {
                            bannersArrayList.clear();
                            resortSpinnerArray.clear();
                            resortTypeArrayList.clear();
                            Constants.CityArrayList.clear();
                            facilitiesArray.clear();
                            JSONObject jo= new JSONObject(result);
                            JSONObject ja = jo.getJSONObject("Success");
                            MainActivity.userNotificationCount = ja.getInt("UnReadNotificationCount");
//                            MainActivity.adapter.notifyDataSetChanged();
                            Intent in = new Intent("data_action");
                            getContext().sendBroadcast(in);
                            JSONArray jsonArray = ja.getJSONArray("PropertyDetails");
                            for (int i=0; i<jsonArray.length(); i++){
                                try {
                                    DecimalFormat priceFormat = new DecimalFormat("0");

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    ArrayList<String> banners = new ArrayList<>();
                                    JSONArray jsonArray5 = jsonObject.getJSONArray("Banners");
                                    for (int k = 0; k<jsonArray5.length(); k++){
                                        JSONObject jsonObject12 = jsonArray5.getJSONObject(k);
                                        banners.add(jsonObject12.getString("ImageName"));
                                    }

                                    for (int k = 0; k < banners.size(); k++) {
                                        ResortDetailsFiltered resortDetails = new ResortDetailsFiltered();
                                        resortDetails.setResortId(jsonObject.getString("ResortId"));
                                        resortDetails.setResortTypeId(jsonObject.getString("ResortTypeId"));
                                        resortDetails.setResortName(jsonObject.getString("ResortName"));
                                        resortDetails.setResortNameAr(jsonObject.getString("ResortName_Ar"));
                                        resortDetails.setCityId(jsonObject.getString("CityId"));
                                        resortDetails.setResortAddress(jsonObject.getString("ResortAddress"));
                                        resortDetails.setResortType(jsonObject.getString("ResortType"));
                                        resortDetails.setDesc(jsonObject.getString("Description"));
                                        resortDetails.setDescAr(jsonObject.getString("DescriptionArabic"));
                                        resortDetails.setMinCapacity(Integer.parseInt(jsonObject.getString("MinCapacity")));
                                        resortDetails.setMaxCapacity(Integer.parseInt(jsonObject.getString("MaxCapacity")));
                                        //                                resortDetails.setNoOfCapacity(jsonObject.getString("No Of Capacity"));
                                        resortDetails.setLatitude(jsonObject.getString("Latitude"));
                                        resortDetails.setLongitude(jsonObject.getString("Longitude"));
                                        resortDetails.setRating(jsonObject.getString("Rating"));
                                        resortDetails.setDiscount(jsonObject.getString("Discount"));
                                        resortDetails.setRate(jsonObject.getString("Rate"));
                                        resortDetails.setVatAvailability(jsonObject.getString("VatAvailability"));
                                        resortDetails.setAfrahcomAdvance(jsonObject.getString("AdvanceAmount"));
                                        resortDetails.setApproved(jsonObject.getString("AfrahcomApproved"));

                                        JSONArray imagesJSON = jsonObject.getJSONArray("ResortImage");
                                        ArrayList<ImagesModel> imagesArrayList = new ArrayList<>();
                                        for (int j = 0; j < imagesJSON.length(); j++) {
                                            ImagesModel imagesModel = new ImagesModel();
                                            JSONObject imagesObject = imagesJSON.getJSONObject(j);
                                            imagesModel.setImageName(imagesObject.getString("ImageName"));
                                            imagesModel.setSource(imagesObject.getString("Source"));
                                            imagesArrayList.add(imagesModel);
                                        }
                                        resortDetails.setResortImagesArray(imagesArrayList);
                                        resortDetails.setBannerImage(banners.get(k));

                                        destinationLatitude = Double.parseDouble(jsonObject.getString("Latitude"));
                                        destinationLongitude = Double.parseDouble(jsonObject.getString("Longitude"));
                                        float rate = Float.parseFloat(jsonObject.getString("Rate"));

                                        Location me = new Location("");
                                        Location dest = new Location("");

                                        me.setLatitude(currentLatitude);
                                        me.setLongitude(currentLongitude);

                                        dest.setLatitude(destinationLatitude);
                                        dest.setLongitude(destinationLongitude);

                                        final DecimalFormat distanceFormat = new DecimalFormat("0");
                                        float dist = (me.distanceTo(dest)) / 1000;
                                        //                                    new getTrafficTime().execute();
                                        resortDetails.setDistance(Integer.toString(Math.round(dist)));

                                        try {
                                            JSONArray jsonArray12 = jsonObject.getJSONArray("Prices");
                                            for (int j = 0; j < jsonArray12.length(); j++) {
                                                JSONObject obj = jsonArray12.getJSONObject(j);
                                                if (obj.has("Saturday")) {
                                                    JSONObject satObj = obj.getJSONObject("Saturday");
                                                    resortDetails.setSatMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                                    resortDetails.setSatWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                                    resortDetails.setSatKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                                    resortDetails.setSatFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                                } else if (obj.has("Sunday")) {
                                                    JSONObject satObj = obj.getJSONObject("Sunday");
                                                    resortDetails.setSunMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                                    resortDetails.setSunWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                                    resortDetails.setSunKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                                    resortDetails.setSunFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                                } else if (obj.has("Monday")) {
                                                    JSONObject satObj = obj.getJSONObject("Monday");
                                                    resortDetails.setMonMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                                    resortDetails.setMonWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                                    resortDetails.setMonKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                                    resortDetails.setMonFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                                } else if (obj.has("Tuesday")) {
                                                    JSONObject satObj = obj.getJSONObject("Tuesday");
                                                    resortDetails.setTueMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                                    resortDetails.setTueWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                                    resortDetails.setTueKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                                    resortDetails.setTueFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                                } else if (obj.has("Wednesday")) {
                                                    JSONObject satObj = obj.getJSONObject("Wednesday");
                                                    resortDetails.setWedMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                                    resortDetails.setWedWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                                    resortDetails.setWedKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                                    resortDetails.setWedFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                                } else if (obj.has("Thursday")) {
                                                    JSONObject satObj = obj.getJSONObject("Thursday");
                                                    resortDetails.setThursMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                                    resortDetails.setThursWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                                    resortDetails.setThursKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                                    resortDetails.setThursFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                                } else if (obj.has("Friday")) {
                                                    JSONObject satObj = obj.getJSONObject("Friday");
                                                    resortDetails.setFriMenPrice(priceFormat.format(Float.parseFloat(satObj.getString("MenPrice"))));
                                                    resortDetails.setFriWomenPrice(priceFormat.format(Float.parseFloat(satObj.getString("WomanPrice"))));
                                                    resortDetails.setFriKitchenPrice(priceFormat.format(Float.parseFloat(satObj.getString("KitchenPrice"))));
                                                    resortDetails.setFriFamilyPrice(priceFormat.format(Float.parseFloat(satObj.getString("FamilyPrice"))));
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        JSONArray jsonArray4 = jsonObject.getJSONArray("Facililty");
                                        ArrayList<Facilities> resortFacilities = new ArrayList<>();
                                        resortFacilities.clear();
                                        for (int j = 0; j < jsonArray4.length(); j++) {
                                            Facilities facilities = new Facilities();
                                            JSONObject jsonObject1 = jsonArray4.getJSONObject(j);
                                            facilities.setFacilityName(jsonObject1.getString("FacilityName"));
                                            facilities.setFacilityNameAr(jsonObject1.getString("FacilityName_Ar"));
                                            facilities.setFacilityImage(jsonObject1.getString("FacilityImage"));
                                            resortFacilities.add(facilities);
                                        }
                                        resortDetails.setResortFacilities(resortFacilities);

                                        bannersArrayList.add(resortDetails);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            JSONArray jsonArray1 = ja.getJSONArray("ResortType");
                            for (int i = 0; i<jsonArray1.length(); i++){
                                ResortType resortType = new ResortType();

                                JSONObject jsonObject = jsonArray1.getJSONObject(i);
                                resortType.setResortTypeId(jsonObject.getString("ResortTypeId"));
                                if(language.equalsIgnoreCase("En")) {
                                    resortType.setResortTypeName(jsonObject.getString("ResortTypeName"));
                                }
                                else{
                                    resortType.setResortTypeName(jsonObject.getString("ResortTypeName_Ar"));
                                }
                                if(language.equalsIgnoreCase("En")) {
                                    resortSpinnerArray.add(jsonObject.getString("ResortTypeName"));
                                }
                                else{
                                    resortSpinnerArray.add(jsonObject.getString("ResortTypeName_Ar"));
                                }
                                resortTypeArrayList.add(resortType);
                            }

                            Constants.resortTypeId = resortTypeArrayList.get(mResortPos).getResortTypeId();

                            JSONArray jsonArray2 = ja.getJSONArray("Cities");
                            for (int i = 0; i<jsonArray2.length(); i++){
                                Cities cities = new Cities();

                                JSONObject jsonObject = jsonArray2.getJSONObject(i);
                                cities.setCityId(jsonObject.getString("CityId"));
                                cities.setCityName(jsonObject.getString("CityName"));
                                if(jsonObject.getString("Booking").equalsIgnoreCase("true")) {
                                    if (language.equalsIgnoreCase("En")) {
                                        citiesSpinnerArray.add(jsonObject.getString("CityName"));
                                    } else {
                                        citiesSpinnerArray.add(jsonObject.getString("CityName_Ar"));
                                    }
                                    citiesArrayList.add(cities);
                                }
                                if(jsonObject.getString("SignUp").equalsIgnoreCase("true")) {
                                    Constants.CityArrayList.add(cities);
                                }
                            }

                            facilitiesArray.clear();
                            JSONArray jsonArray4 = ja.getJSONArray("Facilities");
                            for (int i = 0; i<jsonArray4.length(); i++){
                                JSONObject jsonObject = jsonArray4.getJSONObject(i);
                                if(language.equalsIgnoreCase("En")) {
                                    facilitiesArray.add(jsonObject.getString("FacilityName"));
                                }
                                else{
                                    facilitiesArray.add(jsonObject.getString("FacilityName_Ar"));
                                }
                            }

                            try {
                                JSONObject newObj = ja.getJSONObject("TrackingDetails");
                                final String resortName, foodId, bookingId;
                                if(language.equalsIgnoreCase("En")) {
                                    resortName = newObj.getString("ResortName");
                                }
                                else{
                                    resortName = newObj.getString("ResortName_Ar");
                                }
                                final String resortId = newObj.getString("ResortId");
                                String OrderStatus = newObj.getString("OrderStatus");
                                foodId = newObj.getString("BreakfastId");
                                bookingId = newObj.getString("BookingId");
                                int rating = Integer.parseInt(newObj.getString("Rating"));
//                            OrderStatus = "close";

                                if(OrderStatus.equalsIgnoreCase("close") && rating == 0) {
                                    Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                            R.anim.bottom_up);
                                    rateLayout.startAnimation(bottomUp);
                                    rateLayout.setVisibility(View.VISIBLE);
                                    ratingBar.setRating(0);
                                    if (language.equalsIgnoreCase("En")) {
                                        resortName1.setText("Rate " + resortName);
                                    } else {
                                        resortName1.setText("تقييم القاعة " + resortName);
                                    }

                                    ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                                        @Override
                                        public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                                            if (b) {
                                                rateLayout.setVisibility(View.GONE);
                                                Intent intent = new Intent(getActivity(), RatingActivity.class);
                                                intent.putExtra("resortName", resortName);
                                                intent.putExtra("resortId", resortId);
                                                intent.putExtra("foodId", foodId);
                                                intent.putExtra("BookingId", bookingId);
                                                intent.putExtra("rating", v);
                                                ActivityOptions options =
                                                        ActivityOptions.makeCustomAnimation(getContext(), R.anim.bottom_up, R.anim.bottom_down);
                                                startActivity(intent, options.toBundle());
                                            }
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            mBannersAdapter = new BannersNewAdapter(getContext(), bannersArrayList);
                            defaultViewpager.setAdapter(mBannersAdapter);
                            defaultIndicator.setViewPager(defaultViewpager);
                            defaultViewpager.startAutoScroll();
                            defaultViewpager.setCycle(true);
//                            if(language.equalsIgnoreCase("Ar")){
//                                defaultViewpager.setDirection(AutoScrollViewPager.DIRECTION_LEFT);
//                                defaultViewpager.setCurrentItem((bannersArrayList.size()-1));
//                            }

                            resortTypeSpinner.setAdapter(resortAdapter);
                            citySpinner.setAdapter(cityAdapter);
//                            guestsSpinner.setAdapter(guestsAdapter);
//                            menSpinner.setAdapter(menAdapter);
//                            womenSpinner.setAdapter(womenAdapter);
                            strResortType = resortSpinnerArray.get(0);
                            tvResortTypeName.setText(resortSpinnerArray.get(0));
                            tvCityName.setText(citiesSpinnerArray.get(0));
                            etguests.setSelection(etguests.length());
                            etMenCapacity.setSelection(etMenCapacity.length());
                            etWomenCapacity.setSelection(etWomenCapacity.length());

                            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                            if (currentapiVersion >= Build.VERSION_CODES.M) {
                                if (!canAccessLocation()) {
                                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                                } else {
                                    gps = new GPSTracker(getContext());
                                    getGPSCoordinates();
                                }
                            }else {
                                gps = new GPSTracker(getContext());
                                getGPSCoordinates();
                            }

                        } catch (Exception e) {
//                            new GetBanners().execute(Constants.GET_BANNERS_URL);
                            try {
                                Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                try {
                    Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public class getSearchResults extends AsyncTask<String, Integer, String> {
        String networkStatus;
        ACProgressFlower dialog;
        String response;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.RESORT_SEARCH_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                resortDetailsArrayList.clear();
                                JSONArray ja = jo.getJSONArray("success");

                                for (int i = 0; i<ja.length(); i++){
                                    ResortDetails resortDetails = new ResortDetails();

                                    JSONObject jsonObject = ja.getJSONObject(i);
                                    resortDetails.setResortId(jsonObject.getString("ResortId"));
                                    resortDetails.setResortName(jsonObject.getString("ResortName"));
                                    resortDetails.setResortNameAr(jsonObject.getString("ResortName_Ar"));
                                    resortDetails.setDescription(jsonObject.getString("Description"));
                                    resortDetails.setDescriptionAr(jsonObject.getString("Description_Ar"));
                                    resortDetails.setCity(jsonObject.getString("City"));
                                    resortDetails.setAdvanceAmount(Constants.convertToArabic(jsonObject.getString("AdvanceAmount")));
                                    resortDetails.setResortAddress(jsonObject.getString("ResortAddress"));
                                    resortDetails.setResortType(jsonObject.getString("ResortType"));
                                    resortDetails.setLatitude(Constants.convertToArabic(jsonObject.getString("Latitude")));
                                    resortDetails.setLongitude(Constants.convertToArabic(jsonObject.getString("Longitude")));
                                    resortDetails.setRating(Constants.convertToArabic(jsonObject.getString("Rating")));
                                    resortDetails.setIsFavorite(jsonObject.getString("IsFavourite"));
                                    resortDetails.setVatAvailability(jsonObject.getString("VatAvailability"));
//                                    resortDetails.setVatAvailability("false");
                                    resortDetails.setApproved(jsonObject.getString("AfrahcomApproved"));
                                    float rate = Float.parseFloat(jsonObject.getString("Rate"));
                                    float discount = Float.parseFloat(jsonObject.getString("Discount"));
                                    resortDetails.setDiscount(jsonObject.getString("Discount"));
                                    JSONArray imagesJSON = jsonObject.getJSONArray("ResortImage");
                                    ArrayList<ImagesModel> imagesArrayList = new ArrayList<>();
                                    for(int j = 0; j<imagesJSON.length(); j++){
                                        ImagesModel imagesModel = new ImagesModel();
                                        JSONObject imagesObject = imagesJSON.getJSONObject(j);
                                        imagesModel.setImageName(imagesObject.getString("ImageName"));
                                        imagesModel.setSource(imagesObject.getString("Source"));
                                        imagesArrayList.add(imagesModel);
                                    }
                                    resortDetails.setResortImagesArray(imagesArrayList);

                                    final DecimalFormat priceFormat = new DecimalFormat("0");
                                    float price = 0;
                                    String[] selectedWeeks = strWeeks.split(",");
                                    JSONArray priceArray = jsonObject.getJSONArray("Price");

                                    String banquetSearchTerm, weddingSearchTerm ;
                                    if(language.equalsIgnoreCase("En")){
                                        banquetSearchTerm = "banquet hall";
                                        weddingSearchTerm = "wedding hall";
                                    }
                                    else{
                                        banquetSearchTerm = "قاعات الفنادق";
                                        weddingSearchTerm = "قصور الافراح";
                                    }
                                    if(!resortTypeArrayList.get(mResortPos).getResortTypeName().equalsIgnoreCase(banquetSearchTerm)){
                                        for(int a = 0; a < selectedWeeks.length; a++){
//                                            Log.i("TAG","selectedWeeks "+selectedWeeks[a]);
//                                            JSONObject jsonObject1 = priceArray.getJSONObject(0);
//                                            JSONObject satObj = jsonObject1.getJSONObject("Saturday");
//                                            price = price + Float.parseFloat(satObj.getString("MenPrice"));

                                            if(isManSelected) {
                                                if(isWomanSelected && resortTypeArrayList.get(mResortPos).getResortTypeName().equalsIgnoreCase(weddingSearchTerm)){
                                                    try {
                                                        if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(0);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Saturday");
                                                            price = price + Float.parseFloat(satObj.getString("FamilyPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(1);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Sunday");
                                                            price = price + Float.parseFloat(satObj.getString("FamilyPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(2);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Monday");
                                                            price = price + Float.parseFloat(satObj.getString("FamilyPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(3);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Tuesday");
                                                            price = price + Float.parseFloat(satObj.getString("FamilyPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(4);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Wednesday");
                                                            price = price + Float.parseFloat(satObj.getString("FamilyPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(5);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Thursday");
                                                            price = price + Float.parseFloat(satObj.getString("FamilyPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(6);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Friday");
                                                            price = price + Float.parseFloat(satObj.getString("FamilyPrice"));
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                else {
                                                    try {
                                                        if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(0);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Saturday");
                                                            price = price + Float.parseFloat(satObj.getString("MenPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(1);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Sunday");
                                                            price = price + Float.parseFloat(satObj.getString("MenPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(2);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Monday");
                                                            price = price + Float.parseFloat(satObj.getString("MenPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(3);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Tuesday");
                                                            price = price + Float.parseFloat(satObj.getString("MenPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(4);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Wednesday");
                                                            price = price + Float.parseFloat(satObj.getString("MenPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(5);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Thursday");
                                                            price = price + Float.parseFloat(satObj.getString("MenPrice"));
                                                        } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                                            JSONObject jsonObject1 = priceArray.getJSONObject(6);
                                                            JSONObject satObj = jsonObject1.getJSONObject("Friday");
                                                            price = price + Float.parseFloat(satObj.getString("MenPrice"));
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                            else if(isWomanSelected){
                                                try {
                                                    if (selectedWeeks[a].equalsIgnoreCase("Saturday")) {
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(0);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Saturday");
                                                        price = price + Float.parseFloat(satObj.getString("WomanPrice"));
                                                    } else if (selectedWeeks[a].equalsIgnoreCase("Sunday")) {
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(1);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Sunday");
                                                        price = price + Float.parseFloat(satObj.getString("WomanPrice"));
                                                    } else if (selectedWeeks[a].equalsIgnoreCase("Monday")) {
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(2);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Monday");
                                                        price = price + Float.parseFloat(satObj.getString("WomanPrice"));
                                                    } else if (selectedWeeks[a].equalsIgnoreCase("Tuesday")) {
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(3);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Tuesday");
                                                        price = price + Float.parseFloat(satObj.getString("WomanPrice"));
                                                    } else if (selectedWeeks[a].equalsIgnoreCase("Wednesday")) {
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(4);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Wednesday");
                                                        price = price + Float.parseFloat(satObj.getString("WomanPrice"));
                                                    } else if (selectedWeeks[a].equalsIgnoreCase("Thursday")) {
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(5);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Thursday");
                                                        price = price + Float.parseFloat(satObj.getString("WomanPrice"));
                                                    } else if (selectedWeeks[a].equalsIgnoreCase("Friday")) {
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(6);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Friday");
                                                        price = price + Float.parseFloat(satObj.getString("WomanPrice"));
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }

                                        try {
                                            if(isKitchenSelected) {
                                                JSONObject jsonObject1 = priceArray.getJSONObject(0);
                                                JSONObject satObj = jsonObject1.getJSONObject("Saturday");
                                                price = price + Float.parseFloat(satObj.getString("KitchenPrice"));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
//                                            price = price + 100;
                                        }
                                    }
                                    else {
                                        if(isManSelected && isWomanSelected){
                                            for(int a = 0; a < selectedWeeks.length; a++){
//                                                JSONObject jsonObject1 = priceArray.getJSONObject(0);
//                                                JSONObject satObj = jsonObject1.getJSONObject("Saturday");
//                                                price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(numberofGuestsArray.get(mMenPos))) + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(numberofGuestsArray.get(mWomenPos)));

                                                try {
                                                    if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(0);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Saturday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen)));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(1);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Sunday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen)));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(2);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Monday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen)));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(3);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Tuesday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen)));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(4);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Wednesday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen)));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(5);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Thursday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen)));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(6);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Friday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen) + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen)));
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
//                                                    price = price + 1000;
                                                }
                                            }
                                        }
                                        else if (isManSelected){
                                            for(int a = 0; a < selectedWeeks.length; a++){
//                                                JSONObject jsonObject1 = priceArray.getJSONObject(0);
//                                                JSONObject satObj = jsonObject1.getJSONObject("Saturday");
//                                                price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(numberofGuestsArray.get(mMenPos)));

                                                try {
                                                    if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(0);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Saturday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(1);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Sunday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(2);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Monday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(3);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Tuesday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(4);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Wednesday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(5);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Thursday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(6);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Friday");
                                                        price = price + (Float.parseFloat(satObj.getString("MenPrice")) * Integer.parseInt(Constants.noOfMen));
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
//                                                    price = price + 1000;
                                                }
                                            }
                                        }
                                        else if(isWomanSelected){
                                            for(int a = 0; a < selectedWeeks.length; a++){
//                                                JSONObject jsonObject1 = priceArray.getJSONObject(0);
//                                                JSONObject satObj = jsonObject1.getJSONObject("Saturday");
//                                                price = price + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(numberofGuestsArray.get(mWomenPos)));

                                                try {
                                                    if(selectedWeeks[a].equalsIgnoreCase("Saturday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(0);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Saturday");
                                                        price = price + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Sunday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(1);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Sunday");
                                                        price = price + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Monday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(2);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Monday");
                                                        price = price + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Tuesday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(3);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Tuesday");
                                                        price = price + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Wednesday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(4);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Wednesday");
                                                        price = price + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Thursday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(5);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Thursday");
                                                        price = price + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen));
                                                    }
                                                    else if(selectedWeeks[a].equalsIgnoreCase("Friday")){
                                                        JSONObject jsonObject1 = priceArray.getJSONObject(6);
                                                        JSONObject satObj = jsonObject1.getJSONObject("Friday");
                                                        price = price + (Float.parseFloat(satObj.getString("WomanPrice")) * Integer.parseInt(Constants.noOfWomen));
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
//                                                    price = price + 1000;
                                                }
                                            }
                                        }
                                    }

                                    resortDetails.setActualPrice(priceFormat.format(price * rate));
                                    float discPrice;
                                    if(discount>0) {
                                        float actualPrice = price * rate;
                                        discPrice = actualPrice - (actualPrice * (discount/100));
                                        resortDetails.setDiscountedPrice(priceFormat.format(discPrice));
                                    }
                                    else{
                                        discPrice = price * rate;
                                        resortDetails.setDiscountedPrice(priceFormat.format(price * rate));
                                    }

                                    if(i==0){
                                        minimumPrice = (int)(discPrice);
                                        maximumPrice = (int)(discPrice);
                                    }
                                    else {
                                        int newprice = (int)(discPrice);
                                        if(newprice < minimumPrice){
                                            minimumPrice = newprice;
                                        }
                                        else if(newprice > maximumPrice){
                                            maximumPrice = newprice;
                                        }
                                    }
                                    destinationLatitude = Double.parseDouble(jsonObject.getString("Latitude"));
                                    destinationLongitude = Double.parseDouble(jsonObject.getString("Longitude"));

                                    Location me   = new Location("");
                                    Location dest = new Location("");

                                    me.setLatitude(currentLatitude);
                                    me.setLongitude(currentLongitude);

                                    dest.setLatitude(destinationLatitude);
                                    dest.setLongitude(destinationLongitude);

//                                    final DecimalFormat distanceFormat = new DecimalFormat("0");
                                    float dist = (me.distanceTo(dest))/1000;
//                                    new getTrafficTime().execute();
                                    resortDetails.setDistance(""+(int)Math.ceil(dist));

                                    JSONArray jsonArray4 = jsonObject.getJSONArray("Facilities");
                                    ArrayList<Facilities> resortFacilities = new ArrayList<>();
                                    for (int j = 0; j<jsonArray4.length(); j++){
                                        Facilities facilities = new Facilities();
                                        JSONObject jsonObject1 = jsonArray4.getJSONObject(j);

                                        facilities.setFacilityNameAr(jsonObject1.getString("FacilityName_Ar"));
                                        facilities.setFacilityName(jsonObject1.getString("FacilityName"));
                                        facilities.setFacilityImage(jsonObject1.getString("FacilityImage"));

                                        resortFacilities.add(facilities);
                                    }
                                    resortDetails.setResortFacilities(resortFacilities);

//                                    if(dist<=15) {
                                    resortDetailsArrayList.add(resortDetails);
//                                    }
                                }

                                Intent intent = new Intent(getContext(), SearchResultActivity.class);
                                intent.putExtra("array",resortDetailsArrayList);
                                intent.putExtra("min",minimumPrice);
                                intent.putExtra("max",maximumPrice);
                                intent.putExtra("resort",strResortType);
                                ActivityOptions options =
                                        ActivityOptions.makeCustomAnimation(getContext(), R.anim.enter_from_right , R.anim.exit_to_left);
                                startActivity(intent, options.toBundle());
                            }catch (JSONException je){
                                je.printStackTrace();
//                                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                                String appName, title, positive;
//                                if (language.equalsIgnoreCase("En")) {
//                                    appName = getResources().getString(R.string.app_name);
//                                    title = "No Results Found";
//                                    positive = getResources().getString(R.string.str_btn_ok);
//                                } else {
//                                    appName = getResources().getString(R.string.app_name_ar);
//                                    title = "لم يتم العثور على نتائج";
//                                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                                }
//                                iOSDialog.setTitle(appName);
//                                iOSDialog.setSubtitle(title);
//                                iOSDialog.setPositiveLabel(positive);
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                                showDialog("No Results Found", "لم يتم العثور على نتائج");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public void checkDayValidity(){
        long diff = mCheckInDate.getTime() - mTodayDate.getTime();
        long days = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))+1;
        isAfter3Days = days >= 3;
        if(days >= 1){
            isToday = false;
        }
    }
    public void CalculateNumOfDays(){
        long diff = mCheckOutDate.getTime() - mCheckInDate.getTime();
        NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS))+1;
        tvNumOfDays.setText(""+NumofDaysStr);
        checkDayValidity();
        getDates();
        getWeekDays();
    }

    public void getGPSCoordinates(){

        if(gps != null){
            if (gps.canGetLocation()) {

                currentLatitude = gps.getLatitude();
                currentLongitude = gps.getLongitude();

                try {
                    mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        currentLatitude = location.getLatitude();
                                        currentLongitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.i("TAG","currentLatitude "+currentLatitude);
                Log.i("TAG","currentLongitude "+currentLongitude);

            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(getContext());
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(getContext(), "Location permission denied, Unable to show nearby resorts", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getContext(), perm));
    }

    public void getDates(){
        strCheckInDate = tvCheckInDate.getText().toString();
        strCheckInMonth = tvCheckInMonth.getText().toString();
        strCheckInWeek = tvCheckInWeek.getText().toString();
        strCheckoutDate = tvCheckOutDate.getText().toString();
        strCheckoutMonth = tvCheckOutMonth.getText().toString();
        strCheckoutWeek = tvCheckOutWeek.getText().toString();
        strNumDays = tvNumOfDays.getText().toString();
//        strNumGuests = etguests.getText().toString();
//        Constants.noOfGuests = etguests.getText().toString();
        Constants.startDate = mCheckInDateStr;
        Constants.endDate = mCheckOutDateStr;
    }

    public void getWeekDays(){
        Calendar c1 = Calendar.getInstance();
        c1.setTime(mCheckInDate);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(mCheckOutDate);

        strWeeks = "";

        while (! c1.after(c2)) {
            if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ){
                if(strWeeks.equals("")){
                    strWeeks = "Saturday";
                    if(!checkingWeekFirstTime){
                        todayWeek = "Saturday";
                        checkingWeekFirstTime = true;
                    }
                }
                else{
                    strWeeks = strWeeks + ",Saturday";
                }
            }
            else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                if(strWeeks.equals("")){
                    strWeeks = "Sunday";
                    if(!checkingWeekFirstTime){
                        todayWeek = "Sunday";
                        checkingWeekFirstTime = true;
                    }
                }
                else{
                    strWeeks = strWeeks + ",Sunday";
                }
            }
            else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY){
                if(strWeeks.equals("")){
                    strWeeks = "Monday";
                    if(!checkingWeekFirstTime){
                        todayWeek = "Monday";
                        checkingWeekFirstTime = true;
                    }
                }
                else{
                    strWeeks = strWeeks + ",Monday";
                }
            }
            else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
                if(strWeeks.equals("")){
                    strWeeks = "Tuesday";
                    if(!checkingWeekFirstTime){
                        todayWeek = "Tuesday";
                        checkingWeekFirstTime = true;
                    }
                }
                else{
                    strWeeks = strWeeks + ",Tuesday";
                }
            }
            else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                if(strWeeks.equals("")){
                    strWeeks = "Wednesday";
                    if(!checkingWeekFirstTime){
                        todayWeek = "Wednesday";
                        checkingWeekFirstTime = true;
                    }
                }
                else{
                    strWeeks = strWeeks + ",Wednesday";
                }
            }
            else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                if(strWeeks.equals("")){
                    strWeeks = "Thursday";
                    if(!checkingWeekFirstTime){
                        todayWeek = "Thursday";
                        checkingWeekFirstTime = true;
                    }
                }
                else{
                    strWeeks = strWeeks + ",Thursday";
                }
            }
            else if(c1.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){
                if(strWeeks.equals("")){
                    strWeeks = "Friday";
                    if(!checkingWeekFirstTime){
                        todayWeek = "Friday";
                        checkingWeekFirstTime = true;
                    }
                }
                else{
                    strWeeks = strWeeks + ",Friday";
                }
            }

            c1.add(Calendar.DATE, 1);
        }
    }

    public class TrackOrder extends AsyncTask<String, Integer, String> {
        String  networkStatus;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONObject successObj = jo.getJSONObject("Success");
                            JSONObject newObj = successObj.getJSONObject("New");
                            final String resortName, foodId, bookingId;
                            if(language.equalsIgnoreCase("En")) {
                                resortName = newObj.getString("ResortName");
                            }
                            else{
                                resortName = newObj.getString("ResortName_Ar");
                            }
                            final String resortId = newObj.getString("ResortId");
                            String OrderStatus = newObj.getString("OrderStatus");
                            foodId = newObj.getString("BreakfastId");
                            bookingId = newObj.getString("BookingId");
                            int rating = Integer.parseInt(newObj.getString("Rating"));
//                            OrderStatus = "close";

                            if(OrderStatus.equalsIgnoreCase("close") && rating == 0){
                                Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                        R.anim.bottom_up);
                                rateLayout.startAnimation(bottomUp);
                                rateLayout.setVisibility(View.VISIBLE);
                                ratingBar.setRating(0);
                                if(language.equalsIgnoreCase("En")) {
                                    resortName1.setText("Rate " + resortName);
                                }
                                else{
                                    resortName1.setText("تقييم القاعة " + resortName);
                                }

                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                                    @Override
                                    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                                        if(b){
                                            rateLayout.setVisibility(View.GONE);
                                            Intent intent = new Intent(getActivity(), RatingActivity.class);
                                            intent.putExtra("resortName", resortName);
                                            intent.putExtra("resortId",resortId);
                                            intent.putExtra("foodId", foodId);
                                            intent.putExtra("BookingId", bookingId);
                                            intent.putExtra("rating", v);
                                            ActivityOptions options =
                                                    ActivityOptions.makeCustomAnimation(getContext(), R.anim.bottom_up , R.anim.bottom_down);
                                            startActivity(intent, options.toBundle());
                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            super.onPostExecute(result);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(userId == null){
            userId = "0";
        }
//        new TrackOrder().execute(Constants.TRACK_ORDER_URL+"-1&userid="+userId);
    }

    public void showDialog(String description, String description_ar){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if(language.equalsIgnoreCase("En")) {
            title.setText(getResources().getString(R.string.app_name));
            yes.setText(getResources().getString(R.string.str_btn_ok));
            desc.setText(description);
        }
        else{
            title.setText(getResources().getString(R.string.app_name_ar));
            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
            desc.setText(description_ar);
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
