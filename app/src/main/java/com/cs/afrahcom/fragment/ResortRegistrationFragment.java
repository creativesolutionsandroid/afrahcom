package com.cs.afrahcom.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.afrahcom.Constants;
import com.cs.afrahcom.JSONParser;
import com.cs.afrahcom.NetworkUtil;
import com.cs.afrahcom.R;
import com.cs.afrahcom.activity.MainActivity;
import com.cs.afrahcom.adapter.HostCategoryAdapter;
import com.cs.afrahcom.adapter.HostFacilitiesAdapter;
import com.cs.afrahcom.models.Cities;
import com.cs.afrahcom.models.ResortType;
import com.cs.afrahcom.widgets.CustomGridView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

import static android.app.Activity.RESULT_OK;

/**
 * Created by CS on 30-10-2017.
 */

public class ResortRegistrationFragment extends Fragment {

    public static ArrayList<String> selectedFacilities = new ArrayList<>();
    public static ArrayList<String> selectedFacilityIds = new ArrayList<>();
    public static ArrayList<String> selectedCategories = new ArrayList<>();
    public static String resortTypeID = "1";
    private String cityID = "1";
    int mCityPos = 0;
    ArrayList<String> facilitiesArray = new ArrayList<>();
    ArrayList<String> facilityIDArray = new ArrayList<>();
    ArrayList<ResortType> resortTypeArrayList = new ArrayList<>();
    ArrayList<Cities> citiesArrayList = new ArrayList<>();
    ArrayList<String> citiesSpinnerArray = new ArrayList<>();
    ArrayAdapter<String> cityAdapter;
    Spinner citySpinner;
    HostFacilitiesAdapter mHostFacilitiesAdapter;
    HostCategoryAdapter mHostCategoryAdapter;
    GridView mFacilityGrid;
    CustomGridView mCategoryGrid;
    EditText etResortName, getEtResortNameAr, etResortAddress, etResortDescription, etResortDescriptionAr;
    ImageView mResortImage1, mResortImage2, mResortImage3, mResortImage4, mResortImage5, mResortImage6, mResortImage7, mResortImage8;
    ImageView map, mImage1Delete, mImage2Delete, mImage3Delete, mImage4Delete, mImage5Delete, mImage6Delete, mImage7Delete, mImage8Delete ;
    String imageName1, imageName2, imageName3, imageName4, imageName5, imageName6, imageName7, imageName8, documentImageName = "";
    int imageSelected = 0;
    ImageView documentUpload, tncCheckBox;
    TextView documentName, tvSubmit, tvCityName;
    RelativeLayout tncLayout;
    TextView tncText;
    Boolean isTNCchecked = true;
    String cityName = "", strAddress = "";
    Bitmap thumbnail1 = null, thumbnail2 = null, thumbnail3 = null, thumbnail4 = null,
            thumbnail5 = null, thumbnail6 = null, thumbnail7 = null, thumbnail8 = null, documentthumbnail = null;
    String selectedPath1, selectedPath2, selectedPath3, selectedPath4,
            selectedPath5, selectedPath6, selectedPath7, selectedPath8, crPath;
    LinearLayout photosLayout3, photosLayout4;
    ImageView addMoreImages;
    AlertDialog customDialog;
    public static LinearLayout kitchenLayout;
    public static EditText sundayMen, mondayMen, tuesdayMen, wednesdayMen, thursdayMen, fridayMen, saturdayMen, kitchenPrice, AdvanceAmount;
    public static EditText sundayWomen, mondayWomen, tuesdayWomen, wednesdayWomen, thursdayWomen, fridayWomen, saturdayWomen;
    public static EditText sundayFamily, mondayFamily, tuesdayFamily, wednesdayFamily, thursdayFamily, fridayFamily, saturdayFamily;
    final JSONObject parent = new JSONObject();
    LinearLayout vatYesLayout, vatNoLayout;
    ImageView vatYesImage, vatNoImage;
    Boolean vatApplicable = true;
    EditText vatNumber, bankName, ibanNumber;

    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int STORAGE_REQUEST = 5;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int FILE_SELECT_CODE = 3;
    public static String minCapacity = "", maxCapacity = "";
    String imageResponse;
    private DefaultHttpClient mHttpClient11;
    private String original_imagePath1 = null;
    private String original_imagePath2 = null;
    private String original_imagePath3 = null;
    private String original_imagePath4 = null;
    private String original_imagePath5 = null;
    private String original_imagePath6 = null;
    private String original_imagePath7 = null;
    private String original_imagePath8 = null;
    private String crImagePath = null;
    SharedPreferences hostPrefs;
    SharedPreferences.Editor hostPrefsEditor;
    String hostId;
    Double resortLatitude, resortLongitude;
    ScrollView scrollView;
    SharedPreferences languagePrefs;
    public static String language;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.activity_insert_resort, container, false);
        }
        else{
            rootView = inflater.inflate(R.layout.activity_insert_resort_ar, container, false);
        }

        this.getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        hostPrefs = getContext().getSharedPreferences("HOST_PREFS", Context.MODE_PRIVATE);
        hostPrefsEditor = hostPrefs.edit();
        hostId = hostPrefs.getString("hostId", null);

        citySpinner = (Spinner) rootView.findViewById(R.id.city_spinner);
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);
        mCategoryGrid = (CustomGridView) rootView.findViewById(R.id.categoryGrid);
        mFacilityGrid = (GridView) rootView.findViewById(R.id.facilitiesGridView);

        etResortName = (EditText) rootView.findViewById(R.id.et_ResortName);
        getEtResortNameAr = (EditText) rootView.findViewById(R.id.et_ResortNameAr);
        etResortAddress = (EditText) rootView.findViewById(R.id.et_ResortAddress);
        etResortDescription = (EditText) rootView.findViewById(R.id.et_ResortDescsription);
        etResortDescriptionAr = (EditText) rootView.findViewById(R.id.et_ResortDescsriptionAr);

        documentName = (TextView) rootView.findViewById(R.id.documentName);
        tncText = (TextView) rootView.findViewById(R.id.TnCtext);
        tvSubmit = (TextView) rootView.findViewById(R.id.tv_submit);
        tvCityName = (TextView) rootView.findViewById(R.id.tv_city_name);

        map = (ImageView) rootView.findViewById(R.id.map);
        tncCheckBox = (ImageView) rootView.findViewById(R.id.checkboxTnC);
        documentUpload = (ImageView) rootView.findViewById(R.id.attachDocument);
        mResortImage1 = (ImageView) rootView.findViewById(R.id.resortImage1);
        mResortImage2 = (ImageView) rootView.findViewById(R.id.resortImage2);
        mResortImage3 = (ImageView) rootView.findViewById(R.id.resortImage3);
        mResortImage4 = (ImageView) rootView.findViewById(R.id.resortImage4);
        mResortImage5 = (ImageView) rootView.findViewById(R.id.resortImage5);
        mResortImage6 = (ImageView) rootView.findViewById(R.id.resortImage6);
        mResortImage7 = (ImageView) rootView.findViewById(R.id.resortImage7);
        mResortImage8 = (ImageView) rootView.findViewById(R.id.resortImage8);

        mImage1Delete = (ImageView) rootView.findViewById(R.id.image1Cancel);
        mImage2Delete = (ImageView) rootView.findViewById(R.id.image2Cancel);
        mImage3Delete = (ImageView) rootView.findViewById(R.id.image3Cancel);
        mImage4Delete = (ImageView) rootView.findViewById(R.id.image4Cancel);
        mImage5Delete = (ImageView) rootView.findViewById(R.id.image5Cancel);
        mImage6Delete = (ImageView) rootView.findViewById(R.id.image6Cancel);
        mImage7Delete = (ImageView) rootView.findViewById(R.id.image7Cancel);
        mImage8Delete = (ImageView) rootView.findViewById(R.id.image8Cancel);
        addMoreImages = (ImageView) rootView.findViewById(R.id.addmoreimages);

        sundayMen = (EditText) rootView.findViewById(R.id.sundayMenPrice);
        mondayMen = (EditText) rootView.findViewById(R.id.mondayMenPrice);
        tuesdayMen = (EditText) rootView.findViewById(R.id.tuesdayMenPrice);
        wednesdayMen = (EditText) rootView.findViewById(R.id.wednesdayMenPrice);
        thursdayMen = (EditText) rootView.findViewById(R.id.thursdayMenPrice);
        fridayMen = (EditText) rootView.findViewById(R.id.fridayMenPrice);
        saturdayMen = (EditText) rootView.findViewById(R.id.saturdayMenPrice);
        kitchenPrice = (EditText) rootView.findViewById(R.id.kitchenPrice);
        AdvanceAmount = (EditText) rootView.findViewById(R.id.AdvancePrice);

        photosLayout3 = (LinearLayout) rootView.findViewById(R.id.photos3);
        photosLayout4 = (LinearLayout) rootView.findViewById(R.id.photos4);
        kitchenLayout = (LinearLayout) rootView.findViewById(R.id.kitchenLayout);
        tncLayout = (RelativeLayout) rootView.findViewById(R.id.tncLayout);

        sundayWomen = (EditText) rootView.findViewById(R.id.sundayWomenPrice);
        mondayWomen = (EditText) rootView.findViewById(R.id.mondayWomenPrice);
        tuesdayWomen = (EditText) rootView.findViewById(R.id.tuesdayWomenPrice);
        wednesdayWomen = (EditText) rootView.findViewById(R.id.wednesdayWomenPrice);
        thursdayWomen = (EditText) rootView.findViewById(R.id.thursdayWomenPrice);
        fridayWomen = (EditText) rootView.findViewById(R.id.fridayWomenPrice);
        saturdayWomen = (EditText) rootView.findViewById(R.id.saturdayWomenPrice);

        sundayFamily = (EditText) rootView.findViewById(R.id.sundayFamilyPrice);
        mondayFamily = (EditText) rootView.findViewById(R.id.mondayFamilyPrice);
        tuesdayFamily = (EditText) rootView.findViewById(R.id.tuesdayFamilyPrice);
        wednesdayFamily = (EditText) rootView.findViewById(R.id.wednesdayFamilyPrice);
        thursdayFamily = (EditText) rootView.findViewById(R.id.thursdayFamiyPrice);
        fridayFamily = (EditText) rootView.findViewById(R.id.fridayFamilyPrice);
        saturdayFamily = (EditText) rootView.findViewById(R.id.saturdayFamilyPrice);

        vatNumber = (EditText) rootView.findViewById(R.id.vatNumber);
        bankName = (EditText) rootView.findViewById(R.id.bankName);
        ibanNumber = (EditText) rootView.findViewById(R.id.ibanNumber);

        vatYesLayout = (LinearLayout) rootView.findViewById(R.id.vatYes);
        vatNoLayout = (LinearLayout) rootView.findViewById(R.id.vatNo);

        vatYesImage = (ImageView) rootView.findViewById(R.id.vatYesImage);
        vatNoImage = (ImageView) rootView.findViewById(R.id.vatNoImage);

        selectedCategories.clear();
        selectedFacilities.clear();
        selectedFacilityIds.clear();
        if(language.equalsIgnoreCase("En")) {
            selectedCategories.add("Wedding Hall");
        }
        else{
            selectedCategories.add("قصور الافراح");
        }

        new GetResortsDetails().execute(Constants.GET_BANNERS_URL+"0&UserType=1");
        changePriceFields();

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAddress();
            }
        });

        cityAdapter = new ArrayAdapter<String>(getContext(), R.layout.list_spinner, citiesSpinnerArray) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(18);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.colorSignupText));

                return v;
            }
        };

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvCityName.setText(citiesSpinnerArray.get(i));
                mCityPos = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mFacilityGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(!selectedFacilities.contains(facilitiesArray.get(i))) {
                    selectedFacilities.add(facilitiesArray.get(i));
                    selectedFacilityIds.add(facilityIDArray.get(i));
                    mHostFacilitiesAdapter.notifyDataSetChanged();
                }
                else{
                    selectedFacilities.remove(facilitiesArray.get(i));
                    selectedFacilityIds.remove(facilityIDArray.get(i));
                    mHostFacilitiesAdapter.notifyDataSetChanged();
                }
            }
        });

        vatYesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vatYesImage.setImageResource(R.drawable.radio_on);
                vatNoImage.setImageResource(R.drawable.radio_off);
                vatApplicable = true;
            }
        });

        vatNoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vatNoImage.setImageResource(R.drawable.radio_on);
                vatYesImage.setImageResource(R.drawable.radio_off);
                vatApplicable = false;
            }
        });

        addMoreImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photosLayout3.setVisibility(View.VISIBLE);
                photosLayout4.setVisibility(View.VISIBLE);
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                addMoreImages.setVisibility(View.INVISIBLE);
            }
        });

        mResortImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 1;
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            }
        });

        mResortImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 2;

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            }
        });

        mResortImage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 3;

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            }
        });

        mResortImage4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 4;

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            }
        });

        mResortImage5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 5;
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            }
        });

        mResortImage6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 6;
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            }
        });

        mResortImage7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 7;
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            }
        });

        mResortImage8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 8;
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
            }
        });

        documentUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageSelected = 9;
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {

                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);

//                        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//                        i.setType("*/*");
//                        i.addCategory(Intent.CATEGORY_OPENABLE);
//                        i = Intent.createChooser(i, "Choose a file");
//
//                        startActivityForResult(i, FILE_SELECT_CODE);
                    }
                }else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);

                }

            }
        });

        tncLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isTNCchecked){
                    isTNCchecked = true;
                    tncCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                }
                else{
                    isTNCchecked = false;
                    tncCheckBox.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                }
            }
        });

        mImage1Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResortImage1.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                mImage1Delete.setVisibility(View.GONE);
                thumbnail1 = null;
            }
        });

        mImage2Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResortImage2.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                mImage2Delete.setVisibility(View.GONE);
                thumbnail2 = null;
            }
        });

        mImage3Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResortImage3.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                mImage3Delete.setVisibility(View.GONE);
                thumbnail3 = null;
            }
        });

        mImage4Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResortImage4.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                mImage4Delete.setVisibility(View.GONE);
                thumbnail4 = null;
            }
        });

        mImage5Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResortImage5.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                mImage5Delete.setVisibility(View.GONE);
                thumbnail5 = null;
            }
        });

        mImage6Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResortImage6.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                mImage6Delete.setVisibility(View.GONE);
                thumbnail6 = null;
            }
        });

        mImage7Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResortImage7.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                mImage7Delete.setVisibility(View.GONE);
                thumbnail7 = null;
            }
        });

        mImage8Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mResortImage8.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
                mImage8Delete.setVisibility(View.GONE);
                thumbnail8 = null;
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertData();
            }
        });

        etResortAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (strAddress.equals("")){
                    getAddress();
                }
            }
        });

//        etResortAddress.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                strAddress.equals("")
//            }
//        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode, Intent data) {
        switch(requestCode){
            case PICK_IMAGE_FROM_GALLERY:
                if(resultCode==RESULT_OK)
                {
                    Uri uri = data.getData();

                    try {
                        if(imageSelected == 1) {
                            thumbnail1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            selectedPath1 = getPath(uri);
                        }
                        else if(imageSelected == 2) {
                            thumbnail2 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            selectedPath2 = getPath(uri);
                        }
                        else if(imageSelected == 3) {
                            thumbnail3 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            selectedPath3 = getPath(uri);
                        }
                        else if(imageSelected == 4) {
                            thumbnail4 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            selectedPath4 = getPath(uri);
                        }
                        else if(imageSelected == 5) {
                            thumbnail5 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            selectedPath5 = getPath(uri);
                        }
                        else if(imageSelected == 6) {
                            thumbnail6 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            selectedPath6 = getPath(uri);
                        }
                        else if(imageSelected == 7) {
                            thumbnail7 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            selectedPath7 = getPath(uri);
                        }
                        else if(imageSelected == 8) {
                            thumbnail8 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            selectedPath8 = getPath(uri);
                        }
                        else if(imageSelected == 9) {
                            documentthumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            crPath = getPath(uri);
                            documentName.setText("("+getFileName(uri)+")");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    convertPixel();
                }
                break;

            case FILE_SELECT_CODE:

                break;

            case PLACE_PICKER_REQUEST:
                if(resultCode == RESULT_OK){
                    final Place place = PlacePicker.getPlace(getActivity(), data);

                    if ("".equals(place.getAddress())) {
//                Alert Dialog
                        strAddress = "";
//                        final iOSDialog iOSDialog = new iOSDialog(getContext());
//                        iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                        iOSDialog.setSubtitle("Sorry! we couldn\'t detect your location. Please place the pin on your exact location.");
//                        iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                        iOSDialog.setBoldPositiveLabel(false);
//                        iOSDialog.setPositiveListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                iOSDialog.dismiss();
//                            }
//                        });
//                        iOSDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.app_name));
                            yes.setText(getResources().getString(R.string.str_btn_ok));
                            desc.setText("Sorry! we couldn\'t detect your location. Please place the pin on your exact location.");
                        }
                        else{
                            title.setText(getResources().getString(R.string.app_name_ar));
                            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                            desc.setText("Sorry! we couldn\'t detect your location. Please place the pin on your exact location.");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getActivity().getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                    } else {
                        etResortAddress.setText(place.getAddress());
                        strAddress = place.getAddress().toString();

                        resortLatitude = place.getLatLng().latitude;
                        resortLongitude = place.getLatLng().longitude;

                        String Map_url = "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=130x100&markers=color:red%7C" +
                                resortLatitude + "," + resortLongitude;
                        Glide.with(getContext().getApplicationContext()).load(Map_url).into(map);

                        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                        try
                        {
                            List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude,place.getLatLng().longitude, 1);
//                            cityName = addresses.get(0).getAddressLine(1);
                            cityName = addresses.get(0).getLocality();

                            if(cityName == null){
                                cityName = addresses.get(0).getAddressLine(1);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.i("TAG", "cityName "+cityName);
                    }
                }
                break;
        }
    }

    private void convertPixel() {

        if(imageSelected == 1) {
            thumbnail1 = Bitmap.createScaledBitmap(thumbnail1, 700, 500,
                    false);
            thumbnail1 = codec(thumbnail1, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            mResortImage1.setImageDrawable(drawable);
            mImage1Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 2) {
            thumbnail2 = Bitmap.createScaledBitmap(thumbnail2, 700, 500,
                    false);
            thumbnail2 = codec(thumbnail2, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            mResortImage2.setImageDrawable(drawable);
            mImage2Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 3) {
            thumbnail3 = Bitmap.createScaledBitmap(thumbnail3, 700, 500,
                    false);
            thumbnail3 = codec(thumbnail3, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            mResortImage3.setImageDrawable(drawable);
            mImage3Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 4) {
            thumbnail4 = Bitmap.createScaledBitmap(thumbnail4, 700, 500,
                    false);
            thumbnail4 = codec(thumbnail4, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            mResortImage4.setImageDrawable(drawable);
            mImage4Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 5) {
            thumbnail5 = Bitmap.createScaledBitmap(thumbnail5, 700, 500,
                    false);
            thumbnail5 = codec(thumbnail5, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail5);
            mResortImage5.setImageDrawable(drawable);
            mImage5Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 6) {
            thumbnail6 = Bitmap.createScaledBitmap(thumbnail6, 700, 500,
                    false);
            thumbnail6 = codec(thumbnail6, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail6);
            mResortImage6.setImageDrawable(drawable);
            mImage6Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 7) {
            thumbnail7 = Bitmap.createScaledBitmap(thumbnail7, 700, 500,
                    false);
            thumbnail7 = codec(thumbnail7, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail7);
            mResortImage7.setImageDrawable(drawable);
            mImage7Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 8) {
            thumbnail8 = Bitmap.createScaledBitmap(thumbnail8, 700, 500,
                    false);
            thumbnail8 = codec(thumbnail8, Bitmap.CompressFormat.JPEG, 50);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail8);
            mResortImage8.setImageDrawable(drawable);
            mImage8Delete.setVisibility(View.VISIBLE);
        }
        else if(imageSelected == 9) {
            documentthumbnail = Bitmap.createScaledBitmap(documentthumbnail, 700, 500,
                    false);
            documentthumbnail = codec(documentthumbnail, Bitmap.CompressFormat.JPEG, 50);

        }
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void getAddress(){
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            Intent intent = intentBuilder.build(getActivity());
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public class GetResortsDetails extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        ACProgressFlower dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONObject jo= new JSONObject(result);
                            JSONObject ja = jo.getJSONObject("Success");

                            JSONArray jsonArray1 = ja.getJSONArray("ResortType");
                            for (int i = 0; i<jsonArray1.length(); i++){
                                ResortType resortType = new ResortType();

                                JSONObject jsonObject = jsonArray1.getJSONObject(i);
                                resortType.setResortTypeId(jsonObject.getString("ResortTypeId"));
                                if(language.equalsIgnoreCase("En")) {
                                    resortType.setResortTypeName(jsonObject.getString("ResortTypeName"));
                                }
                                else{
                                    resortType.setResortTypeName(jsonObject.getString("ResortTypeName_Ar"));
                                }
                                resortTypeArrayList.add(resortType);
                            }

                            JSONArray jsonArray4 = ja.getJSONArray("Facilities");
                            for (int i = 0; i<jsonArray4.length(); i++){
                                JSONObject jsonObject = jsonArray4.getJSONObject(i);
                                if(language.equalsIgnoreCase("En")) {
                                    facilitiesArray.add(jsonObject.getString("FacilityName"));
                                }
                                else{
                                    facilitiesArray.add(jsonObject.getString("FacilityName_Ar"));
                                }
                                facilityIDArray.add(jsonObject.getString("FacilityId"));
                            }

                            JSONArray jsonArray2 = ja.getJSONArray("Cities");
                            for (int i = 0; i<jsonArray2.length(); i++){
                                Cities cities = new Cities();

                                JSONObject jsonObject = jsonArray2.getJSONObject(i);
                                cities.setCityId(jsonObject.getString("CityId"));
                                cities.setCityName(jsonObject.getString("CityName"));
                                if(jsonObject.getString("Booking").equalsIgnoreCase("true")) {
                                    if (language.equalsIgnoreCase("En")) {
                                        citiesSpinnerArray.add(jsonObject.getString("CityName"));
                                    } else {
                                        citiesSpinnerArray.add(jsonObject.getString("CityName_Ar"));
                                    }
                                    citiesArrayList.add(cities);
                                }
                            }

                            mHostFacilitiesAdapter = new HostFacilitiesAdapter(getContext(), facilitiesArray, language);
                            mHostCategoryAdapter = new HostCategoryAdapter(getContext(), resortTypeArrayList, language);
                            mCategoryGrid.setAdapter(mHostCategoryAdapter);
                            mFacilityGrid.setAdapter(mHostFacilitiesAdapter);
                            citySpinner.setAdapter(cityAdapter);
                            tvCityName.setText(citiesSpinnerArray.get(0));

                            if(language.equalsIgnoreCase("Ar")){
                                mCategoryGrid.setRotationY(180);
                                mFacilityGrid.setRotationY(180);
                            }

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            getAddress();
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
                else {
                    Toast.makeText(getContext(), "Storage permission denied, Unable to select pictures", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public static void changePriceFields(){
        if(selectedCategories.contains("Banquet Hall") || selectedCategories.contains("قاعات الفنادق")){
            sundayMen.setText("");
            mondayMen.setText("");
            tuesdayMen.setText("");
            wednesdayMen.setText("");
            thursdayMen.setText("");
            fridayMen.setText("");
            saturdayMen.setText("");
            AdvanceAmount.setText("");

            sundayWomen.setText("");
            mondayWomen.setText("");
            tuesdayWomen.setText("");
            wednesdayWomen.setText("");
            thursdayWomen.setText("");
            fridayWomen.setText("");
            saturdayWomen.setText("");

            kitchenLayout.setVisibility(View.GONE);

            sundayWomen.setVisibility(View.VISIBLE);
            mondayWomen.setVisibility(View.VISIBLE);
            tuesdayWomen.setVisibility(View.VISIBLE);
            wednesdayWomen.setVisibility(View.VISIBLE);
            thursdayWomen.setVisibility(View.VISIBLE);
            fridayWomen.setVisibility(View.VISIBLE);
            saturdayWomen.setVisibility(View.VISIBLE);

            sundayFamily.setVisibility(View.INVISIBLE);
            mondayFamily.setVisibility(View.INVISIBLE);
            tuesdayFamily.setVisibility(View.INVISIBLE);
            wednesdayFamily.setVisibility(View.INVISIBLE);
            thursdayFamily.setVisibility(View.INVISIBLE);
            fridayFamily.setVisibility(View.INVISIBLE);
            saturdayFamily.setVisibility(View.INVISIBLE);

            if(language.equalsIgnoreCase("En")) {
                sundayMen.setHint(R.string.str_men_price_hint);
                mondayMen.setHint(R.string.str_men_price_hint);
                tuesdayMen.setHint(R.string.str_men_price_hint);
                wednesdayMen.setHint(R.string.str_men_price_hint);
                thursdayMen.setHint(R.string.str_men_price_hint);
                fridayMen.setHint(R.string.str_men_price_hint);
                saturdayMen.setHint(R.string.str_men_price_hint);

                sundayWomen.setHint(R.string.str_women_price_hint);
                mondayWomen.setHint(R.string.str_women_price_hint);
                tuesdayWomen.setHint(R.string.str_women_price_hint);
                wednesdayWomen.setHint(R.string.str_women_price_hint);
                thursdayWomen.setHint(R.string.str_women_price_hint);
                fridayWomen.setHint(R.string.str_women_price_hint);
                saturdayWomen.setHint(R.string.str_women_price_hint);
                AdvanceAmount.setHint(R.string.str_advance);
            }
            else{
                sundayMen.setHint(R.string.str_men_price_hint_ar);
                mondayMen.setHint(R.string.str_men_price_hint_ar);
                tuesdayMen.setHint(R.string.str_men_price_hint_ar);
                wednesdayMen.setHint(R.string.str_men_price_hint_ar);
                thursdayMen.setHint(R.string.str_men_price_hint_ar);
                fridayMen.setHint(R.string.str_men_price_hint_ar);
                saturdayMen.setHint(R.string.str_men_price_hint_ar);

                sundayWomen.setHint(R.string.str_women_price_hint_ar);
                mondayWomen.setHint(R.string.str_women_price_hint_ar);
                tuesdayWomen.setHint(R.string.str_women_price_hint_ar);
                wednesdayWomen.setHint(R.string.str_women_price_hint_ar);
                thursdayWomen.setHint(R.string.str_women_price_hint_ar);
                fridayWomen.setHint(R.string.str_women_price_hint_ar);
                saturdayWomen.setHint(R.string.str_women_price_hint_ar);
                AdvanceAmount.setHint(R.string.str_advance_ar);
            }
        }
        else if(selectedCategories.contains("Wedding Hall") || selectedCategories.contains("قصور الافراح")){
            sundayMen.setText("");
            mondayMen.setText("");
            tuesdayMen.setText("");
            wednesdayMen.setText("");
            thursdayMen.setText("");
            fridayMen.setText("");
            saturdayMen.setText("");
            kitchenPrice.setText("");
            AdvanceAmount.setText("");

            sundayWomen.setText("");
            mondayWomen.setText("");
            tuesdayWomen.setText("");
            wednesdayWomen.setText("");
            thursdayWomen.setText("");
            fridayWomen.setText("");
            saturdayWomen.setText("");

            sundayFamily.setText("");
            mondayFamily.setText("");
            tuesdayFamily.setText("");
            wednesdayFamily.setText("");
            thursdayFamily.setText("");
            fridayFamily.setText("");
            saturdayFamily.setText("");

            kitchenLayout.setVisibility(View.VISIBLE);

            sundayWomen.setVisibility(View.VISIBLE);
            mondayWomen.setVisibility(View.VISIBLE);
            tuesdayWomen.setVisibility(View.VISIBLE);
            wednesdayWomen.setVisibility(View.VISIBLE);
            thursdayWomen.setVisibility(View.VISIBLE);
            fridayWomen.setVisibility(View.VISIBLE);
            saturdayWomen.setVisibility(View.VISIBLE);

            sundayFamily.setVisibility(View.VISIBLE);
            mondayFamily.setVisibility(View.VISIBLE);
            tuesdayFamily.setVisibility(View.VISIBLE);
            wednesdayFamily.setVisibility(View.VISIBLE);
            thursdayFamily.setVisibility(View.VISIBLE);
            fridayFamily.setVisibility(View.VISIBLE);
            saturdayFamily.setVisibility(View.VISIBLE);

            if(language.equalsIgnoreCase("En")) {
                sundayMen.setHint(R.string.str_man_price_hint);
                mondayMen.setHint(R.string.str_man_price_hint);
                tuesdayMen.setHint(R.string.str_man_price_hint);
                wednesdayMen.setHint(R.string.str_man_price_hint);
                thursdayMen.setHint(R.string.str_man_price_hint);
                fridayMen.setHint(R.string.str_man_price_hint);
                saturdayMen.setHint(R.string.str_man_price_hint);
                AdvanceAmount.setHint(R.string.str_advance);

                sundayWomen.setHint(R.string.str_woman_price_hint);
                mondayWomen.setHint(R.string.str_woman_price_hint);
                tuesdayWomen.setHint(R.string.str_woman_price_hint);
                wednesdayWomen.setHint(R.string.str_woman_price_hint);
                thursdayWomen.setHint(R.string.str_woman_price_hint);
                fridayWomen.setHint(R.string.str_woman_price_hint);
                saturdayWomen.setHint(R.string.str_woman_price_hint);

                sundayFamily.setHint(R.string.str_family_price_hint);
                mondayFamily.setHint(R.string.str_family_price_hint);
                tuesdayFamily.setHint(R.string.str_family_price_hint);
                wednesdayFamily.setHint(R.string.str_family_price_hint);
                thursdayFamily.setHint(R.string.str_family_price_hint);
                fridayFamily.setHint(R.string.str_family_price_hint);
                saturdayFamily.setHint(R.string.str_family_price_hint);
            }
            else{
                sundayMen.setHint(R.string.str_man_price_hint_ar);
                mondayMen.setHint(R.string.str_man_price_hint_ar);
                tuesdayMen.setHint(R.string.str_man_price_hint_ar);
                wednesdayMen.setHint(R.string.str_man_price_hint_ar);
                thursdayMen.setHint(R.string.str_man_price_hint_ar);
                fridayMen.setHint(R.string.str_man_price_hint_ar);
                saturdayMen.setHint(R.string.str_man_price_hint_ar);

                sundayWomen.setHint(R.string.str_woman_price_hint_ar);
                mondayWomen.setHint(R.string.str_woman_price_hint_ar);
                tuesdayWomen.setHint(R.string.str_woman_price_hint_ar);
                wednesdayWomen.setHint(R.string.str_woman_price_hint_ar);
                thursdayWomen.setHint(R.string.str_woman_price_hint_ar);
                fridayWomen.setHint(R.string.str_woman_price_hint_ar);
                saturdayWomen.setHint(R.string.str_woman_price_hint_ar);

                sundayFamily.setHint(R.string.str_family_price_hint_ar);
                mondayFamily.setHint(R.string.str_family_price_hint_ar);
                tuesdayFamily.setHint(R.string.str_family_price_hint_ar);
                wednesdayFamily.setHint(R.string.str_family_price_hint_ar);
                thursdayFamily.setHint(R.string.str_family_price_hint_ar);
                fridayFamily.setHint(R.string.str_family_price_hint_ar);
                saturdayFamily.setHint(R.string.str_family_price_hint_ar);
                AdvanceAmount.setHint(R.string.str_advance_ar);
            }
        }
        else if(selectedCategories.contains("Resort") || selectedCategories.contains("الشاليهات")){
            sundayMen.setText("");
            mondayMen.setText("");
            tuesdayMen.setText("");
            wednesdayMen.setText("");
            thursdayMen.setText("");
            fridayMen.setText("");
            saturdayMen.setText("");
            kitchenPrice.setText("");
            AdvanceAmount.setText("");

            kitchenLayout.setVisibility(View.GONE);

            sundayWomen.setVisibility(View.INVISIBLE);
            mondayWomen.setVisibility(View.INVISIBLE);
            tuesdayWomen.setVisibility(View.INVISIBLE);
            wednesdayWomen.setVisibility(View.INVISIBLE);
            thursdayWomen.setVisibility(View.INVISIBLE);
            fridayWomen.setVisibility(View.INVISIBLE);
            saturdayWomen.setVisibility(View.INVISIBLE);

            sundayFamily.setVisibility(View.INVISIBLE);
            mondayFamily.setVisibility(View.INVISIBLE);
            tuesdayFamily.setVisibility(View.INVISIBLE);
            wednesdayFamily.setVisibility(View.INVISIBLE);
            thursdayFamily.setVisibility(View.INVISIBLE);
            fridayFamily.setVisibility(View.INVISIBLE);
            saturdayFamily.setVisibility(View.INVISIBLE);

            if(language.equalsIgnoreCase("En")) {
                sundayMen.setHint(R.string.str_price_hint);
                mondayMen.setHint(R.string.str_price_hint);
                tuesdayMen.setHint(R.string.str_price_hint);
                wednesdayMen.setHint(R.string.str_price_hint);
                thursdayMen.setHint(R.string.str_price_hint);
                fridayMen.setHint(R.string.str_price_hint);
                saturdayMen.setHint(R.string.str_price_hint);
                AdvanceAmount.setHint(R.string.str_advance);
            }
            else{
                sundayMen.setHint(R.string.str_price_hint_ar);
                mondayMen.setHint(R.string.str_price_hint_ar);
                tuesdayMen.setHint(R.string.str_price_hint_ar);
                wednesdayMen.setHint(R.string.str_price_hint_ar);
                thursdayMen.setHint(R.string.str_price_hint_ar);
                fridayMen.setHint(R.string.str_price_hint_ar);
                saturdayMen.setHint(R.string.str_price_hint_ar);
                AdvanceAmount.setHint(R.string.str_advance_ar);
            }
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void insertData(){
//        resortLatitude = 24.716896;
//        resortLongitude = 46.72761;
        if(selectedCategories.size()>0){
        if(selectedCategories.contains("Banquet Hall") || selectedCategories.contains("قاعات الفنادق")) {
            if (selectedCategories.size() == 0) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                final iOSDialog iOSDialog = new iOSDialog(getActivity());
//                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                iOSDialog.setSubtitle("Please select Category");
//                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select Category", "Please select Category");
            } else if (strAddress.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select Address";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحديد العنوان";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select Address", "يرجى تحديد العنوان");
            } else if (minCapacity.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Minimum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إدخال الحد الأدنى من السعة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Minimum Capacity", "يرجى إدخال الحد الأدنى من السعة");
            } else if (maxCapacity.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Maximum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "الرجاء إدخال السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Maximum Capacity", "الرجاء إدخال السعة القصوى");
            }
            else if (Integer.parseInt(minCapacity)>Integer.parseInt(maxCapacity)) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Minimum capacity should be less than maximum capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يجب أن يكون الحد الأدنى أقل من السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Minimum capacity should be less than maximum capacity", "يجب أن يكون الحد الأدنى أقل من السعة القصوى");
            }
            else if (selectedFacilities.size() == 0) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select facilities";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجي اختيار الخدمات";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter facilities", "يرجي اختيار الخدمات");
            } else if (thumbnail1 == null && thumbnail2 == null && thumbnail3 == null && thumbnail4 == null) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please upload atleast one photo";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحميل صورة واحدة على الأقل";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please upload atleast one photo", "يرجى تحميل صورة واحدة على الأقل");
            }
//            else if (documentthumbnail == null) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please attach Commercial Registration";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إرفاق السجل التجاري";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
//            }
            else if (!isTNCchecked) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please review and accept the terms of service";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "من فضلك راجع وأقبل شروط الخدمة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please review and accept the terms of service", "من فضلك راجع وأقبل شروط الخدمة");
            } else if (etResortName.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortName.setError("Please enter property Name");
                }
                else{
                    etResortName.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (getEtResortNameAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    getEtResortNameAr.setError("Please enter property Name");
                }
                else{
                    getEtResortNameAr.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
            else if (AdvanceAmount.getText().toString().length() == 0 || Integer.parseInt(AdvanceAmount.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    AdvanceAmount.setError("Please enter Advance Amount");
                }
                else{
                    AdvanceAmount.setError("الرجاء إدخال السعر");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }else if (etResortDescription.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescription.setError("Please enter property Description");
                }
                else{
                    etResortDescription.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (etResortDescriptionAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescriptionAr.setError("Please enter property Description");
                }
                else{
                    etResortDescriptionAr.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
            else if (saturdayMen.getText().toString().length() == 0 || Integer.parseInt(saturdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayMen.setError("Please enter Price");
                }
                else{
                    saturdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayMen.getText().toString().length() == 0 || Integer.parseInt(sundayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayMen.setError("Please enter Price");
                }
                else{
                    sundayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayMen.getBottom());
            } else if (mondayMen.getText().toString().length() == 0  || Integer.parseInt(mondayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayMen.setError("Please enter Price");
                }
                else{
                    mondayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayMen.getBottom());
            } else if (tuesdayMen.getText().toString().length() == 0  || Integer.parseInt(tuesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayMen.setError("Please enter Price");
                }
                else{
                    tuesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayMen.getBottom());
            } else if (wednesdayMen.getText().toString().length() == 0  || Integer.parseInt(wednesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayMen.setError("Please enter Price");
                }
                else{
                    wednesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayMen.getBottom());
            } else if (thursdayMen.getText().toString().length() == 0  || Integer.parseInt(thursdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayMen.setError("Please enter Price");
                }
                else{
                    thursdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayMen.getBottom());
            } else if (fridayMen.getText().toString().length() == 0  || Integer.parseInt(fridayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayMen.setError("Please enter Price");
                }
                else{
                    fridayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayMen.getBottom());
            } else if (saturdayWomen.getText().toString().length() == 0  || Integer.parseInt(saturdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayWomen.setError("Please enter Price");
                }
                else{
                    saturdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayWomen.getText().toString().length() == 0  || Integer.parseInt(sundayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayWomen.setError("Please enter Price");
                }
                else{
                    sundayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayWomen.getBottom());
            } else if (mondayWomen.getText().toString().length() == 0 || Integer.parseInt(mondayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayWomen.setError("Please enter Price");
                }
                else{
                    mondayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayWomen.getBottom());
            } else if (tuesdayWomen.getText().toString().length() == 0 || Integer.parseInt(tuesdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayWomen.setError("Please enter Price");
                }
                else{
                    tuesdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayWomen.getBottom());
            } else if (wednesdayWomen.getText().toString().length() == 0 || Integer.parseInt(wednesdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayWomen.setError("Please enter Price");
                }
                else{
                    wednesdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayWomen.getBottom());
            } else if (thursdayWomen.getText().toString().length() == 0 || Integer.parseInt(thursdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayWomen.setError("Please enter Price");
                }
                else{
                    thursdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayWomen.getBottom());
            } else if (fridayWomen.getText().toString().length() == 0 || Integer.parseInt(fridayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayWomen.setError("Please enter Price");
                }
                else{
                    fridayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayWomen.getBottom());
            } else if(vatApplicable && vatNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    vatNumber.setError("Please enter vat number");
                }
                else{
                    vatNumber.setError("الرجاء إدخال رقم ضريبة القيمة المضافة");
                }
            } else if(vatApplicable && bankName.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    bankName.setError("Please enter bank name");
                }
                else{
                    bankName.setError("الرجاء إدخال إسم البنك");
                }
            } else if(vatApplicable && ibanNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    ibanNumber.setError("Please enter iban number");
                }
                else{
                    ibanNumber.setError("الرجاء إدخال رقم الآيبان");
                }
            }
            else {
                new Dobackground().execute();
            }
            }
            else if(selectedCategories.contains("Wedding Hall") || selectedCategories.contains("قصور الافراح")) {
            if (selectedCategories.size() == 0) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                final iOSDialog iOSDialog = new iOSDialog(getActivity());
//                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                iOSDialog.setSubtitle("Please select Category");
//                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select Category", "Please select Category");
            } else if (strAddress.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select Address";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحديد العنوان";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select Address", "يرجى تحديد العنوان");
            } else if (minCapacity.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Minimum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إدخال الحد الأدنى من السعة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Minimum Capacity", "يرجى إدخال الحد الأدنى من السعة");
            } else if (maxCapacity.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Maximum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "الرجاء إدخال السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Maximum Capacity", "الرجاء إدخال السعة القصوى");
            }
            else if (Integer.parseInt(minCapacity)>Integer.parseInt(maxCapacity)) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Minimum capacity should be less than maximum capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يجب أن يكون الحد الأدنى أقل من السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Minimum capacity should be less than maximum capacity", "يجب أن يكون الحد الأدنى أقل من السعة القصوى");
            }
            else if (selectedFacilities.size() == 0) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select facilities";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجي اختيار الخدمات";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter facilities", "يرجي اختيار الخدمات");
            } else if (thumbnail1 == null && thumbnail2 == null && thumbnail3 == null && thumbnail4 == null) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please upload atleast one photo";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحميل صورة واحدة على الأقل";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please upload atleast one photo", "يرجى تحميل صورة واحدة على الأقل");
            }
//            else if (documentthumbnail == null) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please attach Commercial Registration";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إرفاق السجل التجاري";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
//            }
            else if (!isTNCchecked) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please review and accept the terms of service";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "من فضلك راجع وأقبل شروط الخدمة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please review and accept the terms of service", "من فضلك راجع وأقبل شروط الخدمة");
            } else if (etResortName.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortName.setError("Please enter property Name");
                }
                else{
                    etResortName.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (getEtResortNameAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    getEtResortNameAr.setError("Please enter property Name");
                }
                else{
                    getEtResortNameAr.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
            else if (AdvanceAmount.getText().toString().length() == 0 || Integer.parseInt(AdvanceAmount.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    AdvanceAmount.setError("Please enter Advance Amount");
                }
                else{
                    AdvanceAmount.setError("الرجاء إدخال السعر");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }else if (etResortDescription.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescription.setError("Please enter property Description");
                }
                else{
                    etResortDescription.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (etResortDescriptionAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescriptionAr.setError("Please enter property Description");
                }
                else{
                    etResortDescriptionAr.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (saturdayMen.getText().toString().length() == 0 || Integer.parseInt(saturdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayMen.setError("Please enter Price");
                }
                else{
                    saturdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayMen.getText().toString().length() == 0 || Integer.parseInt(sundayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayMen.setError("Please enter Price");
                }
                else{
                    sundayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayMen.getBottom());
            } else if (mondayMen.getText().toString().length() == 0 || Integer.parseInt(mondayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayMen.setError("Please enter Price");
                }
                else{
                    mondayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayMen.getBottom());
            } else if (tuesdayMen.getText().toString().length() == 0 || Integer.parseInt(tuesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayMen.setError("Please enter Price");
                }
                else{
                    tuesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayMen.getBottom());
            } else if (wednesdayMen.getText().toString().length() == 0 || Integer.parseInt(wednesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayMen.setError("Please enter Price");
                }
                else{
                    wednesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayMen.getBottom());
            } else if (thursdayMen.getText().toString().length() == 0 || Integer.parseInt(thursdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayMen.setError("Please enter Price");
                }
                else{
                    thursdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayMen.getBottom());
            } else if (fridayMen.getText().toString().length() == 0 || Integer.parseInt(fridayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayMen.setError("Please enter Price");
                }
                else{
                    fridayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayMen.getBottom());
            } else if (kitchenPrice.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    kitchenPrice.setError("Please enter Price");
                }
                else{
                    kitchenPrice.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, kitchenPrice.getBottom());
            } else if (saturdayWomen.getText().toString().length() == 0 || Integer.parseInt(saturdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayWomen.setError("Please enter Price");
                }
                else{
                    saturdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayWomen.getText().toString().length() == 0 || Integer.parseInt(sundayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayWomen.setError("Please enter Price");
                }
                else{
                    sundayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayWomen.getBottom());
            } else if (mondayWomen.getText().toString().length() == 0 || Integer.parseInt(mondayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayWomen.setError("Please enter Price");
                }
                else{
                    mondayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayWomen.getBottom());
            } else if (tuesdayWomen.getText().toString().length() == 0 || Integer.parseInt(tuesdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayWomen.setError("Please enter Price");
                }
                else{
                    tuesdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayWomen.getBottom());
            } else if (wednesdayWomen.getText().toString().length() == 0 || Integer.parseInt(wednesdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayWomen.setError("Please enter Price");
                }
                else{
                    wednesdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayWomen.getBottom());
            } else if (thursdayWomen.getText().toString().length() == 0 || Integer.parseInt(thursdayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayWomen.setError("Please enter Price");
                }
                else{
                    thursdayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayWomen.getBottom());
            } else if (fridayWomen.getText().toString().length() == 0 || Integer.parseInt(fridayWomen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayWomen.setError("Please enter Price");
                }
                else{
                    fridayWomen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayWomen.getBottom());
            }
            else if (saturdayFamily.getText().toString().length() == 0 || Integer.parseInt(saturdayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayFamily.setError("Please enter Price");
                }
                else{
                    saturdayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayFamily.getText().toString().length() == 0 || Integer.parseInt(sundayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayFamily.setError("Please enter Price");
                }
                else{
                    sundayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayWomen.getBottom());
            } else if (mondayFamily.getText().toString().length() == 0 || Integer.parseInt(mondayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayFamily.setError("Please enter Price");
                }
                else{
                    mondayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayWomen.getBottom());
            } else if (tuesdayFamily.getText().toString().length() == 0 || Integer.parseInt(tuesdayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayFamily.setError("Please enter Price");
                }
                else{
                    tuesdayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayWomen.getBottom());
            } else if (wednesdayFamily.getText().toString().length() == 0 || Integer.parseInt(wednesdayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayFamily.setError("Please enter Price");
                }
                else{
                    wednesdayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayWomen.getBottom());
            } else if (thursdayFamily.getText().toString().length() == 0 || Integer.parseInt(thursdayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayFamily.setError("Please enter Price");
                }
                else{
                    thursdayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayWomen.getBottom());
            } else if (fridayFamily.getText().toString().length() == 0 || Integer.parseInt(fridayFamily.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayFamily.setError("Please enter Price");
                }
                else{
                    fridayFamily.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayWomen.getBottom());
            }
            else if(vatApplicable && vatNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    vatNumber.setError("Please enter vat number");
                }
                else{
                    vatNumber.setError("الرجاء إدخال رقم ضريبة القيمة المضافة");
                }
            } else if(vatApplicable && bankName.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    bankName.setError("Please enter bank name");
                }
                else{
                    bankName.setError("الرجاء إدخال إسم البنك");
                }
            } else if(vatApplicable && ibanNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    ibanNumber.setError("Please enter iban number");
                }
                else{
                    ibanNumber.setError("الرجاء إدخال رقم الآيبان");
                }
            }
            else {
                new Dobackground().execute();
            }
        }
        else if(selectedCategories.contains("Resort") || selectedCategories.contains("الشاليهات")) {
            if (selectedCategories.size() == 0) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                final iOSDialog iOSDialog = new iOSDialog(getActivity());
//                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                iOSDialog.setSubtitle("Please select Category");
//                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select Category", "Please select Category");
            } else if (strAddress.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select Address";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحديد العنوان";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please select Address", "يرجى تحديد العنوان");
            } else if (minCapacity.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Minimum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إدخال الحد الأدنى من السعة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Minimum Capacity", "يرجى إدخال الحد الأدنى من السعة");
            } else if (maxCapacity.equals("")) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please enter Maximum Capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "الرجاء إدخال السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter Maximum Capacity", "الرجاء إدخال السعة القصوى");
            }
            else if (Integer.parseInt(minCapacity)>Integer.parseInt(maxCapacity)) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Minimum capacity should be less than maximum capacity";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يجب أن يكون الحد الأدنى أقل من السعة القصوى";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Minimum capacity should be less than maximum capacity", "يجب أن يكون الحد الأدنى أقل من السعة القصوى");
            }
            else if (selectedFacilities.size() == 0) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please select facilities";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجي اختيار الخدمات";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please enter facilities", "يرجي اختيار الخدمات");
            } else if (thumbnail1 == null && thumbnail2 == null && thumbnail3 == null && thumbnail4 == null) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please upload atleast one photo";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى تحميل صورة واحدة على الأقل";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please upload atleast one photo", "يرجى تحميل صورة واحدة على الأقل");
            }
//            else if (documentthumbnail == null) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please attach Commercial Registration";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "يرجى إرفاق السجل التجاري";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
//            }
            else if (!isTNCchecked) {
//                String appName, title, positive;
//                if(language.equalsIgnoreCase("En")){
//                    appName = getResources().getString(R.string.app_name);
//                    title = "Please review and accept the terms of service";
//                    positive = getResources().getString(R.string.str_btn_ok);
//                }
//                else{
//                    appName = getResources().getString(R.string.app_name_ar);
//                    title = "من فضلك راجع وأقبل شروط الخدمة";
//                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                }
//                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                iOSDialog.setTitle(appName);
//                iOSDialog.setSubtitle(title);
//                iOSDialog.setPositiveLabel(positive);
//                iOSDialog.setBoldPositiveLabel(false);
//                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        iOSDialog.dismiss();
//                    }
//                });
//                iOSDialog.show();
                showDialog("Please review and accept the terms of service", "من فضلك راجع وأقبل شروط الخدمة");
            } else if (etResortName.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortName.setError("Please enter property Name");
                }
                else{
                    etResortName.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (getEtResortNameAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    getEtResortNameAr.setError("Please enter property Name");
                }
                else{
                    getEtResortNameAr.setError("أدخل اسم القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (AdvanceAmount.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    AdvanceAmount.setError("Please enter Advance Amount");
                }
                else{
                    AdvanceAmount.setError("الرجاء إدخال السعر");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (etResortDescription.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescription.setError("Please enter property Description");
                }
                else{
                    etResortDescription.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (etResortDescriptionAr.getText().toString().length() == 0) {
                if(language.equalsIgnoreCase("En")) {
                    etResortDescriptionAr.setError("Please enter property Description");
                }
                else{
                    etResortDescriptionAr.setError("أدخل تفاصيل القاعة");
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            } else if (saturdayMen.getText().toString().length() == 0 || Integer.parseInt(saturdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    saturdayMen.setError("Please enter Price");
                }
                else{
                    saturdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, saturdayMen.getBottom());
            } else if (sundayMen.getText().toString().length() == 0 || Integer.parseInt(sundayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    sundayMen.setError("Please enter Price");
                }
                else{
                    sundayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, sundayMen.getBottom());
            } else if (mondayMen.getText().toString().length() == 0 || Integer.parseInt(mondayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    mondayMen.setError("Please enter Price");
                }
                else{
                    mondayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, mondayMen.getBottom());
            } else if (tuesdayMen.getText().toString().length() == 0 || Integer.parseInt(tuesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    tuesdayMen.setError("Please enter Price");
                }
                else{
                    tuesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, tuesdayMen.getBottom());
            } else if (wednesdayMen.getText().toString().length() == 0 || Integer.parseInt(wednesdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    wednesdayMen.setError("Please enter Price");
                }
                else{
                    wednesdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, wednesdayMen.getBottom());
            } else if (thursdayMen.getText().toString().length() == 0 || Integer.parseInt(thursdayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    thursdayMen.setError("Please enter Price");
                }
                else{
                    thursdayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, thursdayMen.getBottom());
            } else if (fridayMen.getText().toString().length() == 0 || Integer.parseInt(fridayMen.getText().toString()) == 0) {
                if(language.equalsIgnoreCase("En")) {
                    fridayMen.setError("Please enter Price");
                }
                else{
                    fridayMen.setError("الرجاء إدخال السعر");
                }
                scrollView.smoothScrollTo(0, fridayMen.getBottom());
            } else if(vatApplicable && vatNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    vatNumber.setError("Please enter vat number");
                }
                else{
                    vatNumber.setError("الرجاء إدخال رقم ضريبة القيمة المضافة");
                }
            } else if(vatApplicable && bankName.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    bankName.setError("Please enter bank name");
                }
                else{
                    bankName.setError("الرجاء إدخال إسم البنك");
                }
            } else if(vatApplicable && ibanNumber.getText().toString().length() == 0){
                if(language.equalsIgnoreCase("En")) {
                    ibanNumber.setError("Please enter iban number");
                }
                else{
                    ibanNumber.setError("الرجاء إدخال رقم الآيبان");
                }
            } else {
                new Dobackground().execute();
            }
        }
        }
        else {
//            final iOSDialog iOSDialog = new iOSDialog(getActivity());
//            iOSDialog.setTitle(getResources().getString(R.string.app_name));
//            iOSDialog.setSubtitle("Please select Category");
//            iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//            iOSDialog.setBoldPositiveLabel(false);
//            iOSDialog.setPositiveListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    iOSDialog.dismiss();
//                }
//            });
//            iOSDialog.show();
            showDialog("Please select Category", "Please select Category");
        }
    }

    class Dobackground extends AsyncTask<Void, Void, Void> {
        ACProgressFlower dialog;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params1) {
            // TODO Auto-generated method stub
//            submit();

            File file1=null;
            File file2=null;
            File file3=null;
            File file4=null;
            File file5=null;
            File file6=null;
            File file7=null;
            File file8=null;
            File crFile=null;
            HttpResponse httpResponse = null;

            if(thumbnail1!=null){
                original_imagePath1=StoreByteImage(thumbnail1, false);
            }
            if(thumbnail2!=null){
                original_imagePath2=StoreByteImage(thumbnail2, false);
            }
            if(thumbnail3!=null){
                original_imagePath3=StoreByteImage(thumbnail3, false);
            }
            if(thumbnail4!=null){
                original_imagePath4=StoreByteImage(thumbnail4, false);
            }
            if(thumbnail5!=null){
                original_imagePath5=StoreByteImage(thumbnail5, false);
            }
            if(thumbnail6!=null){
                original_imagePath6=StoreByteImage(thumbnail6, false);
            }
            if(thumbnail7!=null){
                original_imagePath7=StoreByteImage(thumbnail7, false);
            }
            if(thumbnail8!=null){
                original_imagePath8=StoreByteImage(thumbnail8, false);
            }
            if(documentthumbnail!=null){
                crImagePath=StoreByteImage(documentthumbnail, true);
            }

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            mHttpClient11 = new DefaultHttpClient(params);

            try{

                HttpPost httppost = new HttpPost(Constants.UPLOAD_IMAGE_URL);

                if(original_imagePath1!=null){
                    file1 = new File(original_imagePath1);
                }
                if(original_imagePath2!=null){
                    file2 = new File(original_imagePath2);
                }
                if(original_imagePath3!=null){
                    file3 = new File(original_imagePath3);
                }
                if(original_imagePath4!=null){
                    file4 = new File(original_imagePath4);
                }
                if(original_imagePath5!=null){
                    file5 = new File(original_imagePath5);
                }
                if(original_imagePath6!=null){
                    file6 = new File(original_imagePath6);
                }
                if(original_imagePath7!=null){
                    file7 = new File(original_imagePath7);
                }
                if(original_imagePath8!=null){
                    file8 = new File(original_imagePath8);
                }
//                if(crImagePath!=null){
//                    crFile = new File(crImagePath);
//                }


                MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                if(null!=file1&& file1.exists()){
                    multipartEntity.addPart("image1", new FileBody(file1));
//                    imageName1 = file1.getName();
                }
                if(null!=file2&& file2.exists()){
                    multipartEntity.addPart("image2", new FileBody(file2));
//                    imageName2 = file2.getName();
                }
                if(null!=file3&& file3.exists()){
                    multipartEntity.addPart("image3", new FileBody(file3));
//                    imageName3 = file3.getName();
                }
                if(null!=file4&& file4.exists()){
                    multipartEntity.addPart("image4", new FileBody(file4));
//                    imageName4 = file4.getName();
                }
                if(null!=file5&& file5.exists()){
                    multipartEntity.addPart("image5", new FileBody(file5));
//                    imageName4 = file4.getName();
                }
                if(null!=file6&& file6.exists()){
                    multipartEntity.addPart("image6", new FileBody(file6));
//                    imageName4 = file4.getName();
                }
                if(null!=file7&& file7.exists()){
                    multipartEntity.addPart("image7", new FileBody(file7));
//                    imageName4 = file4.getName();
                }
                if(null!=file8&& file8.exists()){
                    multipartEntity.addPart("image8", new FileBody(file8));
//                    imageName4 = file4.getName();
                }
//                if(null!=crFile&& crFile.exists()){
//                    multipartEntity.addPart("image9", new FileBody(crFile));
////                    documentImageName = file5.getName();
//                }

                httppost.setEntity(multipartEntity);

                httpResponse=  mHttpClient11.execute(httppost);

	      /* String responseString = EntityUtils.toString(response.getEntity());
	        Log.d("UPLOAD", "------------------------------"+responseString);*/

            }catch(Exception e){
                e.printStackTrace();
            }

            // key and value pair


            InputStream entity = null;
            try {
                entity = httpResponse.getEntity().getContent();
                if(entity != null) {
                    imageResponse = convertInputStreamToString(entity);
                    Log.i("TAG", "user response:" + imageResponse);
                }
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Log.i("TAG","-----------------response-----------");

            // data = parser.getDocument(entity);
            return null;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            if(imageResponse!=null && imageResponse.length()>0) {
//                prepareData();
                try {
                    JSONArray resultArray = new JSONArray(imageResponse);
                    for (int i = 0; i < resultArray.length(); i++){
                        JSONObject jsonObject = resultArray.getJSONObject(i);
                        if(i == 0){
                            imageName1 = jsonObject.getString("Name");
                        }
                        else if(i == 1){
                            imageName2 = jsonObject.getString("Name");
                        }
                        else if(i == 2){
                            imageName3 = jsonObject.getString("Name");
                        }
                        else if(i == 3){
                            imageName4 = jsonObject.getString("Name");
                        }
                        else if(i == 4){
                            imageName5 = jsonObject.getString("Name");
                        }
                        else if(i == 5){
                            imageName6 = jsonObject.getString("Name");
                        }
                        else if(i == 6){
                            imageName7 = jsonObject.getString("Name");
                        }
                        else if(i == 7){
                            imageName8 = jsonObject.getString("Name");
                        }
                        else if(i == 8){
                            documentImageName = jsonObject.getString("Name");
                        }
                    }

                    if(documentthumbnail!=null){
                        new uploadCR().execute();
                    }
                    else{
                        prepareData();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(getContext(), "Images upload failed.Please try again", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getContext(), "خطأ في تحميل الصور ، الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                }
            }
            if(dialog!=null){
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    class uploadCR extends AsyncTask<Void, Void, Void> {
        ACProgressFlower dialog;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params1) {
            // TODO Auto-generated method stub
//            submit();

            File file5=null;
            HttpResponse httpResponse = null;

            if(documentthumbnail!=null){
                crImagePath =StoreByteImage(documentthumbnail, true);
            }

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            mHttpClient11 = new DefaultHttpClient(params);

            try{
                HttpPost httppost = new HttpPost(Constants.UPLOAD_IMAGE_URL);

                if(crImagePath !=null){
                    file5 = new File(crImagePath);
                }

                MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                if(null!=file5&& file5.exists()){
                    multipartEntity.addPart("image5", new FileBody(file5));
//                    documentImageName = file5.getName();
                }

                httppost.setEntity(multipartEntity);

                httpResponse=  mHttpClient11.execute(httppost);

	      /* String responseString = EntityUtils.toString(response.getEntity());
	        Log.d("UPLOAD", "------------------------------"+responseString);*/

            }catch(Exception e){
                e.printStackTrace();
            }

            // key and value pair


            InputStream entity = null;
            try {
                entity = httpResponse.getEntity().getContent();
                if(entity != null) {
                    imageResponse = convertInputStreamToString(entity);
                    Log.i("TAG", "user response:" + imageResponse);
                }
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Log.i("TAG","-----------------response-----------");

            // data = parser.getDocument(entity);
            return null;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            if(imageResponse!=null && imageResponse.length()>0) {
//
                try {
                    JSONArray resultArray = new JSONArray(imageResponse);
                    for (int i = 0; i < resultArray.length(); i++){
                        JSONObject jsonObject = resultArray.getJSONObject(i);
                        if(i == 0){
                            documentImageName = jsonObject.getString("Name");
                        }
                    }
                    prepareData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                Toast.makeText(getContext() ,"Image upload failed.Please try again",Toast.LENGTH_SHORT).show();
            }
            if(dialog!=null){
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    public String StoreByteImage(Bitmap bitmap, boolean isDocument) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        String pictureName = "";
        if(!isDocument) {
            pictureName = "IMG_" + hostId + "_" + timeStamp + ".jpg";
        }
        else{
            pictureName = "DOC_" + hostId + "_" + timeStamp + ".jpg";
        }
        File sdImageMainDirectory = new File("/sdcard/"+ pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

			/*Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,

			imageData.length,options);*/
            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public class RegisterResort extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ACProgressFlower dialog;
        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            dialog = new ACProgressFlower.Builder(getContext())
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.RESORT_REGISTRATION_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }
                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }else{
                    if(result.equals("")){
                        Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
//
                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject ja = jo.getJSONObject("Success");

                                String resortID = ja.getString("ResortId");
                                hostPrefsEditor.putString("resortId",resortID);
                                hostPrefsEditor.commit();

//                                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                                String appName, title, positive;
//                                if(language.equalsIgnoreCase("En")){
//                                    appName = getResources().getString(R.string.app_name);
//                                    title = "Property Registration successful.";
//                                    positive = getResources().getString(R.string.str_btn_ok);
//                                }
//                                else{
//                                    appName = getResources().getString(R.string.app_name_ar);
//                                    title = "تم تسجيل موقعك بنجاح";
//                                    positive = getResources().getString(R.string.str_btn_ok_ar);
//                                }
//                                iOSDialog.setTitle(appName);
//                                iOSDialog.setSubtitle(title);
//                                iOSDialog.setPositiveLabel(positive);
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        Intent intent = new Intent(getContext(), MainActivity.class);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        getActivity().startActivity(intent);
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

//                                showDialog("Property Registration successful.", "تم تسجيل موقعك بنجاح");

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView title = (TextView) dialogView.findViewById(R.id.title);
                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);

                                if(language.equalsIgnoreCase("En")) {
                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.str_btn_ok));
                                    desc.setText("Property Registration successful.");
                                }
                                else{
                                    title.setText(getResources().getString(R.string.app_name_ar));
                                    yes.setText(getResources().getString(R.string.str_btn_ok_ar));
                                    desc.setText("تم تسجيل موقعك بنجاح");
                                }

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                        Intent intent = new Intent(getContext(), MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        getActivity().startActivity(intent);
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getActivity().getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
//                                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle("Property Registration successful.");
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        Intent intent = new Intent(getContext(), MainActivity.class);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        getActivity().startActivity(intent);
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();

                            }catch (JSONException je){
//                                final iOSDialog iOSDialog = new iOSDialog(getContext());
//                                iOSDialog.setTitle(getResources().getString(R.string.app_name));
//                                iOSDialog.setSubtitle(result);
//                                iOSDialog.setPositiveLabel(getResources().getString(R.string.str_btn_ok));
//                                iOSDialog.setBoldPositiveLabel(false);
//                                iOSDialog.setPositiveListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        iOSDialog.dismiss();
//                                    }
//                                });
//                                iOSDialog.show();
                                showDialog(result, result);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(getContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);
        }
    }

    public void prepareData(){
        int vat = 1;
        cityID = citiesArrayList.get(mCityPos).getCityId();
        String vatNumb = null, bankNam= null, iban= null;
        if(vatApplicable){
            vat = 1;
            vatNumb = vatNumber.getText().toString();
            bankNam = bankName.getText().toString();
            iban = ibanNumber.getText().toString();
        }
        else{
            vat = 0;
            if(vatNumber.getText().toString().length() == 0) {
                vatNumb = "";
            }
            else{
                vatNumb = vatNumber.getText().toString();
            }
            if(bankName.getText().toString().length() == 0) {
                bankNam = "";
            }
            else{
                bankNam = bankName.getText().toString();
            }
            if(ibanNumber.getText().toString().length() == 0) {
                iban = "";
            }
            else{
                iban = ibanNumber.getText().toString();
            }
        }
        if(selectedCategories.contains("Banquet Hall") || selectedCategories.contains("قاعات الفنادق")){
            try {
                JSONObject mainObj = new JSONObject();
                mainObj.put("ResortTypeId", resortTypeID);
                mainObj.put("CityId", cityID);
                mainObj.put("IsManAvailability", 1);
                mainObj.put("IsWomanAvailability", 1);
                mainObj.put("IsKitchenavailability", 1);
                mainObj.put("MinCapacity", Integer.parseInt(minCapacity));
                mainObj.put("MaxCapacity", Integer.parseInt(maxCapacity));
                mainObj.put("ResortName", etResortName.getText().toString());
                mainObj.put("ResortName_Ar", getEtResortNameAr.getText().toString());
                mainObj.put("ResortAddress", etResortAddress.getText().toString());
                mainObj.put("AdvanceAmount", AdvanceAmount.getText().toString());
                mainObj.put("Latitude", resortLatitude);
                mainObj.put("Longitude", resortLongitude);
                mainObj.put("AfrahcomApproved",0);
                mainObj.put("Description", etResortDescription.getText().toString());
                mainObj.put("Description_Ar", etResortDescriptionAr.getText().toString());
                mainObj.put("HostId", hostId);
                mainObj.put("VatNumber", vatNumb);
                mainObj.put("BankName", bankNam);
                mainObj.put("IbanNumber", iban);
                mainObj.put("vatavailability", vat);
                mainObj.put("sendfrom","Ar");
                mainObj.put("commercialreg", documentImageName);

//                    Prices Array
                JSONArray pricingArray = new JSONArray();
                JSONObject saturdayObj = new JSONObject();
                saturdayObj.put("Did", "1");
                saturdayObj.put("MenPrice", Constants.convertToArabic(saturdayMen.getText().toString()));
                saturdayObj.put("WomenPrice", Constants.convertToArabic(saturdayWomen.getText().toString()));
                saturdayObj.put("FamilyPrice", "0");
                saturdayObj.put("kitchenprice", "0");
                pricingArray.put(saturdayObj);

                JSONObject sundayObj = new JSONObject();
                sundayObj.put("Did", "2");
                sundayObj.put("MenPrice", Constants.convertToArabic(sundayMen.getText().toString()));
                sundayObj.put("WomenPrice", Constants.convertToArabic(sundayWomen.getText().toString()));
                sundayObj.put("FamilyPrice", "0");
                sundayObj.put("kitchenprice", "0");
                pricingArray.put(sundayObj);

                JSONObject mondayObj = new JSONObject();
                mondayObj.put("Did", "3");
                mondayObj.put("MenPrice", Constants.convertToArabic(mondayMen.getText().toString()));
                mondayObj.put("WomenPrice", Constants.convertToArabic(mondayWomen.getText().toString()));
                mondayObj.put("FamilyPrice", "0");
                mondayObj.put("kitchenprice", "0");
                pricingArray.put(mondayObj);

                JSONObject tuesdayObj = new JSONObject();
                tuesdayObj.put("Did", "4");
                tuesdayObj.put("MenPrice", Constants.convertToArabic(tuesdayMen.getText().toString()));
                tuesdayObj.put("WomenPrice", Constants.convertToArabic(tuesdayWomen.getText().toString()));
                tuesdayObj.put("FamilyPrice", "0");
                tuesdayObj.put("kitchenprice", "0");
                pricingArray.put(tuesdayObj);

                JSONObject wednesdayObj = new JSONObject();
                wednesdayObj.put("Did", "5");
                wednesdayObj.put("MenPrice", Constants.convertToArabic(wednesdayMen.getText().toString()));
                wednesdayObj.put("WomenPrice", Constants.convertToArabic(wednesdayWomen.getText().toString()));
                wednesdayObj.put("FamilyPrice", "0");
                wednesdayObj.put("kitchenprice", "0");
                pricingArray.put(wednesdayObj);

                JSONObject thursdayObj = new JSONObject();
                thursdayObj.put("Did", "6");
                thursdayObj.put("MenPrice", Constants.convertToArabic(thursdayMen.getText().toString()));
                thursdayObj.put("WomenPrice", Constants.convertToArabic(thursdayWomen.getText().toString()));
                thursdayObj.put("FamilyPrice", "0");
                thursdayObj.put("kitchenprice", "0");
                pricingArray.put(thursdayObj);

                JSONObject fridayObj = new JSONObject();
                fridayObj.put("Did", "7");
                fridayObj.put("MenPrice", Constants.convertToArabic(fridayMen.getText().toString()));
                fridayObj.put("WomenPrice", Constants.convertToArabic(fridayWomen.getText().toString()));
                fridayObj.put("FamilyPrice", "0");
                fridayObj.put("kitchenprice", "0");
                pricingArray.put(fridayObj);

                JSONArray imagesArray = new JSONArray();
                if (imageName1 != null) {
                    JSONObject image1 = new JSONObject();
                    image1.put("ImageName", imageName1);
                    imagesArray.put(image1);
                }
                if (imageName2!= null) {
                    JSONObject image2 = new JSONObject();
                    image2.put("ImageName", imageName2);
                    imagesArray.put(image2);
                }
                if (imageName3!= null) {
                    JSONObject image3 = new JSONObject();
                    image3.put("ImageName", imageName3);
                    imagesArray.put(image3);
                }
                if (imageName4!= null) {
                    JSONObject image4 = new JSONObject();
                    image4.put("ImageName", imageName4);
                    imagesArray.put(image4);
                }
                if (imageName5!= null) {
                    JSONObject image5 = new JSONObject();
                    image5.put("ImageName", imageName5);
                    imagesArray.put(image5);
                }
                if (imageName6!= null) {
                    JSONObject image6 = new JSONObject();
                    image6.put("ImageName", imageName6);
                    imagesArray.put(image6);
                }
                if (imageName7!= null) {
                    JSONObject image7 = new JSONObject();
                    image7.put("ImageName", imageName7);
                    imagesArray.put(image7);
                }
                if (imageName8!= null) {
                    JSONObject image8 = new JSONObject();
                    image8.put("ImageName", imageName8);
                    imagesArray.put(image8);
                }

                JSONArray facilitiesArray = new JSONArray();
                for (int i = 0; i < selectedFacilities.size(); i++) {
                    JSONObject faciltyObj = new JSONObject();
                    faciltyObj.put("FacilityId", selectedFacilityIds.get(i));
                    facilitiesArray.put(faciltyObj);
                }

                parent.put("InsertResort", mainObj);
                parent.put("InsertPrices", pricingArray);
                parent.put("InsertResortImages", imagesArray);
                parent.put("InsertResortFacility", facilitiesArray);
                Log.i("TAG", parent.toString());
                new RegisterResort().execute(parent.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(selectedCategories.contains("Wedding Hall") || selectedCategories.contains("قصور الافراح")){

            try {

                JSONObject mainObj = new JSONObject();
                mainObj.put("ResortTypeId", resortTypeID);
                mainObj.put("CityId", cityID);
                mainObj.put("IsManAvailability", 1);
                mainObj.put("IsWomanAvailability", 1);
                mainObj.put("IsKitchenavailability", 1);
                mainObj.put("MinCapacity", Integer.parseInt(minCapacity));
                mainObj.put("MaxCapacity", Integer.parseInt(maxCapacity));
                mainObj.put("ResortName", etResortName.getText().toString());
                mainObj.put("ResortName_Ar", getEtResortNameAr.getText().toString());
                mainObj.put("ResortAddress", etResortAddress.getText().toString());
                mainObj.put("AdvanceAmount", AdvanceAmount.getText().toString());
                mainObj.put("Latitude", resortLatitude);
                mainObj.put("Longitude", resortLongitude);
                mainObj.put("AfrahcomApproved",0);
                mainObj.put("Description", etResortDescription.getText().toString());
                mainObj.put("Description_Ar", etResortDescriptionAr.getText().toString());
                mainObj.put("HostId", hostId);
                mainObj.put("sendfrom","Ar");
                mainObj.put("VatNumber", vatNumb);
                mainObj.put("BankName", bankNam);
                mainObj.put("IbanNumber", iban);
                mainObj.put("vatavailability", vat);
                mainObj.put("sendfrom","Ar");
                mainObj.put("commercialreg", documentImageName);

//                    Prices Array
                JSONArray pricingArray = new JSONArray();
                JSONObject saturdayObj = new JSONObject();
                saturdayObj.put("Did", "1");
                saturdayObj.put("MenPrice", Constants.convertToArabic(saturdayMen.getText().toString()));
                saturdayObj.put("WomenPrice", Constants.convertToArabic(saturdayWomen.getText().toString()));
                saturdayObj.put("FamilyPrice", Constants.convertToArabic(saturdayFamily.getText().toString()));
                saturdayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                pricingArray.put(saturdayObj);

                JSONObject sundayObj = new JSONObject();
                sundayObj.put("Did", "2");
                sundayObj.put("MenPrice", Constants.convertToArabic(sundayMen.getText().toString()));
                sundayObj.put("WomenPrice", Constants.convertToArabic(sundayWomen.getText().toString()));
                sundayObj.put("FamilyPrice", Constants.convertToArabic(sundayFamily.getText().toString()));
                sundayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                pricingArray.put(sundayObj);

                JSONObject mondayObj = new JSONObject();
                mondayObj.put("Did", "3");
                mondayObj.put("MenPrice", Constants.convertToArabic(mondayMen.getText().toString()));
                mondayObj.put("WomenPrice", Constants.convertToArabic(mondayWomen.getText().toString()));
                mondayObj.put("FamilyPrice", Constants.convertToArabic(mondayFamily.getText().toString()));
                mondayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                pricingArray.put(mondayObj);

                JSONObject tuesdayObj = new JSONObject();
                tuesdayObj.put("Did", "4");
                tuesdayObj.put("MenPrice", Constants.convertToArabic(tuesdayMen.getText().toString()));
                tuesdayObj.put("WomenPrice", Constants.convertToArabic(tuesdayWomen.getText().toString()));
                tuesdayObj.put("FamilyPrice", Constants.convertToArabic(tuesdayFamily.getText().toString()));
                tuesdayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                pricingArray.put(tuesdayObj);

                JSONObject wednesdayObj = new JSONObject();
                wednesdayObj.put("Did", "5");
                wednesdayObj.put("MenPrice", Constants.convertToArabic(wednesdayMen.getText().toString()));
                wednesdayObj.put("WomenPrice", Constants.convertToArabic(wednesdayWomen.getText().toString()));
                wednesdayObj.put("FamilyPrice", Constants.convertToArabic(wednesdayFamily.getText().toString()));
                wednesdayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                pricingArray.put(wednesdayObj);

                JSONObject thursdayObj = new JSONObject();
                thursdayObj.put("Did", "6");
                thursdayObj.put("MenPrice", Constants.convertToArabic(thursdayMen.getText().toString()));
                thursdayObj.put("WomenPrice", Constants.convertToArabic(thursdayWomen.getText().toString()));
                thursdayObj.put("FamilyPrice", Constants.convertToArabic(thursdayFamily.getText().toString()));
                thursdayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                pricingArray.put(thursdayObj);

                JSONObject fridayObj = new JSONObject();
                fridayObj.put("Did", "7");
                fridayObj.put("MenPrice", Constants.convertToArabic(fridayMen.getText().toString()));
                fridayObj.put("WomenPrice", Constants.convertToArabic(fridayWomen.getText().toString()));
                fridayObj.put("FamilyPrice", Constants.convertToArabic(fridayFamily.getText().toString()));
                fridayObj.put("kitchenprice", Constants.convertToArabic(kitchenPrice.getText().toString()));
                pricingArray.put(fridayObj);

                JSONArray imagesArray = new JSONArray();

                if (imageName1 != null) {
                    JSONObject image1 = new JSONObject();
                    image1.put("ImageName", imageName1);
                    imagesArray.put(image1);
                }
                if (imageName2!= null) {
                    JSONObject image2 = new JSONObject();
                    image2.put("ImageName", imageName2);
                    imagesArray.put(image2);
                }
                if (imageName3!= null) {
                    JSONObject image3 = new JSONObject();
                    image3.put("ImageName", imageName3);
                    imagesArray.put(image3);
                }
                if (imageName4!= null) {
                    JSONObject image4 = new JSONObject();
                    image4.put("ImageName", imageName4);
                    imagesArray.put(image4);
                }
                if (imageName5!= null) {
                    JSONObject image5 = new JSONObject();
                    image5.put("ImageName", imageName5);
                    imagesArray.put(image5);
                }
                if (imageName6!= null) {
                    JSONObject image6 = new JSONObject();
                    image6.put("ImageName", imageName6);
                    imagesArray.put(image6);
                }
                if (imageName7!= null) {
                    JSONObject image7 = new JSONObject();
                    image7.put("ImageName", imageName7);
                    imagesArray.put(image7);
                }
                if (imageName8!= null) {
                    JSONObject image8 = new JSONObject();
                    image8.put("ImageName", imageName8);
                    imagesArray.put(image8);
                }

                JSONArray facilitiesArray = new JSONArray();
                for (int i = 0; i < selectedFacilities.size(); i++) {
                    JSONObject faciltyObj = new JSONObject();
                    faciltyObj.put("FacilityId", selectedFacilityIds.get(i));
                    facilitiesArray.put(faciltyObj);
                }

                parent.put("InsertResort", mainObj);
                parent.put("InsertPrices", pricingArray);
                parent.put("InsertResortImages", imagesArray);
                parent.put("InsertResortFacility", facilitiesArray);

                Log.i("TAG", parent.toString());
                new RegisterResort().execute(parent.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(selectedCategories.contains("Resort") || selectedCategories.contains("الشاليهات")){
            try {
                JSONObject mainObj = new JSONObject();
                mainObj.put("ResortTypeId", resortTypeID);
                mainObj.put("CityId", cityID);
                mainObj.put("IsManAvailability", 1);
                mainObj.put("IsWomanAvailability", 1);
                mainObj.put("IsKitchenavailability", 1);
                mainObj.put("MinCapacity", Integer.parseInt(minCapacity));
                mainObj.put("MaxCapacity", Integer.parseInt(maxCapacity));
                mainObj.put("ResortName", etResortName.getText().toString());
                mainObj.put("ResortName_Ar", getEtResortNameAr.getText().toString());
                mainObj.put("ResortAddress", etResortAddress.getText().toString());
                mainObj.put("AdvanceAmount", AdvanceAmount.getText().toString());
                mainObj.put("Latitude", resortLatitude);
                mainObj.put("Longitude", resortLongitude);
                mainObj.put("AfrahcomApproved",0);
                mainObj.put("Description", etResortDescription.getText().toString());
                mainObj.put("Description_Ar", etResortDescriptionAr.getText().toString());
                mainObj.put("HostId", hostId);
                mainObj.put("sendfrom","Ar");
                mainObj.put("VatNumber", vatNumb);
                mainObj.put("BankName", bankNam);
                mainObj.put("IbanNumber", iban);
                mainObj.put("vatavailability", vat);
                mainObj.put("sendfrom","Ar");
                mainObj.put("commercialreg", documentImageName);

//                    Prices Array
                JSONArray pricingArray = new JSONArray();
                JSONObject saturdayObj = new JSONObject();
                saturdayObj.put("Did", "1");
                saturdayObj.put("MenPrice", Constants.convertToArabic(saturdayMen.getText().toString()));
                saturdayObj.put("WomenPrice", Constants.convertToArabic(saturdayMen.getText().toString()));
                saturdayObj.put("FamilyPrice", "0");
                saturdayObj.put("kitchenprice", "0");
                pricingArray.put(saturdayObj);

                JSONObject sundayObj = new JSONObject();
                sundayObj.put("Did", "2");
                sundayObj.put("MenPrice", Constants.convertToArabic(sundayMen.getText().toString()));
                sundayObj.put("WomenPrice", Constants.convertToArabic(sundayMen.getText().toString()));
                sundayObj.put("FamilyPrice", "0");
                sundayObj.put("kitchenprice", "0");
                pricingArray.put(sundayObj);

                JSONObject mondayObj = new JSONObject();
                mondayObj.put("Did", "3");
                mondayObj.put("MenPrice", Constants.convertToArabic(mondayMen.getText().toString()));
                mondayObj.put("WomenPrice", Constants.convertToArabic(mondayMen.getText().toString()));
                mondayObj.put("FamilyPrice", "0");
                mondayObj.put("kitchenprice", "0");
                pricingArray.put(mondayObj);

                JSONObject tuesdayObj = new JSONObject();
                tuesdayObj.put("Did", "4");
                tuesdayObj.put("MenPrice", Constants.convertToArabic(tuesdayMen.getText().toString()));
                tuesdayObj.put("WomenPrice", Constants.convertToArabic(tuesdayMen.getText().toString()));
                tuesdayObj.put("FamilyPrice", "0");
                tuesdayObj.put("kitchenprice", "0");
                pricingArray.put(tuesdayObj);

                JSONObject wednesdayObj = new JSONObject();
                wednesdayObj.put("Did", "5");
                wednesdayObj.put("MenPrice", Constants.convertToArabic(wednesdayMen.getText().toString()));
                wednesdayObj.put("WomenPrice", Constants.convertToArabic(wednesdayMen.getText().toString()));
                wednesdayObj.put("FamilyPrice", "0");
                wednesdayObj.put("kitchenprice", "0");
                pricingArray.put(wednesdayObj);

                JSONObject thursdayObj = new JSONObject();
                thursdayObj.put("Did", "6");
                thursdayObj.put("MenPrice", Constants.convertToArabic(thursdayMen.getText().toString()));
                thursdayObj.put("WomenPrice", Constants.convertToArabic(thursdayMen.getText().toString()));
                thursdayObj.put("FamilyPrice", "0");
                thursdayObj.put("kitchenprice", "0");
                pricingArray.put(thursdayObj);

                JSONObject fridayObj = new JSONObject();
                fridayObj.put("Did", "7");
                fridayObj.put("MenPrice", Constants.convertToArabic(fridayMen.getText().toString()));
                fridayObj.put("WomenPrice", Constants.convertToArabic(fridayMen.getText().toString()));
                fridayObj.put("FamilyPrice", "0");
                fridayObj.put("kitchenprice", "0");
                pricingArray.put(fridayObj);

                JSONArray imagesArray = new JSONArray();
                if (imageName1 != null) {
                    JSONObject image1 = new JSONObject();
                    image1.put("ImageName", imageName1);
                    imagesArray.put(image1);
                }
                if (imageName2 != null) {
                    JSONObject image2 = new JSONObject();
                    image2.put("ImageName", imageName2);
                    imagesArray.put(image2);
                }
                if (imageName3 != null) {
                    JSONObject image3 = new JSONObject();
                    image3.put("ImageName", imageName3);
                    imagesArray.put(image3);
                }
                if (imageName4 != null) {
                    JSONObject image4 = new JSONObject();
                    image4.put("ImageName", imageName4);
                    imagesArray.put(image4);
                }
                if (imageName5!= null) {
                    JSONObject image5 = new JSONObject();
                    image5.put("ImageName", imageName5);
                    imagesArray.put(image5);
                }
                if (imageName6!= null) {
                    JSONObject image6 = new JSONObject();
                    image6.put("ImageName", imageName6);
                    imagesArray.put(image6);
                }
                if (imageName7!= null) {
                    JSONObject image7 = new JSONObject();
                    image7.put("ImageName", imageName7);
                    imagesArray.put(image7);
                }
                if (imageName8!= null) {
                    JSONObject image8 = new JSONObject();
                    image8.put("ImageName", imageName8);
                    imagesArray.put(image8);
                }

                JSONArray facilitiesArray = new JSONArray();
                for (int i = 0; i < selectedFacilities.size(); i++) {
                    JSONObject faciltyObj = new JSONObject();
                    faciltyObj.put("FacilityId", selectedFacilityIds.get(i));
                    facilitiesArray.put(faciltyObj);
                }

                parent.put("InsertResort", mainObj);
                parent.put("InsertPrices", pricingArray);
                parent.put("InsertResortImages", imagesArray);
                parent.put("InsertResortFacility", facilitiesArray);
                Log.i("TAG", parent.toString());
                new RegisterResort().execute(parent.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showDialog(String description, String description_ar){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        if(language.equalsIgnoreCase("En")) {
            title.setText(getResources().getString(R.string.app_name));
            yes.setText(getResources().getString(R.string.str_btn_ok));
            desc.setText(description);
        }
        else{
            title.setText(getResources().getString(R.string.app_name_ar));
            yes.setText(getResources().getString(R.string.str_btn_ok_ar));
            desc.setText(description_ar);
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
